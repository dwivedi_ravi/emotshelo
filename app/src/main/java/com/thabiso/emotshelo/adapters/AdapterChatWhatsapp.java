package com.thabiso.emotshelo.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.models.Message;
import com.thabiso.emotshelo.util.preferences.Defaults;

import java.util.ArrayList;
import java.util.List;

public class AdapterChatWhatsapp extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int CHAT_ME = 100;
    private final int CHAT_YOU = 200;

    private List<Message> items = new ArrayList<>();

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, Message obj, int position);

        void onButtonClick(View view, Message obj, int position, TextView text_content);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public AdapterChatWhatsapp(Context context) {
        ctx = context;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView text_content;
        public TextView text_time;
        public View lyt_parent;
        public Button btnAction, btnActionNo;

        public ItemViewHolder(View v) {
            super(v);
            text_content = v.findViewById(R.id.text_content);
            text_time = v.findViewById(R.id.text_time);
            lyt_parent = v.findViewById(R.id.lyt_parent);
            btnAction = v.findViewById(R.id.btnAction);
            btnActionNo = v.findViewById(R.id.btnActionNo);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == CHAT_ME) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_whatsapp_me, parent, false);
            vh = new ItemViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_whatsapp_telegram_you, parent, false);
            vh = new ItemViewHolder(v);
        }
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        try {
            if (holder instanceof ItemViewHolder) {
                //            final Message m = items.get(position);
                final Message m = items.get(holder.getBindingAdapterPosition());
                ItemViewHolder vItem = (ItemViewHolder) holder;
                vItem.text_content.setText(m.getContent());
                vItem.text_time.setText(m.getDate());

                if (m.isShowButton()) {
                    if (m.getTypeOfButton() == Defaults.SHOW_YES_NO_BUTTON) {
                        vItem.btnAction.setText(ctx.getResources().getString(R.string.yes_action));
                        vItem.btnActionNo.setText(ctx.getResources().getString(R.string.no_action));
                        vItem.btnActionNo.setVisibility(View.VISIBLE);
                    } else {
                        if (!m.getShowButtonText().isEmpty())
                            vItem.btnAction.setText(m.getShowButtonText());
                        else
                            vItem.btnAction.setText(ctx.getResources().getString(R.string.touch_to_select));
                        vItem.btnActionNo.setVisibility(View.GONE);
                    }
                    vItem.btnAction.setVisibility(View.VISIBLE);
                } else {
                    vItem.btnAction.setVisibility(View.GONE);
                    vItem.btnActionNo.setVisibility(View.GONE);
                }

                vItem.lyt_parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mOnItemClickListener != null) {
                            mOnItemClickListener.onItemClick(view, m, position);
                        }
                    }
                });

                vItem.btnAction.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mOnItemClickListener != null) {
                            if (m.getTypeOfButton() == Defaults.SHOW_YES_NO_BUTTON) {
                                m.setShowButtonText(ctx.getResources().getString(R.string.yes_action));
                            }
                            mOnItemClickListener.onButtonClick(view, m, position, vItem.text_content);
                        }
                    }
                });

                vItem.btnActionNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mOnItemClickListener != null) {
                            if (m.getTypeOfButton() == Defaults.SHOW_YES_NO_BUTTON) {
                                m.setShowButtonText(ctx.getResources().getString(R.string.no_action));
                            }
                            mOnItemClickListener.onButtonClick(view, m, position, vItem.text_content);
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return this.items.get(position).isFromMe() ? CHAT_ME : CHAT_YOU;
    }

    public void insertItem(Message item) {
        this.items.add(item);
        notifyItemInserted(getItemCount());
    }

    public List<Message> getItems() {
        return items;
    }

    public void setItems(List<Message> items) {
        this.items = items;
    }
}