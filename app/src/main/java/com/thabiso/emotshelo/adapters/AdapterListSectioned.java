package com.thabiso.emotshelo.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.models.People;
import com.thabiso.emotshelo.util.Tools;
import com.thabiso.emotshelo.util.preferences.Defaults;

import java.util.ArrayList;
import java.util.List;

public class AdapterListSectioned extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_ITEM = 1;
    private final int VIEW_SECTION = 0;

    private List<People> items = new ArrayList<>();
    private Context ctx;
    private OnItemClickListener mOnItemClickListener;
    private int screenType;
    private String textToDisplay;
    private Typeface robotoBold, robotoRegular;
    public static Typeface robotoMedium;

    public interface OnItemClickListener {
        void onItemClick(View view, People obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public AdapterListSectioned(Context context, List<People> items, int screenType) {
        this.items = items;
        ctx = context;
        this.screenType = screenType;
        switch (screenType) {
            case Defaults.SCREEN_TYPE_INTEREST_STATEMENT:
                textToDisplay = "Interest paid on withdrawal";
                break;
            case Defaults.SCREEN_TYPE_WITHDRAWALS_PAID_STATEMENT:
                textToDisplay = "Widthrawal paid by cash";
                break;
            case Defaults.SCREEN_TYPE_WITHDRAWALS_OUTSTANDING_STATEMENT:
                textToDisplay = "Outstanding withdrawal";
                break;
        }

        this.robotoBold = Typeface.createFromAsset(context.getAssets(),
                "roboto_bold.ttf");
        this.robotoRegular = Typeface.createFromAsset(context.getAssets(),
                "roboto_regular.ttf");
        robotoMedium = Typeface.createFromAsset(context.getAssets(),
                "roboto_medium.ttf");
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public AppCompatImageView imgProfile;
        public TextView txtDate;
        public TextView txtCategory;
        public TextView txtAmount;
        public TextView txtAddress;
        public RelativeLayout itemContainer;

        public OriginalViewHolder(View v) {
            super(v);
            imgProfile = v.findViewById(R.id.imgProfile);
            txtDate = v.findViewById(R.id.txtDate);
            txtCategory = v.findViewById(R.id.txtCategory);
            txtAmount = v.findViewById(R.id.txtAmount);
            txtAddress = v.findViewById(R.id.txtAddress);
            itemContainer = v.findViewById(R.id.itemContainer);

            txtCategory.setTypeface(robotoMedium);
            txtAddress.setTypeface(robotoRegular);
            txtAmount.setTypeface(robotoMedium);
            txtDate.setTypeface(robotoMedium);
        }
    }

    public static class SectionViewHolder extends RecyclerView.ViewHolder {
        public TextView title_section;

        public SectionViewHolder(View v) {
            super(v);
            title_section = v.findViewById(R.id.title_section);
            title_section.setTypeface(robotoMedium);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_statement, parent, false);
            vh = new OriginalViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_statement_section, parent, false);
            vh = new SectionViewHolder(v);
        }
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
//        People p = items.get(position);
        People p = items.get(holder.getBindingAdapterPosition());

        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;

            view.txtCategory.setText(p.name);
            view.txtAddress.setText(textToDisplay);
            view.txtDate.setText("12-05-2020");
            view.txtAmount.setText("$100.0");
//            view.imgProfile.setImageResource(R.drawable.ic_money);
            Tools.displayImageRound(ctx, view.imgProfile, p.image);

            view.itemContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view, items.get(position), position);
                    }
                }
            });
        } else {
            SectionViewHolder view = (SectionViewHolder) holder;
            view.title_section.setText(p.name);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return this.items.get(position).section ? VIEW_SECTION : VIEW_ITEM;
    }

    public void insertItem(int index, People people) {
        items.add(index, people);
        notifyItemInserted(index);
    }
}