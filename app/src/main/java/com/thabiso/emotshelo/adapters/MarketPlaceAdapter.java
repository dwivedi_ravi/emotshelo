package com.thabiso.emotshelo.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;

import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.activities.business_tool.conversationalform.AddCompanyActivity;
import com.thabiso.emotshelo.activities.groups_tool.conversationalforms.CreateGroupActivity;
import com.thabiso.emotshelo.activities.howitworks.ScreenShotViewerScreen;
import com.thabiso.emotshelo.activities.tools.ToolDetailActivity;
import com.thabiso.emotshelo.models.MarketToolListBO;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;

import java.util.ArrayList;

/***
 * The adapter class for the RecyclerView, contains the chapters data
 */
public class MarketPlaceAdapter extends RecyclerView.Adapter<MarketPlaceAdapter.ViewHolder> {

    private ArrayList<MarketToolListBO> toolListBOArrayList;
    private Context mContext;
    private Typeface robotoRegular, robotoMedium;

    /**
     * Constructor that passes in the chapters data and the context
     *
     * @param marketToolListBOData ArrayList containing the chapters data
     * @param context              Context of the application
     */
    public MarketPlaceAdapter(Context context, ArrayList<MarketToolListBO> marketToolListBOData) {
        this.toolListBOArrayList = marketToolListBOData;
        this.mContext = context;
        this.robotoRegular = Typeface.createFromAsset(context.getAssets(),
                "roboto_regular.ttf");
        this.robotoMedium = Typeface.createFromAsset(context.getAssets(),
                "roboto_medium.ttf");
    }


    /**
     * Required method for creating the viewholder objects.
     *
     * @param parent   The ViewGroup into which the new View will be added after it is bound to an adapter position.
     * @param viewType The view type of the new View.
     * @return The newly create ViewHolder.
     */
    @Override
    public MarketPlaceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.list_item_tools_market_place, parent, false));
    }

    /**
     * Required method that binds the data to the viewholder.
     *
     * @param holder   The viewholder into which the data should be put.
     * @param position The adapter position.
     */
    @Override
    public void onBindViewHolder(MarketPlaceAdapter.ViewHolder holder, int position) {
        MarketToolListBO currentMarketToolListBO = toolListBOArrayList.get(position);
        holder.bindTo(position, currentMarketToolListBO);
    }


    /**
     * Required method for determining the size of the data set.
     *
     * @return Size of the data set.
     */
    @Override
    public int getItemCount() {
        return toolListBOArrayList.size();
    }


    /**
     * ViewHolder class that represents each row of data in the RecyclerView
     */
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //Member Variables for the TextViews
        private TextView txtToolTitle;
        private TextView txtToolDescription;
        private AppCompatButton btnAddTool;
        private AppCompatButton btnDetails;
        private ImageView imgTool;


        /**
         * Constructor for the ViewHolder, used in onCreateViewHolder().
         *
         * @param itemView The rootview of the list_item.xml layout file
         */
        ViewHolder(View itemView) {
            super(itemView);

            txtToolTitle = itemView.findViewById(R.id.txtToolTitle);
            txtToolDescription = itemView.findViewById(R.id.txtToolDescription);
            btnAddTool = itemView.findViewById(R.id.btnEnableTool);
            btnDetails = itemView.findViewById(R.id.btnDetails);
            imgTool = itemView.findViewById(R.id.imgTool);
            imgTool.setVisibility(View.GONE);

//            txtToolTitle.setTypeface(robotoMedium);
//            btnAddTool.setTypeface(robotoRegular);
//            btnDetails.setTypeface(robotoRegular);
//            txtToolDescription.setTypeface(robotoRegular);

            btnAddTool.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utility.checkViewTraingingCompleted((Activity) mContext, v, Defaults.TRAINING_SCREEN_TOOLS_MARKET_PLACE, false, mContext.getResources().getString(R.string.training_title_homescreen_enable_tool), mContext.getResources().getString(R.string.training_description_homescreen_enable_tool))) {
//                        AlertDialog textDialog = Utility.getTextDialog_Material(((AppCompatActivity) mContext), "", mContext.getResources().getString(R.string.confirmation_enable_tool));
//                    textDialog.setButton(DialogInterface.BUTTON_POSITIVE, mContext.getString(R.string.ok_action).toUpperCase(), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            Prefs.setBooleanValue(v.getTag().toString(), true);
//                            notifyDataSetChanged();
//                            Utility.sendUpdateListBroadCast(Defaults.ACTION_TOOL_ENABLED_REFRESH_HOMESCREEN, mContext, null);
//                            Toast.makeText(mContext, mContext.getResources().getString(R.string.enable_tool_success), Toast.LENGTH_LONG).show();
//                        }
//                    });
//                    textDialog.setButton(DialogInterface.BUTTON_NEGATIVE, mContext.getString(R.string.mdtp_cancel).toUpperCase(), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            textDialog.dismiss();
//                        }
//                    });
//                    textDialog.show();
//
//                    textDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(mContext.getResources().getColor(R.color.app_text_color));
//                    textDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(mContext.getResources().getColor(R.color.app_text_color));
//                    textDialog.getButton(DialogInterface.BUTTON_POSITIVE).setBackground(mContext.getResources().getDrawable(R.drawable.ripple));
//                    textDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setBackground(mContext.getResources().getDrawable(R.drawable.ripple));

                        int toolId = (int) v.getTag();
                        Intent intent;
                        if (toolId == Defaults.TOOL_ID_ENTERPRICE) {
                            intent = new Intent(mContext, AddCompanyActivity.class);
                            intent.putExtra("toolId", toolId);
                            intent.putExtra("toolName", txtToolTitle.getText().toString());
                            mContext.startActivity(intent);

                        } else if (toolId == Defaults.TOOL_ID_PERSONAL) {
                            Toast.makeText(mContext, "Tool Under Construction", Toast.LENGTH_LONG).show();
//                    } else if (toolId == Defaults.TOOL_ID_GROUPS) {
                        } else { // DEFAULT ELSE CASE FOR GROUP SAVINGS
//                            intent = new Intent(mContext, CreateGroupActivity.class);
//                            intent.putExtra("toolId", Defaults.TOOL_ID_GROUPS);
//
////                        toolId below will be the subToolID
//                            if (toolId >= Defaults.SUB_TOOL_ID_PREMIUM_GROUPS_GOAL_BASED_SAVINGS)
//                                intent.putExtra("subToolId", Defaults.SUB_TOOL_ID_ROTATING_SAVINGS);
//                            else
//                                intent.putExtra("subToolId", toolId);
//                            intent.putExtra("toolName", txtToolTitle.getText().toString());
//                            mContext.startActivity(intent);

                            intent = new Intent(mContext, CreateGroupActivity.class);
                            intent.putExtra("toolId", Defaults.TOOL_ID_GROUPS);
                            // toolId below will be the subToolID
                            if (toolId >= Defaults.SUB_TOOL_ID_PREMIUM_GROUPS_GOAL_BASED_SAVINGS)
                                intent.putExtra("subToolId", Defaults.SUB_TOOL_ID_ROTATING_SAVINGS);
                            else
                                intent.putExtra("subToolId", toolId);
                            intent.putExtra("toolName", txtToolTitle.getText().toString());

                            mContext.startActivity(intent);
                        }
                    }
                }
            });

            btnDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    if (txtToolTitle.getText().toString().contains(mContext.getResources().getString(R.string.card_title_invoicing))) {
//                        Intent intent = new Intent(mContext, ScreenShotViewerScreen.class);
//                        mContext.startActivity(intent);
//                    } else {
//                        goToDetailsScreen(getBindingAdapterPosition(), imgTool);
//                    }
                    Intent intent = new Intent(mContext, ScreenShotViewerScreen.class);
                    intent.putExtra("toolId", (int) v.getTag());
                    mContext.startActivity(intent);
                }
            });

            itemView.setOnClickListener(this);
        }

        void bindTo(int position, MarketToolListBO currentMarketToolListBO) {

            if (currentMarketToolListBO.getToolID() == Defaults.TOOL_ID_GROUPS) {
                btnAddTool.setTag(currentMarketToolListBO.getSubToolId());
                btnDetails.setTag(currentMarketToolListBO.getToolID());
                btnAddTool.setVisibility(View.VISIBLE);
            } else {
                btnAddTool.setTag(currentMarketToolListBO.getToolID());
                btnDetails.setTag(currentMarketToolListBO.getToolID());
                btnAddTool.setVisibility(View.GONE);
            }

            txtToolTitle.setText(currentMarketToolListBO.getToolTitle());
            txtToolDescription.setText(currentMarketToolListBO.getToolShortDescription());

//            if (currentMarketToolListBO.getToolID() == Defaults.TOOL_ID_ENTERPRICE)
//                Glide.with(mContext).load(mContext.getResources().getDrawable(R.drawable.enterprise_tool)).into(imgTool);
//            else if (currentMarketToolListBO.getToolID() == Defaults.TOOL_ID_GROUPS)
//                Glide.with(mContext).load(mContext.getResources().getDrawable(R.drawable.group_savings_tool)).into(imgTool);
//            else if (currentMarketToolListBO.getToolID() == Defaults.TOOL_ID_PERSONAL)
//                Glide.with(mContext).load(mContext.getResources().getDrawable(R.drawable.personal_tool)).into(imgTool);

//            if (Prefs.getBooleanValue(currentMarketToolListBO.getToolID() + "")) {
//                btnAddTool.setText("ENABLED");
//            } else {
//                btnAddTool.setText("ENABLE TOOL");
//            }
        }

        @Override
        public void onClick(View view) {
            goToDetailsScreen(getBindingAdapterPosition(), imgTool);
        }
    }

    private void goToDetailsScreen(int position, ImageView imgTool) {
        MarketToolListBO currentMarketToolListBO = toolListBOArrayList.get(position);

        Intent intent = new Intent(mContext, ToolDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("currentTool", currentMarketToolListBO);
        intent.putExtras(bundle);

        mContext.startActivity(intent);
//        ((ToolListingActivity) mContext).switchAnimation(imgTool, imgTool, intent, mContext);
    }
}
