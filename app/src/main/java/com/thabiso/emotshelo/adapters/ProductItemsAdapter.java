package com.thabiso.emotshelo.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.selection.ItemKeyProvider;
import androidx.recyclerview.selection.Selection;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.RequestOptions;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.fragments.FragmentContactItem.OnListFragmentInteractionListener;
import com.thabiso.emotshelo.models.getmyconnections.UserContactsItem;
import com.thabiso.emotshelo.util.Utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProductItemsAdapter extends RecyclerView.Adapter<ProductItemsAdapter.ViewHolder> implements Filterable {

    private List<UserContactsItem> mValues;
    private final OnListFragmentInteractionListener mListener;
    private SelectionTracker<Long> selectionTracker;
    private Context context;
    private int pressedBackgroundColor, normalBackgroundColor;
    private RequestOptions requestOptions;
    private boolean mShowCancelButton;
    private Typeface robotoBold, robotoRegular, robotoMedium;

    public ProductItemsAdapter(Context context, List<UserContactsItem> items, OnListFragmentInteractionListener listener, boolean showCancelButton) {
        this.context = context;
        mValues = items;
        mListener = listener;
        pressedBackgroundColor = context.getResources().getColor(R.color.browser_actions_divider_color);
        normalBackgroundColor = context.getResources().getColor(R.color.transparent);
        requestOptions = Utility.getCustomizedRequestOptions(R.color.mdtp_line_background, R.drawable.round_avatar);
        mShowCancelButton = showCancelButton;
        this.robotoBold = Typeface.createFromAsset(context.getAssets(),
                "roboto_bold.ttf");
        this.robotoRegular = Typeface.createFromAsset(context.getAssets(),
                "roboto_regular.ttf");
        this.robotoMedium = Typeface.createFromAsset(context.getAssets(),
                "roboto_medium.ttf");
//        sort();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_product, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") int position) {
//        holder.mItem = mValues.get(position);
        int pos = holder.getBindingAdapterPosition();
        holder.mItem = mValues.get(pos);

        holder.contact_name.setText(mValues.get(pos).getUserName());
        holder.txtAddress.setText(mValues.get(pos).getAvatar());
        holder.txtMobile.setText(mValues.get(pos).getMobile());
        holder.txtEmail.setText(mValues.get(pos).getTemp());
        holder.txtTaxPercentage.setText(mValues.get(pos).getTemp2());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
        holder.details.position = pos;
        if (selectionTracker != null) {
            if (ProductItemsAdapter.this.selectionTracker.isSelected(holder.details.getSelectionKey())) {
                holder.mView.setBackgroundColor(pressedBackgroundColor);
                holder.imgCheck.setVisibility(View.VISIBLE);
                holder.mView.setActivated(true);
            } else {
                holder.mView.setBackgroundColor(normalBackgroundColor);
                holder.imgCheck.setVisibility(View.GONE);
                holder.mView.setActivated(false);
            }
        }
        if (mShowCancelButton) {
            holder.imgCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mValues.remove(pos);
                    notifyItemRemoved(pos);
                    notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void refreshList(List<UserContactsItem> userContactsItemList) {
        this.mValues = userContactsItemList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView contact_name;
        public final TextView txtAddress, txtMobile, txtEmail, txtTaxPercentage;
        public final CircleImageView imgProfile;
        public final ImageView imgCheck;
        public final ImageView imgCancel;
        public UserContactsItem mItem;
        private Details details;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            contact_name = view.findViewById(R.id.contact_name);
            txtAddress = view.findViewById(R.id.txtAddress);
            txtMobile = view.findViewById(R.id.txtMobile);
            txtEmail = view.findViewById(R.id.txtEmail);
            txtTaxPercentage = view.findViewById(R.id.txtTaxPercentage);
            imgProfile = view.findViewById(R.id.imgProfile);
            imgCheck = view.findViewById(R.id.imgCheck);
            imgCancel = view.findViewById(R.id.imgCancel);
            details = new Details();

            contact_name.setTypeface(robotoMedium);
            txtAddress.setTypeface(robotoRegular);
            txtEmail.setTypeface(robotoRegular);
            txtTaxPercentage.setTypeface(robotoRegular);
            txtMobile.setTypeface(robotoMedium);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + txtAddress.getText() + "'";
        }

        Details getItemDetails() {
            return details;
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                String filterString = constraint.toString().toLowerCase();
                FilterResults results = new FilterResults();

                final ArrayList<UserContactsItem> nlist = new ArrayList<UserContactsItem>();
                String filterableString;

                for (int i = 0; i < mValues.size(); i++) {
                    filterableString = mValues.get(i).getUserName();
                    if (filterableString.toLowerCase().contains(filterString)) {
                        nlist.add(mValues.get(i));
                    }
                }
                results.values = nlist;
                results.count = nlist.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                try {
                    mValues = (ArrayList<UserContactsItem>) results.values;
                    notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    public void sort() {
        Collections.sort(mValues, new Comparator<UserContactsItem>() {
            public int compare(UserContactsItem obj1, UserContactsItem obj2) {

                // ## Ascending order
                return obj1.getUserName().compareToIgnoreCase(obj2.getUserName()); // To compare string values
                // return Integer.valueOf(obj1.empId).compareTo(Integer.valueOf(obj2.empId)); // To compare integer values

                // ## Descending order
                // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
                // return Integer.valueOf(obj2.empId).compareTo(Integer.valueOf(obj1.empId)); // To compare integer values
            }
        });
        notifyDataSetChanged();
    }

    //    ******************************************************************************************************************
//  SELECTION TRACKER CODE.
    public void setSelectionTracker(
            SelectionTracker<Long> selectionTracker) {
        this.selectionTracker = selectionTracker;
    }

    public Selection<Long> getAllSelectionTrackerItems() {
        return this.selectionTracker.getSelection();
    }

    static class Details extends ItemDetailsLookup.ItemDetails<Long> {

        long position;

        Details() {
        }

        @Override
        public int getPosition() {
            return (int) position;
        }

        @Nullable
        @Override
        public Long getSelectionKey() {
            return position;
        }

        @Override
        public boolean inSelectionHotspot(@NonNull MotionEvent e) {
            return true;
        }
    }

    public static class KeyProvider extends ItemKeyProvider<Long> {

        public KeyProvider(RecyclerView.Adapter adapter) {
            super(ItemKeyProvider.SCOPE_MAPPED);
        }

        @Nullable
        @Override
        public Long getKey(int position) {
            return (long) position;
        }

        @Override
        public int getPosition(@NonNull Long key) {
            long value = key;
            return (int) value;
        }
    }

    public static class DetailsLookup extends ItemDetailsLookup<Long> {

        private RecyclerView recyclerView;

        public DetailsLookup(RecyclerView recyclerView) {
            this.recyclerView = recyclerView;
        }

        @Nullable
        @Override
        public ItemDetails<Long> getItemDetails(@NonNull MotionEvent e) {
            View view = recyclerView.findChildViewUnder(e.getX(), e.getY());
            if (view != null) {
                RecyclerView.ViewHolder viewHolder = recyclerView.getChildViewHolder(view);
                if (viewHolder instanceof ViewHolder) {
                    return ((ViewHolder) viewHolder).getItemDetails();
                }
            }
            return null;
        }
    }

    public static class Predicate extends SelectionTracker.SelectionPredicate<Long> {

        @Override
        public boolean canSetStateForKey(@NonNull Long key, boolean nextState) {
            return true;
        }

        @Override
        public boolean canSetStateAtPosition(int position, boolean nextState) {
            return true;
        }

        @Override
        public boolean canSelectMultiple() {
            return true;
        }
    }
//    SELECTION TRACKER CODE TILL HERE.
}