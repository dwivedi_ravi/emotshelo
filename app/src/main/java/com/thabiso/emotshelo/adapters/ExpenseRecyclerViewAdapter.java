package com.thabiso.emotshelo.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.selection.ItemKeyProvider;
import androidx.recyclerview.selection.Selection;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.RequestOptions;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.interfaces.OnExpenseListManagerInteractionListener;
import com.thabiso.emotshelo.models.expenses.ExpenseDetails;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link ExpenseDetails} and makes a call to the
 * specified {@link OnExpenseListManagerInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class ExpenseRecyclerViewAdapter extends RecyclerView.Adapter<ExpenseRecyclerViewAdapter.ViewHolder> implements Filterable {

    private List<ExpenseDetails> mValues;
    private final OnExpenseListManagerInteractionListener mListener;
    private SelectionTracker<Long> selectionTracker;
    private Context context;
    private int pressedBackgroundColor, normalBackgroundColor;
    private RequestOptions requestOptions;
    private boolean mShowCancelButton;
    private Typeface robotoBold, robotoRegular, robotoMedium;
    private int screenType;

    public ExpenseRecyclerViewAdapter(Context context, List<ExpenseDetails> items, OnExpenseListManagerInteractionListener listener, boolean showCancelButton, int screenType) {
        this.context = context;
        mValues = items;
        mListener = listener;
        pressedBackgroundColor = context.getResources().getColor(R.color.browser_actions_divider_color);
        normalBackgroundColor = context.getResources().getColor(R.color.transparent);
        requestOptions = Utility.getCustomizedRequestOptions(R.color.mdtp_line_background, R.drawable.round_avatar);
        mShowCancelButton = showCancelButton;
        this.robotoBold = Typeface.createFromAsset(context.getAssets(),
                "roboto_bold.ttf");
        this.robotoRegular = Typeface.createFromAsset(context.getAssets(),
                "roboto_regular.ttf");
        this.robotoMedium = Typeface.createFromAsset(context.getAssets(),
                "roboto_medium.ttf");
        this.screenType = screenType;
        sort();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.expense_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") int position) {
//        holder.mItem = mValues.get(position);
        holder.mItem = mValues.get(holder.getBindingAdapterPosition());

        holder.txtCategory.setText(mValues.get(position).getCategoryText());
        holder.txtParticulars.setText(mValues.get(position).getParticulars());

        if (screenType == Defaults.SCREEN_TYPE_DEPOSITS) {
//            holder.txtAmount.setTextColor(context.getResources().getColor(R.color.md_green_700));
            holder.txtAmount.setTextColor(context.getResources().getColor(R.color.colorAccent));
            holder.txtAmount.setText("+$" + mValues.get(position).getAmount());
            holder.imgProfile.setImageResource(R.drawable.ic_money);
        } else {
            holder.txtAmount.setTextColor(context.getResources().getColor(R.color.red));
            holder.txtAmount.setText("-$" + mValues.get(position).getAmount());
            switch (mValues.get(position).getCategory()) {
                case 2://Shopping
                    holder.imgProfile.setImageResource(R.drawable.ic_shopping_cart);
                    break;
                case 3://Transport
                    holder.imgProfile.setImageResource(R.drawable.ic_transport);
                    break;
                case 4://Food
                    holder.imgProfile.setImageResource(R.drawable.ic_food);
                    break;
                default://Other
                    holder.imgProfile.setImageResource(R.drawable.ic_minus_white_24dp);
                    break;
            }
        }

//        if (position % 2 == 0)
//            Glide.with(context).setDefaultRequestOptions(requestOptions).load(R.drawable.user_1)
//                    .into(holder.imgProfile);
//        else
//            Glide.with(context).setDefaultRequestOptions(requestOptions).load(R.drawable.user_2)
//                    .into(holder.imgProfile);

        if (position % 2 == 0)
            holder.imgProfile.setBackgroundResource(R.drawable.bg_deep_orange_circle);
        else if (position % 3 == 0)
            holder.imgProfile.setBackgroundResource(R.drawable.bg_blue_circle);
        else
            holder.imgProfile.setBackgroundResource(R.drawable.bg_green_circle);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
        holder.details.position = position;
        if (selectionTracker != null) {
            if (ExpenseRecyclerViewAdapter.this.selectionTracker.isSelected(holder.details.getSelectionKey())) {
                holder.mView.setBackgroundColor(pressedBackgroundColor);
                holder.imgCheck.setVisibility(View.VISIBLE);
                holder.mView.setActivated(true);
            } else {
                holder.mView.setBackgroundColor(normalBackgroundColor);
                holder.imgCheck.setVisibility(View.GONE);
                holder.mView.setActivated(false);
            }
        }
        if (mShowCancelButton) {
            holder.imgCancel.setVisibility(View.VISIBLE);
            holder.imgCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mValues.remove(position);
                    notifyItemRemoved(position);
                    notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView txtCategory;
        public final TextView txtParticulars;
        public final TextView txtAmount;
        public final TextView txtDate;
        public final AppCompatImageView imgProfile;
        public final ImageView imgCheck;
        public final ImageView imgCancel;
        public ExpenseDetails mItem;
        private Details details;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            txtCategory = view.findViewById(R.id.txtCategory);
            txtParticulars = view.findViewById(R.id.txtAddress);
            txtAmount = view.findViewById(R.id.txtAmount);
            txtDate = view.findViewById(R.id.txtDate);
            imgProfile = view.findViewById(R.id.imgProfile);
            imgCheck = view.findViewById(R.id.imgCheck);
            imgCancel = view.findViewById(R.id.imgCancel);
            details = new Details();

            txtCategory.setTypeface(robotoMedium);
            txtParticulars.setTypeface(robotoRegular);
            txtAmount.setTypeface(robotoMedium);
            txtDate.setTypeface(robotoMedium);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + txtParticulars.getText() + "'";
        }

        Details getItemDetails() {
            return details;
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                String filterString = constraint.toString().toLowerCase();
                FilterResults results = new FilterResults();

                final ArrayList<ExpenseDetails> nlist = new ArrayList<ExpenseDetails>();
                String filterableString;

                for (int i = 0; i < mValues.size(); i++) {
                    filterableString = mValues.get(i).getTransactionDatetime();
                    if (filterableString.toLowerCase().contains(filterString)) {
                        nlist.add(mValues.get(i));
                    }
                }
                results.values = nlist;
                results.count = nlist.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                try {
                    mValues = (ArrayList<ExpenseDetails>) results.values;
                    notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    public void sort() {
        SimpleDateFormat sdf = new SimpleDateFormat(Defaults.DEVICE_DATE_FORMAT);

        Collections.sort(mValues, (obj1, obj2) -> {
            Date d1 = null;
            Date d2 = null;
            try {
                if (obj1.getTransactionDatetime() == null || obj2.getTransactionDatetime() == null) {
                    return 0;
                }
                d1 = sdf.parse(obj1.getTransactionDatetime());
                d2 = sdf.parse(obj2.getTransactionDatetime());

                return (d1.getTime() > d2.getTime() ? -1 : 1);     //descending
//                    return (d1.getTime() > d2.getTime() ? 1 : -1);     //ascending
            } catch (Exception e) {
                e.printStackTrace();
            }
            return 0;
        });
        notifyDataSetChanged();
    }


    //    ******************************************************************************************************************
//  SELECTION TRACKER CODE.
    public void setSelectionTracker(
            SelectionTracker<Long> selectionTracker) {
        this.selectionTracker = selectionTracker;
    }

    public Selection<Long> getAllSelectionTrackerItems() {
        return this.selectionTracker.getSelection();
    }

    static class Details extends ItemDetailsLookup.ItemDetails<Long> {

        long position;

        Details() {
        }

        @Override
        public int getPosition() {
            return (int) position;
        }

        @Nullable
        @Override
        public Long getSelectionKey() {
            return position;
        }

        @Override
        public boolean inSelectionHotspot(@NonNull MotionEvent e) {
            return true;
        }
    }

    public static class KeyProvider extends ItemKeyProvider<Long> {

        public KeyProvider(RecyclerView.Adapter adapter) {
            super(ItemKeyProvider.SCOPE_MAPPED);
        }

        @Nullable
        @Override
        public Long getKey(int position) {
            return (long) position;
        }

        @Override
        public int getPosition(@NonNull Long key) {
            long value = key;
            return (int) value;
        }
    }

    public static class DetailsLookup extends ItemDetailsLookup<Long> {

        private RecyclerView recyclerView;

        public DetailsLookup(RecyclerView recyclerView) {
            this.recyclerView = recyclerView;
        }

        @Nullable
        @Override
        public ItemDetails<Long> getItemDetails(@NonNull MotionEvent e) {
            View view = recyclerView.findChildViewUnder(e.getX(), e.getY());
            if (view != null) {
                RecyclerView.ViewHolder viewHolder = recyclerView.getChildViewHolder(view);
                if (viewHolder instanceof ViewHolder) {
                    return ((ViewHolder) viewHolder).getItemDetails();
                }
            }
            return null;
        }
    }

    public static class Predicate extends SelectionTracker.SelectionPredicate<Long> {

        @Override
        public boolean canSetStateForKey(@NonNull Long key, boolean nextState) {
            return true;
        }

        @Override
        public boolean canSetStateAtPosition(int position, boolean nextState) {
            return true;
        }

        @Override
        public boolean canSelectMultiple() {
            return true;
        }
    }
//    SELECTION TRACKER CODE TILL HERE.
}
