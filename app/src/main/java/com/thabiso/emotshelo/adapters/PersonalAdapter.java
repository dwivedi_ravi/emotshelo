package com.thabiso.emotshelo.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.activities.howitworks.FinancialEducationActivity;
import com.thabiso.emotshelo.models.grouplist.UserGroupsItem;
import com.thabiso.emotshelo.util.preferences.Defaults;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/***
 * The adapter class for the RecyclerView, contains the chapters data
 */
public class PersonalAdapter extends RecyclerView.Adapter<PersonalAdapter.ViewHolder> {

    //Member variables
    private List<UserGroupsItem> UserGroupsList;
    private Context mContext;
    private Typeface robotoBold, robotoRegular, robotoMedium;

    /**
     * Constructor that passes in the chapters data and the context
     *
     * @param UserGroupsList ArrayList containing the chapters data
     * @param context            Context of the application
     */
    public PersonalAdapter(Context context, List<UserGroupsItem> UserGroupsList) {
        this.UserGroupsList = UserGroupsList;
        this.mContext = context;
        this.robotoBold = Typeface.createFromAsset(context.getAssets(),
                "roboto_bold.ttf");
        this.robotoRegular = Typeface.createFromAsset(context.getAssets(),
                "roboto_regular.ttf");
        this.robotoMedium = Typeface.createFromAsset(context.getAssets(),
                "roboto_medium.ttf");
    }


    /**
     * Required method for creating the viewholder objects.
     *
     * @param parent   The ViewGroup into which the new View will be added after it is bound to an adapter position.
     * @param viewType The view type of the new View.
     * @return The newly create ViewHolder.
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.list_item_personal, parent, false));
    }

    /**
     * Required method that binds the data to the viewholder.
     *
     * @param holder   The viewholder into which the data should be put.
     * @param position The adapter position.
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //Get current chapter
//        UserGroupsItem currentGroup = UserGroupsList.get(position);
        UserGroupsItem currentGroup = UserGroupsList.get(holder.getBindingAdapterPosition());

        holder.bindTo(currentGroup);
//        Glide.with(mContext).load(currentChapter.getImageResource()).into(holder.mChapterImage);
        Glide.with(mContext).load(R.drawable.round_avatar).into(holder.mChapterImage);
//        Glide.with(mContext).load(R.drawable.group_icon).into(holder.mChapterImage);
    }


    /**
     * Required method for determining the size of the data set.
     *
     * @return Size of the data set.
     */
    @Override
    public int getItemCount() {
        return UserGroupsList.size();
    }

    public void refreshList(List<UserGroupsItem> UserGroupsList) {
        this.UserGroupsList = UserGroupsList;
    }

    /**
     * ViewHolder class that represents each row of data in the RecyclerView
     */
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //Member Variables for the TextViews
        private TextView mTitleText;
        private TextView mInfoText;
        private TextView mNewsTitle;
        private TextView mVerseCount;
        private CircleImageView mChapterImage;

        /**
         * Constructor for the ViewHolder, used in onCreateViewHolder().
         *
         * @param itemView The rootview of the list_item.xml layout file
         */
        ViewHolder(View itemView) {
            super(itemView);

            //Initialize the views
            mTitleText = itemView.findViewById(R.id.title);
            mInfoText = itemView.findViewById(R.id.subTitle);
            mNewsTitle = itemView.findViewById(R.id.newsTitle);
            mVerseCount = itemView.findViewById(R.id.verseCount);
            mChapterImage = itemView.findViewById(R.id.chapterImage);

            mTitleText.setTypeface(robotoMedium);
            mNewsTitle.setTypeface(robotoRegular);
            mVerseCount.setTypeface(robotoRegular);
            mInfoText.setTypeface(robotoRegular);

            itemView.setOnClickListener(this);
        }

        void bindTo(UserGroupsItem objUserGroups) {
            //Populate the textviews with data
            mTitleText.setText(objUserGroups.getName());
            mInfoText.setText(objUserGroups.getDescription());

            if (objUserGroups.getId().equals(Defaults.FINANCIAL_LITERACY_TOOL_PERSONAL)) {
                mNewsTitle.setText("Total Tools: " + objUserGroups.getAmountPerRound());
                mVerseCount.setText("");
            } else {
                mNewsTitle.setText("Contribution: " + objUserGroups.getAmountPerRound());
                mVerseCount.setText("Members: " + objUserGroups.getNumberOfMembers());
            }
        }

        @Override
        public void onClick(View view) {
            UserGroupsItem currentGroup = UserGroupsList.get(getBindingAdapterPosition());

            if (currentGroup.getId().equals(Defaults.FINANCIAL_LITERACY_TOOL_PERSONAL)) {
                Intent detailIntent = new Intent(mContext, FinancialEducationActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("currentGroup", currentGroup);
                detailIntent.putExtras(bundle);
                mContext.startActivity(detailIntent);
            } else {
//                Intent detailIntent = new Intent(mContext, InvoicingActivity.class);
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("currentGroup", currentGroup);
//                detailIntent.putExtras(bundle);
//                mContext.startActivity(detailIntent);
            }
        }
    }
}


//package com.thabiso.emotshelo.adapters;
//
//        import android.content.Context;
//        import android.graphics.Typeface;
//        import android.view.LayoutInflater;
//        import android.view.View;
//        import android.view.ViewGroup;
//        import android.widget.TextView;
//
//        import androidx.recyclerview.widget.RecyclerView;
//
//        import com.bumptech.glide.Glide;
//        import com.thabiso.emotshelo.R;
//        import com.thabiso.emotshelo.models.creategroup.UserGroupsItem;
//
//        import java.util.List;
//
//        import de.hdodenhof.circleimageview.CircleImageView;
//
///***
// * The adapter class for the RecyclerView, contains the chapters data
// */
//public class PersonalAdapter extends RecyclerView.Adapter<PersonalAdapter.ViewHolder> {
//
//    //Member variables
//    private List<UserGroupsItem> UserGroupsList;
//    private Context mContext;
//    private Typeface robotoBold, robotoRegular, robotoMedium;
//
//    /**
//     * Constructor that passes in the chapters data and the context
//     *
//     * @param UserGroupsList ArrayList containing the chapters data
//     * @param context            Context of the application
//     */
//    public PersonalAdapter(Context context, List<UserGroupsItem> UserGroupsList) {
//        this.UserGroupsList = UserGroupsList;
//        this.mContext = context;
//        this.robotoBold = Typeface.createFromAsset(context.getAssets(),
//                "roboto_bold.ttf");
//        this.robotoRegular = Typeface.createFromAsset(context.getAssets(),
//                "roboto_regular.ttf");
//        this.robotoMedium = Typeface.createFromAsset(context.getAssets(),
//                "roboto_medium.ttf");
//    }
//
//
//    /**
//     * Required method for creating the viewholder objects.
//     *
//     * @param parent   The ViewGroup into which the new View will be added after it is bound to an adapter position.
//     * @param viewType The view type of the new View.
//     * @return The newly create ViewHolder.
//     */
//    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.list_item_personal, parent, false));
//    }
//
//    /**
//     * Required method that binds the data to the viewholder.
//     *
//     * @param holder   The viewholder into which the data should be put.
//     * @param position The adapter position.
//     */
//    @Override
//    public void onBindViewHolder(ViewHolder holder, int position) {
//        //Get current chapter
//        UserGroupsItem currentGroup = UserGroupsList.get(position);
//        holder.bindTo(currentGroup);
////        Glide.with(mContext).load(currentChapter.getImageResource()).into(holder.mChapterImage);
//        Glide.with(mContext).load(R.drawable.round_avatar).into(holder.mChapterImage);
////        Glide.with(mContext).load(R.drawable.group_icon).into(holder.mChapterImage);
//    }
//
//
//    /**
//     * Required method for determining the size of the data set.
//     *
//     * @return Size of the data set.
//     */
//    @Override
//    public int getItemCount() {
//        return UserGroupsList.size();
//    }
//
//    public void refreshList(List<UserGroupsItem> UserGroupsList) {
//        this.UserGroupsList = UserGroupsList;
//    }
//
//    /**
//     * ViewHolder class that represents each row of data in the RecyclerView
//     */
//    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
//
//        //Member Variables for the TextViews
//        private TextView mTitleText;
//        private TextView mInfoText;
//        private TextView mNewsTitle;
//        private TextView mVerseCount;
//        private CircleImageView mChapterImage;
//
//        /**
//         * Constructor for the ViewHolder, used in onCreateViewHolder().
//         *
//         * @param itemView The rootview of the list_item.xml layout file
//         */
//        ViewHolder(View itemView) {
//            super(itemView);
//
//            //Initialize the views
//            mTitleText = itemView.findViewById(R.id.title);
//            mInfoText = itemView.findViewById(R.id.subTitle);
//            mNewsTitle = itemView.findViewById(R.id.newsTitle);
//            mVerseCount = itemView.findViewById(R.id.verseCount);
//            mChapterImage = itemView.findViewById(R.id.chapterImage);
//
//            mTitleText.setTypeface(robotoMedium);
//            mNewsTitle.setTypeface(robotoRegular);
//            mVerseCount.setTypeface(robotoRegular);
//            mInfoText.setTypeface(robotoRegular);
//
//            itemView.setOnClickListener(this);
//        }
//
//        void bindTo(UserGroupsItem objUserGroups) {
//            //Populate the textviews with data
//            mTitleText.setText(objUserGroups.getName());
//            mInfoText.setText(objUserGroups.getDescription());
//            mNewsTitle.setText("Contribution: " + objUserGroups.getAmountPerRound());
//            mVerseCount.setText("Members: " + objUserGroups.getNumberOfMembers());
//        }
//
//        @Override
//        public void onClick(View view) {
////            UserGroupsItem currentGroup = UserGroupsList.get(getBindingAdapterPosition());
////
////            Intent detailIntent = new Intent(mContext, GroupDetailsActivity.class);
////            Bundle bundle = new Bundle();
////            bundle.putSerializable("currentGroup", currentGroup);
////            detailIntent.putExtras(bundle);
////
////            mContext.startActivity(detailIntent);
//        }
//    }
//}

