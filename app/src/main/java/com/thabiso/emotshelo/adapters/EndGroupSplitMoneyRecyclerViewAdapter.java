package com.thabiso.emotshelo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.models.groupdetails.UserMembersItem;
import com.thabiso.emotshelo.util.Utility;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class EndGroupSplitMoneyRecyclerViewAdapter extends RecyclerView.Adapter<EndGroupSplitMoneyRecyclerViewAdapter.ViewHolder> {

    private List<UserMembersItem> mValues;
    private Context context;
    private RequestOptions requestOptions;
    private String perPersonAmount;

    public EndGroupSplitMoneyRecyclerViewAdapter(Context context, List<UserMembersItem> items, String perPersonAmount) {
        this.context = context;
        mValues = items;
        requestOptions = Utility.getCustomizedRequestOptions(R.color.mdtp_line_background, R.drawable.round_avatar);
        this.perPersonAmount = perPersonAmount;
        sort();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_end_group_split_money, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
//        holder.mItem = mValues.get(position);
        holder.mItem = mValues.get(holder.getBindingAdapterPosition());

        holder.contact_name.setText(mValues.get(position).getUserProfile().getName());
        holder.txtAmount.setText(perPersonAmount);

//        Glide.with(context).setDefaultRequestOptions(requestOptions).load(mValues.get(position).photoUrl)
//                .into(holder.imgProfile);

        if (position % 2 == 0)
            Glide.with(context).setDefaultRequestOptions(requestOptions).load(R.drawable.user_1)
                    .into(holder.imgProfile);
        else
            Glide.with(context).setDefaultRequestOptions(requestOptions).load(R.drawable.user_2)
                    .into(holder.imgProfile);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView contact_name, txtAmount;
        public final CircleImageView imgProfile;
        public UserMembersItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            contact_name = view.findViewById(R.id.contact_name);
            txtAmount = view.findViewById(R.id.txtAmount);
            imgProfile = view.findViewById(R.id.imgProfile);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    public void sort() {
        Collections.sort(mValues, new Comparator<UserMembersItem>() {
            public int compare(UserMembersItem obj1, UserMembersItem obj2) {

                // ## Ascending order
                return obj1.getUserProfile().getName().compareToIgnoreCase(obj2.getUserProfile().getName()); // To compare string values
                // return Integer.valueOf(obj1.empId).compareTo(Integer.valueOf(obj2.empId)); // To compare integer values

                // ## Descending order
                // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
                // return Integer.valueOf(obj2.empId).compareTo(Integer.valueOf(obj1.empId)); // To compare integer values
            }
        });
        notifyDataSetChanged();
    }
}
