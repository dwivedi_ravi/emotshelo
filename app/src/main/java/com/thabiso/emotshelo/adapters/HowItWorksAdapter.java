package com.thabiso.emotshelo.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.activities.howitworks.HowItWorksActivity;
import com.thabiso.emotshelo.activities.howitworks.HowItWorksDetailsActivity;
import com.thabiso.emotshelo.models.HowItWorksBO;
import com.thabiso.emotshelo.util.preferences.Prefs;

import java.util.ArrayList;

/***
 * The adapter class for the RecyclerView, contains the chapters data
 */
public class HowItWorksAdapter extends RecyclerView.Adapter<HowItWorksAdapter.ViewHolder> {

    private ArrayList<HowItWorksBO> mChapterData;
    private Context mContext;
    private Typeface robotoRegular, robotoMedium;
    private int savedChapterIndex;

    /**
     * Constructor that passes in the chapters data and the context
     *
     * @param chapterData ArrayList containing the chapters data
     * @param context     Context of the application
     */
    public HowItWorksAdapter(Context context, ArrayList<HowItWorksBO> chapterData) {
        this.mChapterData = chapterData;
        this.mContext = context;
        this.robotoRegular = Typeface.createFromAsset(context.getAssets(),
                "roboto_regular.ttf");
        this.robotoMedium = Typeface.createFromAsset(context.getAssets(),
                "roboto_medium.ttf");
        setSavedChapterIndex(Prefs.getCurrentHowItWorksVideoIndexFromSharedPreference());
    }


    /**
     * Required method for creating the viewholder objects.
     *
     * @param parent   The ViewGroup into which the new View will be added after it is bound to an adapter position.
     * @param viewType The view type of the new View.
     * @return The newly create ViewHolder.
     */
    @Override
    public HowItWorksAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.list_item_how_it_works, parent, false));
    }

    /**
     * Required method that binds the data to the viewholder.
     *
     * @param holder   The viewholder into which the data should be put.
     * @param position The adapter position.
     */
    @Override
    public void onBindViewHolder(HowItWorksAdapter.ViewHolder holder, int position) {
//        HowItWorksBO currentChapter = mChapterData.get(position);
        HowItWorksBO currentChapter = mChapterData.get(holder.getBindingAdapterPosition());

        holder.bindTo(position, currentChapter);
        Glide.with(mContext).load(currentChapter.getImageResource()).into(holder.mChapterImage);
    }


    /**
     * Required method for determining the size of the data set.
     *
     * @return Size of the data set.
     */
    @Override
    public int getItemCount() {
        return mChapterData.size();
    }

    public void setSavedChapterIndex(int savedChapterIndex) {
        this.savedChapterIndex = savedChapterIndex;
    }

    /**
     * ViewHolder class that represents each row of data in the RecyclerView
     */
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //Member Variables for the TextViews
        private TextView mTitleText;
        private TextView mInfoText;
        private ImageView mChapterImage;

        /**
         * Constructor for the ViewHolder, used in onCreateViewHolder().
         *
         * @param itemView The rootview of the list_item.xml layout file
         */
        ViewHolder(View itemView) {
            super(itemView);

            //Initialize the views
            mTitleText = itemView.findViewById(R.id.title);
            mInfoText = itemView.findViewById(R.id.subTitle);
            mChapterImage = itemView.findViewById(R.id.chapterImage);

            mTitleText.setTypeface(robotoMedium);
            mInfoText.setTypeface(robotoRegular);

            itemView.setOnClickListener(this);
        }

        void bindTo(int position, HowItWorksBO currentChapter) {
            mTitleText.setText(currentChapter.getName());
            mInfoText.setText(currentChapter.getNameMeaning());
        }

        @Override
        public void onClick(View view) {
            HowItWorksBO currentChapter = mChapterData.get(getBindingAdapterPosition());

            Intent detailIntent = new Intent(mContext, HowItWorksDetailsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("currentChapter", currentChapter);
            detailIntent.putExtras(bundle);

            ((HowItWorksActivity) mContext).switchAnimation(mChapterImage, mChapterImage, detailIntent, mContext);
        }
    }
}
