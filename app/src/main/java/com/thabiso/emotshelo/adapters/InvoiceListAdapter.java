package com.thabiso.emotshelo.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.selection.ItemKeyProvider;
import androidx.recyclerview.selection.Selection;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.widget.RecyclerView;

import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.activities.business_tool.invoicing.InvoiceDetailsPage;
import com.thabiso.emotshelo.models.grouplist.UserGroupsItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class InvoiceListAdapter extends RecyclerView.Adapter<InvoiceListAdapter.ViewHolder> implements Filterable {

    private List<UserGroupsItem> mValues;
    private SelectionTracker<Long> selectionTracker;
    private Context context;
    private Typeface robotoBold, robotoRegular, robotoMedium;

    public InvoiceListAdapter(Context context, List<UserGroupsItem> items) {
        this.context = context;
        mValues = items;
        this.robotoBold = Typeface.createFromAsset(context.getAssets(),
                "roboto_bold.ttf");
        this.robotoRegular = Typeface.createFromAsset(context.getAssets(),
                "roboto_regular.ttf");
        this.robotoMedium = Typeface.createFromAsset(context.getAssets(),
                "roboto_medium.ttf");
//        sort();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.invoice_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") int position) {
//        holder.mItem = mValues.get(position);
        int pos = holder.getBindingAdapterPosition();
        holder.mItem = mValues.get(pos);

        holder.txtInvoiceTitle.setText(mValues.get(pos).getName());
        holder.txtParticulars.setText(mValues.get(pos).getDescription());

//        holder.txtAmount.setTextColor(context.getResources().getColor(R.color.colorAccent));
        holder.txtAmount.setText(mValues.get(pos).getAmountPerRound());
        holder.txtDate.setText(mValues.get(pos).getCreatedAt());
        holder.imgProfile.setImageResource(R.drawable.ic_money);

        if (pos % 2 == 0)
            holder.imgProfile.setBackgroundResource(R.drawable.bg_deep_orange_circle);
        else if (pos % 3 == 0)
            holder.imgProfile.setBackgroundResource(R.drawable.bg_blue_circle);
        else
            holder.imgProfile.setBackgroundResource(R.drawable.bg_green_circle);

        holder.details.position = pos;

        if (selectionTracker != null) {
            if (InvoiceListAdapter.this.selectionTracker.isSelected(holder.details.getSelectionKey())) {
                holder.imgCheck.setVisibility(View.VISIBLE);
                holder.mView.setActivated(true);
            } else {
                holder.imgCheck.setVisibility(View.GONE);
                holder.mView.setActivated(false);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final View mView;
        public final TextView txtInvoiceTitle;
        public final TextView txtParticulars;
        public final TextView txtAmount;
        public final TextView txtDate;
        public final AppCompatImageView imgProfile;
        public final ImageView imgCheck;
        public UserGroupsItem mItem;
        private Details details;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            txtInvoiceTitle = view.findViewById(R.id.txtInvoiceTitle);
            txtParticulars = view.findViewById(R.id.txtAddress);
            txtAmount = view.findViewById(R.id.txtAmount);
            txtDate = view.findViewById(R.id.txtDate);
            imgProfile = view.findViewById(R.id.imgProfile);
            imgCheck = view.findViewById(R.id.imgCheck);
            details = new Details();

            txtInvoiceTitle.setTypeface(robotoMedium);
            txtParticulars.setTypeface(robotoRegular);
            txtAmount.setTypeface(robotoMedium);
            txtDate.setTypeface(robotoMedium);

            mView.setOnClickListener(this);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + txtParticulars.getText() + "'";
        }

        Details getItemDetails() {
            return details;
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, InvoiceDetailsPage.class);
            context.startActivity(intent);
        }
    }

    public void refreshList(List<UserGroupsItem> mValues) {
        this.mValues = mValues;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                String filterString = constraint.toString().toLowerCase();
                FilterResults results = new FilterResults();

                final ArrayList<UserGroupsItem> nlist = new ArrayList<UserGroupsItem>();
                String filterableString;

                for (int i = 0; i < mValues.size(); i++) {
                    filterableString = mValues.get(i).getName();
                    if (filterableString.toLowerCase().contains(filterString)) {
                        nlist.add(mValues.get(i));
                    }
                }
                results.values = nlist;
                results.count = nlist.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                try {
                    mValues = (ArrayList<UserGroupsItem>) results.values;
                    notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    public void sort() {
        Collections.sort(mValues, new Comparator<UserGroupsItem>() {
            public int compare(UserGroupsItem obj1, UserGroupsItem obj2) {

                // ## Ascending order
                return obj1.getName().compareToIgnoreCase(obj2.getName()); // To compare string values
                // return Integer.valueOf(obj1.empId).compareTo(Integer.valueOf(obj2.empId)); // To compare integer values

                // ## Descending order
                // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
                // return Integer.valueOf(obj2.empId).compareTo(Integer.valueOf(obj1.empId)); // To compare integer values
            }
        });
        notifyDataSetChanged();
    }

    //    ******************************************************************************************************************
//  SELECTION TRACKER CODE.
    public void setSelectionTracker(
            SelectionTracker<Long> selectionTracker) {
        this.selectionTracker = selectionTracker;
    }

    public Selection<Long> getAllSelectionTrackerItems() {
        return this.selectionTracker.getSelection();
    }

    static class Details extends ItemDetailsLookup.ItemDetails<Long> {

        long position;

        Details() {
        }

        @Override
        public int getPosition() {
            return (int) position;
        }

        @Nullable
        @Override
        public Long getSelectionKey() {
            return position;
        }

        @Override
        public boolean inSelectionHotspot(@NonNull MotionEvent e) {
            return true;
        }
    }

    public static class KeyProvider extends ItemKeyProvider<Long> {

        public KeyProvider(RecyclerView.Adapter adapter) {
            super(ItemKeyProvider.SCOPE_MAPPED);
        }

        @Nullable
        @Override
        public Long getKey(int position) {
            return (long) position;
        }

        @Override
        public int getPosition(@NonNull Long key) {
            long value = key;
            return (int) value;
        }
    }

    public static class DetailsLookup extends ItemDetailsLookup<Long> {

        private RecyclerView recyclerView;

        public DetailsLookup(RecyclerView recyclerView) {
            this.recyclerView = recyclerView;
        }

        @Nullable
        @Override
        public ItemDetails<Long> getItemDetails(@NonNull MotionEvent e) {
            View view = recyclerView.findChildViewUnder(e.getX(), e.getY());
            if (view != null) {
                RecyclerView.ViewHolder viewHolder = recyclerView.getChildViewHolder(view);
                if (viewHolder instanceof ViewHolder) {
                    return ((ViewHolder) viewHolder).getItemDetails();
                }
            }
            return null;
        }
    }

    public static class Predicate extends SelectionTracker.SelectionPredicate<Long> {

        @Override
        public boolean canSetStateForKey(@NonNull Long key, boolean nextState) {
            return true;
        }

        @Override
        public boolean canSetStateAtPosition(int position, boolean nextState) {
            return true;
        }

        @Override
        public boolean canSelectMultiple() {
            return true;
        }
    }
//    SELECTION TRACKER CODE TILL HERE.
}
