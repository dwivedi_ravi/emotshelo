package com.thabiso.emotshelo.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.activities.business_tool.CompanyDetailsActivity;
import com.thabiso.emotshelo.activities.howitworks.FinancialEducationActivity;
import com.thabiso.emotshelo.models.addcompany.DataItem;
import com.thabiso.emotshelo.models.addcompany.RequestUpdateToolStatus;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;
import com.thabiso.emotshelo.util.preferences.Prefs;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/***
 * The adapter class for the RecyclerView, contains the chapters data
 */
public class EnterpriseAdapter extends RecyclerView.Adapter<EnterpriseAdapter.ViewHolder> {

    //Member variables
    private List<DataItem> companyList;
    private Context mContext;
    private Typeface robotoBold, robotoRegular, robotoMedium;
    private RequestOptions requestOptions;

    /**
     * Constructor that passes in the chapters data and the context
     *
     * @param companyList ArrayList containing the chapters data
     * @param context     Context of the application
     */
    public EnterpriseAdapter(Context context, List<DataItem> companyList) {
        this.companyList = companyList;
        this.mContext = context;
        this.robotoBold = Typeface.createFromAsset(context.getAssets(),
                "roboto_bold.ttf");
        this.robotoRegular = Typeface.createFromAsset(context.getAssets(),
                "roboto_regular.ttf");
        this.robotoMedium = Typeface.createFromAsset(context.getAssets(),
                "roboto_medium.ttf");
        requestOptions = Utility.getCustomizedRequestOptions(R.color.mdtp_line_background, R.drawable.round_avatar);
    }


    /**
     * Required method for creating the viewholder objects.
     *
     * @param parent   The ViewGroup into which the new View will be added after it is bound to an adapter position.
     * @param viewType The view type of the new View.
     * @return The newly create ViewHolder.
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.list_item_enterprise, parent, false));
    }

    /**
     * Required method that binds the data to the viewholder.
     *
     * @param holder   The viewholder into which the data should be put.
     * @param position The adapter position.
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //Get current chapter
//        DataItem currentGroup = companyList.get(position);
        DataItem currentGroup = companyList.get(holder.getBindingAdapterPosition());

        holder.bindTo(currentGroup);
//        Glide.with(mContext).load(R.drawable.round_avatar).into(holder.mChapterImage);
    }


    /**
     * Required method for determining the size of the data set.
     *
     * @return Size of the data set.
     */
    @Override
    public int getItemCount() {
        return companyList.size();
    }

    public void refreshList(List<DataItem> UserGroupsList) {
        this.companyList = UserGroupsList;
    }

    /**
     * ViewHolder class that represents each row of data in the RecyclerView
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //Member Variables for the TextViews
        private TextView mTitleText;
        private TextView mInfoText;
        private TextView mNewsTitle;
        private TextView mVerseCount;
        private CircleImageView mChapterImage;

        /**
         * Constructor for the ViewHolder, used in onCreateViewHolder().
         *
         * @param itemView The rootview of the list_item.xml layout file
         */
        ViewHolder(View itemView) {
            super(itemView);

            //Initialize the views
            mTitleText = itemView.findViewById(R.id.title);
            mInfoText = itemView.findViewById(R.id.subTitle);
            mNewsTitle = itemView.findViewById(R.id.newsTitle);
            mVerseCount = itemView.findViewById(R.id.verseCount);
            mChapterImage = itemView.findViewById(R.id.chapterImage);

            mTitleText.setTypeface(robotoMedium);
            mNewsTitle.setTypeface(robotoMedium);
            mVerseCount.setTypeface(robotoRegular);
            mInfoText.setTypeface(robotoRegular);

            itemView.setOnClickListener(this);
        }

        void bindTo(DataItem objUserGroups) {
            //Populate the textviews with data
            mTitleText.setText(objUserGroups.getCompanyName());
            mInfoText.setText(objUserGroups.getCompanyAddress());
            mNewsTitle.setText(objUserGroups.getEmailid());
//            mVerseCount.setText(objUserGroups.getCreatedAt());
            mVerseCount.setText("");

            Glide.with(mContext).setDefaultRequestOptions(requestOptions).load(objUserGroups.getLogo())
                    .into(mChapterImage);
        }

        @Override
        public void onClick(View view) {
            DataItem selectedCompany = companyList.get(getBindingAdapterPosition());

            if (selectedCompany.getId().equals(Defaults.FINANCIAL_LITERACY_TOOL_ENTERPRISE)) {
                Intent detailIntent = new Intent(mContext, FinancialEducationActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("currentGroup", selectedCompany);
                detailIntent.putExtras(bundle);
                mContext.startActivity(detailIntent);
            } else {
//                if (selectedCompany.getTool() != null && selectedCompany.getTool().size() > 0 && selectedCompany.getTool().get(0).getToolId().equals(Defaults.TOOL_ID_ENTERPRICE + "") && selectedCompany.getTool().get(0).getSubToolId().equals(Defaults.SUB_TOOL_ID_INVOICING_AND_QUOTATION + "") && selectedCompany.getTool().get(0).getStatus().equals(Defaults.TOOL_ACTIVATED)) {
//                    Intent detailIntent = new Intent(mContext, InvoicingActivity.class);
//                    Bundle bundle = new Bundle();
//                    bundle.putSerializable("selectedCompany", selectedCompany);
//                    detailIntent.putExtras(bundle);
//                    mContext.startActivity(detailIntent);
//                } else {
//                    // OPEN ENABLE TOOL ACTIVITY
//                    showEnableToolDialog(mContext, companyList, EnterpriseAdapter.this, getBindingAdapterPosition());
//                }

                Intent detailIntent = new Intent(mContext, CompanyDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("position",getBindingAdapterPosition());
                bundle.putSerializable("selectedCompany", selectedCompany);
                detailIntent.putExtras(bundle);
                mContext.startActivity(detailIntent);
            }
        }
    }

    private void showEnableToolDialog(Context context, List<DataItem> companyList, RecyclerView.Adapter<EnterpriseAdapter.ViewHolder> adapter, int position) {
        DataItem selectedCompany = companyList.get(position);

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_enable_tool);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        SwitchCompat switchEnableTool = dialog.findViewById(R.id.switchEnableTool);
        switchEnableTool.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                RequestUpdateToolStatus objRequestUpdateGroupStatus = new RequestUpdateToolStatus();
                objRequestUpdateGroupStatus.setUserId(Prefs.getUserId());
                objRequestUpdateGroupStatus.setCompanyId(selectedCompany.getId());
                objRequestUpdateGroupStatus.setToolId(Defaults.TOOL_ID_ENTERPRICE + "");
                objRequestUpdateGroupStatus.setSubToolId(Defaults.SUB_TOOL_ID_INVOICING_AND_QUOTATION + "");
                objRequestUpdateGroupStatus.setName(context.getResources().getString(R.string.card_title_invoicing));

                if (b) {
                    objRequestUpdateGroupStatus.setStatus(Defaults.TOOL_ACTIVATED);
                } else {
                    objRequestUpdateGroupStatus.setStatus(Defaults.TOOL_DEACTIVATED);
                }
//                ApiCallerUtility.callUpdateToolStatusApi(mContext, objRequestUpdateGroupStatus, companyList, adapter, position);
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }
}
