package com.thabiso.emotshelo.database;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.thabiso.emotshelo.models.editprofile.ResponseEditProfile;
import com.thabiso.emotshelo.models.editprofile.User;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;
import com.thabiso.emotshelo.util.preferences.Prefs;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


public class DBHandler extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "EMOTSHELODB";
    private static final int DATABASE_VERSION = 1;
    private static DBHandler DBHnadlerInstance = null;
    private static SQLiteDatabase db;
    private Context context;

    private static final String TBPROFILEDATA = "TBPROFILEDATA";

    static {
//        register our models
//        cupboard().register(TBGRAPHDATA.class);
    }

    private DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        db = this.getWritableDatabase();
    }

    public static DBHandler getInstance(Context ctx) {
        if (DBHnadlerInstance == null) {
            DBHnadlerInstance = new DBHandler(ctx);
        }
        return DBHnadlerInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        if (DATABASE_VERSION == 1) {
//            db.execSQL("CREATE TABLE IF NOT EXISTS TBIMAGEUPLOAD (FILENAME text, FILEPATH text, UPLOADSTATUS text)");
//            ExecuteQuery("CREATE TABLE IF NOT EXISTS TBIMAGEUPLOAD (FILENAME text, FILEPATH text, UPLOADSTATUS text)");

//            db.execSQL("CREATE TABLE IF NOT EXISTS " + TBPROFILEDATA + " (USER_ID text, NAME text, EMAIL text, EMAIL_VERIFIED_AT text, FIRST_NAME text, LAST_NAME text, MOBILE_NUMBER text, DOB text, ABOUT text, IS_MOBILE_VERIFIED text, CREATED_AT text, UPDATED_AT text, DELETED_AT text)");
            db.execSQL("CREATE TABLE IF NOT EXISTS " + TBPROFILEDATA + " (USER_ID text, MOBILE_NUMBER text, VALUE text)");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            if (oldVersion < newVersion) {
                if (newVersion == 2) {
//                    alterTableAddColumn(db, "TXNSAMPLE", "AMT");
                } else {
//                    db.execSQL("ALTER TABLE TXN102 ADD COLUMN COL22 TEXT");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void alterTableAddColumn(SQLiteDatabase db, String tableName, String columnName) {
        try {
            db.execSQL("ALTER TABLE " + tableName + " ADD COLUMN " + columnName + " TEXT DEFAULT ''");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void createDataBase() throws IOException {

        boolean dbExist = checkDataBase();
        if (dbExist) {
            //do nothing - database already exist
        } else {
            // By calling this method and empty database will be created into the
            // default system path
            // of your application so we are gonna be able to overwrite that
            // database with our database.
            this.getReadableDatabase();
            new DatabaseCopyBackground().execute();
        }
    }

    /**
     * Check if the database already exist to avoid re-copying the file each
     * time you open the application.
     *
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDataBase() {

        SQLiteDatabase checkDB = null;

        try {
            String myPath = context.getDatabasePath(DATABASE_NAME) + "";

            checkDB = SQLiteDatabase.openDatabase(myPath, null,
                    SQLiteDatabase.OPEN_READONLY);

        } catch (SQLiteException e) {
            // database does't exist yet.
        }

        if (checkDB != null) {
            if (ifValuePresentInDB("select chapter_number from chapter_english where chapter_number='1'"))
                return true;
            else
                return false;
        }

        return checkDB != null;
    }

    private void copyDataBase() throws IOException {
        try {
            InputStream fis = context.getAssets().open(DATABASE_NAME);

            String decryptedPath = context.getDatabasePath(DATABASE_NAME) + "";

            FileOutputStream fos = new FileOutputStream(decryptedPath);
            SecretKeySpec sks = new SecretKeySpec(
                    "YOUR_KEY".getBytes(), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            IvParameterSpec ivParameterSpec = new IvParameterSpec("YOUR_KEY".getBytes());
            cipher.init(Cipher.DECRYPT_MODE, sks, ivParameterSpec);

            CipherInputStream cis = new CipherInputStream(fis, cipher);
            int b;
            byte[] d = new byte[8];
            while ((b = cis.read(d)) != -1) {
                fos.write(d, 0, b);
            }
            fos.flush();
            fos.close();
            cis.close();
        } catch (Exception e) {
            Log.e("Exception Caught:", e.getMessage());
        }
    }

    public void ExecuteQuery(String Query) {

        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.execSQL(Query);
        } catch (SQLiteException e) {
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }

    public String[][] genericSelect(String query, int noCols) {
        return genericSelect(query, noCols, false);
    }


    public String[][] genericSelect(String query, int noCols, boolean closeCursor) {
        String strData[][] = null;
        Cursor cur = null;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            cur = db.rawQuery(query, new String[]{});

            if (cur != null) {
                int noOfRows = cur.getCount();
                int count = 0;
                if (noOfRows > 0) {
                    strData = new String[noOfRows][noCols];
                    while (cur.moveToNext()) {
                        for (int i = 0; i < noCols; i++) {
                            strData[count][i] = cur.getString(i);
                        }
                        count++;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (cur != null && !cur.isClosed())
                cur.close();
        }
        return strData;
    }

    public String[][] genericSelect(String select, String tableName,
                                    String where, String groupBy, String having, int noCols) {
        String strData[][] = null;
        String query = "SELECT " + select + " FROM " + tableName;
        if (!where.equals("")) {
            query = query + " WHERE " + where;
        }
        if (!groupBy.equals("")) {
            query = query + " GROUP BY " + groupBy;
        }
        if (!having.equals("")) {
            query = query + " HAVING " + having;
        }
        Cursor cur = null;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            cur = db.rawQuery(query, new String[]{});
            if (cur != null) {
                int noOfRows = cur.getCount();
                int count = 0;
                if (noOfRows > 0) {
                    strData = new String[noOfRows][noCols];
                    while (cur.moveToNext()) {
                        for (int i = 0; i < noCols; i++) {
                            strData[count][i] = cur.getString(i);
                        }
                        count++;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cur != null && !cur.isClosed())
                cur.close();
        }

        return strData;
    }

    public int getTotalRowCount(String tableName, String whereQuery) {
        String query = "select count(*) from " + tableName + " " + whereQuery;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = null;
        int count = 0;

        try {
            cur = db.rawQuery(query, null);

            if (cur != null) {
                cur.moveToFirst();
                count = cur.getInt(0);
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (cur != null && !cur.isClosed())
                cur.close();
        }
        return count;
    }

    public boolean genricDelete(String Tabel_name) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            return db.delete(Tabel_name, null, null) > 0;
        } catch (SQLiteException ex) {
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public int GenricUpdates(String TabelName, String UpdateCol, String dataString, String KEY_COL, String WhereString) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(UpdateCol, dataString);
        return db.update(TabelName, values, KEY_COL + " = ?", new String[]{WhereString});
    }

    public Cursor getCusrsor(String SQl) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            return db.rawQuery(SQl, new String[]{});
        } catch (SQLiteException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deletealltable() {
        Cursor cur = null;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            cur = db.rawQuery(
                    "SELECT name FROM sqlite_master WHERE type='table'", null);

            if (cur.moveToFirst()) {
                while (!cur.isAfterLast()) {
                    String tablename = cur
                            .getString(cur.getColumnIndex("name"));
                    if (!tablename.equals("sqlite_sequence"))
                        db.execSQL("DROP TABLE IF EXISTS " + tablename);

                    cur.moveToNext();
                }
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        } finally {
            if (cur != null && !cur.isClosed())
                cur.close();
        }
    }

    public boolean ifValuePresentInDB(String query) {
        Cursor cursor = null;
        try {
            cursor = getCusrsor(query);
            if (cursor != null) {
                cursor.moveToFirst();

                if (cursor.getCount() > 0) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null && !cursor.isClosed())
                cursor.close();
        }
        return false;
    }

    public void deleteWhereValue(String tableName, String columnName, String whereValue) {
        String query = "DELETE FROM " + tableName + " where " + columnName + "='" + whereValue + "' ";
        ExecuteQuery(query);
    }

    public ArrayList<String> getDBColumns(String tableName) {
        ArrayList<String> columnList = new ArrayList<>();
        Cursor cursor = null;
        try {
            SQLiteDatabase sqldb = this.getWritableDatabase();
            cursor = sqldb.query(tableName, null, null, null, null, null, null);
            if (cursor != null) {
                String[] columnNames = cursor.getColumnNames();
                Collections.addAll(columnList, columnNames);
            }
        } catch (SQLException e) {
        } catch (Throwable t) {
        } finally {
            if (cursor != null && !cursor.isClosed())
                cursor.close();
        }
        return columnList;
    }

//    public void insertTBIMAGEUPLOAD(TBIMAGEUPLOAD objTbimageupload) {
//        SQLiteDatabase sqldb = null;
//        try {
//            sqldb = this.getWritableDatabase();
//            ContentValues cv = new ContentValues();
//
//            cv.put("FILENAME", objTbimageupload.getFILENAME());
//            cv.put("FILEPATH", objTbimageupload.getFILEPATH());
//            cv.put("UPLOADSTATUS", objTbimageupload.getUPLOADSTATUS());
//
//            sqldb.insert("TBIMAGEUPLOAD", null, cv);
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//        }
//    }
//
//    public ArrayList<TBIMAGEUPLOAD> getPendingDownloads() {
//
//        ArrayList<TBIMAGEUPLOAD> tbimageuploadArrayList = new ArrayList<>();
//        Cursor c = null;
//        try {
//            String query = "SELECT * FROM TBIMAGEUPLOAD WHERE UPLOADSTATUS='" + Constants.UPLOAD_PENDING + "' LIMIT 5";
//            SQLiteDatabase db = this.getWritableDatabase();
//            c = db.rawQuery(query, new String[]{});
//
//            if (c != null)
//                while (c.moveToNext()) {
//                    TBIMAGEUPLOAD objTbimageupload = new TBIMAGEUPLOAD();
//
//                    objTbimageupload.setFILENAME(c.getString(c.getColumnIndex("FILENAME")));
//                    objTbimageupload.setFILEPATH(c.getString(c.getColumnIndex("FILEPATH")));
//                    objTbimageupload.setUPLOADSTATUS(c.getString(c.getColumnIndex("UPLOADSTATUS")));
//
//                    tbimageuploadArrayList.add(objTbimageupload);
//                }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (c != null && !c.isClosed())
//                c.close();
//        }
//        return tbimageuploadArrayList;
//    }
//
//    public TBIMAGEUPLOAD getDetailsForFileName(String fileName) {
//        TBIMAGEUPLOAD objTbimageupload = null;
//        Cursor c = null;
//        try {
//            String query = "SELECT * FROM TBIMAGEUPLOAD WHERE FILENAME='" + fileName + "'";
//            SQLiteDatabase db = this.getWritableDatabase();
//            c = db.rawQuery(query, new String[]{});
//
//            if (c != null)
//                while (c.moveToNext()) {
//                    objTbimageupload = new TBIMAGEUPLOAD();
//
//                    objTbimageupload.setFILENAME(c.getString(c.getColumnIndex("FILENAME")));
//                    objTbimageupload.setFILEPATH(c.getString(c.getColumnIndex("FILEPATH")));
//                    objTbimageupload.setUPLOADSTATUS(c.getString(c.getColumnIndex("UPLOADSTATUS")));
//                }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (c != null && !c.isClosed())
//                c.close();
//        }
//        return objTbimageupload;
//    }

    //      Insert using Cupboard
//    public void insertSamples(ArrayList<TXNSAMPLE> txnsample) {
//        try {
//            SQLiteDatabase sqldb = this.getWritableDatabase();
//            cupboard().withDatabase(sqldb).put(txnsample);
//        } catch (SQLException e) {
//        } catch (Throwable t) {
//        }
//    }


//    Insert using content values
//    public void insertTBPLAN(TBPLAN tbPlan) {
//        SQLiteDatabase sqldb = null;
//        try {
//            sqldb = this.getWritableDatabase();
//
//            ContentValues cv = new ContentValues();
//
//            cv.put("BU", tbPlan.getBU());
//            cv.put("PCODE", tbPlan.getPCODE());
//
//            sqldb.insert("TBPLAN", null, cv);
//            sqldb.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (sqldb != null)
//                sqldb.close();
//        }
//    }

//    public ArrayList<Chapter> getAllChapters(Context context, ArrayList<Chapter> mChapterData) {
//        Cursor c = null;
//        try {
//            String query = "SELECT * FROM CHAPTER" + Prefs.getUserLanguageFromSharedPreferences() + " ORDER BY CAST(chapter_number AS INTEGER)";
//            SQLiteDatabase db = this.getWritableDatabase();
//            c = db.rawQuery(query, new String[]{});
//            TypedArray sportsImageResources =
//                    context.getResources().obtainTypedArray(R.array.sports_images);
//            int i = 0;
//            if (c != null)
//                while (c.moveToNext()) {
//                    Chapter objChapter = new Chapter();
//
//                    objChapter.setChapterNumber(c.getInt(c.getColumnIndex("chapter_number")));
//                    objChapter.setChapterSummary(c.getString(c.getColumnIndex("chapter_summary")));
//                    objChapter.setName(c.getString(c.getColumnIndex("name")));
//                    objChapter.setNameMeaning(c.getString(c.getColumnIndex("name_meaning")));
//                    objChapter.setNameTranslation(c.getString(c.getColumnIndex("name_translation")));
////                    objChapter.setNameTransliterated(c.getString(c.getColumnIndex("name_transliterated")));
//                    objChapter.setVersesCount(c.getInt(c.getColumnIndex("verses_count")));
//                    objChapter.setImageResource(sportsImageResources.getResourceId(i++, 0));
//
//                    mChapterData.add(objChapter);
//                }
//            sportsImageResources.recycle();
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (c != null && !c.isClosed())
//                c.close();
//        }
//        return mChapterData;
//    }
//
//    public ArrayList<Verse> getAllVersesForThisChapter(Context context, ArrayList<Verse> verseArrayList, int chapterNumber) {
//        Cursor c = null;
//        try {
//            String query = "SELECT * FROM VERSES" + Prefs.getUserLanguageFromSharedPreferences() + " WHERE CHAPTER_NUMBER = '" + chapterNumber + "' ORDER BY CAST(verse_number AS INTEGER)";
//            SQLiteDatabase db = this.getWritableDatabase();
//            c = db.rawQuery(query, new String[]{});
//            if (c != null)
//                while (c.moveToNext()) {
//                    Verse objVerse = new Verse();
//
//                    objVerse.setChapterNumber(c.getInt(c.getColumnIndex("chapter_number")));
//                    objVerse.setVerseNumber(c.getString(c.getColumnIndex("verse_number")));
//                    objVerse.setMeaning(c.getString(c.getColumnIndex("meaning")));
//                    objVerse.setText(c.getString(c.getColumnIndex("text")));
//                    objVerse.setTransliteration(c.getString(c.getColumnIndex("transliteration")));
//                    objVerse.setWordMeanings(c.getString(c.getColumnIndex("word_meanings")));
//
//                    verseArrayList.add(objVerse);
//                }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (c != null && !c.isClosed())
//                c.close();
//        }
//        return verseArrayList;
//    }

    public void insertTBPROFILEDATA(User objUser) {
        SQLiteDatabase sqldb = null;
        try {
            genricDelete("TBPROFILEDATA");

            sqldb = this.getWritableDatabase();
            ContentValues cv = new ContentValues();

            cv.put("USER_ID", objUser.getId());
            cv.put("NAME", objUser.getName());
            cv.put("EMAIL", objUser.getEmail());
            cv.put("EMAIL_VERIFIED_AT", objUser.getEmailVerifiedAt());
            cv.put("FIRST_NAME", objUser.getFirstname());
            cv.put("LAST_NAME", objUser.getLastname());
            cv.put("MOBILE_NUMBER", objUser.getMobileNumber());
            cv.put("DOB", objUser.getDob());
            cv.put("ABOUT", objUser.getAbout());
            cv.put("IS_MOBILE_VERIFIED", objUser.getIsMobileVerified());
            cv.put("CREATED_AT", objUser.getCreatedAt());
            cv.put("UPDATED_AT", objUser.getUpdatedAt());
            cv.put("DELETED_AT", objUser.getDeletedAt());

            sqldb.insert(TBPROFILEDATA, null, cv);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }

    public void insertTBPROFILEDATA(ResponseEditProfile objResponseEditProfile) {
        SQLiteDatabase sqldb = null;
        try {
            genricDelete("TBPROFILEDATA");

            Prefs.setUserId(objResponseEditProfile.getData().getUser().getId());
            Prefs.setUserName(objResponseEditProfile.getData().getUser().getName());
            Prefs.setUserProfilePicUrl(objResponseEditProfile.getData().getUser().getAvtar());

            sqldb = this.getWritableDatabase();
            ContentValues cv = new ContentValues();

            cv.put("USER_ID", objResponseEditProfile.getData().getUser().getId());
            cv.put("MOBILE_NUMBER", objResponseEditProfile.getData().getUser().getMobileNumber());

            Gson gson = new Gson();
            // Java object to JSON string
            String jsonInString = gson.toJson(objResponseEditProfile);
            cv.put("VALUE", jsonInString);

            sqldb.insert(TBPROFILEDATA, null, cv);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }

    public ResponseEditProfile getProfileDataFromDB() {
        ResponseEditProfile objResponseEditProfile = null;
        String jsonValue = "";
        Cursor c = null;
        try {
            String query = "SELECT * FROM " + TBPROFILEDATA + " WHERE MOBILE_NUMBER='" + Prefs.getMobileNumber() + "'";
            SQLiteDatabase db = this.getWritableDatabase();
            c = db.rawQuery(query, new String[]{});

            if (c != null)
                while (c.moveToNext()) {

                    jsonValue = c.getString(c.getColumnIndex("VALUE"));
                    Gson gson = new Gson();
                    // JSON string to Java object
                    objResponseEditProfile = gson.fromJson(jsonValue, ResponseEditProfile.class);
                }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null && !c.isClosed())
                c.close();
        }
        return objResponseEditProfile;
    }

    class DatabaseCopyBackground extends AsyncTask<Void, Void, Boolean> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Preparing Bhagavad Gita");
            progressDialog.setMessage("Please wait while we are preparing Bhagavad Gita for you...");
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                copyDataBase();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
            Bundle mBundle = new Bundle();
            Utility.sendUpdateListBroadCast(Defaults.ACTION_REFRESH_GROUP_LIST_SCREEN, context, mBundle);
        }
    }
}
