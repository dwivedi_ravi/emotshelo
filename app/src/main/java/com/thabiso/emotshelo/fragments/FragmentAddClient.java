package com.thabiso.emotshelo.fragments;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.snackbar.Snackbar;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.databinding.FragmentAddClientBinding;
import com.thabiso.emotshelo.models.addcustomer.RequestAddEditCustomer;
import com.thabiso.emotshelo.models.addcustomer.RequestDeleteCustomer;
import com.thabiso.emotshelo.models.addcustomer.ResponseAddEditCustomer;
import com.thabiso.emotshelo.models.addcustomer.customerlist.DataItem;
import com.thabiso.emotshelo.models.imageupload.ResponseGenericImageUpload;
import com.thabiso.emotshelo.network.ApiCallerUtility;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;
import com.thabiso.emotshelo.util.preferences.Prefs;

import java.io.IOException;
import java.util.Map;

public class FragmentAddClient extends Fragment {

    private FragmentAddClientBinding binding;

    private DataItem currentCustomer;
    private MyReceiver objMyReceiver;
    private RequestOptions requestOptions;
    private String recordId = "";

//    ActivityResultLauncher<String> mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(),
//            new ActivityResultCallback<Uri>() {
//                @Override
//                public void onActivityResult(Uri uri) {
//                    // Handle the returned Uri
//                }
//            });
//
//    ActivityResultLauncher<Intent> mStartActivityForResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
//            new ActivityResultCallback<ActivityResult>() {
//                @Override
//                public void onActivityResult(ActivityResult result) {
//                    if (result.getResultCode() == RESULT_OK) {
//                        result.getData();
//                    }
//                }
//            });

    private ActivityResultLauncher mRequestMultiplePermissions = registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), new ActivityResultCallback<Map<String, Boolean>>() {
        @Override
        public void onActivityResult(Map<String, Boolean> result) {
            for (Map.Entry<String, Boolean> entry : result.entrySet()) {
                switch (entry.getKey()) {
                    case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                        if (entry.getValue()) {
                            mGetContent.launch("image/*");
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.storage_permission), Toast.LENGTH_LONG).show();
                        }
                        break;
                    case Manifest.permission.READ_EXTERNAL_STORAGE:
//                        if (entry.getValue()) {
//                            mGetContent.launch("image/*");
//                        } else {
////                            Toast.makeText(getActivity(), getString(R.string.storage_permission), Toast.LENGTH_LONG).show();
//                        }
                        break;
                }
            }
        }
    });

    private ActivityResultLauncher<String> mRequestPermission = registerForActivityResult(new ActivityResultContracts.RequestPermission(), new ActivityResultCallback<Boolean>() {
        @Override
        public void onActivityResult(Boolean result) {
            if (result) {
                mGetContent.launch("image/*");
            } else {
                Toast.makeText(getActivity(), getString(R.string.storage_permission), Toast.LENGTH_LONG).show();
            }
        }
    });

    private ActivityResultLauncher<String> mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(), new ActivityResultCallback<Uri>() {
        @Override
        public void onActivityResult(Uri resultUri) {
            // Handle the returned Uri
            try {
                if (resultUri != null) {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), resultUri);
                    Glide.with(getActivity()).setDefaultRequestOptions(requestOptions).load(bitmap)
                            .into(binding.imgProfile);

                    binding.uploadProgress.setVisibility(View.VISIBLE);
                    binding.imgProfile.setClickable(false);

                    ApiCallerUtility.callGenericImageUploadApi(getActivity(), resultUri, Defaults.ACTION_CUSTOMER_LOGO_UPLOAD_SUCCESS, Defaults.ACTION_CUSTOMER_LOGO_UPLOAD_FAILED, Defaults.TYPE_CUSTOMER_IMAGE_UPLOAD, recordId);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    });

    public FragmentAddClient() {
    }

    public FragmentAddClient(DataItem currentCustomer) {
        this.currentCustomer = currentCustomer;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View root = inflater.inflate(R.layout.fragment_add_client, container, false);
        binding = FragmentAddClientBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        initComponent(root);
        setListeners();

        return root;
    }

    private void initComponent(View root) {
//        editName = root.findViewById(R.id.editName);
//        editEmail = root.findViewById(R.id.editEmail);
//        editPhone = root.findViewById(R.id.editPhone);
//        editPhysicalAddress = root.findViewById(R.id.editPhysicalAddress);
//        editPostalAddress = root.findViewById(R.id.editPostalAddress);
//        imgProfile = root.findViewById(R.id.imgProfile);
//        uploadProgress = root.findViewById(R.id.uploadProgress);
//        btnAddCustomer = root.findViewById(R.id.btnAddCustomer);

        objMyReceiver = new MyReceiver();
        requestOptions = Utility.getCustomizedRequestOptions(R.color.mdtp_line_background, R.drawable.no_image_place_holder);
        registerReciever();

        if (currentCustomer != null) {
            setValues();
        }
    }

    private void setValues() {
        recordId = currentCustomer.getId();
        Glide.with(getActivity()).setDefaultRequestOptions(requestOptions).load(currentCustomer.getAvatar())
                .into(binding.imgProfile);

        binding.editName.setText(currentCustomer.getName());
        binding.editEmail.setText(currentCustomer.getEmail());
        binding.editPhone.setText(currentCustomer.getMobile());
        binding.editPhysicalAddress.setText(currentCustomer.getPhysicalAddress());
        binding.editPostalAddress.setText(currentCustomer.getPostalAddress());

        binding.btnAddCustomer.setText(getResources().getString(R.string.lbl_save));
        binding.btnDeleteCustomer.setVisibility(View.VISIBLE);
    }

    private void setListeners() {
        binding.imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRequestMultiplePermissions.launch(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE});
            }
        });

        binding.btnAddCustomer.setOnClickListener(v -> {
            if (isFormValid()) {
                RequestAddEditCustomer objRequestAddEditCustomer = new RequestAddEditCustomer();

                objRequestAddEditCustomer.setName(binding.editName.getText().toString().trim());
                objRequestAddEditCustomer.setEmail(binding.editEmail.getText().toString().trim());
                objRequestAddEditCustomer.setMobile(binding.editPhone.getText().toString().trim());
                objRequestAddEditCustomer.setPhysicalAddress(binding.editPhysicalAddress.getText().toString().trim());
                objRequestAddEditCustomer.setPostalAddress(binding.editPostalAddress.getText().toString().trim());
                objRequestAddEditCustomer.setUserId(Prefs.getUserId());
                objRequestAddEditCustomer.setId(recordId);

                ApiCallerUtility.callAddEditCustomerApi(getActivity(), objRequestAddEditCustomer);
            }
        });

        binding.btnDeleteCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog textDialog = Utility.getTextDialog_Material(getActivity(), "Confirmation", "Are you sure? You want to delete this customer?");
                textDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok_action).toUpperCase(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        RequestDeleteCustomer objRequestDeleteCustomer = new RequestDeleteCustomer();
                        objRequestDeleteCustomer.setId(recordId);
                        ApiCallerUtility.callDeleteCustomerApi(getActivity(), objRequestDeleteCustomer);
                    }
                });
                textDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel_action).toUpperCase(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        textDialog.dismiss();
                    }
                });
                textDialog.show();

                textDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.app_text_color));
                textDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.app_text_color));
                textDialog.getButton(DialogInterface.BUTTON_POSITIVE).setBackground(getResources().getDrawable(R.drawable.ripple));
                textDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setBackground(getResources().getDrawable(R.drawable.ripple));
            }
        });
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_CUSTOMER_EDITED);
        filter.addAction(Defaults.ACTION_CUSTOMER_LOGO_UPLOAD_SUCCESS);
        filter.addAction(Defaults.ACTION_CUSTOMER_LOGO_UPLOAD_FAILED);
        Utility.registerReciever(getContext(), filter, objMyReceiver);
    }

    @Override
    public void onDestroy() {
        Utility.unRegisterReciever(getContext(), objMyReceiver);
        super.onDestroy();
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Defaults.ACTION_CUSTOMER_EDITED)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        ResponseAddEditCustomer objResponseAddEditCustomer = (ResponseAddEditCustomer) bundle.getSerializable("ResponseAddEditCustomer");
                        if (objResponseAddEditCustomer != null && objResponseAddEditCustomer.getData() != null && objResponseAddEditCustomer.getData().getId() != null) {
                            recordId = objResponseAddEditCustomer.getData().getId();
                            Toast.makeText(getContext(), "Customer details updated successfully", Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (intent.getAction().equals(Defaults.ACTION_CUSTOMER_LOGO_UPLOAD_SUCCESS)) {
                try {
                    binding.uploadProgress.setVisibility(View.GONE);
                    binding.imgProfile.setClickable(true);
                    Toast.makeText(context, "Customer Photo Uploaded Successfully", Toast.LENGTH_LONG).show();

                    if (intent != null && intent.getExtras() != null && intent.getExtras().getBundle("bundle") != null) {
                        Bundle bundle = intent.getExtras().getBundle("bundle");
                        ResponseGenericImageUpload objResponseGenericImageUpload = (ResponseGenericImageUpload) bundle.getSerializable("ResponseGenericImageUpload");
                        if (objResponseGenericImageUpload != null && objResponseGenericImageUpload.getData() != null && objResponseGenericImageUpload.getData().getId() != null) {
                            recordId = objResponseGenericImageUpload.getData().getId();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (intent.getAction().equals(Defaults.ACTION_CUSTOMER_LOGO_UPLOAD_FAILED)) {
                try {
                    binding.uploadProgress.setVisibility(View.GONE);
                    binding.imgProfile.setClickable(true);
                    Toast.makeText(context, "Customer Photo Upload Failed", Toast.LENGTH_LONG).show();
                    if (intent != null && intent.getExtras() != null && intent.getExtras().getBundle("bundle") != null) {
                        Bundle bundle = intent.getExtras().getBundle("bundle");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private boolean isFormValid() {
        if (binding.editName.getVisibility() == View.VISIBLE && binding.editName.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.editName, "Name can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.editEmail.getVisibility() == View.VISIBLE && binding.editEmail.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.editEmail, "Email can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.editPhone.getVisibility() == View.VISIBLE && binding.editPhone.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.editPhone, "Contact number can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.editPhysicalAddress.getVisibility() == View.VISIBLE && binding.editPhysicalAddress.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.editPhysicalAddress, "Physical address can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.editPostalAddress.getVisibility() == View.VISIBLE && binding.editPostalAddress.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.editPostalAddress, "Postal address can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }
        return true;
    }
}