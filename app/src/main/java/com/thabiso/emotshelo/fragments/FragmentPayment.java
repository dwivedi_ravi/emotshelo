package com.thabiso.emotshelo.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.thabiso.emotshelo.databinding.FragmentPaymentBinding;

public class FragmentPayment extends Fragment {

    private FragmentPaymentBinding binding;

    public FragmentPayment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View root = inflater.inflate(R.layout.fragment_payment, container, false);
        binding = FragmentPaymentBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        return root;
    }
}