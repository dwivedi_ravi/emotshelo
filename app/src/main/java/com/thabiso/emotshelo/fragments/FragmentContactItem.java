package com.thabiso.emotshelo.fragments;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.selection.StorageStrategy;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.adapters.ContactItemRecyclerViewAdapter;
import com.thabiso.emotshelo.models.ContactsItem;
import com.thabiso.emotshelo.models.getmyconnections.RequestGetMyConnections;
import com.thabiso.emotshelo.models.getmyconnections.UserContactsItem;
import com.thabiso.emotshelo.network.ApiCallerUtility;
import com.thabiso.emotshelo.util.preferences.Prefs;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class FragmentContactItem extends Fragment {
    public static final String TAG = "ContactItemFragment";
    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    public ContactItemRecyclerViewAdapter contactItemRecyclerViewAdapter;
    private SelectionTracker<Long> selectionTracker;
    private Toolbar toolbar;
    private Context context;
    private RecyclerView recyclerView;

    public List<UserContactsItem> myConnectionList;
    public List<ContactsItem> myContactList;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public FragmentContactItem() {
    }

    public FragmentContactItem(Context context, Toolbar toolbar) {
        this.context = context;
        this.toolbar = toolbar;
    }


    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static FragmentContactItem newInstance(int columnCount) {
        FragmentContactItem fragment = new FragmentContactItem();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            if (getArguments() != null) {
                mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
            }

            if (savedInstanceState != null) {
                selectionTracker.onRestoreInstanceState(savedInstanceState);
            }

            myConnectionList = new ArrayList<>();
            myContactList = new ArrayList<>();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_item_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            getMyConnectionsList();
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnExpenseListManagerInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(UserContactsItem item);
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (selectionTracker != null)
            selectionTracker.onSaveInstanceState(outState);
    }

//    public List<ContactsItem> getMyContactList() {
//        if (myContactList != null)
//            myContactList.clear();
//
//        ContentResolver cr = context.getContentResolver();
//        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
//                null, null, null, null);
//
//        if ((cur != null ? cur.getCount() : 0) > 0) {
//            while (cur != null && cur.moveToNext()) {
//                String id = cur.getString(
//                        cur.getColumnIndex(ContactsContract.Contacts._ID));
//                String name = cur.getString(cur.getColumnIndex(
//                        ContactsContract.Contacts.DISPLAY_NAME));
//                String photo = cur.getString(cur.getColumnIndex(
//                        ContactsContract.Contacts.PHOTO_URI));
//
//                if (cur.getInt(cur.getColumnIndex(
//                        ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
//
//                    ContactsItem objContactsItem = new ContactsItem();
//                    objContactsItem.setContactFirstname(name);
//                    objContactsItem.setContactLastname("");
//                    objContactsItem.setEmail("");
//                    objContactsItem.setBirthdate("1950-01-01");
//                    objContactsItem.setDetails("");
//
//                    Cursor pCur = cr.query(
//                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
//                            null,
//                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
//                            new String[]{id}, null);
//                    while (pCur.moveToNext()) {
//                        String phoneNo = pCur.getString(pCur.getColumnIndex(
//                                ContactsContract.CommonDataKinds.Phone.NUMBER));
////                        try {
////                            if (!pCur.getString(pCur.getColumnIndex(
////                                    ContactsContract.CommonDataKinds.Phone.IS_PRIMARY)).equals("0")) {
//////								myContactList.add(new DummyContent.DummyItem(id, name, phoneNo, photo));
////                                objContactsItem.setContactPhone(phoneNo);
////                                myContactList.add(objContactsItem);
////                            }
////                        } catch (Exception e) {
//////                            myContactList.add(new DummyContent.DummyItem(id, name, phoneNo, photo));
////                            objContactsItem.setContactPhone(phoneNo);
////                            myContactList.add(objContactsItem);
////                        }
//
//                        phoneNo = phoneNo.replaceAll("[^+0-9]", "");
////                        phoneNo.replaceFirst("0", "");
////                        phoneNo.replaceFirst("/+91", "");
//                        objContactsItem.setContactPhone(phoneNo);
//                        myContactList.add(objContactsItem);
//                        break;
//                    }
//                    pCur.close();
//                }
//            }
//        }
//        if (cur != null) {
//            cur.close();
//        }
//        return myContactList;
//    }

    public List<ContactsItem> getMyContactList() {
        if (myContactList != null)
            myContactList.clear();

        ContentResolver cr = context.getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        if ((cur != null ? cur.getCount() : 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                try {
                    String id = cur.getString(
                            cur.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                    String name = cur.getString(cur.getColumnIndexOrThrow(
                            ContactsContract.Contacts.DISPLAY_NAME));
                    String photo = cur.getString(cur.getColumnIndexOrThrow(
                            ContactsContract.Contacts.PHOTO_URI));

                    if (cur.getInt(cur.getColumnIndexOrThrow(
                            ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {

                        Cursor pCur = cr.query(
                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                new String[]{id}, null);

                        while (pCur.moveToNext()) {

                            ContactsItem objContactsItem = new ContactsItem();
                            objContactsItem.setContactFirstname(name);
                            objContactsItem.setContactLastname("");
                            objContactsItem.setEmail("");
                            objContactsItem.setBirthdate("1950-01-01");
                            objContactsItem.setDetails("");

                            String phoneNo = pCur.getString(pCur.getColumnIndexOrThrow(
                                    ContactsContract.CommonDataKinds.Phone.NUMBER));
                            phoneNo = phoneNo.replaceAll("[^+0-9]", "");

                            objContactsItem.setContactPhone(phoneNo);
                            myContactList.add(objContactsItem);
                        }
                        pCur.close();
                    }
                } catch (Exception ex) {

                }
            }
        }
        if (cur != null) {
            cur.close();
        }

        Set<ContactsItem> unique = new LinkedHashSet<>(myContactList);
        myContactList = new ArrayList<>(unique);

        return myContactList;
    }

    public void getMyConnectionsList() {
        RequestGetMyConnections objRequestGetMyConnections = new RequestGetMyConnections();
        objRequestGetMyConnections.setUserId(Prefs.getUserId());
        ApiCallerUtility.callGetMyConnectionsApi(context, objRequestGetMyConnections);
    }

    public void updateList(List<UserContactsItem> userContacts) {
//        myConnectionList = userContacts;
//        setRecyclerViewAdapter();
        myConnectionList = userContacts;
        if (recyclerView != null && recyclerView.getAdapter() != null) {
            selectionTracker.clearSelection();
            recyclerView.getAdapter().notifyDataSetChanged();
        } else {
            setRecyclerViewAdapter();
        }
    }

    public void refreshList(List<UserContactsItem> userContacts) {
        myConnectionList = userContacts;
        if (recyclerView != null && recyclerView.getAdapter() != null) {
            recyclerView.getAdapter().notifyItemInserted(myConnectionList.size() - 1);
            recyclerView.getAdapter().notifyDataSetChanged();
        } else {
            setRecyclerViewAdapter();
        }
    }

//    public void setOrUpdateRecyclerViewAdapter() {
//        if (recyclerViewMemberList != null && recyclerViewMemberList.getAdapter() == null) {
//            if (mColumnCount <= 1) {
//                recyclerViewMemberList.setLayoutManager(new LinearLayoutManager(AddContactScreen.this));
//            } else {
//                recyclerViewMemberList.setLayoutManager(new GridLayoutManager(AddContactScreen.this, mColumnCount));
//            }
//            contactItemRecyclerViewAdapter = new ContactItemRecyclerViewAdapter(AddContactScreen.this, memberList, null, true);
//            recyclerViewMemberList.setAdapter(contactItemRecyclerViewAdapter);
//
//        } else if (recyclerViewMemberList != null) {
//            recyclerViewMemberList.getAdapter().notifyItemInserted(1);
//            recyclerViewMemberList.getAdapter().notifyDataSetChanged();
//        }
//    }

    private void setRecyclerViewAdapter() {
        if (recyclerView != null) {
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            contactItemRecyclerViewAdapter = new ContactItemRecyclerViewAdapter(context, myConnectionList, mListener, false);
            recyclerView.setAdapter(contactItemRecyclerViewAdapter);

            ContactItemRecyclerViewAdapter.KeyProvider objKeyProvider = new ContactItemRecyclerViewAdapter.KeyProvider(recyclerView.getAdapter());
            selectionTracker = new SelectionTracker.Builder<>("my_selection",
                    recyclerView,
                    objKeyProvider,
                    new ContactItemRecyclerViewAdapter.DetailsLookup(recyclerView),
                    StorageStrategy.createLongStorage())
                    .withSelectionPredicate(new ContactItemRecyclerViewAdapter.Predicate())
                    .build();

            selectionTracker.addObserver(new SelectionTracker.SelectionObserver() {
                @Override
                public void onSelectionChanged() {
                    super.onSelectionChanged();
                    try {
                        if (selectionTracker.hasSelection()) {
                            toolbar.setTitle(getResources().getString(R.string.title_activity_contact_list) + "  " + selectionTracker.getSelection().size() + " Connection(s)");
                        } else if (!selectionTracker.hasSelection()) {
                            toolbar.setTitle(getResources().getString(R.string.title_activity_contact_list));
                        } else {
                            toolbar.setTitle(getResources().getString(R.string.title_activity_contact_list) + "  " + selectionTracker.getSelection().size() + " Connection(s)");
                        }

//                        if (objKeyProvider != null) {
//                            if (selectionTracker.isSelected(objKeyProvider.getKey(((ContactItemRecyclerViewAdapter) recyclerView.getAdapter()).getCurrentlySelectedItemPosition()))) {
//                                selectionTracker.deselect(objKeyProvider.getKey(((ContactItemRecyclerViewAdapter) recyclerView.getAdapter()).getCurrentlySelectedItemPosition()));
//                                Toast.makeText(context, "Limit Reached", Toast.LENGTH_SHORT).show();
//                            }
//                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            contactItemRecyclerViewAdapter.setSelectionTracker(selectionTracker);
        }
    }
}
