package com.thabiso.emotshelo.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.adapters.ProductItemsAdapter;
import com.thabiso.emotshelo.databinding.FragmentAddInvoiceEstimateBinding;
import com.thabiso.emotshelo.models.creategroup.UserGroups;
import com.thabiso.emotshelo.models.getmyconnections.UserContactsItem;
import com.thabiso.emotshelo.util.preferences.Defaults;
import com.thabiso.emotshelo.util.preferences.Prefs;

import java.util.ArrayList;
import java.util.List;

public class FragmentAddInvoiceEstimate extends Fragment {

    private FragmentAddInvoiceEstimateBinding binding;

    UserGroups currentGroup;
    private List<UserContactsItem> clientList;

    public FragmentAddInvoiceEstimate() {
    }

    public FragmentAddInvoiceEstimate(UserGroups currentGroup) {
        this.currentGroup = currentGroup;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View root = inflater.inflate(R.layout.fragment_add_invoice_estimate, container, false);
        binding = FragmentAddInvoiceEstimateBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        clientList = new ArrayList<>();
        binding.rcvItems.setLayoutManager(new LinearLayoutManager(getActivity()));
        ProductItemsAdapter mProductItemsAdapter = new ProductItemsAdapter(getActivity(), clientList, null, true);
        binding.rcvItems.setAdapter(mProductItemsAdapter);

        binding.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFormValid()) {
                    addItem();
                }
            }
        });

        binding.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        if (currentGroup != null) {
            if (currentGroup.getGroupType() == Defaults.SCREEN_TYPE_INVOICES + "") {
                binding.btnAddEstimate.setText(getResources().getString(R.string.lbl_add_invoice));
            } else if (currentGroup.getGroupType() == Defaults.SCREEN_TYPE_ESTIMATES + "") {
                binding.btnAddEstimate.setText(getResources().getString(R.string.lbl_add_estimate));
            }
        }

        return root;
    }

    private boolean isFormValid() {
        if (binding.editItemName.getVisibility() == View.VISIBLE && binding.editItemName.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.editItemName, "Name can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.editItemDescription.getVisibility() == View.VISIBLE && binding.editItemDescription.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.editItemDescription, "Description can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.editItemRate.getVisibility() == View.VISIBLE && binding.editItemRate.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.editItemRate, "Rate can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.editItemQuantity.getVisibility() == View.VISIBLE && binding.editItemQuantity.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.editItemQuantity, "Quantity can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }
        return true;
    }

    private void addItem() {
        UserContactsItem objUserContactsItem = new UserContactsItem();
        objUserContactsItem.setUserId(Prefs.getUniqueIntegerValueFromSharedPrefs() + "");
        objUserContactsItem.setUserName(binding.editItemName.getText().toString());
        objUserContactsItem.setTemp("Rate: " + binding.editItemRate.getText().toString() + "     " + "Qty: " + binding.editItemQuantity.getText().toString());
        objUserContactsItem.setTemp2("Tax: " + binding.editTaxPercentage.getText().toString() + "     " + "Discount: " + binding.editDiscountPercentage.getText().toString());
        objUserContactsItem.setMobile("Price: " + (Integer.parseInt(binding.editItemRate.getText().toString()) * Integer.parseInt(binding.editItemQuantity.getText().toString())));
        objUserContactsItem.setAvatar(binding.editItemDescription.getText().toString());
        clientList.add(objUserContactsItem);

        ((ProductItemsAdapter) binding.rcvItems.getAdapter()).refreshList(clientList);
        binding.rcvItems.getAdapter().notifyItemInserted(0);
        binding.rcvItems.getAdapter().notifyDataSetChanged();

        binding.editItemName.setText("");
        binding.editItemDescription.setText("");
        binding.editItemRate.setText("");
        binding.editItemQuantity.setText("");
        binding.editTaxPercentage.setText("");
        binding.editDiscountPercentage.setText("");
        binding.lblNoItems.setVisibility(View.GONE);
    }
}