package com.thabiso.emotshelo.models.ads;

import android.graphics.drawable.Drawable;

public class ModelAds {

    public int image;
    public Drawable imageDrw;
    public String name;
    public String brief;
    public Integer counter = null;

}
