package com.thabiso.emotshelo.models.creategroup;

import com.google.gson.annotations.SerializedName;

public class GroupMembersItem{

	@SerializedName("user_id")
	private String userId;

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}
}