package com.thabiso.emotshelo.models.expenses;

import com.google.gson.annotations.SerializedName;
import com.thabiso.emotshelo.util.preferences.Defaults;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Expense implements Serializable {

    @SerializedName("section_list")
    private List<String> sectionList;

    @SerializedName("total_deposit_amount")
    private double totalDepositAmount = 2000;

    @SerializedName("total_expense_amount")
    private double totalExpenseAmount = 1400;

    @SerializedName("total_remaining_balance")
    private double totalDepositBalance = 600;

    @SerializedName("deposit_list")
    private List<ExpenseDetails> depositList;

    @SerializedName("expense_list")
    private List<ExpenseDetails> expenseList;

    @SerializedName("interest_list")
    private List<ExpenseDetails> interestList;

    public List<String> getSectionList() {
        if (sectionList == null) {
            sectionList = new ArrayList<>();
            sectionList.add(Defaults.TAB_TITLE_DEPOSITS);
            sectionList.add(Defaults.TAB_TITLE_EXPENSES);
//            sectionList.add(Defaults.TAB_TITLE_TRANSFERS);
        }
        return sectionList;
    }

    public List<String> getSectionListForPersonalExpense() {
        if (sectionList == null) {
            sectionList = new ArrayList<>();
            sectionList.add(Defaults.TAB_TITLE_DEPOSITS);
            sectionList.add(Defaults.TAB_TITLE_WITHDRAWAL);
        }
        return sectionList;
    }

    public List<String> getSectionListForPersonalExpensePeerToPeer() {
        if (sectionList == null) {
            sectionList = new ArrayList<>();
            sectionList.add(Defaults.TAB_TITLE_DEPOSITS);
            sectionList.add(Defaults.TAB_TITLE_WITHDRAWAL);
            sectionList.add(Defaults.TAB_TITLE_INTEREST);
        }
        return sectionList;
    }

    public List<ExpenseDetails> getDepositList() {
        if (depositList == null) {
            depositList = new ArrayList<>();
        }
        return depositList;
    }

    public void setDepositList(List<ExpenseDetails> depositList) {
        this.depositList = depositList;
    }

    public List<ExpenseDetails> getExpenseList() {
        if (expenseList == null) {
            expenseList = new ArrayList<>();
        }
        return expenseList;
    }

    public void setExpenseList(List<ExpenseDetails> expenseList) {
        this.expenseList = expenseList;
    }

    public double getTotalDepositAmount() {
        return totalDepositAmount;
    }

    public void setTotalDepositAmount(double totalDepositAmount) {
        this.totalDepositAmount = totalDepositAmount;
    }

    public double getTotalExpenseAmount() {
        return totalExpenseAmount;
    }

    public void setTotalExpenseAmount(double totalExpenseAmount) {
        this.totalExpenseAmount = totalExpenseAmount;
    }

    public double getTotalDepositBalance() {
        return totalDepositBalance;
    }

    public void setTotalDepositBalance(double totalDepositBalance) {
        this.totalDepositBalance = totalDepositBalance;
    }

    public List<ExpenseDetails> getInterestList() {
        if (interestList == null) {
            interestList = new ArrayList<>();
        }
        return interestList;
    }

    public void setInterestList(List<ExpenseDetails> interestList) {
        this.interestList = interestList;
    }
}