package com.thabiso.emotshelo.models.addcompany;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Data implements Serializable {

	@SerializedName("website")
	private String website;

	@SerializedName("address")
	private Address address;

	@SerializedName("address_id")
	private String addressId;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("emailid")
	private String emailid;

	@SerializedName("additional_title")
	private String additionalTitle;

	@SerializedName("language")
	private String language;

	@SerializedName("tool")
	private List<ToolItem> tools;

	@SerializedName("tax_id")
	private String taxId;

	@SerializedName("company_address")
	private String companyAddress;

	@SerializedName("tags")
	private String tags;

	@SerializedName("user_type")
	private String userType;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("company_name")
	private String companyName;

	@SerializedName("logo")
	private String logo;

	@SerializedName("phone_number")
	private String phoneNumber;

	@SerializedName("designation")
	private String designation;

	@SerializedName("id")
	private String id;

	@SerializedName("mobile_number")
	private String mobileNumber;

	@SerializedName("vat_number")
	private String vatNumber;

	@SerializedName("postal_address")
	private String postalAddress;

	public void setWebsite(String website){
		this.website = website;
	}

	public String getWebsite(){
		return website;
	}

	public void setAddress(Address address){
		this.address = address;
	}

	public Address getAddress(){
		return address;
	}

	public void setAddressId(String addressId){
		this.addressId = addressId;
	}

	public String getAddressId(){
		return addressId;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setEmailid(String emailid){
		this.emailid = emailid;
	}

	public String getEmailid(){
		return emailid;
	}

	public void setAdditionalTitle(String additionalTitle){
		this.additionalTitle = additionalTitle;
	}

	public String getAdditionalTitle(){
		return additionalTitle;
	}

	public void setLanguage(String language){
		this.language = language;
	}

	public String getLanguage(){
		return language;
	}

	public void setTools(List<ToolItem> tools){
		this.tools = tools;
	}

	public List<ToolItem> getTools(){
		return tools;
	}


	public void setTaxId(String taxId){
		this.taxId = taxId;
	}

	public String getTaxId(){
		return taxId;
	}

	public void setCompanyAddress(String companyAddress){
		this.companyAddress = companyAddress;
	}

	public String getCompanyAddress(){
		return companyAddress;
	}

	public void setTags(String tags){
		this.tags = tags;
	}

	public String getTags(){
		return tags;
	}

	public void setUserType(String userType){
		this.userType = userType;
	}

	public String getUserType(){
		return userType;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setCompanyName(String companyName){
		this.companyName = companyName;
	}

	public String getCompanyName(){
		return companyName;
	}

	public void setLogo(String logo){
		this.logo = logo;
	}

	public String getLogo(){
		return logo;
	}

	public void setPhoneNumber(String phoneNumber){
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneNumber(){
		return phoneNumber;
	}

	public void setDesignation(String designation){
		this.designation = designation;
	}

	public String getDesignation(){
		return designation;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setMobileNumber(String mobileNumber){
		this.mobileNumber = mobileNumber;
	}

	public String getMobileNumber(){
		return mobileNumber;
	}

	public String getVatNumber() {
		return vatNumber;
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

	public String getPostalAddress() {
		return postalAddress;
	}

	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}
}