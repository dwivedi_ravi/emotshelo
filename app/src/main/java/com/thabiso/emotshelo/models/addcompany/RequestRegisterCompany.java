package com.thabiso.emotshelo.models.addcompany;

import com.google.gson.annotations.SerializedName;
import com.thabiso.emotshelo.R;

import java.io.Serializable;
import java.util.List;

import butterknife.BindString;

public class RequestRegisterCompany implements Serializable {

    @SerializedName("user_id")
    public String userId = "";

    @SerializedName("user_type")
    @BindString(R.string.add_company_setup_question_register_user_type)
    public String a11_registerUserType = "";

    @SerializedName("company_name")
    @BindString(R.string.add_company_setup_question_name)
    public String a12_companyName;

    @SerializedName("company_address")
    @BindString(R.string.add_company_setup_question_physical_address)
    public String a13_companyAddress;

//    @SerializedName("city")
//    @BindString(R.string.add_company_setup_question_city)
//    public String a14_city = "";

    @SerializedName("postal_address") // NEW KEY TO BE ADDED IN API REQUEST.
    @BindString(R.string.add_company_setup_question_postal_address)
    public String a14_postal_address;

    @SerializedName("state")
    @BindString(R.string.add_company_setup_question_states)
    public String a15_state = "";

    @SerializedName("country")
    @BindString(R.string.add_company_setup_question_countries)
    public String a16_country = "";

    @SerializedName("zip_code")
    @BindString(R.string.add_company_setup_question_zip_code)
    public String a17_zipCode = "";

    @SerializedName("tax_id")
    @BindString(R.string.add_company_setup_question_tax_id)
    public String a18_taxId = "";

    @SerializedName("designation")
    @BindString(R.string.add_company_setup_question_desig)
    public String a19_designation = "";

    @SerializedName("phone_number")
    @BindString(R.string.add_company_setup_question_phone)
    public String a20_phoneNumber;

    @SerializedName("mobile_number")
    @BindString(R.string.add_company_setup_question_mobile)
    public String a21_mobileNumber = "";

    @SerializedName("emailid")
    @BindString(R.string.add_company_setup_question_email)
    public String a22_emailId;

    @SerializedName("website")
    @BindString(R.string.add_company_setup_question_website)
    public String a23_website = "";

    @SerializedName("additional_title")
    @BindString(R.string.add_company_setup_question_title)
    public String a24_additionalTitle = "";

    @SerializedName("language")
    @BindString(R.string.add_company_setup_question_lanugage)
    public String a25_language = "";

    @SerializedName("tags")
    @BindString(R.string.add_company_setup_question_tag)
    public String a26_tag = "";

    @SerializedName("tools")
    private List<ToolItem> tools;

    @SerializedName("city")
    @BindString(R.string.add_company_setup_question_city)
    public String a27_city = "";

    @BindString(R.string.add_company_setup_question_vat_registration)
    public String a28_vat_registered;

    @SerializedName("vat_number") // NEW KEY TO BE ADDED IN API REQUEST.
    @BindString(R.string.add_company_setup_question_vat_registration_number)
    public String a29_vat_number;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRegisterUserType() {
        return a11_registerUserType;
    }

    public void setRegisterUserType(String registerUserType) {
        this.a11_registerUserType = registerUserType;
    }

    public String getCompanyName() {
        return a12_companyName;
    }

    public void setCompanyName(String companyName) {
        this.a12_companyName = companyName;
    }

    public String getCompanyAddress() {
        return a13_companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.a13_companyAddress = companyAddress;
    }

    public String getPostalAddress() {
        return a14_postal_address;
    }

    public void setPostalAddress(String a14_postal_address) {
        this.a14_postal_address = a14_postal_address;
    }

    public String getCity() {
        return a27_city;
    }

    public void setCity(String city) {
        this.a27_city = city;
    }

    public String getState() {
        return a15_state;
    }

    public void setState(String state) {
        this.a15_state = state;
    }

    public String getCountry() {
        return a16_country;
    }

    public void setCountry(String country) {
        this.a16_country = country;
    }

    public String getZipCode() {
        return a17_zipCode;
    }

    public void setZipCode(String zipCode) {
        this.a17_zipCode = zipCode;
    }

    public String getTaxId() {
        return a18_taxId;
    }

    public void setTaxId(String taxId) {
        this.a18_taxId = taxId;
    }

    public String getDesignation() {
        return a19_designation;
    }

    public void setDesignation(String designation) {
        this.a19_designation = designation;
    }

    public String getPhoneNumber() {
        return a20_phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.a20_phoneNumber = phoneNumber;
    }

    public String getMobileNumber() {
        return a21_mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.a21_mobileNumber = mobileNumber;
    }

    public String getEmailId() {
        return a22_emailId;
    }

    public void setEmailId(String emailId) {
        this.a22_emailId = emailId;
    }

    public String getWebsite() {
        return a23_website;
    }

    public void setWebsite(String website) {
        this.a23_website = website;
    }

    public String getAdditionalTitle() {
        return a24_additionalTitle;
    }

    public void setAdditionalTitle(String additionalTitle) {
        this.a24_additionalTitle = additionalTitle;
    }

    public String getLanguage() {
        return a25_language;
    }

    public void setLanguage(String language) {
        this.a25_language = language;
    }

    public String getTag() {
        return a26_tag;
    }

    public void setTag(String tag) {
        this.a26_tag = tag;
    }

    public void setTools(List<ToolItem> tools) {
        this.tools = tools;
    }

    public List<ToolItem> getTools() {
        return tools;
    }

    public String getVatRegistered() {
        return a28_vat_registered;
    }

    public void setVatRegistered(String a28_vat_registered) {
        this.a28_vat_registered = a28_vat_registered;
    }

    public String getVatNumber() {
        return a29_vat_number;
    }

    public void setVatNumber(String a29_vat_number) {
        this.a29_vat_number = a29_vat_number;
    }
}