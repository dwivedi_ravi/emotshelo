package com.thabiso.emotshelo.models.creategroup;

import com.google.gson.annotations.SerializedName;
import com.thabiso.emotshelo.models.groupdetails.UserMembersItem;

import java.io.Serializable;
import java.util.List;

public class UserGroups implements Serializable {

    @SerializedName("end_date")
    private String endDate;

    @SerializedName("gaping_between_withdrawal")
    private String gapingBetweenWithdrawal;

    @SerializedName("number_of_members")
    private String numberOfMembers;

    @SerializedName("description")
    private String description;

    @SerializedName("number_of_installments")
    private String numberOfInstallments;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("group_type")
    private String groupType;

    @SerializedName("group_avatar")
    private String groupAvatar;

    @SerializedName("number_of_withdrawal_limit")
    private String numberOfWithdrawalLimit;

    @SerializedName("mode")
    private String mode;

    @SerializedName("is_admin")
    private boolean isAdmin;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("current_receiver")
    private String currentReceiver;

    @SerializedName("invitation_status")
    private String invitationStatus;

    @SerializedName("group_sub_admin")
    private String groupSubAdmin;

    @SerializedName("interest_rate")
    private String interestRate;

    @SerializedName("currency")
    private String currency;

    @SerializedName("id")
    private String id;

    @SerializedName("amount_per_round")
    private String amountPerRound;

    @SerializedName("total_amount_per_round")
    private String totalAmountPerRound;

    @SerializedName("start_date")
    private String startDate;

    @SerializedName("interest_type")
    private String interestType;

    @SerializedName("group_admin")
    private String groupAdmin;

    @SerializedName("deleted_at")
    private String deletedAt;

    @SerializedName("total_withdrawal_limit")
    private String totalWithdrawalLimit;

    @SerializedName("next_receiver")
    private String nextReceiver;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("total_amount")
    private String totalAmount;

    @SerializedName("name")
    private String name;

    @SerializedName("number_of_rounds")
    private String numberOfRounds;

    @SerializedName("previous")
    private String previousReceiver;

    @SerializedName("current_rank")
    private String currentRank;

    @SerializedName("status")
    private String status;

    @SerializedName("user_members")
    private List<UserMembersItem> userMembers;

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setGapingBetweenWithdrawal(String gapingBetweenWithdrawal) {
        this.gapingBetweenWithdrawal = gapingBetweenWithdrawal;
    }

    public String getGapingBetweenWithdrawal() {
        return gapingBetweenWithdrawal;
    }

    public void setNumberOfMembers(String numberOfMembers) {
        this.numberOfMembers = numberOfMembers;
    }

    public String getNumberOfMembers() {
        return numberOfMembers;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setNumberOfInstallments(String numberOfInstallments) {
        this.numberOfInstallments = numberOfInstallments;
    }

    public String getNumberOfInstallments() {
        return numberOfInstallments;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupAvatar(String groupAvatar) {
        this.groupAvatar = groupAvatar;
    }

    public String getGroupAvatar() {
        return groupAvatar;
    }

    public void setNumberOfWithdrawalLimit(String numberOfWithdrawalLimit) {
        this.numberOfWithdrawalLimit = numberOfWithdrawalLimit;
    }

    public String getNumberOfWithdrawalLimit() {
        return numberOfWithdrawalLimit;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getMode() {
        return mode;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setCurrentReceiver(String currentReceiver) {
        this.currentReceiver = currentReceiver;
    }

    public String getCurrentReceiver() {
        return currentReceiver;
    }

    public void setInvitationStatus(String invitationStatus) {
        this.invitationStatus = invitationStatus;
    }

    public String getInvitationStatus() {
        return invitationStatus;
    }

    public void setGroupSubAdmin(String groupSubAdmin) {
        this.groupSubAdmin = groupSubAdmin;
    }

    public String getGroupSubAdmin() {
        return groupSubAdmin;
    }

    public void setInterestRate(String interestRate) {
        this.interestRate = interestRate;
    }

    public String getInterestRate() {
        return interestRate;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setAmountPerRound(String amountPerRound) {
        this.amountPerRound = amountPerRound;
    }

    public String getAmountPerRound() {
        return amountPerRound;
    }

    public void setTotalAmountPerRound(String totalAmountPerRound) {
        this.totalAmountPerRound = totalAmountPerRound;
    }

    public String getTotalAmountPerRound() {
        return totalAmountPerRound;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setInterestType(String interestType) {
        this.interestType = interestType;
    }

    public String getInterestType() {
        return interestType;
    }

    public void setGroupAdmin(String groupAdmin) {
        this.groupAdmin = groupAdmin;
    }

    public String getGroupAdmin() {
        return groupAdmin;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setTotalWithdrawalLimit(String totalWithdrawalLimit) {
        this.totalWithdrawalLimit = totalWithdrawalLimit;
    }

    public String getTotalWithdrawalLimit() {
        return totalWithdrawalLimit;
    }

    public void setNextReceiver(String nextReceiver) {
        this.nextReceiver = nextReceiver;
    }

    public String getNextReceiver() {
        return nextReceiver;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setNumberOfRounds(String numberOfRounds) {
        this.numberOfRounds = numberOfRounds;
    }

    public String getNumberOfRounds() {
        return numberOfRounds;
    }

    public void setPreviousReceiver(String previousReceiver) {
        this.previousReceiver = previousReceiver;
    }

    public String getPreviousReceiver() {
        return previousReceiver;
    }

    public void setCurrentRank(String currentRank) {
        this.currentRank = currentRank;
    }

    public String getCurrentRank() {
        return currentRank;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public List<UserMembersItem> getUserMembers() {
        return userMembers;
    }

    public void setUserMembers(List<UserMembersItem> userMembers) {
        this.userMembers = userMembers;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }
}