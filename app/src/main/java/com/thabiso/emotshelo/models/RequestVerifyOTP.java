package com.thabiso.emotshelo.models;

import com.google.gson.annotations.SerializedName;

public class RequestVerifyOTP {

    //    @SerializedName("request_type")
    private String requestType = "";

    @SerializedName("otp")
    private String otp;

    @SerializedName("mobile_number")
    private String mobileNumber;

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getOtp() {
        return otp;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    @Override
    public String toString() {
        return
                "RequestVerifyOTP{" +
                        "otp = '" + otp + '\'' +
                        ",mobile_number = '" + mobileNumber + '\'' +
                        "}";
    }
}