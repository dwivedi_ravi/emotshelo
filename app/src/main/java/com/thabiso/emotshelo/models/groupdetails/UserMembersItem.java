package com.thabiso.emotshelo.models.groupdetails;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserMembersItem implements Serializable {

    @SerializedName("status_changed_datetime")
    private String statusChangedDatetime;

    @SerializedName("invitation_hash")
    private String invitationHash;

    @SerializedName("invitation_datetime")
    private String invitationDatetime;

    @SerializedName("invitee_phone")
    private String inviteePhone;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("invitation_expiration")
    private String invitationExpiration;

    @SerializedName("user_profile")
    private UserProfile userProfile;

    @SerializedName("deleted_at")
    private String deletedAt;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("group_id")
    private String groupId;

    @SerializedName("invitation_status")
    private String invitationStatus;

    @SerializedName("id")
    private String id;

    @SerializedName("invitee_name")
    private String inviteeName;

    @SerializedName("indexs")
    private String groupMemberDisplayIndex;

    public void setStatusChangedDatetime(String statusChangedDatetime) {
        this.statusChangedDatetime = statusChangedDatetime;
    }

    public String getStatusChangedDatetime() {
        return statusChangedDatetime;
    }

    public void setInvitationHash(String invitationHash) {
        this.invitationHash = invitationHash;
    }

    public String getInvitationHash() {
        return invitationHash;
    }

    public void setInvitationDatetime(String invitationDatetime) {
        this.invitationDatetime = invitationDatetime;
    }

    public String getInvitationDatetime() {
        return invitationDatetime;
    }

    public void setInviteePhone(String inviteePhone) {
        this.inviteePhone = inviteePhone;
    }

    public String getInviteePhone() {
        return inviteePhone;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setInvitationExpiration(String invitationExpiration) {
        this.invitationExpiration = invitationExpiration;
    }

    public String getInvitationExpiration() {
        return invitationExpiration;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setInvitationStatus(String invitationStatus) {
        this.invitationStatus = invitationStatus;
    }

    public String getInvitationStatus() {
        return invitationStatus;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setInviteeName(String inviteeName) {
        this.inviteeName = inviteeName;
    }

    public String getInviteeName() {
        return inviteeName;
    }

    public String getGroupMemberDisplayIndex() {
        return groupMemberDisplayIndex;
    }

    public void setGroupMemberDisplayIndex(String groupMemberDisplayIndex) {
        this.groupMemberDisplayIndex = groupMemberDisplayIndex;
    }
}