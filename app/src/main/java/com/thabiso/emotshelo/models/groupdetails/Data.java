package com.thabiso.emotshelo.models.groupdetails;

import com.google.gson.annotations.SerializedName;
import com.thabiso.emotshelo.models.creategroup.UserGroups;

public class Data{

	@SerializedName("userGroups")
	private UserGroups userGroups;

	public void setUserGroups(UserGroups userGroups){
		this.userGroups = userGroups;
	}

	public UserGroups getUserGroups(){
		return userGroups;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"userGroups = '" + userGroups + '\'' + 
			"}";
		}
}