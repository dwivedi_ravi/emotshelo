package com.thabiso.emotshelo.models.editprofile;

import com.google.gson.annotations.SerializedName;

public class RequestEditProfile {

	@SerializedName("firstname")
	private String firstname;

	@SerializedName("dob")
	private String dob;

	@SerializedName("about")
	private String about;

	@SerializedName("mobile_number")
	private String mobileNumber;

	@SerializedName("email")
	private String email;

	@SerializedName("lastname")
	private String lastname;

	public void setFirstname(String firstname){
		this.firstname = firstname;
	}

	public String getFirstname(){
		return firstname;
	}

	public void setDob(String dob){
		this.dob = dob;
	}

	public String getDob(){
		return dob;
	}

	public void setAbout(String about){
		this.about = about;
	}

	public String getAbout(){
		return about;
	}

	public void setMobileNumber(String mobileNumber){
		this.mobileNumber = mobileNumber;
	}

	public String getMobileNumber(){
		return mobileNumber;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setLastname(String lastname){
		this.lastname = lastname;
	}

	public String getLastname(){
		return lastname;
	}

	@Override
 	public String toString(){
		return 
			"RequestEditProfile{" +
			"firstname = '" + firstname + '\'' + 
			",dob = '" + dob + '\'' + 
			",about = '" + about + '\'' + 
			",mobile_number = '" + mobileNumber + '\'' + 
			",email = '" + email + '\'' + 
			",lastname = '" + lastname + '\'' + 
			"}";
		}
}