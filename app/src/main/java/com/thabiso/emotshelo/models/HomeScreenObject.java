package com.thabiso.emotshelo.models;

import com.google.gson.annotations.SerializedName;
import com.thabiso.emotshelo.models.addcompany.DataItem;
import com.thabiso.emotshelo.models.grouplist.UserGroupsItem;
import com.thabiso.emotshelo.util.preferences.Defaults;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class HomeScreenObject implements Serializable {

    @SerializedName("section_list")
    private List<String> sectionList;

    @SerializedName("enterprise_list")
    private List<DataItem> enterpriseList;

    @SerializedName("groups_list")
    private List<UserGroupsItem> groupList;

    @SerializedName("personal_list")
    private List<UserGroupsItem> personalList;

    @SerializedName("home_list")
    private List<UserGroupsItem> homeList;

    public List<String> getSectionList() {
        if (sectionList == null) {
            sectionList = new ArrayList<>();
            sectionList.add(Defaults.TAB_TITLE_HOME);
            sectionList.add(Defaults.TAB_TITLE_PERSONAL);
            sectionList.add(Defaults.TAB_TITLE_GROUPS);
            sectionList.add(Defaults.TAB_TITLE_ENTERPRISE);
        }
        return sectionList;
    }

    public List<DataItem> getEnterpriseList() {
        if (enterpriseList == null) {
            enterpriseList = new ArrayList<>();

//            DataItem objDataItem = new DataItem();
//            objDataItem.setId(Defaults.FINANCIAL_LITERACY_TOOL_ENTERPRISE);
//            objDataItem.setCompanyName("Financial Literacy Tool");
//            objDataItem.setCompanyAddress("Literate Yourself About Enterprise Tools");
//            objDataItem.setAdditionalTitle("Total Tools: 3");
//            objDataItem.setCreatedAt("");
//            enterpriseList.add(0, objDataItem);
        }
        return enterpriseList;
    }

    public void setEnterpriseList(List<DataItem> enterpriseList) {
        this.enterpriseList = enterpriseList;

//        DataItem objDataItem = new DataItem();
//        objDataItem.setId(Defaults.FINANCIAL_LITERACY_TOOL_ENTERPRISE);
//        objDataItem.setCompanyName("Financial Literacy Tool");
//        objDataItem.setCompanyAddress("Literate Yourself About Enterprise Tools");
//        objDataItem.setAdditionalTitle("Total Tools: 3");
//        objDataItem.setCreatedAt("");
//        enterpriseList.add(0, objDataItem);
    }

    public List<UserGroupsItem> getGroupList() {
        if (groupList == null) {
            groupList = new ArrayList<>();

//            UserGroupsItem objUserGroups = new UserGroupsItem();
//            objUserGroups.setId(Defaults.FINANCIAL_LITERACY_TOOL_GROUPS);
//            objUserGroups.setName("Financial Literacy Tool");
//            objUserGroups.setDescription("Literate Yourself About Group Tools");
//            objUserGroups.setAmountPerRound("7");
//            objUserGroups.setNumberOfMembers(" - ");
//            objUserGroups.setCreatedAt("");
//            groupList.add(0, objUserGroups);
        }
        return groupList;
    }

    public void setGroupList(List<UserGroupsItem> groupList) {
        this.groupList = groupList;

//        UserGroupsItem objUserGroups = new UserGroupsItem();
//        objUserGroups.setId(Defaults.FINANCIAL_LITERACY_TOOL_GROUPS);
//        objUserGroups.setName("Financial Literacy Tool");
//        objUserGroups.setDescription("Literate Yourself About Group Tools");
//        objUserGroups.setAmountPerRound("7");
//        objUserGroups.setNumberOfMembers(" - ");
//        objUserGroups.setCreatedAt("");
//        groupList.add(0, objUserGroups);
    }

    public List<UserGroupsItem> getPersonalList() {
        if (personalList == null) {
            personalList = new ArrayList<>();

//            UserGroupsItem objUserGroups = new UserGroupsItem();
//            objUserGroups.setId(Defaults.FINANCIAL_LITERACY_TOOL_PERSONAL);
//            objUserGroups.setName("Financial Literacy Tool");
//            objUserGroups.setDescription("Literate Yourself About Personal Tools");
//            objUserGroups.setAmountPerRound("2");
//            objUserGroups.setNumberOfMembers(" - ");
//            objUserGroups.setCreatedAt("");
//            personalList.add(0, objUserGroups);
        }
        return personalList;
    }

    public void setPersonalList(List<UserGroupsItem> personalList) {
        this.personalList = personalList;
    }

    public List<UserGroupsItem> getHomeList() {
        if (homeList == null) {
            homeList = new ArrayList<>();
        }
        return homeList;
    }

    public void setHomeList(List<UserGroupsItem> homeList) {
        this.homeList = homeList;
    }
}


//package com.thabiso.emotshelo.models;
//
//import com.google.gson.annotations.SerializedName;
//import com.thabiso.emotshelo.models.creategroup.UserGroups;
//import com.thabiso.emotshelo.util.preferences.Defaults;
//
//import java.io.Serializable;
//import java.util.ArrayList;
//import java.util.List;
//
//public class HomeScreenObject implements Serializable {
//
//    @SerializedName("section_list")
//    private List<String> sectionList;
//
//    @SerializedName("enterprise_list")
//    private List<UserGroups> enterpriseList;
//
//    @SerializedName("groups_list")
//    private List<UserGroupsItem> groupList;
//
//    @SerializedName("personal_list")
//    private List<UserGroups> personalList;
//
//    public List<String> getSectionList() {
//        if (sectionList == null) {
//            sectionList = new ArrayList<>();
//            sectionList.add(Defaults.TAB_TITLE_ENTERPRISE);
//            sectionList.add(Defaults.TAB_TITLE_GROUPS);
//            sectionList.add(Defaults.TAB_TITLE_PERSONAL);
//        }
//        return sectionList;
//    }
//
//    public List<UserGroups> getEnterpriseList() {
//        if (enterpriseList == null) {
//            enterpriseList = new ArrayList<>();
//
//            UserGroups objUserGroups = new UserGroups();
//            objUserGroups.setId("1");
//            objUserGroups.setName("Brandapps Invoicing");
//            objUserGroups.setDescription("Gaborone Botswana");
//            objUserGroups.setAmountPerRound("Invoice: +P39.00");
//            objUserGroups.setStartDate("TODAY");
//            enterpriseList.add(objUserGroups);
//
//            objUserGroups = new UserGroups();
//            objUserGroups.setId("2");
//            objUserGroups.setName("KhoeSoft Quotations");
//            objUserGroups.setDescription("Botswana");
//            objUserGroups.setAmountPerRound("Quotation: +P100.00");
//            objUserGroups.setStartDate("22/04/2020");
//            enterpriseList.add(objUserGroups);
//
//            objUserGroups = new UserGroups();
//            objUserGroups.setId("3");
//            objUserGroups.setName("Elite Solution Invoicing");
//            objUserGroups.setDescription("Botswana");
//            objUserGroups.setAmountPerRound("Invoice: +P240.00");
//            objUserGroups.setStartDate("18/04/2020");
//            enterpriseList.add(objUserGroups);
//
//            objUserGroups = new UserGroups();
//            objUserGroups.setId("4");
//            objUserGroups.setName("BrandBase Invoicing");
//            objUserGroups.setDescription("Molepolole Botswana");
//            objUserGroups.setAmountPerRound("Invoice: +P80.00");
//            objUserGroups.setStartDate("01/01/2020");
//            enterpriseList.add(objUserGroups);
//        }
//        return enterpriseList;
//    }
//
//    public void setEnterpriseList(List<UserGroups> enterpriseList) {
//        this.enterpriseList = enterpriseList;
//    }
//
//    public List<UserGroups> getGroupList() {
//        if (groupList == null) {
//            groupList = new ArrayList<>();
//        }
//        return groupList;
//    }
//
//    public void setGroupList(List<UserGroups> groupList) {
//        this.groupList = groupList;
//    }
//
//    public List<UserGroups> getPersonalList() {
//        if (personalList == null) {
//            personalList = new ArrayList<>();
//        }
//        return personalList;
//    }
//
//    public void setPersonalList(List<UserGroups> personalList) {
//        this.personalList = personalList;
//    }
//}