package com.thabiso.emotshelo.models.expenses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ExpenseDetails implements Serializable {

    @SerializedName("transaction_datetime")
    private String transactionDatetime;

    @SerializedName("time_in_miliseconds")
    private Long timeInMiliseconds;

    //    CATEGORIES LIKE FOOD, TRANSPORT, HOTEL etc. So that we can see reports category wise.
    @SerializedName("category")
    private int category;

    @SerializedName("categoryText")
    private String categoryText;

    //    CASH OR BANK
    @SerializedName("mode")
    private int mode;

    @SerializedName("modeText")
    private String modeText;

    @SerializedName("amount")
    private double amount;

    @SerializedName("particulars")
    private String particulars;

    public String getTransactionDatetime() {
        return transactionDatetime;
    }

    public void setTransactionDatetime(String transactionDatetime) {
        this.transactionDatetime = transactionDatetime;
    }

    public Long getTimeInMiliseconds() {
        return timeInMiliseconds;
    }

    public void setTimeInMiliseconds(Long timeInMiliseconds) {
        this.timeInMiliseconds = timeInMiliseconds;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getParticulars() {
        return particulars;
    }

    public void setParticulars(String particulars) {
        this.particulars = particulars;
    }

    public String getCategoryText() {
        return categoryText;
    }

    public void setCategoryText(String categoryText) {
        this.categoryText = categoryText;
    }

    public String getModeText() {
        return modeText;
    }

    public void setModeText(String modeText) {
        this.modeText = modeText;
    }
}