package com.thabiso.emotshelo.models.addcustomer;

import com.google.gson.annotations.SerializedName;

public class RequestDeleteCustomer{

	@SerializedName("id")
	private String id;

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}
}