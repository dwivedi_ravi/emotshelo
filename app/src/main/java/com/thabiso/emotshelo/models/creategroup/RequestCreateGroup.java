package com.thabiso.emotshelo.models.creategroup;

import com.google.gson.annotations.SerializedName;
import com.thabiso.emotshelo.R;

import java.io.Serializable;
import java.util.List;

import butterknife.BindString;

public class RequestCreateGroup implements Serializable {

    @SerializedName("user_id")
    public String userId = "";

    @SerializedName("total_amount")
    @BindString(R.string.create_group_question_total_amount)
    public String totalAmount;

    @SerializedName("name")
    @BindString(R.string.create_group_question_name)
    public String a11_name;

    @SerializedName("group_type")
    @BindString(R.string.create_group_question_group_type)
    public String a12_groupType;

    @SerializedName("currency")
    @BindString(R.string.create_group_question_currency)
    public String a13_currency;

    @SerializedName("number_of_members")
    @BindString(R.string.create_group_question_number_of_members)
    public String a14_numberOfMembers;

    @SerializedName("number_of_rounds")
    @BindString(R.string.create_group_question_number_of_rounds)
    public String a15_numberOfRounds;

    @SerializedName("amount_per_round")
    @BindString(R.string.create_group_question_amount_per_round)
    public String a16_amountPerRound;

    @SerializedName("total_amount_per_round")
    @BindString(R.string.create_group_question_total_amount_per_round)
    public String a17_totalAmountPerRound;

    @SerializedName("mode")
    @BindString(R.string.create_group_question_group_mode)
    public String a18_mode;


    @SerializedName("interest_type")
    @BindString(R.string.create_group_question_interest_type)
    public String a19_interestType;

    @SerializedName("interest_rate")
    @BindString(R.string.create_group_question_interest_rate)
    public String a20_interestRate;

    @SerializedName("total_withdrawal_limit")
    @BindString(R.string.create_group_question_total_withdrawal_limit)
    public String a21_totalWithdrawalLimit;

    @SerializedName("number_of_withdrawal_limit")
    @BindString(R.string.create_group_question_number_of_withdrawal_limit)
    public String a22_numberOfWithdrawalLimit;

    @SerializedName("gaping_between_withdrawal")
    @BindString(R.string.create_group_question_gaping_between_withdrawals)
    public String a23_gapingBetweenWithdrawals;


    @SerializedName("description")
    @BindString(R.string.create_group_question_description)
    public String a24_description;

    @SerializedName("start_date")
    @BindString(R.string.create_group_question_start_Date)
    public String a25_startDate;

    @SerializedName("end_date")
    @BindString(R.string.create_group_question_end_date)
    public String a26_endDate;

    @SerializedName("group_members")
    public List<GroupMembersItem> a27_groupMembers;


    //******************************************
    @SerializedName("group_admin")
    private String groupAdmin;

    @SerializedName("group_sub_admin")
    private String groupSubAdmin;

    @SerializedName("group_status")
    private String groupStatus;

    @SerializedName("invitation_status")
    private String invitationStatus;

    @SerializedName("current_receiver")
    private String currentReceiver;

    @SerializedName("next_receiver")
    private String nextReceiver;

    @SerializedName("previous_receiver")
    private String previousReceiver;

    @SerializedName("group_avatar")
    private String groupAvatar;

    //******************************************

    public void setEndDate(String endDate) {
        this.a26_endDate = endDate;
    }

    public String getEndDate() {
        return a26_endDate;
    }

    public void setGroupMembers(List<GroupMembersItem> groupMembers) {
        this.a27_groupMembers = groupMembers;
    }

    public List<GroupMembersItem> getGroupMembers() {
        return a27_groupMembers;
    }

    public void setNumberOfMembers(String numberOfMembers) {
        this.a14_numberOfMembers = numberOfMembers;
    }

    public String getNumberOfMembers() {
        return a14_numberOfMembers;
    }

    public void setDescription(String description) {
        this.a24_description = description;
    }

    public String getDescription() {
        return a24_description;
    }

    public void setGroupType(String groupType) {
        this.a12_groupType = groupType;
    }

    public String getGroupType() {
        return a12_groupType;
    }

    public void setMode(String mode) {
        this.a18_mode = mode;
    }

    public String getMode() {
        return a18_mode;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setName(String name) {
        this.a11_name = name;
    }

    public String getName() {
        return a11_name;
    }

    public void setCurrency(String currency) {
        this.a13_currency = currency;
    }

    public String getCurrency() {
        return a13_currency;
    }

    public void setAmountPerRound(String amountPerRound) {
        this.a16_amountPerRound = amountPerRound;
    }

    public String getAmountPerRound() {
        return a16_amountPerRound;
    }

    public void setNumberOfRounds(String numberOfRounds) {
        this.a15_numberOfRounds = numberOfRounds;
    }

    public String getNumberOfRounds() {
        return a15_numberOfRounds;
    }

    public void setTotalAmountPerRound(String totalAmountPerRound) {
        this.a17_totalAmountPerRound = totalAmountPerRound;
    }

    public String getTotalAmountPerRound() {
        return a17_totalAmountPerRound;
    }

    public void setStartDate(String startDate) {
        this.a25_startDate = startDate;
    }

    public String getStartDate() {
        return a25_startDate;
    }


    public String getInterestType() {
        return a19_interestType;
    }

    public void setInterestType(String a19_interestType) {
        this.a19_interestType = a19_interestType;
    }

    public String getInterestRate() {
        return a20_interestRate;
    }

    public void setInterestRate(String a20_interestRate) {
        this.a20_interestRate = a20_interestRate;
    }

    public String getTotalWithdrawalLimit() {
        return a21_totalWithdrawalLimit;
    }

    public void setTotalWithdrawalLimit(String a21_totalWithdrawalLimit) {
        this.a21_totalWithdrawalLimit = a21_totalWithdrawalLimit;
    }

    public String getNumberOfWithdrawalLimit() {
        return a22_numberOfWithdrawalLimit;
    }

    public void setNumberOfWithdrawalLimit(String a22_numberOfWithdrawalLimit) {
        this.a22_numberOfWithdrawalLimit = a22_numberOfWithdrawalLimit;
    }

    public String getGapingBetweenWithdrawals() {
        return a23_gapingBetweenWithdrawals;
    }

    public void setGapingBetweenWithdrawals(String a23_gapingBetweenWithdrawals) {
        this.a23_gapingBetweenWithdrawals = a23_gapingBetweenWithdrawals;
    }

    public String getGroupAdmin() {
        return groupAdmin;
    }

    public void setGroupAdmin(String groupAdmin) {
        this.groupAdmin = groupAdmin;
    }

    public String getGroupSubAdmin() {
        return groupSubAdmin;
    }

    public void setGroupSubAdmin(String groupSubAdmin) {
        this.groupSubAdmin = groupSubAdmin;
    }

    public String getGroupStatus() {
        return groupStatus;
    }

    public void setGroupStatus(String groupStatus) {
        this.groupStatus = groupStatus;
    }

    public String getInvitationStatus() {
        return invitationStatus;
    }

    public void setInvitationStatus(String invitationStatus) {
        this.invitationStatus = invitationStatus;
    }

    public String getCurrentReceiver() {
        return currentReceiver;
    }

    public void setCurrentReceiver(String currentReceiver) {
        this.currentReceiver = currentReceiver;
    }

    public String getNextReceiver() {
        return nextReceiver;
    }

    public void setNextReceiver(String nextReceiver) {
        this.nextReceiver = nextReceiver;
    }

    public String getPreviousReceiver() {
        return previousReceiver;
    }

    public void setPreviousReceiver(String previousReceiver) {
        this.previousReceiver = previousReceiver;
    }

    public String getGroupAvatar() {
        return groupAvatar;
    }

    public void setGroupAvatar(String groupAvatar) {
        this.groupAvatar = groupAvatar;
    }
}