package com.thabiso.emotshelo.models.grouplist;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Data  implements Serializable {

	@SerializedName("userGroups")
	private List<UserGroupsItem> userGroups;

	public void setUserGroups(List<UserGroupsItem> userGroups){
		this.userGroups = userGroups;
	}

	public List<UserGroupsItem> getUserGroups(){
		return userGroups;
	}
}