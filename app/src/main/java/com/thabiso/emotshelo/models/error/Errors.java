package com.thabiso.emotshelo.models.error;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Errors{

	@SerializedName("firstname")
	private List<String> firstname;

	public void setFirstname(List<String> firstname){
		this.firstname = firstname;
	}

	public List<String> getFirstname(){
		return firstname;
	}

	@Override
 	public String toString(){
		return 
			"Errors{" + 
			"firstname = '" + firstname + '\'' + 
			"}";
		}
}