package com.thabiso.emotshelo.models;

public class ResponseRequestOTP {
	private String message;
	private boolean status;

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ResponseRequestOTP{" +
			"message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}
