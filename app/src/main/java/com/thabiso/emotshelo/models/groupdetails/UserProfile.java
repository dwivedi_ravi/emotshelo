package com.thabiso.emotshelo.models.groupdetails;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserProfile implements Serializable {

    @SerializedName("firstname")
    private String firstname;

    @SerializedName("about")
    private String about;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("email_verified_at")
    private String emailVerifiedAt;

    @SerializedName("otp")
    private String otp;

    @SerializedName("is_mobile_verified")
    private String isMobileVerified;

    @SerializedName("deleted_at")
    private String deletedAt;

    @SerializedName("lastname")
    private String lastname;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("dob")
    private String dob;

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private String userId;

    @SerializedName("mobile_number")
    private String mobileNumber;

    @SerializedName("avtar")
    private String avtar;

    @SerializedName("email")
    private String email;

    @SerializedName("status")
    private String userInvitationStatus;

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getAbout() {
        return about;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setEmailVerifiedAt(String emailVerifiedAt) {
        this.emailVerifiedAt = emailVerifiedAt;
    }

    public String getEmailVerifiedAt() {
        return emailVerifiedAt;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getOtp() {
        return otp;
    }

    public void setIsMobileVerified(String isMobileVerified) {
        this.isMobileVerified = isMobileVerified;
    }

    public String getIsMobileVerified() {
        return isMobileVerified;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getDob() {
        return dob;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getAvtar() {
        return avtar;
    }

    public void setAvtar(String avtar) {
        this.avtar = avtar;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public String getUserInvitationStatus() {
        return userInvitationStatus;
    }

    public void setUserInvitationStatus(String userInvitationStatus) {
        this.userInvitationStatus = userInvitationStatus;
    }
}