package com.thabiso.emotshelo.models.creategroup;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Data implements Serializable {

    @SerializedName("userGroups")
    private UserGroups userGroups;

    public void setUserGroups(UserGroups userGroups) {
        this.userGroups = userGroups;
    }

    public UserGroups getUserGroups() {
        return userGroups;
    }
}