package com.thabiso.emotshelo.models.registerdevice;

public class RequestRegisterDevice {
	private String deviceOSVer;
	private String deviceOS;
	private String deviceName;
	private String deviceUUID;

	public void setDeviceOSVer(String deviceOSVer){
		this.deviceOSVer = deviceOSVer;
	}

	public String getDeviceOSVer(){
		return deviceOSVer;
	}

	public void setDeviceOS(String deviceOS){
		this.deviceOS = deviceOS;
	}

	public String getDeviceOS(){
		return deviceOS;
	}

	public void setDeviceName(String deviceName){
		this.deviceName = deviceName;
	}

	public String getDeviceName(){
		return deviceName;
	}

	public void setDeviceUUID(String deviceUUID){
		this.deviceUUID = deviceUUID;
	}

	public String getDeviceUUID(){
		return deviceUUID;
	}

	@Override
 	public String toString(){
		return 
			"RequestRegisterDevice{" +
			"deviceOSVer = '" + deviceOSVer + '\'' + 
			",deviceOS = '" + deviceOS + '\'' + 
			",deviceName = '" + deviceName + '\'' + 
			",deviceUUID = '" + deviceUUID + '\'' + 
			"}";
		}
}
