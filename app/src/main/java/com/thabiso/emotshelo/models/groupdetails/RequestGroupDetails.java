package com.thabiso.emotshelo.models.groupdetails;

import com.google.gson.annotations.SerializedName;

public class RequestGroupDetails {

	@SerializedName("user_id")
	private String userId;

	@SerializedName("group_id")
	private String groupId;

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setGroupId(String groupId){
		this.groupId = groupId;
	}

	public String getGroupId(){
		return groupId;
	}

	@Override
 	public String toString(){
		return 
			"RequestGroupDetails{" +
			"user_id = '" + userId + '\'' + 
			",group_id = '" + groupId + '\'' + 
			"}";
		}
}