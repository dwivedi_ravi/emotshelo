package com.thabiso.emotshelo.models.editprofile;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class User implements Serializable {

    @SerializedName("firstname")
    private String firstname;

    @SerializedName("about")
    private String about;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("email_verified_at")
    private String emailVerifiedAt;

    @SerializedName("is_mobile_verified")
    private String isMobileVerified;

    @SerializedName("deleted_at")
    private String deletedAt;

    @SerializedName("lastname")
    private String lastname;

    @SerializedName("country_code")
    private String countryCode;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("dob")
    private String dob;

    @SerializedName("name")
    private String name;

    @SerializedName("country_name")
    private String countryName;

    @SerializedName("id")
    private String id;

    @SerializedName("mobile_number")
    private String mobileNumber;

    @SerializedName("email")
    private String email;

    @SerializedName("avtar")
    private String avtar;

    @SerializedName("status")
    private String status;

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getAbout() {
        return about;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setEmailVerifiedAt(String emailVerifiedAt) {
        this.emailVerifiedAt = emailVerifiedAt;
    }

    public String getEmailVerifiedAt() {
        return emailVerifiedAt;
    }

    public void setIsMobileVerified(String isMobileVerified) {
        this.isMobileVerified = isMobileVerified;
    }

    public String getIsMobileVerified() {
        return isMobileVerified;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getDob() {
        return dob;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setAvtar(String avtar) {
        this.avtar = avtar;
    }

    public String getAvtar() {
        return avtar;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}