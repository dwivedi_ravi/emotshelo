package com.thabiso.emotshelo.models.grouplist;

import com.google.gson.annotations.SerializedName;

public class RequestGroupList {

    @SerializedName("user_id")
    private String userId;

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

}