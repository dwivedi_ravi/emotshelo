package com.thabiso.emotshelo.models;

import java.io.Serializable;

public class ResponseVerifyOTP implements Serializable {
	private String message;
	private String code;
	private boolean status;

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}

