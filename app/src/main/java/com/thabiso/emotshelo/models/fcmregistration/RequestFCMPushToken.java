package com.thabiso.emotshelo.models.fcmregistration;

import com.google.gson.annotations.SerializedName;

public class RequestFCMPushToken {

    @SerializedName("gcm_id")
    private String gcmId;

    @SerializedName("user_id")
    private String userId;

    public void setGcmId(String gcmId) {
        this.gcmId = gcmId;
    }

    public String getGcmId() {
        return gcmId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    @Override
    public String toString() {
        return
                "RequestFCMPushToken{" +
                        "gcm_id = '" + gcmId + '\'' +
                        ",user_id = '" + userId + '\'' +
                        "}";
    }
}