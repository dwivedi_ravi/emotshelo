package com.thabiso.emotshelo.models.addcompany;

import com.google.gson.annotations.SerializedName;

public class RequestUpdateToolStatus {

	@SerializedName("company_id")
	private String companyId;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("tool_id")
	private String toolId;

	@SerializedName("sub_tool_id")
	private String subToolId;

	@SerializedName("status")
	private String status;

	@SerializedName("name")
	private String name;

	public void setCompanyId(String companyId){
		this.companyId = companyId;
	}

	public String getCompanyId(){
		return companyId;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setToolId(String toolId){
		this.toolId = toolId;
	}

	public String getToolId(){
		return toolId;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public String getSubToolId() {
		return subToolId;
	}

	public void setSubToolId(String subToolId) {
		this.subToolId = subToolId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}