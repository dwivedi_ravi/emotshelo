package com.thabiso.emotshelo.models.getmyconnections;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserContactsItem implements Serializable {

    @SerializedName("id")
    private String userId;

    @SerializedName("name")
    private String userName;

    @SerializedName("mobile")
    private String mobile;

    @SerializedName("avatar")
    private String avatar;

    private String temp;
    private String temp2;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getTemp2() {
        return temp2;
    }

    public void setTemp2(String temp2) {
        this.temp2 = temp2;
    }
}