package com.thabiso.emotshelo.models.imageupload;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Errors {

    @SerializedName("image")
    private List<String> image;

    public void setImage(List<String> image) {
        this.image = image;
    }

    public List<String> getImage() {
        return image;
    }
}