package com.thabiso.emotshelo.models.addcompany;

import com.google.gson.annotations.SerializedName;

public class RequestEditCompanyDetails {

    @SerializedName("website")
    private String website;

    @SerializedName("company_id")
    private String companyId;

    @SerializedName("city")
    private String city;

    @SerializedName("emailid")
    private String emailid;

    @SerializedName("additional_title")
    private String additionalTitle;

    @SerializedName("language")
    private String language;

    @SerializedName("tax_id")
    private String taxId;

    @SerializedName("zip_code")
    private String zipCode;

    @SerializedName("tags")
    private String tags;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("company_name")
    private String companyName;

    @SerializedName("phone_number")
    private String phoneNumber;

    @SerializedName("state")
    private String state;

    @SerializedName("designation")
    private String designation;

    @SerializedName("mobile_number")
    private String mobileNumber;

    @SerializedName("company_address")
    private String companyAddress;

    @SerializedName("postal_address") // NEW KEY TO BE ADDED IN API REQUEST.
    private String postalAddress;

    @SerializedName("vat_number") // NEW KEY TO BE ADDED IN API REQUEST.
    private String vatNumber;

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getWebsite() {
        return website;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setAdditionalTitle(String additionalTitle) {
        this.additionalTitle = additionalTitle;
    }

    public String getAdditionalTitle() {
        return additionalTitle;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getTags() {
        return tags;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDesignation() {
        return designation;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }
}