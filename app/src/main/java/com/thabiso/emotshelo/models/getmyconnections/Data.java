package com.thabiso.emotshelo.models.getmyconnections;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data{

	@SerializedName("userContacts")
	private List<UserContactsItem> userContacts;

	public void setUserContacts(List<UserContactsItem> userContacts){
		this.userContacts = userContacts;
	}

	public List<UserContactsItem> getUserContacts(){
		return userContacts;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"userContacts = '" + userContacts + '\'' + 
			"}";
		}
}