package com.thabiso.emotshelo.models;

import com.thabiso.emotshelo.util.preferences.Defaults;

import java.io.Serializable;

public class Message implements Serializable {
    private long id;
    private String date;
    private String content;
    private boolean fromMe;
    private boolean showTime = true;
    private boolean showButton = false;
    private String showButtonText = "";
    private int typeOfButton = Defaults.SHOW_DATE_POP_UP;

    public Message(long id, String content, boolean fromMe, String date) {
        this.id = id;
        this.date = date;
        this.content = content;
        this.fromMe = fromMe;
    }

    public Message(long id, String content, boolean fromMe, boolean showTime, String date) {
        this.id = id;
        this.date = date;
        this.content = content;
        this.fromMe = fromMe;
        this.showTime = showTime;
    }

    public long getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public String getContent() {
        return content;
    }

    public boolean isFromMe() {
        return fromMe;
    }

    public boolean isShowTime() {
        return showTime;
    }

    public boolean isShowButton() {
        return showButton;
    }

    public void setShowButton(boolean showButton) {
        this.showButton = showButton;
    }

    public int getTypeOfButton() {
        return typeOfButton;
    }

    public void setTypeOfButton(int typeOfButton) {
        this.typeOfButton = typeOfButton;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getShowButtonText() {
        return showButtonText;
    }

    public void setShowButtonText(String showButtonText) {
        this.showButtonText = showButtonText;
    }
}