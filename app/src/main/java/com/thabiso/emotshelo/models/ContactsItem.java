package com.thabiso.emotshelo.models;

import com.google.gson.annotations.SerializedName;

public class ContactsItem {

    @SerializedName("contact_lastname")
    private String contactLastname;

    @SerializedName("birthdate")
    private String birthdate;

    @SerializedName("contact_phone")
    private String contactPhone;

    @SerializedName("contact_firstname")
    private String contactFirstname;

    @SerializedName("details")
    private String details;

    @SerializedName("email")
    private String email;

    public void setContactLastname(String contactLastname) {
        this.contactLastname = contactLastname;
    }

    public String getContactLastname() {
        return contactLastname;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactFirstname(String contactFirstname) {
        this.contactFirstname = contactFirstname;
    }

    public String getContactFirstname() {
        return contactFirstname;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getDetails() {
        return details;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return
                "ContactsItem{" +
                        "contact_lastname = '" + contactLastname + '\'' +
                        ",birthdate = '" + birthdate + '\'' +
                        ",contact_phone = '" + contactPhone + '\'' +
                        ",contact_firstname = '" + contactFirstname + '\'' +
                        ",details = '" + details + '\'' +
                        ",email = '" + email + '\'' +
                        "}";
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;

        if (!(obj instanceof ContactsItem))
            return false;
        return contactPhone.equals(((ContactsItem) obj).getContactPhone());
    }

    @Override
    public int hashCode() {
        return (contactPhone == null) ? 0 : contactPhone.hashCode();
    }
}