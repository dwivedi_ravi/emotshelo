package com.thabiso.emotshelo.models.getallnotifications;

import com.google.gson.annotations.SerializedName;

public class RequestGetAllNotification{

	@SerializedName("to_user_id")
	private String toUserId;

	@SerializedName("from_user_id")
	private String fromUserId;

	public void setToUserId(String toUserId){
		this.toUserId = toUserId;
	}

	public String getToUserId(){
		return toUserId;
	}

	public void setFromUserId(String fromUserId){
		this.fromUserId = fromUserId;
	}

	public String getFromUserId(){
		return fromUserId;
	}
}