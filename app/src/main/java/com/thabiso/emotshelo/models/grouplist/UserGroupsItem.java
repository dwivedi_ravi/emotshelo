package com.thabiso.emotshelo.models.grouplist;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserGroupsItem implements Serializable {

    @SerializedName("number_of_members")
    private String numberOfMembers;

    @SerializedName("description")
    private String description;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("group_type")
    private String groupType;

    @SerializedName("group_avatar")
    private String groupAvatar;

    @SerializedName("is_admin")
    private boolean isAdmin;

    @SerializedName("total_withdrawal_limit")
    private String totalWithdrawalLimit;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("invitation_status")
    private String invitationStatus;

    @SerializedName("name")
    private String name;

    @SerializedName("interest_rate")
    private String interestRate;

    @SerializedName("id")
    private String id;

    @SerializedName("amount_per_round")
    private String amountPerRound;

    @SerializedName("total_amount_per_round")
    private String totalAmountPerRound;

    @SerializedName("status")
    private String status;

    public void setNumberOfMembers(String numberOfMembers) {
        this.numberOfMembers = numberOfMembers;
    }

    public String getNumberOfMembers() {
        return numberOfMembers;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupAvatar(String groupAvatar) {
        this.groupAvatar = groupAvatar;
    }

    public String getGroupAvatar() {
        return groupAvatar;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public boolean isIsAdmin() {
        return isAdmin;
    }

    public void setTotalWithdrawalLimit(String totalWithdrawalLimit) {
        this.totalWithdrawalLimit = totalWithdrawalLimit;
    }

    public String getTotalWithdrawalLimit() {
        return totalWithdrawalLimit;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setInvitationStatus(String invitationStatus) {
        this.invitationStatus = invitationStatus;
    }

    public String getInvitationStatus() {
        return invitationStatus;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setInterestRate(String interestRate) {
        this.interestRate = interestRate;
    }

    public String getInterestRate() {
        return interestRate;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setAmountPerRound(String amountPerRound) {
        this.amountPerRound = amountPerRound;
    }

    public String getAmountPerRound() {
        return amountPerRound;
    }

    public void setTotalAmountPerRound(String totalAmountPerRound) {
        this.totalAmountPerRound = totalAmountPerRound;
    }

    public String getTotalAmountPerRound() {
        return totalAmountPerRound;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}