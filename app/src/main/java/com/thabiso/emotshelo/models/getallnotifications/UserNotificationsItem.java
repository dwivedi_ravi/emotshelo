package com.thabiso.emotshelo.models.getallnotifications;

import com.google.gson.annotations.SerializedName;

public class UserNotificationsItem{

	@SerializedName("message_payload")
	private String messagePayload;

	@SerializedName("group_id")
	private String groupId;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("type")
	private String type;

	@SerializedName("deleted_at")
	private String deletedAt;

	@SerializedName("sent_datetime")
	private String sentDatetime;

	@SerializedName("from_user_name")
	private String fromUserName;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("to_user_profile_url")
	private String toUserProfileUrl;

	@SerializedName("to_user_id")
	private String toUserId;

	@SerializedName("to_user_name")
	private String toUserName;

	@SerializedName("id")
	private String id;

	@SerializedName("from_user_profile_url")
	private String fromUserProfileUrl;

	@SerializedName("from_user_id")
	private String fromUserId;

	public void setMessagePayload(String messagePayload){
		this.messagePayload = messagePayload;
	}

	public String getMessagePayload(){
		return messagePayload;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setDeletedAt(String deletedAt){
		this.deletedAt = deletedAt;
	}

	public String getDeletedAt(){
		return deletedAt;
	}

	public void setSentDatetime(String sentDatetime){
		this.sentDatetime = sentDatetime;
	}

	public String getSentDatetime(){
		return sentDatetime;
	}

	public void setFromUserName(String fromUserName){
		this.fromUserName = fromUserName;
	}

	public String getFromUserName(){
		return fromUserName;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setToUserProfileUrl(String toUserProfileUrl){
		this.toUserProfileUrl = toUserProfileUrl;
	}

	public String getToUserProfileUrl(){
		return toUserProfileUrl;
	}

	public void setToUserId(String toUserId){
		this.toUserId = toUserId;
	}

	public String getToUserId(){
		return toUserId;
	}

	public void setToUserName(String toUserName){
		this.toUserName = toUserName;
	}

	public String getToUserName(){
		return toUserName;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setFromUserProfileUrl(String fromUserProfileUrl){
		this.fromUserProfileUrl = fromUserProfileUrl;
	}

	public String getFromUserProfileUrl(){
		return fromUserProfileUrl;
	}

	public void setFromUserId(String fromUserId){
		this.fromUserId = fromUserId;
	}

	public String getFromUserId(){
		return fromUserId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
}