package com.thabiso.emotshelo.models;

public class RequestRegenerateAccessToken {
	private String deviceUUID;

	public void setDeviceUUID(String deviceUUID){
		this.deviceUUID = deviceUUID;
	}

	public String getDeviceUUID(){
		return deviceUUID;
	}

	@Override
 	public String toString(){
		return 
			"RequestRegenerateAccessToken{" +
			"deviceUUID = '" + deviceUUID + '\'' + 
			"}";
		}
}
