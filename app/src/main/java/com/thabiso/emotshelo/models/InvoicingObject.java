package com.thabiso.emotshelo.models;

import com.google.gson.annotations.SerializedName;
import com.thabiso.emotshelo.models.addcustomer.customerlist.DataItem;
import com.thabiso.emotshelo.models.grouplist.UserGroupsItem;
import com.thabiso.emotshelo.util.preferences.Defaults;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class InvoicingObject implements Serializable {

    @SerializedName("section_list")
    private List<String> sectionList;

    @SerializedName("invoice_list")
    private List<UserGroupsItem> invoiceList;

    @SerializedName("estimate_list")
    private List<UserGroupsItem> estimateList;

    @SerializedName("client_list")
    private List<DataItem> clientList;

    public List<String> getSectionList() {
        if (sectionList == null) {
            sectionList = new ArrayList<>();
            sectionList.add(Defaults.TAB_TITLE_INVOICES);
            sectionList.add(Defaults.TAB_TITLE_ESTIMATES);
            sectionList.add(Defaults.TAB_TITLE_CLIENTS);
        }
        return sectionList;
    }

    public List<UserGroupsItem> getInvoiceList() {
        if (invoiceList == null) {
            invoiceList = new ArrayList<>();

//            UserGroupsItem objUserGroups = new UserGroupsItem();
//            objUserGroups.setId("1");
//            objUserGroups.setName("XYZ Limited");
//            objUserGroups.setDescription("Mask Sold");
//            objUserGroups.setAmountPerRound("$1000.00");
//            objUserGroups.setCreatedAt("Today");
//            invoiceList.add(objUserGroups);
//
//            objUserGroups = new UserGroupsItem();
//            objUserGroups.setId("2");
//            objUserGroups.setName("ABC Limited");
//            objUserGroups.setDescription("Goods Sold");
//            objUserGroups.setAmountPerRound("$250.00");
//            objUserGroups.setCreatedAt("22/04/2020");
//            invoiceList.add(objUserGroups);
//
//            objUserGroups = new UserGroupsItem();
//            objUserGroups.setId("3");
//            objUserGroups.setName("Invoice 1");
//            objUserGroups.setDescription("Sales Invoice");
//            objUserGroups.setAmountPerRound("$250.00");
//            objUserGroups.setCreatedAt("18/04/2020");
//            invoiceList.add(objUserGroups);
//
//            objUserGroups = new UserGroupsItem();
//            objUserGroups.setId("4");
//            objUserGroups.setName("Invoice 2");
//            objUserGroups.setDescription("Purchase Invoice");
//            objUserGroups.setAmountPerRound("$550.00");
//            objUserGroups.setCreatedAt("01/01/2020");
//            invoiceList.add(objUserGroups);
//
//            objUserGroups = new UserGroupsItem();
//            objUserGroups.setId("5");
//            objUserGroups.setName("Invoice 5");
//            objUserGroups.setDescription("Sales Invoice");
//            objUserGroups.setAmountPerRound("$250.00");
//            objUserGroups.setCreatedAt("18/04/2020");
//            invoiceList.add(objUserGroups);
//
//            objUserGroups = new UserGroupsItem();
//            objUserGroups.setId("6");
//            objUserGroups.setName("Invoice 6");
//            objUserGroups.setDescription("Sales Invoice");
//            objUserGroups.setAmountPerRound("$250.00");
//            objUserGroups.setCreatedAt("18/04/2020");
//            invoiceList.add(objUserGroups);
//
//            objUserGroups = new UserGroupsItem();
//            objUserGroups.setId("7");
//            objUserGroups.setName("Invoice 7");
//            objUserGroups.setDescription("Sales Invoice");
//            objUserGroups.setAmountPerRound("$250.00");
//            objUserGroups.setCreatedAt("18/04/2020");
//            invoiceList.add(objUserGroups);
//
//            objUserGroups = new UserGroupsItem();
//            objUserGroups.setId("8");
//            objUserGroups.setName("Invoice 8");
//            objUserGroups.setDescription("Sales Invoice");
//            objUserGroups.setAmountPerRound("$250.00");
//            objUserGroups.setCreatedAt("18/04/2020");
//            invoiceList.add(objUserGroups);
//
//            objUserGroups = new UserGroupsItem();
//            objUserGroups.setId("9");
//            objUserGroups.setName("Invoice 9");
//            objUserGroups.setDescription("Sales Invoice");
//            objUserGroups.setAmountPerRound("$250.00");
//            objUserGroups.setCreatedAt("18/04/2020");
//            invoiceList.add(objUserGroups);
//
//            objUserGroups = new UserGroupsItem();
//            objUserGroups.setId("10");
//            objUserGroups.setName("Invoice 10");
//            objUserGroups.setDescription("Sales Invoice");
//            objUserGroups.setAmountPerRound("$250.00");
//            objUserGroups.setCreatedAt("18/04/2020");
//            invoiceList.add(objUserGroups);
        }
        return invoiceList;
    }

    public void setInvoiceList(List<UserGroupsItem> invoiceList) {
        this.invoiceList = invoiceList;
    }

    public List<UserGroupsItem> getEstimateList() {
        if (estimateList == null) {
            estimateList = new ArrayList<>();

//            UserGroupsItem objUserGroups = new UserGroupsItem();
//            objUserGroups.setId("1");
//            objUserGroups.setName("XYZ Limited");
//            objUserGroups.setDescription("Mask Sold");
//            objUserGroups.setAmountPerRound("$1000.00");
//            objUserGroups.setCreatedAt("Today");
//            estimateList.add(objUserGroups);
//
//            objUserGroups = new UserGroupsItem();
//            objUserGroups.setId("2");
//            objUserGroups.setName("ABC Limited");
//            objUserGroups.setDescription("Goods Sold");
//            objUserGroups.setAmountPerRound("$250.00");
//            objUserGroups.setCreatedAt("22/04/2020");
//            estimateList.add(objUserGroups);
//
//            objUserGroups = new UserGroupsItem();
//            objUserGroups.setId("3");
//            objUserGroups.setName("Estimate 1");
//            objUserGroups.setDescription("Sales Estimate");
//            objUserGroups.setAmountPerRound("$250.00");
//            objUserGroups.setCreatedAt("18/04/2020");
//            estimateList.add(objUserGroups);
//
//            objUserGroups = new UserGroupsItem();
//            objUserGroups.setId("4");
//            objUserGroups.setName("Estimate 2");
//            objUserGroups.setDescription("Sales Estimate");
//            objUserGroups.setAmountPerRound("$550.00");
//            objUserGroups.setCreatedAt("01/01/2020");
//            estimateList.add(objUserGroups);
//
//            objUserGroups = new UserGroupsItem();
//            objUserGroups.setId("5");
//            objUserGroups.setName("Estimate 5");
//            objUserGroups.setDescription("Sales Estimate");
//            objUserGroups.setAmountPerRound("$250.00");
//            objUserGroups.setCreatedAt("18/04/2020");
//            estimateList.add(objUserGroups);
//
//            objUserGroups = new UserGroupsItem();
//            objUserGroups.setId("6");
//            objUserGroups.setName("Estimate 6");
//            objUserGroups.setDescription("Sales Estimate");
//            objUserGroups.setAmountPerRound("$250.00");
//            objUserGroups.setCreatedAt("18/04/2020");
//            estimateList.add(objUserGroups);
//
//            objUserGroups = new UserGroupsItem();
//            objUserGroups.setId("7");
//            objUserGroups.setName("Estimate 7");
//            objUserGroups.setDescription("Sales Estimate");
//            objUserGroups.setAmountPerRound("$250.00");
//            objUserGroups.setCreatedAt("18/04/2020");
//            estimateList.add(objUserGroups);
//
//            objUserGroups = new UserGroupsItem();
//            objUserGroups.setId("8");
//            objUserGroups.setName("Estimate 8");
//            objUserGroups.setDescription("Sales Estimate");
//            objUserGroups.setAmountPerRound("$250.00");
//            objUserGroups.setCreatedAt("18/04/2020");
//            estimateList.add(objUserGroups);
//
//            objUserGroups = new UserGroupsItem();
//            objUserGroups.setId("9");
//            objUserGroups.setName("Estimate 9");
//            objUserGroups.setDescription("Sales Estimate");
//            objUserGroups.setAmountPerRound("$250.00");
//            objUserGroups.setCreatedAt("18/04/2020");
//            estimateList.add(objUserGroups);
//
//            objUserGroups = new UserGroupsItem();
//            objUserGroups.setId("10");
//            objUserGroups.setName("Estimate 10");
//            objUserGroups.setDescription("Sales Estimate");
//            objUserGroups.setAmountPerRound("$250.00");
//            objUserGroups.setCreatedAt("18/04/2020");
//            estimateList.add(objUserGroups);
        }
        return estimateList;
    }

    public void setEstimateList(List<UserGroupsItem> estimateList) {
        this.estimateList = estimateList;
    }

    public List<DataItem> getClientList() {
        if (clientList == null) {
            clientList = new ArrayList<>();

            //            UserContactsItem objUserContactsItem = new UserContactsItem();
//            objUserContactsItem.setUserId("1");
//            objUserContactsItem.setUserName("XYZ Limited");
//            objUserContactsItem.setMobile("74443701");
//            objUserContactsItem.setTemp("tmabaka@gmail.com");
//            objUserContactsItem.setAvatar("Gaborone Botswana");
//            clientList.add(objUserContactsItem);
//
//            objUserContactsItem = new UserContactsItem();
//            objUserContactsItem.setUserId("2");
//            objUserContactsItem.setUserName("ABC Limited");
//            objUserContactsItem.setMobile("74443701");
//            objUserContactsItem.setTemp("thabiso@moneychaps.com");
//            objUserContactsItem.setAvatar("Botswana");
//            clientList.add(objUserContactsItem);
//
//            objUserContactsItem = new UserContactsItem();
//            objUserContactsItem.setUserId("3");
//            objUserContactsItem.setUserName("Customer 3");
//            objUserContactsItem.setMobile("9428627128");
//            objUserContactsItem.setTemp("tmabaka@gmail.com");
//            objUserContactsItem.setAvatar("Gaborone Botswana");
//            clientList.add(objUserContactsItem);
//
//            objUserContactsItem = new UserContactsItem();
//            objUserContactsItem.setUserId("4");
//            objUserContactsItem.setUserName("Customer 4");
//            objUserContactsItem.setMobile("74443701");
//            objUserContactsItem.setTemp("tmabaka@gmail.com");
//            objUserContactsItem.setAvatar("Gaborone Botswana");
//            clientList.add(objUserContactsItem);
//
//            objUserContactsItem = new UserContactsItem();
//            objUserContactsItem.setUserId("5");
//            objUserContactsItem.setUserName("Customer 5");
//            objUserContactsItem.setMobile("74443701");
//            objUserContactsItem.setTemp("tmabaka@gmail.com");
//            objUserContactsItem.setAvatar("Gaborone Botswana");
//            clientList.add(objUserContactsItem);
//
//            objUserContactsItem = new UserContactsItem();
//            objUserContactsItem.setUserId("6");
//            objUserContactsItem.setUserName("Customer 6");
//            objUserContactsItem.setMobile("74443701");
//            objUserContactsItem.setTemp("tmabaka@gmail.com");
//            objUserContactsItem.setAvatar("Gaborone Botswana");
//            clientList.add(objUserContactsItem);
//
//            objUserContactsItem = new UserContactsItem();
//            objUserContactsItem.setUserId("7");
//            objUserContactsItem.setUserName("Customer 7");
//            objUserContactsItem.setMobile("74443701");
//            objUserContactsItem.setTemp("thabiso@moneychaps.com");
//            objUserContactsItem.setAvatar("Botswana");
//            clientList.add(objUserContactsItem);
//
//            objUserContactsItem = new UserContactsItem();
//            objUserContactsItem.setUserId("8");
//            objUserContactsItem.setUserName("Customer 8");
//            objUserContactsItem.setMobile("74443701");
//            objUserContactsItem.setTemp("thabiso@moneychaps.com");
//            objUserContactsItem.setAvatar("Botswana");
//            clientList.add(objUserContactsItem);
//
//            objUserContactsItem = new UserContactsItem();
//            objUserContactsItem.setUserId("9");
//            objUserContactsItem.setUserName("Customer 9");
//            objUserContactsItem.setMobile("74443701");
//            objUserContactsItem.setTemp("thabiso@moneychaps.com");
//            objUserContactsItem.setAvatar("Botswana");
//            clientList.add(objUserContactsItem);
//
//            objUserContactsItem = new UserContactsItem();
//            objUserContactsItem.setUserId("10");
//            objUserContactsItem.setUserName("Customer 10");
//            objUserContactsItem.setMobile("74443701");
//            objUserContactsItem.setTemp("tmabaka@gmail.com");
//            objUserContactsItem.setAvatar("Gaborone Botswana");
//            clientList.add(objUserContactsItem);
//
//            objUserContactsItem = new UserContactsItem();
//            objUserContactsItem.setUserId("11");
//            objUserContactsItem.setUserName("Customer 11");
//            objUserContactsItem.setMobile("74443701");
//            objUserContactsItem.setTemp("tmabaka@gmail.com");
//            objUserContactsItem.setAvatar("Gaborone Botswana");
//            clientList.add(objUserContactsItem);
//
//            objUserContactsItem = new UserContactsItem();
//            objUserContactsItem.setUserId("12");
//            objUserContactsItem.setUserName("Customer 12");
//            objUserContactsItem.setMobile("74443701");
//            objUserContactsItem.setTemp("tmabaka@gmail.com");
//            objUserContactsItem.setAvatar("Gaborone Botswana");
//            clientList.add(objUserContactsItem);
        }
        return clientList;
    }

    public void setClientList(List<DataItem> clientList) {
        this.clientList = clientList;
    }
}