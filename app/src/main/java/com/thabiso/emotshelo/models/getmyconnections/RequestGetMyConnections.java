package com.thabiso.emotshelo.models.getmyconnections;

import com.google.gson.annotations.SerializedName;

public class RequestGetMyConnections {

    @SerializedName("user_id")
    private String userId;

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    @Override
    public String toString() {
        return
                "RequestGetMyConnections{" +
                        "user_id = '" + userId + '\'' +
                        "}";
    }
}