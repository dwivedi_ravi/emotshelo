package com.thabiso.emotshelo.models.getallnotifications;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data{

	@SerializedName("userNotifications")
	private List<UserNotificationsItem> userNotifications;

	public void setUserNotifications(List<UserNotificationsItem> userNotifications){
		this.userNotifications = userNotifications;
	}

	public List<UserNotificationsItem> getUserNotifications(){
		return userNotifications;
	}
}