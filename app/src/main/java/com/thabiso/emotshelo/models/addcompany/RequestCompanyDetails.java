package com.thabiso.emotshelo.models.addcompany;

import com.google.gson.annotations.SerializedName;

public class RequestCompanyDetails {

	@SerializedName("company_id")
	private String companyId;

	@SerializedName("user_id")
	private String userId;

	public void setCompanyId(String companyId){
		this.companyId = companyId;
	}

	public String getCompanyId(){
		return companyId;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}
}