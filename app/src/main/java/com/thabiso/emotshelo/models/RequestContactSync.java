package com.thabiso.emotshelo.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RequestContactSync {

    @SerializedName("user_id")
    private String userId;

    @SerializedName("contacts")
    private List<ContactsItem> contacts;

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setContacts(List<ContactsItem> contacts) {
        this.contacts = contacts;
    }

    public List<ContactsItem> getContacts() {
        return contacts;
    }

    @Override
    public String toString() {
        return
                "RequestContactSync{" +
                        "user_id = '" + userId + '\'' +
                        ",contacts = '" + contacts + '\'' +
                        "}";
    }
}