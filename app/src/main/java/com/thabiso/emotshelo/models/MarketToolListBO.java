
package com.thabiso.emotshelo.models;


import java.io.Serializable;

public class MarketToolListBO implements Serializable {

    private int toolID;
    private int subToolId;
    private String toolTitle;
    private String toolShortDescription;
    private String toolLongDescription;
    private int totalSales;

    public int getToolID() {
        return toolID;
    }

    public void setToolID(int toolID) {
        this.toolID = toolID;
    }

    public String getToolTitle() {
        return toolTitle;
    }

    public void setToolTitle(String toolTitle) {
        this.toolTitle = toolTitle;
    }

    public String getToolShortDescription() {
        return toolShortDescription;
    }

    public void setToolShortDescription(String toolShortDescription) {
        this.toolShortDescription = toolShortDescription;
    }

    public String getToolLongDescription() {
        return toolLongDescription;
    }

    public void setToolLongDescription(String toolLongDescription) {
        this.toolLongDescription = toolLongDescription;
    }

    public int getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(int totalSales) {
        this.totalSales = totalSales;
    }

    public int getSubToolId() {
        return subToolId;
    }

    public void setSubToolId(int subToolId) {
        this.subToolId = subToolId;
    }
}
