package com.thabiso.emotshelo.models.registerdevice;

public class Data{
	private String token;

	public void setToken(String token){
		this.token = token;
	}

	public String getToken(){
		return token;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"token = '" + token + '\'' + 
			"}";
		}
}
