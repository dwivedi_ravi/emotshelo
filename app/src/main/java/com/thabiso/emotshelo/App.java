package com.thabiso.emotshelo;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.MultiDexApplication;

import com.orhanobut.hawk.Hawk;
import com.thabiso.emotshelo.util.ApplicationUtils;
import com.thabiso.emotshelo.util.preferences.Prefs;

//import com.squareup.leakcanary.LeakCanary;

/**
 * Created by dnld on 28/04/16.
 */
public class App extends MultiDexApplication {

    private static App mInstance;

    public static App getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        ApplicationUtils.init(this);
        initialiseStorage();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private void initialiseStorage() {
        Prefs.init(this);
        Hawk.init(this).build();
    }
}