package com.thabiso.emotshelo.interfaces;

import com.thabiso.emotshelo.models.expenses.ExpenseDetails;

public interface OnExpenseListManagerInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(ExpenseDetails item);
    }