package com.thabiso.emotshelo.network;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.activities.SplashScreen;
import com.thabiso.emotshelo.activities.homescreen.HomeScreenActivity;
import com.thabiso.emotshelo.activities.signup.EditProfileActivity;
import com.thabiso.emotshelo.activities.signup.VerifyOTPScreen;
import com.thabiso.emotshelo.database.DBHandler;
import com.thabiso.emotshelo.models.RequestContactSync;
import com.thabiso.emotshelo.models.RequestOTP;
import com.thabiso.emotshelo.models.RequestRegenerateAccessToken;
import com.thabiso.emotshelo.models.RequestRotateGroup;
import com.thabiso.emotshelo.models.RequestVerifyOTP;
import com.thabiso.emotshelo.models.ResponseRequestOTP;
import com.thabiso.emotshelo.models.ResponseVerifyOTP;
import com.thabiso.emotshelo.models.addcompany.RequestCompanyDetails;
import com.thabiso.emotshelo.models.addcompany.RequestEditCompanyDetails;
import com.thabiso.emotshelo.models.addcompany.RequestRegisterCompany;
import com.thabiso.emotshelo.models.addcompany.RequestUpdateToolStatus;
import com.thabiso.emotshelo.models.addcompany.ResponseAddCompany;
import com.thabiso.emotshelo.models.addcompany.ResponseCompanyList;
import com.thabiso.emotshelo.models.addcustomer.RequestAddEditCustomer;
import com.thabiso.emotshelo.models.addcustomer.RequestDeleteCustomer;
import com.thabiso.emotshelo.models.addcustomer.ResponseAddEditCustomer;
import com.thabiso.emotshelo.models.addcustomer.customerlist.ResponseGetCustomerList;
import com.thabiso.emotshelo.models.checkuserstatus.ResponseCheckUserStatus;
import com.thabiso.emotshelo.models.creategroup.RequestCreateGroup;
import com.thabiso.emotshelo.models.creategroup.ResponseCreateGroup;
import com.thabiso.emotshelo.models.creategroup.UserGroups;
import com.thabiso.emotshelo.models.editprofile.RequestEditProfile;
import com.thabiso.emotshelo.models.editprofile.ResponseEditProfile;
import com.thabiso.emotshelo.models.error.ErrorResponse;
import com.thabiso.emotshelo.models.error.Errors;
import com.thabiso.emotshelo.models.fcmregistration.RequestFCMPushToken;
import com.thabiso.emotshelo.models.fcmregistration.ResponseFCMPushToken;
import com.thabiso.emotshelo.models.getallnotifications.RequestGetAllNotification;
import com.thabiso.emotshelo.models.getallnotifications.ResponseGetAllNotifications;
import com.thabiso.emotshelo.models.getmyconnections.RequestGetMyConnections;
import com.thabiso.emotshelo.models.getmyconnections.ResponseGetMyConnections;
import com.thabiso.emotshelo.models.groupdetails.RequestGroupDetails;
import com.thabiso.emotshelo.models.groupdetails.ResponseGroupDetails;
import com.thabiso.emotshelo.models.grouplist.RequestGroupList;
import com.thabiso.emotshelo.models.grouplist.ResponseGroupList;
import com.thabiso.emotshelo.models.groupstatus.RequestUpdateGroupStatus;
import com.thabiso.emotshelo.models.imageupload.ResponseGenericImageUpload;
import com.thabiso.emotshelo.models.registerdevice.RequestRegisterDevice;
import com.thabiso.emotshelo.models.registerdevice.ResponseRegisterDevice;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;
import com.thabiso.emotshelo.util.preferences.Prefs;
import com.thabiso.emotshelo.util.storage.StorageHelper;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiCallerUtility {

    public static void callRegisterDeviceApi(AppCompatActivity activity) {
        try {
            if (Connectivity.isConnected(activity)) {
                RequestRegisterDevice objRequestRegisterDevice = new RequestRegisterDevice();
                objRequestRegisterDevice.setDeviceName(Build.MODEL);
                objRequestRegisterDevice.setDeviceOS(Build.VERSION.RELEASE);
                objRequestRegisterDevice.setDeviceOSVer(Build.VERSION.SDK_INT + "");
                objRequestRegisterDevice.setDeviceUUID(Settings.Secure.getString(activity.getContentResolver(),
                        Settings.Secure.ANDROID_ID));

                RestClient.GitApiInterface service = RestClient.getClient(activity);
                final Call<ResponseRegisterDevice> registerDeviceApiCall = service.callRegisterDevice(objRequestRegisterDevice);
                registerDeviceApiCall.enqueue(new Callback<ResponseRegisterDevice>() {
                    @Override
                    public void onResponse(Call<ResponseRegisterDevice> call, Response<ResponseRegisterDevice> response) {

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Prefs.setAuthKey(response.body().getData().getToken());
                                    ((SplashScreen) activity).takeUserIntoTheApp();
                                } else {
//                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
//                                    ADDED IN CASE OF DEVICE ALREADY REGISTERED THEN WE'LL NEED TO REGENERATE THE ACCESS TOKEN.
                                    callRegenerateAccessTokenAPI(activity);
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseRegisterDevice> call, Throwable t) {
                        Toast.makeText(activity, "Device registration failed. Please try again.", Toast.LENGTH_LONG).show();
                    }
                });
            } else {
                Toast.makeText(activity, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callRegenerateAccessTokenAPI(AppCompatActivity activity) {
        try {
            if (Connectivity.isConnected(activity)) {
                RequestRegenerateAccessToken objRequestRegenerateAccessToken = new RequestRegenerateAccessToken();
                objRequestRegenerateAccessToken.setDeviceUUID(Settings.Secure.getString(activity.getContentResolver(),
                        Settings.Secure.ANDROID_ID));

                final Call<ResponseRegisterDevice> requestRegenerateAccessToken = RestClient.getClient(activity).callRegenerateAccessToken(objRequestRegenerateAccessToken);
                requestRegenerateAccessToken.enqueue(new Callback<ResponseRegisterDevice>() {
                    @Override
                    public void onResponse(Call<ResponseRegisterDevice> call, Response<ResponseRegisterDevice> response) {

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Prefs.setAuthKey(response.body().getData().getToken());
                                    ((SplashScreen) activity).takeUserIntoTheApp();
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to register device", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseRegisterDevice> call, Throwable t) {
                        Toast.makeText(activity, "Device registration failed. Please try again.", Toast.LENGTH_LONG).show();
                    }
                });
            } else {
                Toast.makeText(activity, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callRequestOTPApi(Context context, RequestOTP objRequestOTP, boolean navigate) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;

                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Sending OTP..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient(context);
//                Map<String, String> map = new HashMap<>();
//                map.put("Authorization", Prefs.getAuthKey());
//                map.put("Content-Type", "application/json");
//                final Call<ResponseRequestOTP> requestOTPApiCall = service.callRequestOTP(map, objRequestOTP);

                final Call<ResponseRequestOTP> requestOTPApiCall = service.callRequestOTP(objRequestOTP);
                requestOTPApiCall.enqueue(new Callback<ResponseRequestOTP>() {
                    @Override
                    public void onResponse(Call<ResponseRequestOTP> call, Response<ResponseRequestOTP> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
//                            Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_LONG).show();
                                    if (navigate) {
                                        Intent inte = new Intent(activity, VerifyOTPScreen.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putSerializable("objRequestOTP", objRequestOTP);
                                        inte.putExtras(bundle);
                                        activity.startActivity(inte);
                                        activity.finish();
                                    }
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to send otp", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseRequestOTP> call, Throwable t) {
                        Toast.makeText(activity, "OTP sending failed. Please try again.", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callVerifyOTPApi(Context context, RequestOTP objRequestOTP, String otp) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                RequestVerifyOTP requestVerifyOTP = new RequestVerifyOTP();
                requestVerifyOTP.setMobileNumber(objRequestOTP.getMobile_number());
                requestVerifyOTP.setOtp(otp);
                requestVerifyOTP.setRequestType(objRequestOTP.getRequestType());

                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Verifying OTP..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient(activity);
                final Call<ResponseVerifyOTP> verifyOTPApiCall = service.callVerifyOTP(requestVerifyOTP);
                verifyOTPApiCall.enqueue(new Callback<ResponseVerifyOTP>() {
                    @Override
                    public void onResponse(Call<ResponseVerifyOTP> call, Response<ResponseVerifyOTP> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
//                                    if (requestVerifyOTP.getRequestType().equalsIgnoreCase(Defaults.REQUEST_TYPE_FORGOT_PASSWORD)) {
////                                        Intent inte = new Intent(activity, ChangePasswordScreen.class);
////                                        activity.startActivity(inte);
////                                        Toast.makeText(activity, "Mobile Number Verified Successfully. Please Do Change Password.", Toast.LENGTH_LONG).show();
////                                        activity.finish();
//                                    } else {
                                    Prefs.setPhoneNumberVerified(true);
                                    Prefs.setMobileNumber(requestVerifyOTP.getMobileNumber());
                                    Intent inte = new Intent(activity, EditProfileActivity.class);
                                    inte.putExtra("navigate", true);
                                    activity.startActivity(inte);
                                    activity.finish();
//                                    }
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to verify otp", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseVerifyOTP> call, Throwable t) {
                        Toast.makeText(activity, "OTP verification failed.", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callGetProfileDataApi(Context context, RequestOTP objRequestOTP) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Getting profile information..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient(activity);

                final Call<ResponseEditProfile> callGetProfile = service.callGetProfile(objRequestOTP);
                callGetProfile.enqueue(new Callback<ResponseEditProfile>() {
                    @Override
                    public void onResponse(Call<ResponseEditProfile> call, Response<ResponseEditProfile> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Prefs.setProfileCompleted(true);
//                                    DBHandler.getInstance(context).insertTBPROFILEDATA(response.body().getData().getUser());
                                    DBHandler.getInstance(context).insertTBPROFILEDATA(response.body());

                                    Bundle mBundle = new Bundle();
                                    mBundle.putSerializable("ResponseEditProfile", response.body());
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_FILL_PROFILE_DATA, context, mBundle);
                                } else {
//                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to get profile", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseEditProfile> call, Throwable t) {
                        Toast.makeText(activity, "Failed to get profile", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        Bundle mBundle = new Bundle();
                        mBundle.putSerializable("ResponseEditProfile", DBHandler.getInstance(context).getProfileDataFromDB());
                        Utility.sendUpdateListBroadCast(Defaults.ACTION_FILL_PROFILE_DATA, context, mBundle);
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
                Bundle mBundle = new Bundle();
                mBundle.putSerializable("ResponseEditProfile", DBHandler.getInstance(context).getProfileDataFromDB());
                Utility.sendUpdateListBroadCast(Defaults.ACTION_FILL_PROFILE_DATA, context, mBundle);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callEditProfileApi(Context context, RequestEditProfile objRequestEditProfile, boolean navigate) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Updating Profile..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient(activity);

                final Call<ResponseEditProfile> editProfileCall = service.callEditProfile(objRequestEditProfile);
                editProfileCall.enqueue(new Callback<ResponseEditProfile>() {
                    @Override
                    public void onResponse(Call<ResponseEditProfile> call, Response<ResponseEditProfile> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Prefs.setProfileCompleted(true);
//                                    DBHandler.getInstance(context).insertTBPROFILEDATA(response.body().getData().getUser());
                                    DBHandler.getInstance(context).insertTBPROFILEDATA(response.body());

                                    Bundle mBundle = new Bundle();
                                    mBundle.putSerializable("ResponseEditProfile", response.body());
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_PROFILE_DATA_UPDATED, context, mBundle);

                                    if (navigate) {
                                        Intent inte = new Intent(activity, HomeScreenActivity.class);
                                        activity.startActivity(inte);
                                        activity.finish();
                                    } else {
                                        activity.finish();
                                    }
                                } else {
                                    showErrorMessageFromServer((AppCompatActivity) context, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to update profile", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseEditProfile> call, Throwable t) {
                        Toast.makeText(activity, "Profile updation failed.", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callCreateGroupApi(Context context, RequestCreateGroup objRequestCreateGroup) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Creating group..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient(activity);

                final Call<ResponseCreateGroup> createGroupCall = service.callCreateGroup(objRequestCreateGroup);
                createGroupCall.enqueue(new Callback<ResponseCreateGroup>() {
                    @Override
                    public void onResponse(Call<ResponseCreateGroup> call, Response<ResponseCreateGroup> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Bundle mBundle = new Bundle();
                                    mBundle.putSerializable("ResponseCreateGroup", response.body());
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_FETCH_GROUP_LIST_FROM_SERVER, context, mBundle);
                                    activity.finish();
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to create group", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseCreateGroup> call, Throwable t) {
                        Toast.makeText(activity, "Group creation failed.", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callGroupListApi(Context context, RequestGroupList objRequestGroupList, LinearLayout lytNoConnection) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Getting group list..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient(activity);

                final Call<ResponseGroupList> groupListCall = service.callGroupList(objRequestGroupList);
                groupListCall.enqueue(new Callback<ResponseGroupList>() {
                    @Override
                    public void onResponse(Call<ResponseGroupList> call, Response<ResponseGroupList> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Bundle mBundle = new Bundle();
                                    mBundle.putSerializable("ResponseGroupList", response.body());
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_REFRESH_GROUP_LIST_SCREEN, context, mBundle);
                                } else {
//                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to get groups", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseGroupList> call, Throwable t) {
                        Toast.makeText(activity, "Failed to get groups.", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
//                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
                if (lytNoConnection != null) {
                    lytNoConnection.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callGroupDetailsApi(Context context, RequestGroupDetails objRequestGroupDetails) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Getting group details..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient(activity);

                final Call<ResponseGroupDetails> groupDetailsCall = service.callGroupDetails(objRequestGroupDetails);
                groupDetailsCall.enqueue(new Callback<ResponseGroupDetails>() {
                    @Override
                    public void onResponse(Call<ResponseGroupDetails> call, Response<ResponseGroupDetails> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Bundle mBundle = new Bundle();
                                    mBundle.putSerializable("ResponseGroupDetails", response.body());
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_FILL_GROUP_DETAILS_DATA, context, mBundle);
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_CALL_REFRESH_GROUP_LIST_API, context, null);
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to get group details", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseGroupDetails> call, Throwable t) {
                        Toast.makeText(activity, "Failed to get group details.", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callGetMyConnectionsApi(Context context, RequestGetMyConnections objRequestGetMyConnections) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Getting Connections..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient(activity);

                final Call<ResponseGetMyConnections> getMyConnectionsCall = service.callGetMyConnections(objRequestGetMyConnections);
                getMyConnectionsCall.enqueue(new Callback<ResponseGetMyConnections>() {
                    @Override
                    public void onResponse(Call<ResponseGetMyConnections> call, Response<ResponseGetMyConnections> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Bundle mBundle = new Bundle();
                                    mBundle.putSerializable("ResponseGetMyConnections", response.body());
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_UPDATE_CONNECTIONS_LIST, context, mBundle);
                                } else {
//                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to get connections", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseGetMyConnections> call, Throwable t) {
                        Toast.makeText(activity, "Failed to get connections.", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callRegisterPushTokenApi(Context context, RequestFCMPushToken objRequestFCMPushToken) {
        try {
            if (Connectivity.isConnected(context)) {
                RestClient.GitApiInterface service = RestClient.getClient(context);

                final Call<ResponseFCMPushToken> responseFCMPushTokenCall = service.callRegisterPushToken(objRequestFCMPushToken);
                responseFCMPushTokenCall.enqueue(new Callback<ResponseFCMPushToken>() {
                    @Override
                    public void onResponse(Call<ResponseFCMPushToken> call, Response<ResponseFCMPushToken> response) {
                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
//                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
//                            PAYMENT REQUIRED MESSAGE WAS COMING SO COMMENTED ABOVE CODE.
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Prefs.setPushTokenRegistered(true);
                                }
                            } else {
//                            Toast.makeText(context, "Unable to get connections", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseFCMPushToken> call, Throwable t) {
//                        Toast.makeText(context, "Failed to get push token.", Toast.LENGTH_LONG).show();
                    }
                });
            } else {
//                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callUploadProfileImageApi(Context context, Uri uri) {
        try {
            if (Connectivity.isConnected(context)) {
                RestClient.GitApiInterface service = RestClient.getClient(context);

                String path = StorageHelper.getMediaPath(context, uri);
                File file = new File(path);

                try {
                    Double imageSize = file.length() / 1024.0 / 1024.0;
                    if (imageSize > Defaults.MAXIMUM_IMAGES_SIZE_TO_UPLOAD) {
                        Toast.makeText(context, "Please upload image less than 2mb", Toast.LENGTH_LONG).show();
                        Utility.sendUpdateListBroadCast(Defaults.ACTION_PROFILE_PIC_UPLOAD_FAILED, context, null);
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // create RequestBody instance from file
                RequestBody requestFile =
                        RequestBody.create(
                                MediaType.parse(context.getContentResolver().getType(uri)),
                                file
                        );

                // MultipartBody.Part is used to send also the actual file name
                MultipartBody.Part body =
                        MultipartBody.Part.createFormData("avatar", file.getName(), requestFile);

                // add another part within the multipart request
                String mobileNumber = Prefs.getMobileNumber();
                RequestBody requestBody =
                        RequestBody.create(
                                okhttp3.MultipartBody.FORM, mobileNumber);

                // finally, execute the request
                Call<ResponseEditProfile> call = service.callUploadProfileImage(requestBody, body);
                call.enqueue(new Callback<ResponseEditProfile>() {
                    @Override
                    public void onResponse(Call<ResponseEditProfile> call, Response<ResponseEditProfile> response) {
                        Log.v("Upload", "success");
                        if (response != null && response.body() != null && response.body().isStatus()) {
                            Bundle mBundle = new Bundle();
                            mBundle.putSerializable("ResponseEditProfile", response.body());
                            DBHandler.getInstance(context).insertTBPROFILEDATA(response.body());
                            Utility.sendUpdateListBroadCast(Defaults.ACTION_PROFILE_PIC_UPLOAD_SUCCESS, context, mBundle);
                        } else {
                            Utility.sendUpdateListBroadCast(Defaults.ACTION_PROFILE_PIC_UPLOAD_FAILED, context, null);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseEditProfile> call, Throwable t) {
                        Log.e("Upload error:", t.getMessage());
                        Utility.sendUpdateListBroadCast(Defaults.ACTION_PROFILE_PIC_UPLOAD_FAILED, context, null);
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
                Utility.sendUpdateListBroadCast(Defaults.ACTION_PROFILE_PIC_UPLOAD_FAILED, context, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Utility.sendUpdateListBroadCast(Defaults.ACTION_PROFILE_PIC_UPLOAD_FAILED, context, null);
        }
    }

    public static void callUPloadProfileImageApiAdditional_FOR_REFERENCE(Context context, Uri uri) {
        try {
            if (Connectivity.isConnected(context)) {
                RestClient.GitApiInterface service = RestClient.getClient(context);

                File file = new File(StorageHelper.getMediaPath(context, uri));

                // Parsing any Media type file
                RequestBody requestFile = RequestBody.create(MediaType.parse("*/*"), file);
                MultipartBody.Part body = MultipartBody.Part.createFormData("avatar", file.getName(), requestFile);
                RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), Prefs.getUserId());

//                RequestBody requestFile =
//                        RequestBody.create(MediaType.parse("multipart/form-data"), file);

//                MultipartBody.Part body =
//                        MultipartBody.Part.createFormData("avatar", file.getName(), requestFile);

//                RequestBody userId =
//                        RequestBody.create(MediaType.parse("multipart/form-data"), Prefs.getUserId());

                Call<ResponseEditProfile> call = service.callUploadProfileImage(userId, body);
                call.enqueue(new Callback<ResponseEditProfile>() {
                    @Override
                    public void onResponse(Call<ResponseEditProfile> call, Response<ResponseEditProfile> response) {
                        Log.v("Upload", "success");
                        Bundle mBundle = new Bundle();
                        mBundle.putSerializable("ResponseEditProfile", response.body());
                        Utility.sendUpdateListBroadCast(Defaults.ACTION_PROFILE_PIC_UPLOAD_SUCCESS, context, mBundle);
                    }

                    @Override
                    public void onFailure(Call<ResponseEditProfile> call, Throwable t) {
                        Log.e("Upload error:", t.getMessage());
                        Utility.sendUpdateListBroadCast(Defaults.ACTION_PROFILE_PIC_UPLOAD_FAILED, context, null);
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Utility.sendUpdateListBroadCast(Defaults.ACTION_PROFILE_PIC_UPLOAD_FAILED, context, null);
        }
    }

    public static void callSyncMyContactsApi(Context context, RequestContactSync requestContactSync) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Sync in progress");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient(activity);

                final Call<ResponseEditProfile> callSyncMyContacts = service.callSyncMyContacts(requestContactSync);
                callSyncMyContacts.enqueue(new Callback<ResponseEditProfile>() {
                    @Override
                    public void onResponse(Call<ResponseEditProfile> call, Response<ResponseEditProfile> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_LONG).show();
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_CALL_GET_MY_CONNECTIONS, context, null);
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to update profile", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseEditProfile> call, Throwable t) {
                        Toast.makeText(activity, "Profile updation failed.", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callUpdateGroupStatusApi(Context context, RequestUpdateGroupStatus objRequestUpdateGroupStatus) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Changing Group Status");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient(activity);

                final Call<ResponseGroupDetails> updateGroupStatusCall = service.callUpdateGroupStatus(objRequestUpdateGroupStatus);
                updateGroupStatusCall.enqueue(new Callback<ResponseGroupDetails>() {
                    @Override
                    public void onResponse(Call<ResponseGroupDetails> call, Response<ResponseGroupDetails> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Bundle mBundle = new Bundle();
                                    mBundle.putSerializable("ResponseGroupDetails", response.body());
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_FILL_GROUP_DETAILS_DATA, context, mBundle);
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_CALL_REFRESH_GROUP_LIST_API, context, null);
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to perform this action", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseGroupDetails> call, Throwable t) {
                        Toast.makeText(activity, "Failed to perform this action", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callUpdateInvitationStatusApi(Context context, RequestUpdateGroupStatus objRequestUpdateGroupStatus) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Changing Invitation Status");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient(activity);

                final Call<ResponseGroupDetails> updateInvitationStatusCall = service.callUpdateInvitationStatus(objRequestUpdateGroupStatus);
                updateInvitationStatusCall.enqueue(new Callback<ResponseGroupDetails>() {
                    @Override
                    public void onResponse(Call<ResponseGroupDetails> call, Response<ResponseGroupDetails> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Bundle mBundle = new Bundle();
                                    mBundle.putSerializable("ResponseGroupDetails", response.body());
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_FILL_GROUP_DETAILS_DATA, context, mBundle);
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_CALL_REFRESH_GROUP_LIST_API, context, null);
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to perform this action", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseGroupDetails> call, Throwable throwable) {
                        Toast.makeText(activity, "Failed to perform this action", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callGetAllNotificationsApi(Context context, RequestGetAllNotification objRequestGetAllNotification) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Getting All Notifications");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient(activity);
                final Call<ResponseGetAllNotifications> getAllNotificationsCall = service.callGetAllNotifications(objRequestGetAllNotification);
                getAllNotificationsCall.enqueue(new Callback<ResponseGetAllNotifications>() {
                    @Override
                    public void onResponse(Call<ResponseGetAllNotifications> call, Response<ResponseGetAllNotifications> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Bundle mBundle = new Bundle();
                                    mBundle.putSerializable("ResponseGetAllNotifications", response.body());
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_REFRESH_NOTIFICATION_LIST, context, mBundle);
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to perform this action", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseGetAllNotifications> call, Throwable t) {
                        Toast.makeText(activity, "Failed to perform this action", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callRotateGroupApi(Context context, RequestRotateGroup objRequestRotateGroup) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Rotating Group");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient(activity);

                final Call<ResponseGroupDetails> rotateGroupCall = service.callRotateGroup(objRequestRotateGroup);
                rotateGroupCall.enqueue(new Callback<ResponseGroupDetails>() {
                    @Override
                    public void onResponse(Call<ResponseGroupDetails> call, Response<ResponseGroupDetails> response) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Bundle mBundle = new Bundle();
                                    mBundle.putSerializable("ResponseGroupDetails", response.body());
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_FILL_GROUP_DETAILS_DATA, context, mBundle);
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_CALL_REFRESH_GROUP_LIST_API, context, null);
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to perform this action", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseGroupDetails> call, Throwable throwable) {
                        Toast.makeText(activity, "Failed to perform this action", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callAddCompanyApi(Context context, RequestRegisterCompany objRequestRegisterCompany) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Adding Company..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient(activity);

                final Call<ResponseAddCompany> addCompanyApiCall = service.callAddCompany(objRequestRegisterCompany);
                addCompanyApiCall.enqueue(new Callback<ResponseAddCompany>() {
                    @Override
                    public void onResponse(Call<ResponseAddCompany> call, Response<ResponseAddCompany> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Bundle mBundle = new Bundle();
                                    mBundle.putSerializable("ResponseAddCompany", response.body());
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_FETCH_COMPANY_LIST_FROM_SERVER, context, mBundle);
                                    activity.finish();
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to perform this action", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseAddCompany> call, Throwable t) {
                        Toast.makeText(activity, "Failed to perform this action", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });

            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callCompanyListApi(Context context, RequestGroupList objRequestGroupList, LinearLayout lytNoConnection) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Getting company list..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient(activity);

                final Call<ResponseCompanyList> companyListCall = service.callCompanyList(objRequestGroupList);
                companyListCall.enqueue(new Callback<ResponseCompanyList>() {
                    @Override
                    public void onResponse(Call<ResponseCompanyList> call, Response<ResponseCompanyList> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Bundle mBundle = new Bundle();
                                    mBundle.putSerializable("ResponseCompanyList", response.body());
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_REFRESH_COMPANY_LIST_SCREEN, context, mBundle);
                                } else {
//                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(activity, "Unable to get company list", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseCompanyList> call, Throwable t) {
                        Toast.makeText(activity, "Failed to get companies.", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });

            } else {
//                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
                if (lytNoConnection != null) {
                    lytNoConnection.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callGetCompanyDetailsApi(Context context, RequestCompanyDetails objRequestCompanyDetails) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Getting Company Details..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient(activity);

                final Call<ResponseAddCompany> addCompanyApiCall = service.callCompanyDetails(objRequestCompanyDetails);
                addCompanyApiCall.enqueue(new Callback<ResponseAddCompany>() {
                    @Override
                    public void onResponse(Call<ResponseAddCompany> call, Response<ResponseAddCompany> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Bundle mBundle = new Bundle();
                                    mBundle.putSerializable("ResponseAddCompany", response.body());
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_FILL_COMPANY_DETAILS, context, mBundle);
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to perform this action", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseAddCompany> call, Throwable t) {
                        Toast.makeText(activity, "Failed to perform this action", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });

            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callDeleteCompanyApi(Context context, RequestCompanyDetails objRequestCompanyDetails) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Deleting Company..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient(activity);

                final Call<ResponseVerifyOTP> addCompanyApiCall = service.callDeleteCompany(objRequestCompanyDetails);
                addCompanyApiCall.enqueue(new Callback<ResponseVerifyOTP>() {
                    @Override
                    public void onResponse(Call<ResponseVerifyOTP> call, Response<ResponseVerifyOTP> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_FETCH_COMPANY_LIST_FROM_SERVER, context, null);
                                    activity.finish();
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to perform this action", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseVerifyOTP> call, Throwable t) {
                        Toast.makeText(activity, "Failed to perform this action", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });

            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callUpdateToolStatusApi(Context context, RequestUpdateToolStatus objRequestUpdateGroupStatus, int position) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Enabling Tool..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient(activity);

                final Call<ResponseAddCompany> addCompanyApiCall = service.callUpdateToolStatusForCompany(objRequestUpdateGroupStatus);
                addCompanyApiCall.enqueue(new Callback<ResponseAddCompany>() {
                    @Override
                    public void onResponse(Call<ResponseAddCompany> call, Response<ResponseAddCompany> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Bundle mBundle = new Bundle();
                                    mBundle.putInt("position", position);
                                    mBundle.putSerializable("ResponseAddCompany", response.body());
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_TOOL_STATUS_UPDATED, context, mBundle);
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to perform this action", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseAddCompany> call, Throwable t) {
                        Toast.makeText(activity, "Failed to perform this action", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });

            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callUploadCompanyLogoApi(Context context, Uri uri, String companyId) {
        try {
            if (Connectivity.isConnected(context)) {
                RestClient.GitApiInterface service = RestClient.getClient(context);

                String path = StorageHelper.getMediaPath(context, uri);
                File file = new File(path);

                try {
                    Double imageSize = file.length() / 1024.0 / 1024.0;
                    if (imageSize > Defaults.MAXIMUM_IMAGES_SIZE_TO_UPLOAD) {
                        Toast.makeText(context, "Please upload image less than 2 MB", Toast.LENGTH_LONG).show();
                        Utility.sendUpdateListBroadCast(Defaults.ACTION_COMPANY_LOGO_UPLOAD_FAILED, context, null);
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // create RequestBody instance from file
                RequestBody requestFile =
                        RequestBody.create(
                                MediaType.parse(context.getContentResolver().getType(uri)),
                                file
                        );

                // MultipartBody.Part is used to send also the actual file name
                MultipartBody.Part body =
                        MultipartBody.Part.createFormData("logo", file.getName(), requestFile);

                // add another part within the multipart request
                RequestBody requestBody =
                        RequestBody.create(
                                okhttp3.MultipartBody.FORM, companyId);

                // finally, execute the request
                Call<ResponseAddCompany> call = service.callUploadCompanyLogo(requestBody, body);
                call.enqueue(new Callback<ResponseAddCompany>() {
                    @Override
                    public void onResponse(Call<ResponseAddCompany> call, Response<ResponseAddCompany> response) {
                        Log.v("Upload", "success");
                        if (response != null && response.body() != null && response.body().isStatus()) {
                            Bundle mBundle = new Bundle();
                            mBundle.putSerializable("ResponseAddCompany", response.body());
                            Utility.sendUpdateListBroadCast(Defaults.ACTION_COMPANY_LOGO_UPLOAD_SUCCESS, context, mBundle);
                            Utility.sendUpdateListBroadCast(Defaults.ACTION_FETCH_COMPANY_LIST_FROM_SERVER, context, null);
                        } else {
                            Utility.sendUpdateListBroadCast(Defaults.ACTION_COMPANY_LOGO_UPLOAD_FAILED, context, null);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseAddCompany> call, Throwable t) {
                        Log.e("Upload error:", t.getMessage());
                        Utility.sendUpdateListBroadCast(Defaults.ACTION_COMPANY_LOGO_UPLOAD_FAILED, context, null);
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
                Utility.sendUpdateListBroadCast(Defaults.ACTION_COMPANY_LOGO_UPLOAD_FAILED, context, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Utility.sendUpdateListBroadCast(Defaults.ACTION_COMPANY_LOGO_UPLOAD_FAILED, context, null);
        }
    }

    public static void callEditCompanyDetailsApi(Context context, RequestEditCompanyDetails objRequestEditCompanyDetails) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Updating Company Details..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient(activity);

                final Call<ResponseAddCompany> addCompanyApiCall = service.callUpdateCompanyDetails(objRequestEditCompanyDetails);
                addCompanyApiCall.enqueue(new Callback<ResponseAddCompany>() {
                    @Override
                    public void onResponse(Call<ResponseAddCompany> call, Response<ResponseAddCompany> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Bundle mBundle = new Bundle();
                                    mBundle.putSerializable("ResponseAddCompany", response.body());
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_COMPANY_EDITED, context, mBundle);
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_FETCH_COMPANY_LIST_FROM_SERVER, context, null);
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to perform this action", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseAddCompany> call, Throwable t) {
                        Toast.makeText(activity, "Failed to perform this action", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });

            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callUploadGroupProfilePicApi(Context context, Uri uri, UserGroups objUserGroups) {
        try {
            if (Connectivity.isConnected(context)) {
                RestClient.GitApiInterface service = RestClient.getClient(context);

                String path = StorageHelper.getMediaPath(context, uri);
                File file = new File(path);

                try {
                    Double imageSize = file.length() / 1024.0 / 1024.0;
                    if (imageSize > Defaults.MAXIMUM_IMAGES_SIZE_TO_UPLOAD) {
                        Toast.makeText(context, "Please upload image less than 2 MB", Toast.LENGTH_LONG).show();
                        Utility.sendUpdateListBroadCast(Defaults.ACTION_GROUP_ICON_UPLOAD_FAILED, context, null);
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // create RequestBody instance from file
                RequestBody requestFile =
                        RequestBody.create(
                                MediaType.parse(context.getContentResolver().getType(uri)),
                                file
                        );

                // MultipartBody.Part is used to send also the actual file name
                MultipartBody.Part body =
                        MultipartBody.Part.createFormData("avatar", file.getName(), requestFile);

                // add another part within the multipart request
                String userId = Prefs.getUserId();
                RequestBody requestBody =
                        RequestBody.create(
                                okhttp3.MultipartBody.FORM, userId);

                String groupId = objUserGroups.getId();
                RequestBody requestBody2 =
                        RequestBody.create(
                                okhttp3.MultipartBody.FORM, groupId);

                // finally, execute the request
                Call<ResponseGroupDetails> call = service.callUploadGroupProfilePic(requestBody, requestBody2, body);
                call.enqueue(new Callback<ResponseGroupDetails>() {
                    @Override
                    public void onResponse(Call<ResponseGroupDetails> call, Response<ResponseGroupDetails> response) {
                        Log.v("Upload", "success");
                        if (response != null && response.body() != null && response.body().isStatus()) {
                            Bundle mBundle = new Bundle();
                            mBundle.putSerializable("ResponseGroupDetails", response.body());
                            Utility.sendUpdateListBroadCast(Defaults.ACTION_GROUP_ICON_UPLOAD_SUCCESS, context, mBundle);
                            Utility.sendUpdateListBroadCast(Defaults.ACTION_CALL_REFRESH_GROUP_LIST_API, context, null);
                        } else {
                            Utility.sendUpdateListBroadCast(Defaults.ACTION_GROUP_ICON_UPLOAD_FAILED, context, null);
                            Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseGroupDetails> call, Throwable t) {
                        Log.e("Upload error:", t.getMessage());
                        Utility.sendUpdateListBroadCast(Defaults.ACTION_GROUP_ICON_UPLOAD_FAILED, context, null);
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
                Utility.sendUpdateListBroadCast(Defaults.ACTION_GROUP_ICON_UPLOAD_FAILED, context, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Utility.sendUpdateListBroadCast(Defaults.ACTION_GROUP_ICON_UPLOAD_FAILED, context, null);
        }
    }

    public static void callCheckUserStatusApi(Context context) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                RestClient.GitApiInterface service = RestClient.getClient(context);
                RequestGroupList objRequestGroupList = new RequestGroupList();
                objRequestGroupList.setUserId(Prefs.getUserId());

                final Call<ResponseCheckUserStatus> checkUserStatusCall = service.callCheckUserStatus(objRequestGroupList);
                checkUserStatusCall.enqueue(new Callback<ResponseCheckUserStatus>() {
                    @Override
                    public void onResponse(Call<ResponseCheckUserStatus> call, Response<ResponseCheckUserStatus> response) {
                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
//                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    final AlertDialog textDialog = Utility.getTextDialog_Material(activity, activity.getResources().getString(R.string.dialog_title_alert), response.body().getData().getMessage());
                                    textDialog.setButton(DialogInterface.BUTTON_POSITIVE, activity.getString(R.string.ok_action).toUpperCase(), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            textDialog.dismiss();
                                            activity.finish();
                                        }
                                    });

                                    if (response.body().getData().getCode() != Defaults.USER_STATUS_ACTIVE)
                                        textDialog.show();

                                } else {
//                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
//                                Toast.makeText(activity, "Unable to get groups", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseCheckUserStatus> call, Throwable t) {
//                        Toast.makeText(activity, "Failed to get groups.", Toast.LENGTH_LONG).show();
                    }
                });
            } else {
//                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callGenericImageUploadApi(Context context, Uri uri, String successBroadcast, String failedBroadcast, String type, String id) {
        try {
            if (Connectivity.isConnected(context)) {
                RestClient.GitApiInterface service = RestClient.getClient(context);

                String path = StorageHelper.getMediaPath(context, uri);
                File file = new File(path);

                try {
                    Double imageSize = file.length() / 1024.0 / 1024.0;
                    if (imageSize > Defaults.MAXIMUM_IMAGES_SIZE_TO_UPLOAD) {
                        Toast.makeText(context, "Please upload image less than 2 MB", Toast.LENGTH_LONG).show();
                        Utility.sendUpdateListBroadCast(failedBroadcast, context, null);
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // create RequestBody instance from file
                RequestBody requestFile =
                        RequestBody.create(
                                MediaType.parse(context.getContentResolver().getType(uri)),
                                file
                        );

                // MultipartBody.Part is used to send also the actual file name
                MultipartBody.Part body =
                        MultipartBody.Part.createFormData("image", file.getName(), requestFile);

                // add another part within the multipart request
                String userId = Prefs.getUserId();
                RequestBody requestBodyUserId =
                        RequestBody.create(
                                okhttp3.MultipartBody.FORM, userId);

                RequestBody requestBodyId =
                        RequestBody.create(
                                okhttp3.MultipartBody.FORM, id);

                RequestBody requestBodyType =
                        RequestBody.create(
                                okhttp3.MultipartBody.FORM, type);

                // finally, execute the request
                Call<ResponseGenericImageUpload> call = service.callGenericImageUpload(requestBodyType, requestBodyUserId, requestBodyId, body);
                call.enqueue(new Callback<ResponseGenericImageUpload>() {
                    @Override
                    public void onResponse(Call<ResponseGenericImageUpload> call, Response<ResponseGenericImageUpload> response) {
                        Log.v("Upload", "success");
                        if (response != null && response.body() != null && response.body().isStatus()) {
                            Bundle mBundle = new Bundle();
                            mBundle.putSerializable("ResponseGenericImageUpload", response.body());
                            Utility.sendUpdateListBroadCast(successBroadcast, context, mBundle);
                            if (successBroadcast.equals(Defaults.ACTION_CUSTOMER_LOGO_UPLOAD_SUCCESS)) {
                                Utility.sendUpdateListBroadCast(Defaults.ACTION_FETCH_CUSTOMER_LIST_FROM_SERVER, context, null);
                            }
                        } else {
                            Utility.sendUpdateListBroadCast(failedBroadcast, context, null);
                            Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseGenericImageUpload> call, Throwable t) {
                        Log.e("Upload error:", t.getMessage());
                        Utility.sendUpdateListBroadCast(failedBroadcast, context, null);
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
                Utility.sendUpdateListBroadCast(failedBroadcast, context, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Utility.sendUpdateListBroadCast(failedBroadcast, context, null);
        }
    }

    public static void callAddEditCustomerApi(Context context, RequestAddEditCustomer objRequestAddEditCustomer) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Updating Customer Details..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient(activity);

                final Call<ResponseAddEditCustomer> addEditCustomerApiCall = service.callAddEditCustomer(objRequestAddEditCustomer);
                addEditCustomerApiCall.enqueue(new Callback<ResponseAddEditCustomer>() {
                    @Override
                    public void onResponse(Call<ResponseAddEditCustomer> call, Response<ResponseAddEditCustomer> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Bundle mBundle = new Bundle();
                                    mBundle.putSerializable("ResponseAddEditCustomer", response.body());
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_CUSTOMER_EDITED, context, mBundle);
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_FETCH_CUSTOMER_LIST_FROM_SERVER, context, null);
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to perform this action", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseAddEditCustomer> call, Throwable t) {
                        Toast.makeText(activity, "Failed to perform this action", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });

            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callGetCustomerListApi(Context context, RequestGetMyConnections objRequestGetMyConnections, LinearLayout lytNoConnection) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Getting customer list..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient(activity);

                final Call<ResponseGetCustomerList> customerListCall = service.callGetCustomerList(objRequestGetMyConnections);
                customerListCall.enqueue(new Callback<ResponseGetCustomerList>() {
                    @Override
                    public void onResponse(Call<ResponseGetCustomerList> call, Response<ResponseGetCustomerList> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Bundle mBundle = new Bundle();
                                    mBundle.putSerializable("ResponseGetCustomerList", response.body());
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_REFRESH_CUSTOMER_LIST_SCREEN, context, mBundle);
                                } else {
//                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(activity, "Unable to get customer list", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseGetCustomerList> call, Throwable t) {
                        Toast.makeText(activity, "Failed to get customers.", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
                if (lytNoConnection != null) {
                    lytNoConnection.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callDeleteCustomerApi(Context context, RequestDeleteCustomer objRequestDeleteCustomer) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Deleting Customer..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient(activity);

                final Call<ResponseVerifyOTP> deleteCustomerApiCall = service.callDeleteCustomer(objRequestDeleteCustomer);
                deleteCustomerApiCall.enqueue(new Callback<ResponseVerifyOTP>() {
                    @Override
                    public void onResponse(Call<ResponseVerifyOTP> call, Response<ResponseVerifyOTP> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_FETCH_CUSTOMER_LIST_FROM_SERVER, context, null);
                                    activity.finish();
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to perform this action", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseVerifyOTP> call, Throwable t) {
                        Toast.makeText(activity, "Failed to perform this action", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showErrorMessageFromServer(AppCompatActivity activity, ResponseBody errorBody, String messageFromServer) {
        try {
            AlertDialog textDialog = null;

            if (errorBody != null) {
                Gson gson = new Gson();
                ErrorResponse objErrorResponse = gson.fromJson(errorBody.string(), ErrorResponse.class);
                Errors objErrors = objErrorResponse.getErrors();

                if (objErrors != null && objErrors.getFirstname() != null && objErrors.getFirstname().size() > 0) {
                    String errMsg = "";
                    for (int i = 0; i < objErrors.getFirstname().size(); i++) {
                        errMsg += "\n" + objErrors.getFirstname().get(i) + "\n";
                    }

                    textDialog = Utility.getTextDialog_Material(activity, activity.getResources().getString(R.string.dialog_title_information), errMsg);
                    textDialog.setButton(DialogInterface.BUTTON_POSITIVE, activity.getString(R.string.ok_action).toUpperCase(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    });
                    textDialog.show();
                }
            } else {
                if (messageFromServer != null && !messageFromServer.trim().equals("")) {
                    textDialog = Utility.getTextDialog_Material(activity, activity.getResources().getString(R.string.dialog_title_information), messageFromServer);
                    textDialog.setButton(DialogInterface.BUTTON_POSITIVE, activity.getString(R.string.ok_action).toUpperCase(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    });
                    textDialog.show();
                }
            }

            if (textDialog != null) {
                if (textDialog.getButton(DialogInterface.BUTTON_POSITIVE) != null) {
                    textDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(activity.getResources().getColor(R.color.app_text_color));
                    textDialog.getButton(DialogInterface.BUTTON_POSITIVE).setBackground(activity.getResources().getDrawable(R.drawable.ripple));
                }
                if (textDialog.getButton(DialogInterface.BUTTON_NEGATIVE) != null) {
                    textDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(activity.getResources().getColor(R.color.app_text_color));
                    textDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setBackground(activity.getResources().getDrawable(R.drawable.ripple));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
