package com.thabiso.emotshelo.network;

//import com.squareup.okhttp.Interceptor;
//import com.squareup.okhttp.OkHttpClient;
//import com.squareup.okhttp.DirectionsResponse;666

import android.content.Context;

import com.thabiso.emotshelo.models.RequestContactSync;
import com.thabiso.emotshelo.models.RequestOTP;
import com.thabiso.emotshelo.models.RequestRegenerateAccessToken;
import com.thabiso.emotshelo.models.RequestRotateGroup;
import com.thabiso.emotshelo.models.RequestVerifyOTP;
import com.thabiso.emotshelo.models.ResponseRequestOTP;
import com.thabiso.emotshelo.models.ResponseVerifyOTP;
import com.thabiso.emotshelo.models.addcompany.RequestCompanyDetails;
import com.thabiso.emotshelo.models.addcompany.RequestEditCompanyDetails;
import com.thabiso.emotshelo.models.addcompany.RequestRegisterCompany;
import com.thabiso.emotshelo.models.addcompany.RequestUpdateToolStatus;
import com.thabiso.emotshelo.models.addcompany.ResponseAddCompany;
import com.thabiso.emotshelo.models.addcompany.ResponseCompanyList;
import com.thabiso.emotshelo.models.addcustomer.RequestAddEditCustomer;
import com.thabiso.emotshelo.models.addcustomer.RequestDeleteCustomer;
import com.thabiso.emotshelo.models.addcustomer.ResponseAddEditCustomer;
import com.thabiso.emotshelo.models.addcustomer.customerlist.ResponseGetCustomerList;
import com.thabiso.emotshelo.models.checkuserstatus.ResponseCheckUserStatus;
import com.thabiso.emotshelo.models.creategroup.RequestCreateGroup;
import com.thabiso.emotshelo.models.creategroup.ResponseCreateGroup;
import com.thabiso.emotshelo.models.editprofile.RequestEditProfile;
import com.thabiso.emotshelo.models.editprofile.ResponseEditProfile;
import com.thabiso.emotshelo.models.fcmregistration.RequestFCMPushToken;
import com.thabiso.emotshelo.models.fcmregistration.ResponseFCMPushToken;
import com.thabiso.emotshelo.models.getallnotifications.RequestGetAllNotification;
import com.thabiso.emotshelo.models.getallnotifications.ResponseGetAllNotifications;
import com.thabiso.emotshelo.models.getmyconnections.RequestGetMyConnections;
import com.thabiso.emotshelo.models.getmyconnections.ResponseGetMyConnections;
import com.thabiso.emotshelo.models.groupdetails.RequestGroupDetails;
import com.thabiso.emotshelo.models.groupdetails.ResponseGroupDetails;
import com.thabiso.emotshelo.models.grouplist.RequestGroupList;
import com.thabiso.emotshelo.models.grouplist.ResponseGroupList;
import com.thabiso.emotshelo.models.groupstatus.RequestUpdateGroupStatus;
import com.thabiso.emotshelo.models.imageupload.ResponseGenericImageUpload;
import com.thabiso.emotshelo.models.registerdevice.RequestRegisterDevice;
import com.thabiso.emotshelo.models.registerdevice.ResponseRegisterDevice;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Prefs;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public class RestClient {

    private static GitApiInterface gitApiInterface;
    //    public static String baseUrl = "http://eventapi.shyamdeveloper.co.in/api/";
    public static String baseUrl = "http://eventapi.pgbooking.co.in/api/";

    public static GitApiInterface getClient(Context context) {

        if (gitApiInterface == null) {
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    // Request customization: add request headers
//                    Request.Builder requestBuilder = original.newBuilder()
//                            .header("Authorization", "auth-value"); // <-- this is the important line

                    Request.Builder requestBuilder = original.newBuilder();
                    requestBuilder.addHeader("Authorization", Prefs.getAuthKey());
                    requestBuilder.addHeader("DeviceID", Utility.getDeviceId(context));
                    requestBuilder.addHeader("Content-Type", "application/json");

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });
            OkHttpClient okClient = httpClient.build();

//            Gson gson = new GsonBuilder()
//                    .setLenient()
//                    .create();

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(okClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            gitApiInterface = client.create(GitApiInterface.class);
        }
        return gitApiInterface;
    }

    public interface GitApiInterface {

        //        @POST("register-device")
        @POST("new-device")
        Call<ResponseRegisterDevice> callRegisterDevice(@Body RequestRegisterDevice objRequestRegisterDevice);

        @POST("token-revoke")
        Call<ResponseRegisterDevice> callRegenerateAccessToken(@Body RequestRegenerateAccessToken objRequestRegenerateAccessToken);

        @POST("user/request-otp")
        Call<ResponseRequestOTP> callRequestOTP(@Body RequestOTP objRequestOTP);

        @POST("user/verify-otp")
        Call<ResponseVerifyOTP> callVerifyOTP(@Body RequestVerifyOTP objRequestVerifyOTP);

        @POST("user/get-profile")
        Call<ResponseEditProfile> callGetProfile(@Body RequestOTP objRequestOTP);

        @POST("user/update-profile")
        Call<ResponseEditProfile> callEditProfile(@Body RequestEditProfile objRequestEditProfile);

        @POST("groups/create")
        Call<ResponseCreateGroup> callCreateGroup(@Body RequestCreateGroup objRequestCreateGroup);

        @POST("groups")
        Call<ResponseGroupList> callGroupList(@Body RequestGroupList objRequestGroupList);

        @POST("groups/detail")
        Call<ResponseGroupDetails> callGroupDetails(@Body RequestGroupDetails objRequestCreateGroup);

        @POST("groups/get-contacts")
        Call<ResponseGetMyConnections> callGetMyConnections(@Body RequestGetMyConnections objGetMyConnections);

        @POST("user/user-gcm")
        Call<ResponseFCMPushToken> callRegisterPushToken(@Body RequestFCMPushToken objRequestFCMPushToken);

        @Multipart
        @POST("user/avatar")
        Call<ResponseEditProfile> callUploadProfileImage(
                @Part("mobile_number") RequestBody mobile,
                @Part MultipartBody.Part file
        );

//        @Multipart
//        @POST("user/upload-multiple-file")
//        Call<ResponseProfile> callUploadMultipleImages(
//                @Part("property_id") RequestBody propertyId,
//                @Part MultipartBody.Part file1,
//                @Part MultipartBody.Part file2,
//                @Part MultipartBody.Part file3
//        );

        @POST("user/sync-contacts")
        Call<ResponseEditProfile> callSyncMyContacts(@Body RequestContactSync objRequestOTP);

        @POST("groups/update-group-status")
        Call<ResponseGroupDetails> callUpdateGroupStatus(@Body RequestUpdateGroupStatus objRequestUpdateGroupStatus);

        @POST("groups/update-invitation-status")
        Call<ResponseGroupDetails> callUpdateInvitationStatus(@Body RequestUpdateGroupStatus objRequestUpdateGroupStatus);

        @POST("notification")
        Call<ResponseGetAllNotifications> callGetAllNotifications(@Body RequestGetAllNotification objRequestGetAllNotification);

        @POST("groups/rotated-group")
        Call<ResponseGroupDetails> callRotateGroup(@Body RequestRotateGroup objRequestRotateGroup);

        @POST("company/add-company")
        Call<ResponseAddCompany> callAddCompany(@Body RequestRegisterCompany objRequestRegisterCompany);

        @POST("company/company-list")
        Call<ResponseCompanyList> callCompanyList(@Body RequestGroupList objRequestGroupList);

        @POST("company/company-detail")
        Call<ResponseAddCompany> callCompanyDetails(@Body RequestCompanyDetails objRequestCompanyDetails);

        @POST("company/delete-company")
        Call<ResponseVerifyOTP> callDeleteCompany(@Body RequestCompanyDetails objRequestCompanyDetails);

        @POST("company/update-status")
        Call<ResponseAddCompany> callUpdateToolStatusForCompany(@Body RequestUpdateToolStatus objRequestUpdateGroupStatus);

        @Multipart
        @POST("company/upload-company-logo")
        Call<ResponseAddCompany> callUploadCompanyLogo(
                @Part("company_id") RequestBody companyId,
                @Part MultipartBody.Part file
        );

        @POST("company/update-company")
        Call<ResponseAddCompany> callUpdateCompanyDetails(@Body RequestEditCompanyDetails objRequestEditCompanyDetails);

        @Multipart
        @POST("groups/avatar")
        Call<ResponseGroupDetails> callUploadGroupProfilePic(
                @Part("user_id") RequestBody userId,
                @Part("group_id") RequestBody groupId,
                @Part MultipartBody.Part file
        );

        @POST("user/validate-user")
        Call<ResponseCheckUserStatus> callCheckUserStatus(@Body RequestGroupList objRequestGroupList);

        @Multipart
        @POST("image-upload")
        Call<ResponseGenericImageUpload> callGenericImageUpload(
                @Part("type") RequestBody type,
                @Part("user_id") RequestBody userId,
                @Part("id") RequestBody id,
                @Part MultipartBody.Part image
        );

        @POST("customer/add-customer")
        Call<ResponseAddEditCustomer> callAddEditCustomer(@Body RequestAddEditCustomer objRequestAddEditCustomer);

        @POST("customer/customer-list")
        Call<ResponseGetCustomerList> callGetCustomerList(@Body RequestGetMyConnections objGetMyConnections);

        @POST("customer/delete-customer")
        Call<ResponseVerifyOTP> callDeleteCustomer(@Body RequestDeleteCustomer objRequestDeleteCustomer);
    }
}
