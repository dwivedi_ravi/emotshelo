package com.thabiso.emotshelo.util.preferences;

/**
 * Class for Preference keys.
 */
public final class Keys {

    // Prevent class instantiation
    private Keys() {
    }

    public static final String PREF_USER_FIRST_TIME = "user_first_time";
    public static final String PREF_UNIQUE_INTEGER_VALUE = "unique_integer_value";
    public static final String PREF_NOTIFICATION_COUNT_VALUE = "notification_count_value";
    public static final String PREF_VERSION_CODE_INFO = "version_code_info";
    public static final String PREF_SHOW_TRAINING_HOME_SCREEN = "show_training_home_screen";
    public static final String PREF_SHOW_TRAINING_DETAIL_SCREEN = "show_training_detail_screen";
    public static final String PREF_ANIMATIONS_DISABLED = "disable_animations";
    public static final String PREF_AUTH_KEY = "auth_key";
    public static final String PREF_PHONE_NUMBER_VERIFIED = "phone_number_verified";
    public static final String PREF_PROFILE_STATUS = "profile_status";
    public static final String PREF_MOBILE_NUMBER = "mobile_number";
    public static final String PREF_USER_ID = "user_id";
    public static final String PREF_USER_NAME = "user_name";
    public static final String PREF_USER_PROFILE_PIC_URL = "user_profile_pic_url";
    public static final String PREF_SOCIAL_MEDIA_AUTH_KEY = "social_media_auth_key";
    public static final String PREF_APPLICATION_PASSWORD_SET = "application_password_set";
    public static final String KEY_CURRENT_HOW_IT_WORKS_VIDEO_INDEX = "how_it_works_video_index";
    //    FIREBASE
    public static final String PREF_FIREBASE_INSTANCE_ID = "firebase_instance_id";
    public static final String PREF_FIREBASE_GLOBAL_BROADCAST_TOPIC_SUBSCRIPTION = "firebase_emotshelo_global_topic_subscription";
    public static final String PREF_FIREBASE_INDIAN_BROADCAST_TOPIC_SUBSCRIPTION = "firebase_emotshelo_indian_topic_subscription";
    public static final String PREF_REGISTER_PUSH_TOKEN_STATUS = "register_push_token_status";
}
