package com.thabiso.emotshelo.util.storage;

import android.content.Context;
import android.net.Uri;

import androidx.core.content.FileProvider;

import com.thabiso.emotshelo.util.ApplicationUtils;

import java.io.File;

/**
 * Created by dnld on 16/10/17.
 */

public class LegacyCompatFileProvider extends FileProvider {

    public static Uri getUri(Context context, File file) {
        return getUriForFile(context, ApplicationUtils.getPackageName() + ".provider", file);
    }
}
