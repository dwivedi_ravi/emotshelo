package com.thabiso.emotshelo.util.preferences;

/**
 * Class for storing Preference default values.
 */
public final class Defaults {

    public static final String FOLDER_PATH = "MoneyChaps/";
    public static final String FOLDER_PATH_PROFILE = FOLDER_PATH + "Profile/";
    public static final String FOLDER_PATH_PDF = FOLDER_PATH + "PDF/";

    public static final String DEVICE_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String SERVER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'.000000Z'";
    public static final String NUMERIC_FIELD_IDENTIFIER = "..";
    public static final String NUMERIC_FIELD_IDENTIFIER_FOR_OPTIONS = "Reply with the option number..";
    public static final int FOLDER_COLUMNS_PORTRAIT = 2;
    public static final int FOLDER_COLUMNS_LANDSCAPE = 3;
    public static final boolean SHOW_VIDEOS = true;
    public static final boolean ANIMATIONS_DISABLED = false;
    public static final String ACTION_REFRESH_MEDIA_AFTER_PHOTO_EDITING = "refresh_media_after_photo_editing";
    public static final String ACTION_REFRESH_GROUP_LIST_SCREEN = "action_refresh_group_list";
    public static final String ACTION_CALL_REFRESH_GROUP_LIST_API = "action_call_refresh_group_list_api";
    public static final String ACTION_REFRESH_COMPANY_LIST_SCREEN = "action_refresh_company_list";
    public static final String ACTION_FETCH_GROUP_LIST_FROM_SERVER = "action_fetch_group_list_from_server";
    public static final String ACTION_FETCH_COMPANY_LIST_FROM_SERVER = "action_fetch_company_list_from_server";
    public static final String ACTION_FILL_COMPANY_DETAILS = "action_fill_company_details";
    public static final String ACTION_COMPANY_DELETED = "action_company_deleted";
    public static final String ACTION_COMPANY_EDITED = "action_company_edited";
    public static final String ACTION_TOOL_STATUS_UPDATED = "action_tool_status_updated";
    public static final String ACTION_BROADCAST_SELECTED_CONTACT_LIST = "broadcast_selected_contact_list";
    public static final String ACTION_FILL_PROFILE_DATA = "action_fill_profile_data";
    public static final String ACTION_FILL_GROUP_DETAILS_DATA = "action_fill_group_details_data";
    public static final String ACTION_UPDATE_CONNECTIONS_LIST = "action_update_connections_list";
    public static final String ACTION_CALL_GET_MY_CONNECTIONS = "action_call_get_my_connections";
    public static final String ACTION_PROFILE_PIC_UPLOAD_SUCCESS = "action_profile_pic_upload_success";
    public static final String ACTION_REFRESH_NOTIFICATION_LIST = "action_refresh_notification_list";
    public static final String ACTION_PROFILE_PIC_UPLOAD_FAILED = "action_profile_pic_upload_failed";
    public static final String ACTION_GROUP_ICON_UPLOAD_SUCCESS = "action_group_icon_upload_success";
    public static final String ACTION_GROUP_ICON_UPLOAD_FAILED = "action_group_icon_upload_failed";
    public static final String ACTION_PROFILE_DATA_UPDATED = "action_fill_profile_data_updated";
    public static final String ACTION_OTP_RECEIVED = "otp_received";
    public static final String ACTION_COMPANY_LOGO_UPLOAD_SUCCESS = "action_company_logo_upload_success";
    public static final String ACTION_COMPANY_LOGO_UPLOAD_FAILED = "action_company_logo_upload_failed";
    public static final String ACTION_CUSTOMER_EDITED = "action_customer_edited";
    public static final String ACTION_CUSTOMER_LOGO_UPLOAD_SUCCESS = "action_customer_logo_upload_success";
    public static final String ACTION_CUSTOMER_LOGO_UPLOAD_FAILED = "action_customer_logo_upload_failed";
    public static final String ACTION_REFRESH_CUSTOMER_LIST_SCREEN = "action_call_refresh_customer_list";
    public static final String ACTION_FETCH_CUSTOMER_LIST_FROM_SERVER = "action_fetch_customer_list_from_server";

    public static final String ACTION_REFRESH_INVOICE_LIST_SCREEN = "action_refresh_invoice_list";
    public static final String ACTION_FETCH_INVOICE_LIST_FROM_SERVER = "action_fetch_invoice_list_from_server";
    public static final String ACTION_TOOL_ENABLED_REFRESH_INVOICESCREEN = "action_tool_enabled_refresh_invoicescreen";

    public static final int PERMISSION_CODE_MY_CAMERA = 100;
    public static final int PERMISSION_CODE_WRITE_STORAGE = 101;
    public static final int PERMISSION_CODE_READ_CONTACTS = 102;
    public static final int RESOLVE_HINT = 103;
    public static final String DUMMY_YEAR_PREFIX = "1950-";

    //    FIREBASE
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
    public static final String FIREBASE_TOPIC_GLOBAL_BROADCAST = "eMothsheloNotifications";
    public static final String FIREBASE_TOPIC_INDIAN_BROADCAST = "eMothsheloNotificationsIndia";
    public static final String FIREBASE_TOPIC_TEST_BROADCAST = "TestNotificationseMothshelo";
    public static final String CONTRY_CODE_INDIA = "in";
    public static final String GROUP_KEY_MONEY_CHAPS_NOTIFICATIONS = "group_key_eMothshelo_notifications";

    public static final int RESPONSE_CODE_OK = 200;
    public static final int RESPONSE_CODE_NOT_FOUND = 404;
    public static final int RESPONSE_CODE_UN_AUTHORIZED = 401;
    public static final int RESPONSE_CODE_INTERNAL_SERVER_ERROR = 500;

    public static final String TAB_TITLE_DEPOSITS = "DEPOSIT";
    public static final String TAB_TITLE_EXPENSES = "EXPENSE";
    public static final String TAB_TITLE_TRANSFERS = "TRANSFER";
    public static final String TAB_TITLE_WITHDRAWAL = "WITHDRAWAL";
    public static final String TAB_TITLE_INTEREST = "INTEREST";
    public static final int SCREEN_TYPE_DEPOSITS = 0;
    public static final int SCREEN_TYPE_EXPENSES = 1;
    public static final int SCREEN_TYPE_WITHDRAWAL = 1;
    public static final int SCREEN_TYPE_TRANSFERS = 2;
    public static final int SCREEN_TYPE_INTEREST = 2;

    //    public static final String TAB_TITLE_ENTERPRISE = "MY BUSINESS";
//    public static final String TAB_TITLE_GROUPS = "GROUPS";
//    public static final String TAB_TITLE_PERSONAL = "PERSONAL";
    public static final String TAB_TITLE_ENTERPRISE = "Business";
    public static final String TAB_TITLE_GROUPS = "Groups";
    public static final String TAB_TITLE_PERSONAL = "Personal";
    public static final String TAB_TITLE_HOME = "Home";
    //    public static final int SCREEN_TYPE_ENTERPRISE = 0;
//    public static final int SCREEN_TYPE_GROUPS = 1;
//    public static final int SCREEN_TYPE_PERSONAL = 2;
//    public static final int SCREEN_TYPE_HOME = 4;
    public static final int SCREEN_TYPE_ENTERPRISE = 3;
    public static final int SCREEN_TYPE_GROUPS = 2;
    public static final int SCREEN_TYPE_PERSONAL = 1;
    public static final int SCREEN_TYPE_HOME = 0;

    public static final String TAB_TITLE_INVOICES = "INVOICES";
    public static final String TAB_TITLE_ESTIMATES = "ESTIMATES";
    public static final String TAB_TITLE_CLIENTS = "CLIENTS";
    public static final int SCREEN_TYPE_INVOICES = 0;
    public static final int SCREEN_TYPE_ESTIMATES = 1;
    public static final int SCREEN_TYPE_CLIENTS = 2;

    public static final int TOOL_ID_ENTERPRICE = 101;
    public static final int TOOL_ID_GROUPS = 102;
    public static final int TOOL_ID_PERSONAL = 103;
    public static final int TOOL_ID_HOME = 104;

    public static final int TOOL_ID_PREMIUM_ENTERPRICE = 101;
    public static final int TOOL_ID_PREMIUM_GROUPS = 102;
    public static final int TOOL_ID_PREMIUM_PERSONAL = 103;

    public static final int SUB_TOOL_ID_ROTATING_SAVINGS = 1; // 0-Select Group*, 1- Birthday, 2- Stockvel
    public static final int SUB_TOOL_ID_BIRTHDAY = 1; //
    public static final int SUB_TOOL_ID_STOCKVEL = 2;
    public static final int SUB_TOOL_ID_WEDDINGS = 3; //
    public static final int SUB_TOOL_ID_FUNERALS = 4;
    public static final int SUB_TOOL_ID_BABY_SHOWER = 5;
    public static final int SUB_TOOL_ID_HOLIDAY_ROAD_TRIP = 6;
    public static final int SUB_TOOL_ID_PEER_TO_PEER = 7;

    public static final int SUB_TOOL_ID_INVOICING_AND_QUOTATION = 8;
    public static final int SUB_TOOL_ID_REVENUE_AND_EXPENSE_TRACKER = 9;
    public static final int SUB_TOOL_ID_STOCK_TRACKER = 10;
    public static final int SUB_TOOL_ID_WAGES_CALCULUTOR = 11;

    public static final String ACTION_TOOL_ENABLED_REFRESH_HOMESCREEN = "action_tool_enabled_refresh_homescreen";
    public static final String ACTION_TOOL_ENABLED_REFRESH_MARKETPLACE = "action_tool_enabled_refresh_marketplace";

    public static final int SCREEN_TYPE_INTEREST_STATEMENT = 1;
    public static final int SCREEN_TYPE_WITHDRAWALS_PAID_STATEMENT = 2;
    public static final int SCREEN_TYPE_WITHDRAWALS_OUTSTANDING_STATEMENT = 3;

    public static final int SUB_TOOL_ID_PREMIUM_INVOICING_AND_QUOTATION = 12;
    public static final int SUB_TOOL_ID_PREMIUM_INVENTORY_MANAGEMENT = 13;
    public static final int SUB_TOOL_ID_PREMIUM_BUSINESS_INCOME_AND_EXPENSE_TRACKER = 14;
    public static final int SUB_TOOL_ID_PREMIUM_ACCESS_TO_CREDIT = 15;
    public static final int SUB_TOOL_ID_PREMIUM_ENTERPRISE_WALLET_SERVICES = 16;

    public static final int SUB_TOOL_ID_PREMIUM_GROUPS_GOAL_BASED_SAVINGS = 17;
    public static final int SUB_TOOL_ID_PREMIUM_GROUPS_LOYALTY_REWARDS = 18;
    public static final int SUB_TOOL_ID_PREMIUM_GROUPS_WALLET_SERVICES = 19;

    public static final int SUB_TOOL_ID_PREMIUM_PERSONAL_BUDGET_EXPENSE_TRACKER = 20;
    public static final int SUB_TOOL_ID_PREMIUM_PERSONAL_LOYALTY_REWARDS = 21;
    public static final int SUB_TOOL_ID_PREMIUM_PERSONAL_WALLET_SERVICES = 22;

    public static final String TRAINING_SCREEN_HOMESCREEN = "home";
    public static final String TRAINING_SCREEN_TOOLS_MARKET_PLACE = "tool";

    public static final int SHOW_DATE_POP_UP = 1;
    public static final int SHOW_SPINNER_POP_UP = 2;
    public static final int SHOW_ANOTHER_SCREEN = 3;
    public static final int SHOW_CONFIRMATION_DIALOG = 4;
    public static final int SHOW_YES_NO_BUTTON = 5;

    public static final String REQUEST_TYPE_REGISTRATION = "Registration";
    public static final String REQUEST_TYPE_FORGOT_PASSWORD = "ForgotPassword";

    public static final String FINANCIAL_LITERACY_TOOL_ENTERPRISE = "A";
    public static final String FINANCIAL_LITERACY_TOOL_GROUPS = "A";
    public static final String FINANCIAL_LITERACY_TOOL_PERSONAL = "A";

    public static final String GROUP_STATUS_PENDING_TO_START = "101";
    public static final String GROUP_STATUS_STARTED = "102";
    public static final String GROUP_STATUS_CLOSED = "103";
    public static final String GROUP_STATUS_DELETED = "104";

//    public static final String INVITATION_STATUS_INITIATOR_OR_OWNER = "101";
//    public static final String INVITATION_STATUS_PENDING_OR_INVITED = "102";
//    public static final String INVITATION_STATUS_ACCEPTED_OR_JOINED = "103";
//    public static final String INVITATION_STATUS_REJECTED = "104";

    public static final String INVITATION_STATUS_PENDING_OR_INVITED = "101";
    public static final String INVITATION_STATUS_ACCEPTED_OR_JOINED = "102";
    public static final String INVITATION_STATUS_REJECTED = "103";

    public static final String TOOL_ACTIVATED = "101";
    public static final String TOOL_DEACTIVATED = "102";

    public static final int USER_STATUS_ACTIVE = 103;

    public static final Double MAXIMUM_IMAGES_SIZE_TO_UPLOAD = 2.0;

    public static final String TYPE_CUSTOMER_IMAGE_UPLOAD = "customer";

    // Prevent class instantiation
    private Defaults() {
    }
}