package com.thabiso.emotshelo.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateTimeUtils {
//    public static final String WHOLE_DATE_WITHOUT_SEPARATOR = "ddMMyyyyhhmmss";
        public static final String WHOLE_DATE_WITHOUT_SEPARATOR = "ddMMyyyyhhmmssSSS";
    public static final String WHOLE_DATE_WITH_SEPARATOR = "dd/MM/yyyy hh:mm:ss";
    public static final String DATE_DD_MM_YYYY = "dd/MM/yyyy";
    public static final String TXN_DATETIME = "dd/MM/yyyy hh:mm:ss";
    public static final String DATE_DD_MM_YYYY_HYPHEN = "dd-MM-yyyy";
    public static final String TIME_12_HRS = "hh:mm:ss a";
    public static final String TIME_24_HRS = "hh:mm:ss";

    public static String getDate(String dateFormat) {
        try {
            String timeStamp = new SimpleDateFormat(dateFormat,
                    Locale.ENGLISH).format(Calendar.getInstance().getTime());
            return timeStamp;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getTime(String timeFormat) {
        try {
            String timeStamp = new SimpleDateFormat(timeFormat,
                    Locale.ENGLISH).format(Calendar.getInstance().getTime());
            return timeStamp;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static boolean isDateBefore(String time, String endtime) {
        String pattern = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        try {
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(endtime);

            return date1.before(date2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isDateAfter(String time, String endtime) {
        String pattern = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        try {
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(endtime);

            return date1.after(date2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isDateEquals(String time, String endtime) {
        String pattern = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        try {
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(endtime);

            return date1.getTime() == date2.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
