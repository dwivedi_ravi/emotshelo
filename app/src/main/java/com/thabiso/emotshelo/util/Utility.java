package com.thabiso.emotshelo.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.activities.homescreen.HomeScreenActivity;
import com.thabiso.emotshelo.activities.onboardingscreen.activities.OnboardingPagerActivity;
import com.thabiso.emotshelo.firebasenotifications.firebaseutils.NotificationUtils;
import com.thabiso.emotshelo.util.preferences.Defaults;
import com.thabiso.emotshelo.util.preferences.Keys;
import com.thabiso.emotshelo.util.preferences.Prefs;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by dwive on 02-07-2018.
 */

public class Utility {
    public static int REQUEST_CODE_SECURITY_SETTINGS = 1;

    public static boolean checkIfVersionGreaterOrEqual(int buildVersion) {
        if (Build.VERSION.SDK_INT >= buildVersion)
            return true;
        else
            return false;
    }

    /**
     * sends a broadcast using LocalBroadcastManager with the given action and
     * bundle
     *
     * @param action  Action you want to set on the Intent
     * @param context
     * @param bundle  Pass a bundle instance if u want any
     */
    public static void sendUpdateListBroadCast(String action, Context context,
                                               Bundle bundle) {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(action);
        if (bundle != null)
            broadcastIntent.putExtra("bundle", bundle);
        LocalBroadcastManager.getInstance(context).sendBroadcast(
                broadcastIntent);
    }

    /**
     * Registers a LocalBroadcastManager with the given action and bundle
     *
     * @param reciever BroadcastReceiver you want to register to
     *                 LocalBroadcastManager
     */
    public static void registerReciever(Context context, IntentFilter filter,
                                        BroadcastReceiver reciever) {
        LocalBroadcastManager.getInstance(context).registerReceiver(reciever,
                filter);
    }

    /**
     * Unregisters the BroadcastReciever
     */
    public static void unRegisterReciever(Context context,
                                          BroadcastReceiver reciever) {
        try {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(
                    reciever);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Used to launch Rate This App On Playstore Intent
     *
     * @param context
     */
    public static void shareThisApp(Context context) {
        try {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, context.getResources().getString(R.string.share_this_app_message_text) + "https://play.google.com/store/apps/details?id=" + context.getPackageName());
            sendIntent.setType("text/plain");
            context.startActivity(Intent.createChooser(sendIntent, context.getResources().getText(R.string.send_to)));
        } catch (Exception e) {
        }
    }

    /**
     * Used to Report a Bug, directly to the developer.
     *
     * @param context
     */
    public static void reportABug(Context context, String[] addresses, String subject) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, getDeviceDetails());
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    //    FUNCTIONS RELATED TO ONBOARDING PAGER.
    public static int getToolbarHeight(Context context) {
        int height = (int) context.getResources().getDimension(R.dimen.abc_action_bar_default_height_material);
        return height;
    }

    public static int getStatusBarHeight(Context context) {
        int height = (int) context.getResources().getDimension(R.dimen.statusbar_size);
        return height;
    }

    public static Drawable tintMyDrawable(Drawable drawable, int color) {
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, color);
        DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_IN);
        return drawable;
    }

    /**
     * @return returns the Device details
     */
    public static String getDeviceDetails() {
        String deviceDetails = "\n\n\nModel: ";
        deviceDetails = deviceDetails + Build.MODEL;
        deviceDetails = deviceDetails + "\nAndroid Version: " + Build.VERSION.RELEASE;
        deviceDetails = deviceDetails + "\nAPI Level: " + Build.VERSION.SDK_INT;
        deviceDetails = deviceDetails + "\nBrand: " + Build.BRAND;
        deviceDetails = deviceDetails + "\nManufacturer: " + Build.MANUFACTURER;
        deviceDetails = deviceDetails + "\nTime: " + new Date(Build.TIME).toString();

        return deviceDetails;
    }

    public static void checkVersionUpdate(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            int newVersionCode = packageInfo.versionCode;
            int oldVersionCode = Prefs.getCurrentVersionCodeFromSharedPrefs();

            if (newVersionCode != oldVersionCode) {
                Prefs.setCurrentVersionCodeInSharedPrefs(newVersionCode);
                if (oldVersionCode <= 5) {
//                    AS WE DON'T WANT TO SHOW NOTIFICATION TO VERSION CODE 6 BECAUSE FOR VERSION 6 IT WAS ALREADY SHOWN.
//                    Utility.showInAppWelcomeNotification(context);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public static void checkAndShowIntroScreen(Context context) {
//        boolean isUserFirstTime = Prefs.getIsUserFirstTimeFromSharedPreferences();
//        if (isUserFirstTime && Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
//            Intent introIntent = new Intent(context, OnboardingPagerActivity.class);
//            introIntent.putExtra(Keys.PREF_USER_FIRST_TIME, isUserFirstTime);
//            context.startActivity(introIntent);
//        }
//    }
    //    FUNCTIONS RELATED TO ONBOARDING PAGER TILL HERE.

    /**
     * Used to launch Rate This App On Playstore Intent
     *
     * @param context
     */
    public static void rateThisAppOnPlaystore(Context context, String packageName) {
        Uri uri = Uri.parse("market://details?id=" + packageName);
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            context.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + packageName)));
        }
    }

    public static void showInAppWelcomeNotification(Context context) {
        try {
            NotificationUtils notificationUtils = new NotificationUtils(context);
//            notificationUtils.playNotificationSound();

            Intent resultIntent = new Intent(context, HomeScreenActivity.class);
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            resultIntent.putExtra("title", context.getResources().getString(R.string.welcome_notification_title));
            resultIntent.putExtra("message", context.getResources().getString(R.string.welcome_notification_text));
            resultIntent.putExtra("icon", "");

            notificationUtils.showNotificationMessage(context.getResources().getString(R.string.welcome_notification_title), context.getResources().getString(R.string.welcome_notification_text), Calendar.getInstance().getTimeInMillis(), resultIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Used to launch Privacy Policy Url.
     *
     * @param context
     */
    public static void loadUrlIntoBrowser(Context context, String url) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(url)));
        } catch (Exception e) {

        }
    }

    public static AlertDialog getTextDialogWithColoredTitleBar(AppCompatActivity activity, @StringRes int title, String Message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, android.R.style.Theme_Material_Light_Dialog_NoActionBar);
        View dialogLayout = activity.getLayoutInflater().inflate(R.layout.dialog_text, null);

        TextView dialogTitle = dialogLayout.findViewById(R.id.text_dialog_title);
        TextView dialogMessage = dialogLayout.findViewById(R.id.text_dialog_message);

        ((CardView) dialogLayout.findViewById(R.id.message_card)).setCardBackgroundColor(activity.getResources().getColor(R.color.white));
        dialogTitle.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDark));
        dialogMessage.setTextColor(activity.getResources().getColor(R.color.app_text_color));
        dialogTitle.setText(title);
        dialogMessage.setText(Message);
        builder.setView(dialogLayout);
        return builder.create();
    }

//    public static AlertDialog getTextDialog_Material(AppCompatActivity activity, String title, String Message) {
//        AlertDialog.Builder builder = new AlertDialog.Builder(activity, android.R.style.Theme_Material_Light_Dialog_NoActionBar);
//
//        builder.setCancelable(false);
//        if (!title.trim().equals(""))
//            builder.setTitle(title);
//        builder.setMessage(Message);
//
//        return builder.create();
//    }

    public static AlertDialog getTextDialog_Material(Activity activity, String title, String Message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, android.R.style.Theme_Material_Light_Dialog_NoActionBar);

        builder.setCancelable(false);
        if (!title.trim().equals(""))
            builder.setTitle(title);
        builder.setMessage(Message);

        return builder.create();
    }

    public void composeEmail(Context context, String[] addresses, String subject, Uri attachment) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_STREAM, attachment);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    public void composeEmail(Context context, String[] addresses, String subject) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:dwivedi.ravi.1807@gmail.com")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarColor(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().setStatusBarColor(activity.getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setTransperentStatusBarColor(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().setStatusBarColor(activity.getResources().getColor(R.color.black_overlay));
        }
    }

    /**
     * Returns true if the string is null or 0-length.
     *
     * @param str the string to be examined
     * @return true if str is null or zero length
     */
    public static boolean isEmpty(@Nullable String str) {
        return str == null || str.trim().length() == 0;
    }

//    public static IconicsDrawable getToolbarIcon(Context context, IIcon icon) {
//        return new IconicsDrawable(context).icon(icon).color(Color.WHITE).sizeDp(18);
//    }

    /**
     * Alert dialog to navigate to app settings
     * to enable necessary permissions
     */
    @TargetApi(Build.VERSION_CODES.M)
    public static void showPermissionsAlert(final Activity activity, final String permission, final int permissionCode, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Permission required!")
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        activity.requestPermissions(new String[]{permission},
                                permissionCode);
                    }
                }).show();
    }

    public static AlertDialog getProgressDialog(AppCompatActivity activity, String title, String Message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, android.R.style.Theme_Material_Light_Dialog_NoActionBar);
        View dialogLayout = activity.getLayoutInflater().inflate(R.layout.layout_loading_dialog, null);

        TextView dialogTitle = dialogLayout.findViewById(R.id.titleText);
        ProgressBar progressBar = dialogLayout.findViewById(R.id.progressBar);
        TextView dialogMessage = dialogLayout.findViewById(R.id.messageText);

        if (!title.trim().equals("")) {
            dialogTitle.setText(title);
            dialogTitle.setVisibility(View.VISIBLE);
        }
        dialogMessage.setText(Message);

        builder.setView(dialogLayout);
        builder.setCancelable(false);
        return builder.create();
    }

    public static boolean checkIfUserFromIndia(Context context) {
        try {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            String countryCode = tm.getSimCountryIso();
            if (countryCode != null && countryCode.equalsIgnoreCase(Defaults.CONTRY_CODE_INDIA))
                return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static RequestOptions getCustomizedRequestOptions(int placeHolder, int errorPlaceHolder) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(placeHolder);
        requestOptions.error(errorPlaceHolder);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.AUTOMATIC); // RECOMMENDED- FIRST
//        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE); // RECOMMENDED- SECOND
//        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL); // RECOMMENDED- THIRD

//        // With thumbnail url
//        Glide.with(context).load(url)
//                .thumbnail(Glide.with(context).load(thumbUrl))
//                .apply(requestOptions).into(imageView)
//
//// Without thumbnail url
//
//// If you know thumbnail size
//        Glide.with(context).load(url)
//                .thumbnail(Glide.with(context).load(url).apply(RequestOptions().override(thumbSize)))
//                .apply(requestOptions).into(imageView)
//
//// With size multiplier
//        Glide.with(context).load(url)
//                .thumbnail(0.25f)
//                .apply(requestOptions).into(imageView)

        return requestOptions;
    }

//    /**
//     * DOWNLOAD IMAGE USING GLIDE AND SAVE IT TO THE SDCARD.
//     */
//    public static void downloadImageUsingGlide() {
//        Glide.with(mContext)
//                .load("YOUR_URL")
//                .asBitmap()
//                .into(new SimpleTarget<Bitmap>(100, 100) {
//                    @Override
//                    public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
//                        saveImage(resource);
//                    }
//                });
//    }
//
//    private String saveImage(Bitmap image) {
//        String savedImagePath = null;
//
//        String imageFileName = "JPEG_" + "FILE_NAME" + ".jpg";
//        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
//                + "/YOUR_FOLDER_NAME");
//        boolean success = true;
//        if (!storageDir.exists()) {
//            success = storageDir.mkdirs();
//        }
//        if (success) {
//            File imageFile = new File(storageDir, imageFileName);
//            savedImagePath = imageFile.getAbsolutePath();
//            try {
//                OutputStream fOut = new FileOutputStream(imageFile);
//                image.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
//                fOut.close();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            // Add the image to the system gallery
//            galleryAddPic(savedImagePath);
//            Toast.makeText(mContext, "IMAGE SAVED", Toast.LENGTH_LONG).show();
//        }
//        return savedImagePath;
//    }
//
//    private void galleryAddPic(String imagePath) {
//        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//        File f = new File(imagePath);
//        Uri contentUri = Uri.fromFile(f);
//        mediaScanIntent.setData(contentUri);
//        sendBroadcast(mediaScanIntent);
//    }

    public static void hideKeyboard(Context context, IBinder token) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(token, 0);
    }

    public static void checkAndShowIntroScreen(Context context) {
        boolean isUserFirstTime = Prefs.getIsUserFirstTimeFromSharedPreferences();
        if (isUserFirstTime && Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            Intent introIntent = new Intent(context, OnboardingPagerActivity.class);
            introIntent.putExtra(Keys.PREF_USER_FIRST_TIME, isUserFirstTime);
            context.startActivity(introIntent);
        }
//        Intent introIntent = new Intent(context, OnboardingPagerActivity.class);
//        introIntent.putExtra(Keys.PREF_USER_FIRST_TIME, true);
//        context.startActivity(introIntent);
    }

    public static boolean checkViewTraingingCompleted(Activity context, View view, String screenType, boolean lightColor, String title, String message) {
        boolean trainingScreenShown = true;
        try {
            if (view != null && !Prefs.getBooleanValue(view.getId() + screenType)) {
                int color = context.getResources().getColor(R.color.colorAccent);
                if (lightColor)
                    color = context.getResources().getColor(R.color.colorAccentLightCard);
                trainingScreenShown = false;
                final SpannableString spnDescription = new SpannableString(message);
                spnDescription.setSpan(new StyleSpan(Typeface.BOLD_ITALIC), spnDescription.length() - 31, spnDescription.length(), 0);

                final TapTargetSequence sequence = new TapTargetSequence(context)
                        .targets(
                                TapTarget.forView(view, title, spnDescription)
                                        .dimColor(R.color.black)
//                                        .outerCircleColorInt(context.getResources().getColor(R.color.colorAccent))
                                        .outerCircleColorInt(color)
                                        .targetCircleColorInt(context.getResources().getColor(R.color.colorPrimary))
                                        .transparentTarget(true)
                                        .descriptionTextAlpha(1)
                                        .descriptionTextColor(android.R.color.white)
                                        .textColor(android.R.color.white)
                                        .tintTarget(true)
                                        .cancelable(true)
                                        .id(1)
                        )
                        .listener(new TapTargetSequence.Listener() {
                            @Override
                            public void onSequenceFinish() {
                                Prefs.setBooleanValue(view.getId() + screenType, true);
                            }

                            @Override
                            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                            }

                            @Override
                            public void onSequenceCanceled(TapTarget lastTarget) {
                            }
                        });
                sequence.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return trainingScreenShown;
    }

    public static String getRealPathFromURI(Context context, Uri contentUri) {
        String filePath = null;
        if (contentUri != null && "content".equals(contentUri.getScheme())) {
            Cursor cursor = context.getContentResolver().query(contentUri, new String[]{android.provider.MediaStore.Images.ImageColumns.DATA}, null, null, null);
            cursor.moveToFirst();
            filePath = cursor.getString(0);
            cursor.close();
        } else {
            filePath = contentUri.getPath();
        }
        Log.d("", "Chosen path = " + filePath);
        return filePath;
    }

    /**
     * On Android 4.3 and lower, if you want your app to retrieve a file from another app, it must invoke an intent such as ACTION_PICK or ACTION_GET_CONTENT. The user must then select a single app from which to pick a file and the selected app must provide a user interface for the user to browse and pick from the available files.
     * <p>
     * On Android 4.4 (API level 19) and higher, you have the additional option of using the ACTION_OPEN_DOCUMENT intent, which displays a system-controlled picker UI controlled that allows the user to browse all files that other apps have made available. From this single UI, the user can pick a file from any of the supported apps.
     * <p>
     * On Android 5.0 (API level 21) and higher, you can also use the ACTION_OPEN_DOCUMENT_TREE intent, which allows the user to choose a directory for a client app to access.
     */
    // FOR OPENING MEDIA FILES LIKE IMAGES, VIDEOS AND AUDIOS etc.
    public static void openImageChooserIntent(Activity activity, int PICK_REQUEST) {
        Intent chooseFromGalleryIntent = new Intent(Intent.ACTION_GET_CONTENT); // FOR READING MEDIA FILES LIKE IMAGES, VIDEOS AND AUDIOS etc.
        chooseFromGalleryIntent.setType("image/*");
        chooseFromGalleryIntent.addCategory(Intent.CATEGORY_OPENABLE);
//        chooseFromGalleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        activity.startActivityForResult(Intent.createChooser(chooseFromGalleryIntent, "Select Picture"), PICK_REQUEST);

//        startActivityForResult(getPickImageChooserIntent(), PICK_REQUEST);
    }

    // FOR OPENING NON-MEDIA FILES LIKE PDF, TEXT etc.
    public static void intentPickDocument(Activity activity, int PICK_PDF_REQUEST) {
        Intent intentPickDocument = new Intent();
        intentPickDocument.setAction(Intent.ACTION_OPEN_DOCUMENT); // FOR READING NON-MEDIA FILES LIKE PDF, TEXT etc. SELECT A FILE.
        intentPickDocument.addCategory(Intent.CATEGORY_OPENABLE);
        intentPickDocument.setType("application/pdf");
        activity.startActivityForResult(Intent.createChooser(intentPickDocument, "Select PDF"), PICK_PDF_REQUEST);
    }

    // FOR CREATING NON-MEDIA FILES LIKE PDF, TEXT etc.
    public static void createDocument(Activity activity, int CREATE_PDF_REQUEST) {
        Intent intentCreateDocument = new Intent();
        intentCreateDocument.setAction(Intent.ACTION_CREATE_DOCUMENT); // FOR READING NON-MEDIA FILES LIKE PDF, TEXT etc. SELECT A FILE.
        intentCreateDocument.addCategory(Intent.CATEGORY_OPENABLE);
        intentCreateDocument.setType("application/pdf");
        intentCreateDocument.putExtra(Intent.EXTRA_TITLE, "invoice.pdf");
        activity.startActivityForResult(intentCreateDocument, CREATE_PDF_REQUEST);
    }

    public static Intent getPickImageChooserIntent(Context context) {
        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = context.getPackageManager();

        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    public static GoogleApiClient getCreadenticalApiClient(AppCompatActivity activity) {
        return new GoogleApiClient.Builder(activity)
//                .addConnectionCallbacks(context)
//                .enableAutoManage(context, context)
                .addApi(Auth.CREDENTIALS_API)
                .build();
    }

    // Construct a request for phone numbers and show the picker
    public static void requestHint(AppCompatActivity activity) {
        try {
            HintRequest hintRequest = new HintRequest.Builder()
//                    .setHintPickerConfig(new CredentialPickerConfig.Builder()
//                            .setShowCancelButton(true)
//                            .build())
                    .setPhoneNumberIdentifierSupported(true)
                    .build();

            PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(
                    getCreadenticalApiClient(activity), hintRequest);
            activity.startIntentSenderForResult(intent.getIntentSender(),
                    Defaults.RESOLVE_HINT, null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void startSmsRetriever(AppCompatActivity activity) {
        // Get an instance of SmsRetrieverClient, used to start listening for a matching
// SMS message.
        SmsRetrieverClient client = SmsRetriever.getClient(activity /* context */);

// Starts SmsRetriever, which waits for ONE matching SMS message until timeout
// (5 minutes). The matching SMS message will be sent via a Broadcast Intent with
// action SmsRetriever#SMS_RETRIEVED_ACTION.
        Task<Void> task = client.startSmsRetriever();

// Listen for success/failure of the start Task. If in a background thread, this
// can be made blocking using Tasks.await(task, [timeout]);
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // Successfully started retriever, expect broadcast intent
                // ...
            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Failed to start retriever, inspect Exception for more details
                // ...
            }
        });
    }

    public static void clearGlideCachingData(Activity activity) {
        try {
            // This method must be called on the main thread.
            Glide.get(activity).clearMemory();
            new Thread(() -> {
                Glide.get(activity).clearDiskCache();
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }
}
