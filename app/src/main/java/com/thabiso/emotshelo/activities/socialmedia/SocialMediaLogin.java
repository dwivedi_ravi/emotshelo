package com.thabiso.emotshelo.activities.socialmedia;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.activities.premium_tool.LoginToPremiumAccount;
import com.thabiso.emotshelo.databinding.ActivitySocialMediaLoginBinding;
import com.thabiso.emotshelo.util.Tools;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Prefs;

public class SocialMediaLogin extends AppCompatActivity {

    private ActivitySocialMediaLoginBinding binding;

    private View parent_view;
    private AlertDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivitySocialMediaLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

//        parent_view = findViewById(android.R.id.content);
        parent_view = binding.getRoot();

        Tools.setSystemBarColor(this, R.color.colorPrimaryDark);

        binding.btnSignInWithGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogWithMessage("signing in using Google...");
            }
        });
        binding.btnSignInWithFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogWithMessage("signing in using Facebook...");
            }
        });
    }

    private void showDialogWithMessage(String msg) {
        progressDialog = Utility.getProgressDialog(SocialMediaLogin.this, "Sign in", msg);
        progressDialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null && progressDialog.isShowing()) {
                    Toast.makeText(SocialMediaLogin.this, "Sign in successfull!!", Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                    progressDialog = null;
                    Prefs.setSocialMediaAuthKey("AuthorizationTokenGotFromGoogleorFacebook");

                    Intent intent = new Intent(SocialMediaLogin.this, LoginToPremiumAccount.class);

//                    Bundle bundle = new Bundle();
//                    bundle.putBoolean("isLogin", false);
//                    intent.putExtras(bundle);

                    startActivity(intent);
                    SocialMediaLogin.this.finish();
                }
            }
        }, 3000);
    }
}
