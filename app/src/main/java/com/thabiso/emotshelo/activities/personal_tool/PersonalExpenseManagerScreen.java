package com.thabiso.emotshelo.activities.personal_tool;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.snackbar.Snackbar;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.activities.groups_tool.MemberCreditScoreActivity;
import com.thabiso.emotshelo.activities.groups_tool.expensemanager.SectionsPagerAdapter;
import com.thabiso.emotshelo.database.DBHandler;
import com.thabiso.emotshelo.databinding.ActivityReadChapterBinding;
import com.thabiso.emotshelo.interfaces.OnExpenseListManagerInteractionListener;
import com.thabiso.emotshelo.models.expenses.Expense;
import com.thabiso.emotshelo.models.expenses.ExpenseDetails;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;
import com.thabiso.emotshelo.util.storage.LegacyCompatFileProvider;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class PersonalExpenseManagerScreen extends AppCompatActivity implements OnExpenseListManagerInteractionListener {

    private ActivityReadChapterBinding binding;

    private BottomSheetBehavior bottomSheetBehavior;
    private SectionsPagerAdapter sectionsPagerAdapter;
    private Expense objExpense;
    private String user_id, group_id, group_type;
    private DBHandler dbHandler;
    private int position = 0;

    private ActivityResultLauncher mRequestMultiplePermissions = registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), new ActivityResultCallback<Map<String, Boolean>>() {
        @Override
        public void onActivityResult(Map<String, Boolean> result) {
            for (Map.Entry<String, Boolean> entry : result.entrySet()) {
                switch (entry.getKey()) {
                    case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                        if (entry.getValue()) {
                            createPdf();
                        } else {
                            Toast.makeText(PersonalExpenseManagerScreen.this, getString(R.string.storage_permission), Toast.LENGTH_LONG).show();
                        }
                        break;
                    case Manifest.permission.READ_EXTERNAL_STORAGE:
//                        if (entry.getValue()) {
//                            mGetContent.launch("image/*");
//                        } else {
////                            Toast.makeText(EditProfileActivity.this, getString(R.string.storage_permission), Toast.LENGTH_LONG).show();
//                        }
                        break;
                }
            }
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityReadChapterBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.fab.shrink();

        setSupportActionBar(binding.toolbar);
        binding.toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white_24dp));
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    binding.viewOverlay.setVisibility(View.GONE);
                } else
                    finish();
            }
        });
        setStatusBarColor();

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
//                    if (isFormValid()) {
//                        if (binding.tabs.getSelectedTabPosition() == 0) {
//                            addDepositOrExpense(Defaults.SCREEN_TYPE_DEPOSITS);
//                        } else {
//                            addDepositOrExpense(Defaults.SCREEN_TYPE_WITHDRAWAL);
//                        }
//                    }
//                } else {
//                    binding.fab.extend();
//                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                    binding.viewOverlay.setVisibility(View.VISIBLE);
//                }
            }
        });

        dbHandler = DBHandler.getInstance(this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            user_id = bundle.getString("user_id");
            group_id = bundle.getString("group_id");
            group_type = bundle.getString("group_type");
        }

//      BASED ON THE USER_ID AND GROUP_ID RECEIVED INITIALIZE EXPENSE FROM DB OR SERVER.
        initializeData();
        initializeBottomSheetLayout();

        if (objExpense != null) {
            binding.toolbar.setTitle(getResources().getString(R.string.title_activity_personal_expense_manager));
            if (group_type.equals(Defaults.SUB_TOOL_ID_PEER_TO_PEER + "")) {
                objExpense.getSectionListForPersonalExpensePeerToPeer();
                binding.viewPager.setOffscreenPageLimit(2);
            } else {
                objExpense.getSectionListForPersonalExpense();
            }
            sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager(), objExpense, this);
            binding.viewPager.setAdapter(sectionsPagerAdapter);
            binding.tabs.setupWithViewPager(binding.viewPager);

            binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    PersonalExpenseManagerScreen.this.position = position;
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }
    }

    /**
     * Method for initializing the sports data from resources.
     */
    private void initializeData() {
        objExpense = new Expense();

        for (int i = 0; i < 10; i++) {
            ExpenseDetails objExpenseDetails = new ExpenseDetails();
            objExpenseDetails.setAmount(100.00);
            objExpenseDetails.setCategory(1);
            objExpenseDetails.setCategoryText("Deposit: " + (i + 1));
            objExpenseDetails.setMode(2);
            objExpenseDetails.setModeText("Cash");
            objExpenseDetails.setParticulars("Deposit made by Mavis");
            objExpenseDetails.setTimeInMiliseconds(Calendar.getInstance().getTimeInMillis());
            objExpenseDetails.setTransactionDatetime(Calendar.getInstance().getTime().toString());

            objExpense.getDepositList().add(objExpenseDetails);
        }
        for (int i = 0; i < 14; i++) {
            ExpenseDetails objExpenseDetails = new ExpenseDetails();
            objExpenseDetails.setAmount(100.00);
            if (i % 2 == 0) {
                objExpenseDetails.setCategory(4);
                objExpenseDetails.setCategoryText("Food Expenditure");
                objExpenseDetails.setParticulars("Food expenses incurred during the trip");
            } else if (i % 3 == 0) {
                objExpenseDetails.setCategory(2);
                objExpenseDetails.setCategoryText("Shopping");
                objExpenseDetails.setParticulars("Shopping done at Relience Fresh");
            } else {
                objExpenseDetails.setCategory(3);
                objExpenseDetails.setCategoryText("Transport");
                objExpenseDetails.setParticulars("Transportation costs during the trip");
            }

            objExpenseDetails.setMode(2);
            objExpenseDetails.setModeText("Cash");
            objExpenseDetails.setTimeInMiliseconds(Calendar.getInstance().getTimeInMillis());
            objExpenseDetails.setTransactionDatetime(Calendar.getInstance().getTime().toString());

            objExpense.getExpenseList().add(objExpenseDetails);
        }

        if (group_type.equals(Defaults.SUB_TOOL_ID_PEER_TO_PEER + "")) {
            for (int i = 0; i < 10; i++) {
                ExpenseDetails objExpenseDetails = new ExpenseDetails();
                objExpenseDetails.setAmount(100.00);
                objExpenseDetails.setCategory(1);
                objExpenseDetails.setCategoryText("Inerest Paid: " + (i + 1));
                objExpenseDetails.setMode(2);
                objExpenseDetails.setModeText("Cash");
                objExpenseDetails.setParticulars("Interest Paid On Withdraval");
                objExpenseDetails.setTimeInMiliseconds(Calendar.getInstance().getTimeInMillis());
                objExpenseDetails.setTransactionDatetime(Calendar.getInstance().getTime().toString());

                objExpense.getInterestList().add(objExpenseDetails);
            }
        }
    }

    private void initializeBottomSheetLayout() {
        bottomSheetBehavior = BottomSheetBehavior.from(binding.contentBottomSheet.bottomSheet);

        binding.contentBottomSheet.closeBottomSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                binding.fab.shrink();
                binding.viewOverlay.setVisibility(View.GONE);
            }
        });

        bottomSheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    Utility.hideKeyboard(PersonalExpenseManagerScreen.this, binding.contentBottomSheet.closeBottomSheet.getWindowToken());
                    binding.fab.shrink();
                    binding.viewOverlay.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected void setStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private boolean isFormValid() {
        if (binding.contentBottomSheet.editTransactionDate.getVisibility() == View.VISIBLE && binding.contentBottomSheet.editTransactionDate.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentBottomSheet.editTransactionDate, "Date can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentBottomSheet.spnCategory.getVisibility() == View.VISIBLE && binding.contentBottomSheet.spnCategory.getSelectedItemPosition() == 0) {
            Snackbar.make(binding.contentBottomSheet.spnCategory, "Please select category", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentBottomSheet.spnMode.getVisibility() == View.VISIBLE && binding.contentBottomSheet.spnMode.getSelectedItemPosition() == 0) {
            Snackbar.make(binding.contentBottomSheet.spnMode, "Please select mode", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentBottomSheet.editAmount.getVisibility() == View.VISIBLE && binding.contentBottomSheet.editAmount.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentBottomSheet.editAmount, "Amount can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentBottomSheet.editParticulars.getVisibility() == View.VISIBLE && binding.contentBottomSheet.editParticulars.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentBottomSheet.editParticulars, "Particulars can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            binding.viewOverlay.setVisibility(View.GONE);
        } else
            super.onBackPressed();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                Rect outRect = new Rect();
                binding.viewOverlay.getGlobalVisibleRect(outRect);

                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    binding.viewOverlay.setVisibility(View.GONE);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    private boolean addDepositOrExpense(int screenType) {
        try {
            ExpenseDetails objExpenseDetails = new ExpenseDetails();

            objExpenseDetails.setAmount(Integer.parseInt(binding.contentBottomSheet.editAmount.getText().toString()));
            objExpenseDetails.setCategory(binding.contentBottomSheet.spnCategory.getSelectedItemPosition());
            objExpenseDetails.setCategoryText(binding.contentBottomSheet.spnCategory.getSelectedItem().toString());
            objExpenseDetails.setMode(binding.contentBottomSheet.spnMode.getSelectedItemPosition());
            objExpenseDetails.setModeText(binding.contentBottomSheet.spnMode.getSelectedItem().toString());

            objExpenseDetails.setParticulars(binding.contentBottomSheet.editParticulars.getText().toString());
            objExpenseDetails.setTimeInMiliseconds(Calendar.getInstance().getTimeInMillis());
            objExpenseDetails.setTransactionDatetime(Calendar.getInstance().getTime().toString());
//            objExpenseDetails.setTransactionDatetime(binding.contentBottomSheet.editTransactionDate.getText().toString());

            if (screenType == Defaults.SCREEN_TYPE_DEPOSITS)
                objExpense.getDepositList().add(0, objExpenseDetails);
            else
                objExpense.getExpenseList().add(0, objExpenseDetails);

            if (sectionsPagerAdapter != null && sectionsPagerAdapter.getFragment(screenType) != null) {
                sectionsPagerAdapter.getFragment(screenType).refreshList(objExpense);
            }

            if (screenType == Defaults.SCREEN_TYPE_DEPOSITS)
                Toast.makeText(PersonalExpenseManagerScreen.this, "Deposit Details Added Successfully", Toast.LENGTH_SHORT).show();
            else if (screenType == Defaults.SCREEN_TYPE_WITHDRAWAL)
                Toast.makeText(PersonalExpenseManagerScreen.this, "Expense Details Added Successfully", Toast.LENGTH_SHORT).show();

            binding.contentBottomSheet.editTransactionDate.setText("");
            binding.contentBottomSheet.spnCategory.setSelection(0);
            binding.contentBottomSheet.spnMode.setSelection(0);
            binding.contentBottomSheet.editAmount.setText("");
            binding.contentBottomSheet.editParticulars.setText("");

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onListFragmentInteraction(ExpenseDetails item) {
        Toast.makeText(this, item.getParticulars() + " Clicked", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_print_pdf, menu);
        if (group_type.equals(Defaults.SUB_TOOL_ID_PEER_TO_PEER + ""))
            menu.findItem(R.id.action_see_credit_scrore).setVisible(true);
        else
            menu.findItem(R.id.action_see_credit_scrore).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_print_pdf) {
            mRequestMultiplePermissions.launch(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE});
            return true;
        } else if (id == R.id.action_see_credit_scrore) {
//            showCreditScoreDialog();
            Intent intent = new Intent(PersonalExpenseManagerScreen.this, MemberCreditScoreActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void createPdf() {
        try {
            DisplayMetrics displaymetrics = new DisplayMetrics();
            this.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            float height = displaymetrics.heightPixels;
            float width = displaymetrics.widthPixels;
            int convertHeight = (int) height, convertWidth = (int) width;

//        PdfDocument document = new PdfDocument();
//        for (int i = 1; i <= 5; i++) {
//            PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(convertWidth, convertHighet, i).create();
//            PdfDocument.Page page = document.startPage(pageInfo);
//
//            Canvas canvas = page.getCanvas();
//            Paint paint = new Paint();
//            paint.setTextAlign(Paint.Align.CENTER);
//            paint.setTextSize(50);
//            paint.setTypeface(Typeface.DEFAULT_BOLD);
////            paint.setUnderlineText(true);
////            canvas.drawPaint(paint);
////            paint.setColor(Color.BLUE);
//
//            Resources mResources = getResources();
//            Bitmap bitmap = BitmapFactory.decodeResource(mResources, R.drawable.image_23);
//            bitmap = Bitmap.createScaledBitmap(bitmap, convertWidth, convertHighet, true);
//
////            canvas.drawText(("Page: " + i), 0, 0, paint);
//
//            // draw something on the page
////            View content = binding.contentBottomSheet.editAmount;
////            content.draw(page.getCanvas());
//
////            binding.viewPager.draw(page.getCanvas());
//
//            int screenType;
//            if (binding.tabs.getSelectedTabPosition() == 0)
//                screenType = Defaults.SCREEN_TYPE_DEPOSITS;
//            else
//                screenType = Defaults.SCREEN_TYPE_WITHDRAWAL;
//
//            if (sectionsPagerAdapter != null && sectionsPagerAdapter.getFragment(screenType) != null) {
////                sectionsPagerAdapter.getFragment(screenType).getRecyclerViewForPrinting().draw(page.getCanvas());
//
//                RecyclerView recyclerView = sectionsPagerAdapter.getFragment(screenType).getRecyclerViewForPrinting();
//                int y = 0;
//                for (int j = 0; j < recyclerView.getAdapter().getItemCount(); j++) {
//                    canvas.save();
//                    canvas.translate(0, y);
//                    View view = recyclerView.getLayoutManager().findViewByPosition(j);
//                    view.draw(canvas);
//                    canvas.restore();
//                    y += view.getHeight();
//                }
//            }
//
////            canvas.drawBitmap(bitmap, 0, 50, null);
//            document.finishPage(page);
//        }
//
//        // write the document content
//        String targetPdf = "/sdcard/pdffromlayout.pdf";
//        File filePath;
//        filePath = new File(targetPdf);
//        try {
//            document.writeTo(new FileOutputStream(filePath));
//        } catch (IOException e) {
//            e.printStackTrace();
//            Toast.makeText(this, "Something wrong: " + e.toString(), Toast.LENGTH_LONG).show();
//        }
//
//        // close the document
//        document.close();
//        Toast.makeText(this, "PDF is created!!!", Toast.LENGTH_SHORT).show();
//        openGeneratedPDF();

            int screenType;
            if (binding.tabs.getSelectedTabPosition() == 0)
                screenType = Defaults.SCREEN_TYPE_DEPOSITS;
            else
                screenType = Defaults.SCREEN_TYPE_WITHDRAWAL;

            if (sectionsPagerAdapter != null && sectionsPagerAdapter.getFragment(screenType) != null) {
                RecyclerView recyclerView = sectionsPagerAdapter.getFragment(screenType).getRecyclerViewForPrinting();

                if (recyclerView != null && recyclerView.getAdapter() != null && recyclerView.getAdapter().getItemCount() > 0) {
                    PdfDocument document = new PdfDocument();
                    PdfDocument.PageInfo pageInfo;
                    PdfDocument.Page page = null;
                    Canvas canvas = null;
                    int y = 0;
                    int pageNumber = 0;
                    int printingHeight = convertHeight - recyclerView.getLayoutManager().findViewByPosition(0).getHeight();

                    for (int i = 0; i < recyclerView.getAdapter().getItemCount(); i++) {
                        if (y == 0 || y > printingHeight) {
                            if (i > 0)
                                document.finishPage(page);
                            pageInfo = new PdfDocument.PageInfo.Builder(convertWidth, convertHeight, pageNumber).create();
                            page = document.startPage(pageInfo);
                            canvas = page.getCanvas();
                            pageNumber++;
                            y = 0;
                        }

                        if (canvas != null) {
                            canvas.save();
                            canvas.translate(0, y);
                            View view = recyclerView.getLayoutManager().findViewByPosition(i);
                            view.draw(canvas);
                            canvas.restore();
                            y += view.getHeight();
                        }

                        if (i == recyclerView.getAdapter().getItemCount() - 1) {
                            if (page != null) {
                                document.finishPage(page);
                                break;
                            }
                        }
                    }

                    File internalDirectoryPath = new File(getFilesDir().getAbsolutePath() + "/" + Defaults.FOLDER_PATH_PDF);
                    if (!internalDirectoryPath.exists())
                        internalDirectoryPath.mkdirs();

                    File pdfFile = new File(internalDirectoryPath, Calendar.getInstance().getTimeInMillis() + ".pdf");
                    if (!pdfFile.exists())
                        pdfFile.createNewFile();

                    try {
                        document.writeTo(new FileOutputStream(pdfFile));
                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(this, "Something wrong: " + e.toString(), Toast.LENGTH_LONG).show();
                    }
                    // close the document
                    document.close();
                    Toast.makeText(this, "PDF is created!!!", Toast.LENGTH_SHORT).show();
                    openGeneratedPDF(pdfFile.getPath());
                }
            }
        } catch (Exception ex) {
            Toast.makeText(this, "Unable to create pdf", Toast.LENGTH_LONG).show();
        }
    }

    private void openGeneratedPDF(String filePath) {
        File file = new File(filePath);
        if (file.exists()) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
//            Uri uri = Uri.fromFile(file);
            Uri uri = LegacyCompatFileProvider.getUri(PersonalExpenseManagerScreen.this, file);
            intent.setDataAndType(uri, "application/pdf");
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(PersonalExpenseManagerScreen.this, "No Application available to view pdf", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void showCreditScoreDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_show_credit_score);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((TextView) dialog.findViewById(R.id.title)).setText("Name");
        ((CircleImageView) dialog.findViewById(R.id.image)).setImageResource(R.drawable.user_1);

        ((ImageButton) dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        ((AppCompatButton) dialog.findViewById(R.id.bt_follow)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Follow Clicked", Toast.LENGTH_SHORT).show();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }
}