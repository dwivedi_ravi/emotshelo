package com.thabiso.emotshelo.activities.tools;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.activities.base.SharedActivity;
import com.thabiso.emotshelo.databinding.ActivityToolsMarketPlaceBinding;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;

public class ToolMarketPlaceActivity extends SharedActivity implements View.OnClickListener {

    private ActivityToolsMarketPlaceBinding binding;

    private MyReceiver objMyReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityToolsMarketPlaceBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        objMyReceiver = new MyReceiver();

        setSupportActionBar(binding.appbarLayout.toolbar);
        Utility.setStatusBarColor(this);
        registerReciever();

        binding.appbarLayout.toolbar.setTitle(getResources().getString(R.string.title_activity_tools_market_place));
        binding.appbarLayout.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        binding.appbarLayout.toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        binding.appbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        setListeners();

        Bundle bundle = getIntent().getExtras();
        int toolID;
        if (bundle != null) {
            toolID = bundle.getInt("toolID", Defaults.TOOL_ID_ENTERPRICE);
            navigateToNextActivity(toolID);
        }
    }

    private void setListeners() {
        binding.contentToolsMarketPlace.lytParentEnterprise.setOnClickListener(this);
        binding.contentToolsMarketPlace.lytParentGroups.setOnClickListener(this);
        binding.contentToolsMarketPlace.lytParentPersonal.setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_BROADCAST_SELECTED_CONTACT_LIST);
        Utility.registerReciever(ToolMarketPlaceActivity.this, filter, objMyReceiver);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lyt_parentEnterprise:
                navigateToNextActivity(Defaults.TOOL_ID_ENTERPRICE);
                break;
            case R.id.lyt_parentGroups:
                navigateToNextActivity(Defaults.TOOL_ID_GROUPS);
                break;
            case R.id.lyt_parentPersonal:
                navigateToNextActivity(Defaults.TOOL_ID_PERSONAL);
                break;
        }
    }

    private void navigateToNextActivity(int toolId) {
        Intent intent = new Intent(ToolMarketPlaceActivity.this, ToolListingActivity.class);
        intent.putExtra("clickedTool", toolId);
        startActivity(intent);
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Defaults.ACTION_BROADCAST_SELECTED_CONTACT_LIST)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

