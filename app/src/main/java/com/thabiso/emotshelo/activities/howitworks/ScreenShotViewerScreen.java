package com.thabiso.emotshelo.activities.howitworks;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.databinding.ActivityScreenShotViewerBinding;
import com.thabiso.emotshelo.databinding.ItemScreenshotViewerScreenBinding;
import com.thabiso.emotshelo.util.Measure;
import com.thabiso.emotshelo.util.Tools;
import com.thabiso.emotshelo.util.preferences.Defaults;

public class ScreenShotViewerScreen extends AppCompatActivity {

    private ActivityScreenShotViewerBinding binding;

    private int MAX_STEP = 4;
    private MyViewPagerAdapter myViewPagerAdapter;
    private int screenType = Defaults.TOOL_ID_ENTERPRICE;

//    private String title_array[] = {
//            "Ready to Travel",
//            "Pick the Ticket",
//            "Flight to Destination",
//            "Enjoy Holiday"
//    };
//    private String description_array[] = {
//            "Choose your destination, plan Your trip. Pick the best place for Your holiday",
//            "Select the day, pick Your ticket. We give you the best prices. We guarantee!",
//            "Safe and Comfort flight is our priority. Professional crew and services.",
//            "Enjoy your holiday, Dont forget to feel the moment and take a photo!",
//    };

    private int enterprise_tools_screenshots_array[] = {
            R.drawable.screenshot_invoice_listing,
            R.drawable.screenshot_invoice_details,
            R.drawable.screenshot_add_invoice,
            R.drawable.screenshot_client_listing
    };

    private int group_tools_screenshots_array[] = {
            R.drawable.screenshot_create_group,
            R.drawable.screenshot_group_listing,
            R.drawable.screenshot_group_details,
            R.drawable.screenshot_personal_expense_tracker,
            R.drawable.screenshot_activity_log
    };

    private int personal_tools_screenshots_array[] = {
            R.drawable.screenshot_create_group,
            R.drawable.screenshot_group_listing,
            R.drawable.screenshot_group_details,
            R.drawable.screenshot_personal_expense_tracker,
            R.drawable.screenshot_deposit_listing,
            R.drawable.screenshot_expense_listing,
            R.drawable.screenshot_activity_log
    };
    private int color_array[] = {
            R.color.blue_grey_600,
            R.color.blue_grey_600,
            R.color.blue_grey_600,
            R.color.blue_grey_600
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityScreenShotViewerBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initComponent();

        Tools.setSystemBarColor(this, R.color.blue_grey_600);
    }

    private void initComponent() {
        if (getIntent() != null && getIntent().getExtras() != null) {
            screenType = getIntent().getExtras().getInt("toolId", Defaults.TOOL_ID_ENTERPRICE);
            if (screenType == Defaults.TOOL_ID_ENTERPRICE)
                MAX_STEP = 4;
            else if (screenType == Defaults.TOOL_ID_GROUPS)
                MAX_STEP = 5;
            else if (screenType == Defaults.TOOL_ID_PERSONAL)
                MAX_STEP = 7;
        }
        // adding bottom dots
        bottomProgressDots(0);

        myViewPagerAdapter = new MyViewPagerAdapter();
        binding.viewPager.setAdapter(myViewPagerAdapter);
        binding.viewPager.setClipToPadding(false);
        binding.viewPager.setPageMargin((int) Measure.dpToPx(25, ScreenShotViewerScreen.this));
        binding.viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
    }

    private void bottomProgressDots(int current_index) {
        ImageView[] dots = new ImageView[MAX_STEP];

        binding.layoutDots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new ImageView(this);
            int width_height = 15;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(width_height, width_height));
            params.setMargins(10, 10, 10, 10);
            dots[i].setLayoutParams(params);
            dots[i].setImageResource(R.drawable.shape_circle);
            dots[i].setColorFilter(getResources().getColor(R.color.overlay_dark_30), PorterDuff.Mode.SRC_IN);
            binding.layoutDots.addView(dots[i]);
        }

        if (dots.length > 0) {
            dots[current_index].setImageResource(R.drawable.shape_circle);
            dots[current_index].setColorFilter(getResources().getColor(R.color.grey_10), PorterDuff.Mode.SRC_IN);
        }
    }

    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(final int position) {
            bottomProgressDots(position);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private ItemScreenshotViewerScreenBinding binding;

        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

//            View view = layoutInflater.inflate(R.layout.item_screenshot_viewer_screen, container, false);
            binding = ItemScreenshotViewerScreenBinding.inflate(layoutInflater, container, false);
            View view = binding.getRoot();

//            binding.title.setText(title_array[position]);
//            binding.description.setText(description_array[position]);

            if (screenType == Defaults.TOOL_ID_ENTERPRICE)
                binding.image.setImageResource(enterprise_tools_screenshots_array[position]);
            else if (screenType == Defaults.TOOL_ID_GROUPS)
                binding.image.setImageResource(group_tools_screenshots_array[position]);
            else if (screenType == Defaults.TOOL_ID_PERSONAL)
                binding.image.setImageResource(personal_tools_screenshots_array[position]);

            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            if (screenType == Defaults.TOOL_ID_ENTERPRICE)
                return enterprise_tools_screenshots_array.length;
            else if (screenType == Defaults.TOOL_ID_GROUPS)
                return group_tools_screenshots_array.length;
            else if (screenType == Defaults.TOOL_ID_PERSONAL)
                return personal_tools_screenshots_array.length;
            else
                return enterprise_tools_screenshots_array.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}