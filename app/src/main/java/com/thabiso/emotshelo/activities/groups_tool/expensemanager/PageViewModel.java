package com.thabiso.emotshelo.activities.groups_tool.expensemanager;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.thabiso.emotshelo.models.expenses.Expense;

public class PageViewModel extends ViewModel {

    private MutableLiveData<Integer> mIndex = new MutableLiveData<>();
    private LiveData<String> mText = Transformations.map(mIndex, new Function<Integer, String>() {
        @Override
        public String apply(Integer input) {
            return "Hello world from section: " + input;
        }
    });

    public void setIndex(int index) {
        mIndex.setValue(index);
    }

    public LiveData<String> getText() {
        return mText;
    }

    public MutableLiveData<Integer> mScreenType = new MutableLiveData<>();
    private LiveData<Integer> mScreenTypeLD = Transformations.map(mScreenType, new Function<Integer, Integer>() {
        @Override
        public Integer apply(Integer input) {
            return input;
        }
    });

    public void setScreentType(int screentType) {
        mScreenType.setValue(screentType);
    }

    public LiveData<Integer> getScreentType() {
        return mScreenTypeLD;
    }

    private MutableLiveData<Expense> verseMutableLiveData = new MutableLiveData<>();
    private LiveData<Expense> mTextVerse = Transformations.map(verseMutableLiveData, new Function<Expense, Expense>() {
        @Override
        public Expense apply(Expense input) {
            return input;
        }
    });

    public void setVerseObject(Expense verseObject) {
        verseMutableLiveData.setValue(verseObject);
    }

    public LiveData<Expense> getVerseObject() {
        return mTextVerse;
    }
}