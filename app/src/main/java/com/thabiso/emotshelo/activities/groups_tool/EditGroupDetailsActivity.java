package com.thabiso.emotshelo.activities.groups_tool;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.snackbar.Snackbar;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.adapters.ContactItemCreateGroupRecyclerViewAdapter;
import com.thabiso.emotshelo.databinding.ActivityEditGroupDetailsBinding;
import com.thabiso.emotshelo.interfaces.DateTimeUpdateListener;
import com.thabiso.emotshelo.models.creategroup.GroupMembersItem;
import com.thabiso.emotshelo.models.creategroup.RequestCreateGroup;
import com.thabiso.emotshelo.models.creategroup.UserGroups;
import com.thabiso.emotshelo.models.getmyconnections.ResponseGetMyConnections;
import com.thabiso.emotshelo.models.getmyconnections.UserContactsItem;
import com.thabiso.emotshelo.models.groupdetails.ResponseGroupDetails;
import com.thabiso.emotshelo.network.ApiCallerUtility;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;
import com.thabiso.emotshelo.util.preferences.Prefs;
import com.thabiso.emotshelo.util.storage.LegacyCompatFileProvider;
import com.thabiso.emotshelo.widget.DateTimePicker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EditGroupDetailsActivity extends AppCompatActivity implements DateTimeUpdateListener, View.OnClickListener, View.OnTouchListener {

    private ActivityEditGroupDetailsBinding binding;

    private int mColumnCount = 3;
    private List<UserContactsItem> myContactsList;
    private int shortAnimationDuration;
    private DateTimeUpdateListener updateListener;
    private MyReceiver objMyReceiver;
    private int subToolId;
    private UserGroups objUserGroups;
    private RequestOptions requestOptions;
    public ContactItemCreateGroupRecyclerViewAdapter contactItemCreateGroupRecyclerViewAdapter;

    private ActivityResultLauncher mRequestMultiplePermissions = registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), new ActivityResultCallback<Map<String, Boolean>>() {
        @Override
        public void onActivityResult(Map<String, Boolean> result) {
            for (Map.Entry<String, Boolean> entry : result.entrySet()) {
                switch (entry.getKey()) {
                    case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                        if (entry.getValue()) {
                            mGetContent.launch("image/*");
                        } else {
                            Toast.makeText(EditGroupDetailsActivity.this, getString(R.string.storage_permission), Toast.LENGTH_LONG).show();
                        }
                        break;
                    case Manifest.permission.READ_EXTERNAL_STORAGE:
//                        if (entry.getValue()) {
//                            mGetContent.launch("image/*");
//                        } else {
////                            Toast.makeText(EditProfileActivity.this, getString(R.string.storage_permission), Toast.LENGTH_LONG).show();
//                        }
                        break;
                }
            }
        }
    });

    private ActivityResultLauncher<String> mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(), new ActivityResultCallback<Uri>() {
        @Override
        public void onActivityResult(Uri resultUri) {
            // Handle the returned Uri
            try {
                if (resultUri != null) {
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri);

                        Glide.with(EditGroupDetailsActivity.this).setDefaultRequestOptions(requestOptions).load(bitmap)
                                .thumbnail(0.25f).into(binding.contentCreateGroup.imgProfile);
                        Glide.with(EditGroupDetailsActivity.this).setDefaultRequestOptions(requestOptions).load(bitmap)
                                .thumbnail(0.25f).into(binding.imgFullView);

                        crossfade(binding.contentCreateGroup.imgProfile, binding.fullScreenContainer);
                        binding.contentCreateGroup.uploadProgress.setVisibility(View.VISIBLE);
                        binding.imgEditProfile.setClickable(false);

                        ApiCallerUtility.callUploadGroupProfilePicApi(EditGroupDetailsActivity.this, resultUri, objUserGroups);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityEditGroupDetailsBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());

        objMyReceiver = new MyReceiver();

        setSupportActionBar(binding.appbarLayout.toolbar);
        Utility.setStatusBarColor(EditGroupDetailsActivity.this);
        updateListener = this;
        registerReciever();
        requestOptions = Utility.getCustomizedRequestOptions(R.color.mdtp_line_background, R.drawable.group_place_holder);

        binding.appbarLayout.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        binding.appbarLayout.toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        binding.appbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        binding.contentCreateGroup.editStartDate.setOnTouchListener(this);
        binding.contentCreateGroup.editEndDate.setOnTouchListener(this);
        binding.fullScreenContainer.setOnTouchListener(this);

        binding.fabNext.shrink();
        binding.fabNext.setOnClickListener(this);
        binding.imgEditProfile.setOnClickListener(this);
        binding.imgShare.setOnClickListener(this);
        binding.contentCreateGroup.imgProfile.setOnClickListener(this);
        binding.imgBack.setOnClickListener(this);
        binding.contentCreateGroup.touchComsumerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

        subToolId = Defaults.SUB_TOOL_ID_ROTATING_SAVINGS;
        int toolId;
        String toolName;
        if (getIntent() != null && getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getExtras();
            subToolId = bundle.getInt("subToolId", Defaults.SUB_TOOL_ID_ROTATING_SAVINGS);
            toolId = bundle.getInt("toolId", Defaults.TOOL_ID_GROUPS);
            toolName = bundle.getString("toolName", "");
            objUserGroups = (UserGroups) bundle.getSerializable("currentGroup");
            if (objUserGroups != null) {
                setValues();
            }
        }

//        if (subToolId == Defaults.SUB_TOOL_ID_ROTATING_SAVINGS) {
//            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.group_type_listing_rotating_savings, android.R.layout.simple_spinner_item);
//            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//            binding.contentCreateGroup.spnGroupType.setAdapter(adapter);
//        } else {
//            binding.contentCreateGroup.spnGroupType.setSelection(subToolId);
//            binding.contentCreateGroup.spnGroupType.setEnabled(false);
//        }

        binding.contentCreateGroup.spnGroupType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (subToolId == Defaults.SUB_TOOL_ID_ROTATING_SAVINGS) {
                    if (position == 1) {
                        binding.contentCreateGroup.txtInputLayoutStartDate.setVisibility(View.GONE);
                        binding.contentCreateGroup.txtInputLayoutEndDate.setVisibility(View.GONE);
                        binding.contentCreateGroup.spnRoundInterval.setVisibility(View.GONE);
                    } else {
                        binding.contentCreateGroup.txtInputLayoutStartDate.setVisibility(View.VISIBLE);
                        binding.contentCreateGroup.txtInputLayoutEndDate.setVisibility(View.VISIBLE);
                        binding.contentCreateGroup.spnRoundInterval.setVisibility(View.VISIBLE);
                    }
                } else if (subToolId == Defaults.SUB_TOOL_ID_PEER_TO_PEER) {
                    binding.contentCreateGroup.containerPeerToPeer.setVisibility(View.VISIBLE);
                } else {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.contentCreateGroup.editNumberOfMembers.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (s.toString().equals("")) {
                        binding.contentCreateGroup.editTotalAmountPerRound.setText("");
                    } else if (!binding.contentCreateGroup.editAmountPerRound.getText().toString().trim().equals("")) {
                        binding.contentCreateGroup.editTotalAmountPerRound.setText("" + Long.parseLong(binding.contentCreateGroup.editAmountPerRound.getText().toString().trim()) * (Long.parseLong(s.toString().trim()) - 1));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                binding.contentCreateGroup.editNumberOfRounds.setText(s.toString());
            }
        });

        binding.contentCreateGroup.editAmountPerRound.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (s.toString().equals("")) {
                        binding.contentCreateGroup.editTotalAmountPerRound.setText("");
                    } else if (!binding.contentCreateGroup.editNumberOfMembers.getText().toString().trim().equals("") && !s.toString().equals("")) {
                        binding.contentCreateGroup.editTotalAmountPerRound.setText("" + (Long.parseLong(binding.contentCreateGroup.editNumberOfMembers.getText().toString().trim()) - 1) * Long.parseLong(s.toString().trim()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        // Retrieve and cache the system's default "short" animation time.
        shortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (resultCode == RESULT_OK) {
//            switch (requestCode) {
//                case CAMERA_REQUEST:
//                    Bitmap photo = (Bitmap) data.getExtras().get("data");
//                    binding.contentCreateGroup.imgProfile.setImageBitmap(photo);
//                    binding.imgFullView.setImageBitmap(photo);
//                    break;
//                case PICK_REQUEST:
////                    try {
////                        Uri uri = data.getData();
////                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
////                        binding.contentCreateGroup.imgProfile.setImageBitmap(bitmap);
////                        binding.imgFullView.setImageBitmap(bitmap);
////                    } catch (IOException e) {
////                        e.printStackTrace();
////                    }
//                    break;
//            }
//        }
//        super.onActivityResult(requestCode, resultCode, data);
//    }

    @Override
    public void onBackPressed() {
        if (!toggleFullScreen()) {
            super.onBackPressed();
        }
    }

    private boolean toggleFullScreen() {
        if (binding.fullScreenContainer.getVisibility() == View.VISIBLE) {
//            binding.fullScreenContainer.setVisibility(View.GONE);
            crossfade(binding.contentCreateGroup.imgProfile, binding.fullScreenContainer);
            binding.appbarLayout.toolbar.setVisibility(View.VISIBLE);
            binding.fabNext.setVisibility(View.VISIBLE);
            Utility.setStatusBarColor(EditGroupDetailsActivity.this);
            return true;
        } else {
            //                binding.fullScreenContainer.setVisibility(View.VISIBLE);
            crossfade(binding.fullScreenContainer, binding.contentCreateGroup.imgProfile);
            binding.appbarLayout.toolbar.setVisibility(View.GONE);
            binding.fabNext.setVisibility(View.GONE);
            Utility.setTransperentStatusBarColor(EditGroupDetailsActivity.this);
            return false;
        }
    }

    private void crossfade(View view1, View view2) {

        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        view1.setAlpha(0f);
        view1.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        view1.animate()
                .alpha(1f)
                .setDuration(shortAnimationDuration)
                .setListener(null);

        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        view2.animate()
                .alpha(0f)
                .setDuration(shortAnimationDuration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view2.setVisibility(View.GONE);
                    }
                });
    }

    private boolean isFormValid() {
        if (binding.contentCreateGroup.editGroupName.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentCreateGroup.editGroupName, "Group name can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentCreateGroup.spnGroupType.getSelectedItemPosition() == 0) {
            Snackbar.make(binding.contentCreateGroup.spnGroupType, "Please select group type", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentCreateGroup.spnCurrency.getSelectedItemPosition() == 0) {
            Snackbar.make(binding.contentCreateGroup.spnCurrency, "Please select currency", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentCreateGroup.editNumberOfMembers.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentCreateGroup.editNumberOfMembers, "Number of members can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentCreateGroup.editNumberOfMembers.getText().toString().trim().length() > 12) {
            Snackbar.make(binding.contentCreateGroup.editNumberOfMembers, "Number of members can not exceed more than 12", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentCreateGroup.editNumberOfRounds.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentCreateGroup.editNumberOfRounds, "Number of rounds field can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentCreateGroup.editAmountPerRound.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentCreateGroup.editAmountPerRound, "Member contribution per round can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentCreateGroup.editTotalAmountPerRound.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentCreateGroup.editTotalAmountPerRound, "Total amount per round can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentCreateGroup.spnRoundInterval.getVisibility() == View.VISIBLE && binding.contentCreateGroup.spnRoundInterval.getSelectedItemPosition() == 0) {
            Snackbar.make(binding.contentCreateGroup.spnRoundInterval, "Please select mode", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentCreateGroup.containerPeerToPeer.getVisibility() == View.VISIBLE && binding.contentCreateGroup.spnInterestType.getSelectedItemPosition() == 0) {
            Snackbar.make(binding.contentCreateGroup.spnInterestType, "Please select Interest Type", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentCreateGroup.containerPeerToPeer.getVisibility() == View.VISIBLE && binding.contentCreateGroup.editInterestRate.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentCreateGroup.editInterestRate, "Please specify the Interest Rate", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentCreateGroup.containerPeerToPeer.getVisibility() == View.VISIBLE && binding.contentCreateGroup.editTotalWithdrawalLimit.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentCreateGroup.editTotalWithdrawalLimit, "Please specify Total Widrawal Limit", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentCreateGroup.containerPeerToPeer.getVisibility() == View.VISIBLE && binding.contentCreateGroup.editNumberOfWithdrawalsLimit.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentCreateGroup.editNumberOfWithdrawalsLimit, "Please specify Number of Withdrawals Limit", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentCreateGroup.containerPeerToPeer.getVisibility() == View.VISIBLE && binding.contentCreateGroup.editGapingBetweenWithdrawals.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentCreateGroup.editGapingBetweenWithdrawals, "Please specify the Minimum Gap Between Withdrawals", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentCreateGroup.editGroupDescription.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentCreateGroup.editGroupDescription, "Group Description can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentCreateGroup.txtInputLayoutStartDate.getVisibility() == View.VISIBLE && binding.contentCreateGroup.editStartDate.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentCreateGroup.editStartDate, "Start date can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentCreateGroup.txtInputLayoutEndDate.getVisibility() == View.VISIBLE && binding.contentCreateGroup.editEndDate.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentCreateGroup.editEndDate, "End Date can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (myContactsList == null || myContactsList.size() < Integer.parseInt(binding.contentCreateGroup.editNumberOfMembers.getText().toString())) {
            Snackbar.make(binding.contentCreateGroup.editGroupName, "Please select at-least " + Integer.parseInt(binding.contentCreateGroup.editNumberOfMembers.getText().toString()) + " contacts", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_create_group, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_done: {
                if (isFormValid()) {
                    RequestCreateGroup objRequestCreateGroup = new RequestCreateGroup();
                    objRequestCreateGroup.setEndDate(binding.contentCreateGroup.editEndDate.getText().toString());
                    objRequestCreateGroup.setGroupType(binding.contentCreateGroup.spnGroupType.getSelectedItemPosition() + "");
                    objRequestCreateGroup.setMode(binding.contentCreateGroup.spnRoundInterval.getSelectedItemPosition() + "");
                    objRequestCreateGroup.setUserId(Prefs.getUserId());
                    objRequestCreateGroup.setCurrency(binding.contentCreateGroup.spnCurrency.getSelectedItemPosition() + "");
                    objRequestCreateGroup.setAmountPerRound(binding.contentCreateGroup.editAmountPerRound.getText().toString());
                    objRequestCreateGroup.setNumberOfMembers(binding.contentCreateGroup.editNumberOfMembers.getText().toString());
                    objRequestCreateGroup.setDescription(binding.contentCreateGroup.editGroupDescription.getText().toString());

//                    objRequestCreateGroup.setDescription(binding.contentCreateGroup.editInterestRate.getText().toString());
//                    objRequestCreateGroup.setCurrency(binding.contentCreateGroup.spnInterestType.getSelectedItemPosition() + "");
//                    objRequestCreateGroup.setDescription(binding.contentCreateGroup.editTotalWithdrawalLimit.getText().toString());
//                    objRequestCreateGroup.setDescription(binding.contentCreateGroup.editNumberOfWithdrawalsLimit.getText().toString());
//                    objRequestCreateGroup.setDescription(binding.contentCreateGroup.editGapingBetweenWithdrawals.getText().toString());

                    objRequestCreateGroup.setNumberOfRounds(binding.contentCreateGroup.editNumberOfRounds.getText().toString());
                    objRequestCreateGroup.setName(binding.contentCreateGroup.editGroupName.getText().toString());
                    objRequestCreateGroup.setTotalAmount(binding.contentCreateGroup.editTotalAmountPerRound.getText().toString());// Both are same
                    objRequestCreateGroup.setTotalAmountPerRound(binding.contentCreateGroup.editTotalAmountPerRound.getText().toString());
                    objRequestCreateGroup.setStartDate(binding.contentCreateGroup.editStartDate.getText().toString());

                    List<GroupMembersItem> groupMembers = new ArrayList<>();
                    for (int i = 0; i < myContactsList.size(); i++) {
                        GroupMembersItem objGroupMembersItem = new GroupMembersItem();
//                        objGroupMembersItem.setUserId(memberList.get(i).details);
                        objGroupMembersItem.setUserId(myContactsList.get(i).getUserId());
                        groupMembers.add(objGroupMembersItem);
                    }
                    objRequestCreateGroup.setGroupMembers(groupMembers);

                    ApiCallerUtility.callCreateGroupApi(this, objRequestCreateGroup);
                }
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDateUpdate(String year, String monthOfYear, String dayOfMonth, TextView textView) {
//        String date = dayOfMonth + "-" + (monthOfYear) + "-" + year;
        String date = year + "-" + (monthOfYear) + "-" + dayOfMonth;
        textView.setText(date);
    }

    @Override
    public void onTimeUpdate(int hourOfDay, int minute, int second, String am_pm, TextView textView) {

    }

    @Override
    public void onTimeUpdate(int hourOfDay, int minute, int second, TextView textView) {

    }

    public void getDate(View view) {
        DateTimePicker.getInstance().getDate(EditGroupDetailsActivity.this, updateListener, null, (TextView) view);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_next:
                if (!binding.contentCreateGroup.editNumberOfMembers.getText().toString().trim().equals("")) {
                    Intent inte = new Intent(EditGroupDetailsActivity.this, ContactListActivity.class);

                    Bundle bundle = new Bundle();
                    bundle.putInt("totalMembers", Integer.parseInt(binding.contentCreateGroup.editNumberOfMembers.getText().toString()));
                    inte.putExtras(bundle);

                    startActivity(inte);
                } else {
                    Snackbar.make(binding.contentCreateGroup.editNumberOfMembers, "Please Enter Number of Members", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }

//                if (!binding.contentCreateGroup.editNumberOfMembers.getText().toString().trim().equals("")) {
//                    Intent inte = new Intent(CreateGroupActivity.this, AddContactScreen.class);
//                    inte.putExtra("totalMembers", Integer.parseInt(binding.contentCreateGroup.editNumberOfMembers.getText().toString()));
//                    startActivity(inte);
//                } else {
//                    Snackbar.make(binding.contentCreateGroup.editNumberOfMembers, "Please Enter Number of Members", Snackbar.LENGTH_LONG)
//                            .setAction("Action", null).show();
//                }
                break;
            case R.id.imgEditProfile:
                mRequestMultiplePermissions.launch(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE});
                break;
            case R.id.imgProfile:
                setImageFromServer();
                toggleFullScreen();
                break;
            case R.id.imgShare:
                try {
                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.setType("image/*");
                    BitmapDrawable drawable = (BitmapDrawable) binding.contentCreateGroup.imgProfile.getDrawable();
                    Bitmap bitmap = drawable.getBitmap();

//                    File filepath = Environment.getExternalStorageDirectory();
//                    File filepath = StorageHelper.getSdcardFile(EditProfileActivity.this);
//                    File filepath = getExternalFilesDir("external");
//                    File filepath = getFilesDir();

                    // Create a new folder AndroidBegin in SD Card
                    File internalDirectoryPath = new File(getFilesDir().getAbsolutePath() + "/" + Defaults.FOLDER_PATH_PROFILE);
                    if (!internalDirectoryPath.exists())
                        internalDirectoryPath.mkdirs();

                    // Create a name for the saved image
                    File imageFile = new File(internalDirectoryPath, "group_profile_pic.png");
                    if (!imageFile.exists())
                        imageFile.createNewFile();

                    try {
                        // Saves the file into SD Card
                        OutputStream output = new FileOutputStream(imageFile);
                        // Compress into png format image from 0% - 100%, using 100% for this tutorial
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
                        output.flush();
                        output.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (imageFile != null) {
                        Uri uri1 = LegacyCompatFileProvider.getUri(this, imageFile);
                        share.putExtra(Intent.EXTRA_STREAM, uri1);
                        share.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        startActivity(Intent.createChooser(share, getString(R.string.send_to)));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.imgBack:
                toggleFullScreen();
                break;
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        switch (view.getId()) {
            case R.id.editStartDate:
                if (event.getAction() == MotionEvent.ACTION_DOWN)
                    getDate(view);
                return true;
            case R.id.editEndDate:
                if (event.getAction() == MotionEvent.ACTION_DOWN)
                    getDate(view);
                return true;
            case R.id.fullScreenContainer:
                return true;
        }
        return false;
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_BROADCAST_SELECTED_CONTACT_LIST);
        filter.addAction(Defaults.ACTION_GROUP_ICON_UPLOAD_SUCCESS);
        filter.addAction(Defaults.ACTION_GROUP_ICON_UPLOAD_FAILED);
        Utility.registerReciever(EditGroupDetailsActivity.this, filter, objMyReceiver);
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Defaults.ACTION_BROADCAST_SELECTED_CONTACT_LIST)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        ResponseGetMyConnections objResponseGetMyConnections = (ResponseGetMyConnections) bundle.getSerializable("selectionList");

                        myContactsList = objResponseGetMyConnections.getData().getUserContacts();

                        boolean addDefaultMember = bundle.getBoolean("addDefaultMember", true);
                        if (addDefaultMember) {
                            UserContactsItem objUserContactsItem = new UserContactsItem();
                            objUserContactsItem.setUserName(Prefs.getUserName());
                            objUserContactsItem.setUserId(Prefs.getUserId());
                            myContactsList.add(0, objUserContactsItem);
                        }

                        binding.contentCreateGroup.rcvSelectedContacts.setLayoutManager(new GridLayoutManager(EditGroupDetailsActivity.this, mColumnCount));
                        contactItemCreateGroupRecyclerViewAdapter = new ContactItemCreateGroupRecyclerViewAdapter(EditGroupDetailsActivity.this, myContactsList);
                        binding.contentCreateGroup.rcvSelectedContacts.setAdapter(contactItemCreateGroupRecyclerViewAdapter);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (intent.getAction().equals(Defaults.ACTION_GROUP_ICON_UPLOAD_SUCCESS)) {
                try {
                    binding.contentCreateGroup.uploadProgress.setVisibility(View.GONE);
                    binding.imgEditProfile.setClickable(true);
                    Toast.makeText(context, "Group Icon Uploaded Successfully", Toast.LENGTH_LONG).show();

                    if (intent != null && intent.getExtras() != null && intent.getExtras().getBundle("bundle") != null) {
                        Bundle bundle = intent.getExtras().getBundle("bundle");
                        ResponseGroupDetails objResponseGroupDetails = (ResponseGroupDetails) bundle.getSerializable("ResponseGroupDetails");
                        if (objResponseGroupDetails != null && objResponseGroupDetails.getData() != null && objResponseGroupDetails.getData().getUserGroups() != null) {
                            objUserGroups.setGroupAvatar(objResponseGroupDetails.getData().getUserGroups().getGroupAvatar());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (intent.getAction().equals(Defaults.ACTION_GROUP_ICON_UPLOAD_FAILED)) {
                try {
                    binding.contentCreateGroup.uploadProgress.setVisibility(View.GONE);
                    binding.imgEditProfile.setClickable(true);
                    Toast.makeText(context, "Group Icon Upload Failed", Toast.LENGTH_LONG).show();
                    if (intent != null && intent.getExtras() != null && intent.getExtras().getBundle("bundle") != null) {
                        Bundle bundle = intent.getExtras().getBundle("bundle");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void setImageFromServer() {
        try {
            if (objUserGroups != null) {
                binding.imgFullView.setImageResource(R.drawable.group_place_holder);
                Glide.with(EditGroupDetailsActivity.this).setDefaultRequestOptions(requestOptions).load(objUserGroups.getGroupAvatar())/*.listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        bigImageProgressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        bigImageProgressBar.setVisibility(View.GONE);
                        return false;
                    }
                })*/.thumbnail(0.25f).into(binding.imgFullView);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setValues() {
        try {
            Glide.with(EditGroupDetailsActivity.this).setDefaultRequestOptions(requestOptions).load(objUserGroups.getGroupAvatar())
                    .thumbnail(0.25f).into(binding.contentCreateGroup.imgProfile);
            Glide.with(EditGroupDetailsActivity.this).setDefaultRequestOptions(requestOptions).load(objUserGroups.getGroupAvatar())
                    .thumbnail(0.25f).into(binding.imgFullView);

            binding.contentCreateGroup.editGroupName.setText(objUserGroups.getName());
            try {
                int position = Integer.parseInt(objUserGroups.getGroupType());
                if (position < binding.contentCreateGroup.spnGroupType.getCount())
                    binding.contentCreateGroup.spnGroupType.setSelection(position);
                else
                    binding.contentCreateGroup.spnGroupType.setSelection(0);
            } catch (Exception e) {
                e.printStackTrace();
                binding.contentCreateGroup.spnGroupType.setSelection(0);
            }
            try {
                int position = Integer.parseInt(objUserGroups.getCurrency());
                if (position < binding.contentCreateGroup.spnCurrency.getCount())
                    binding.contentCreateGroup.spnCurrency.setSelection(position);
                else
                    binding.contentCreateGroup.spnCurrency.setSelection(0);
            } catch (Exception e) {
                e.printStackTrace();
                binding.contentCreateGroup.spnCurrency.setSelection(0);
            }
            binding.contentCreateGroup.editNumberOfMembers.setText(objUserGroups.getNumberOfMembers());
            binding.contentCreateGroup.editNumberOfRounds.setText(objUserGroups.getNumberOfRounds());
            binding.contentCreateGroup.editAmountPerRound.setText(objUserGroups.getAmountPerRound());
            binding.contentCreateGroup.editTotalAmountPerRound.setText(objUserGroups.getTotalAmountPerRound());
            binding.contentCreateGroup.editGroupDescription.setText(objUserGroups.getDescription());

            if (binding.contentCreateGroup.spnRoundInterval.getVisibility() == View.VISIBLE) {
                try {
                    int position = Integer.parseInt(objUserGroups.getMode());
                    if (position < binding.contentCreateGroup.spnRoundInterval.getCount())
                        binding.contentCreateGroup.spnRoundInterval.setSelection(position);
                    else
                        binding.contentCreateGroup.spnRoundInterval.setSelection(0);
                } catch (Exception e) {
                    e.printStackTrace();
                    binding.contentCreateGroup.spnRoundInterval.setSelection(0);
                }
            }

            if (binding.contentCreateGroup.containerPeerToPeer.getVisibility() == View.VISIBLE) {
                binding.contentCreateGroup.editInterestRate.setText(objUserGroups.getInterestRate());
                try {
                    int position = Integer.parseInt(objUserGroups.getInterestType());
                    if (position < binding.contentCreateGroup.spnInterestType.getCount())
                        binding.contentCreateGroup.spnInterestType.setSelection(position);
                    else
                        binding.contentCreateGroup.spnInterestType.setSelection(0);
                } catch (Exception e) {
                    e.printStackTrace();
                    binding.contentCreateGroup.spnInterestType.setSelection(0);
                }
                binding.contentCreateGroup.editTotalWithdrawalLimit.setText(objUserGroups.getTotalWithdrawalLimit());
                binding.contentCreateGroup.editNumberOfWithdrawalsLimit.setText(objUserGroups.getNumberOfWithdrawalLimit());
                binding.contentCreateGroup.editGapingBetweenWithdrawals.setText(objUserGroups.getGapingBetweenWithdrawal());
            }
            if (binding.contentCreateGroup.txtInputLayoutStartDate.getVisibility() == View.VISIBLE) {
                binding.contentCreateGroup.editStartDate.setText(objUserGroups.getStartDate());
            }
            if (binding.contentCreateGroup.txtInputLayoutEndDate.getVisibility() == View.VISIBLE) {
                binding.contentCreateGroup.editEndDate.setText(objUserGroups.getEndDate());
            }
        } catch (Exception ex) {
        }
    }
}
