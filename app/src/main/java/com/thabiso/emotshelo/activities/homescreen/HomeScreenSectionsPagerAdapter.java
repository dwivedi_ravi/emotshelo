package com.thabiso.emotshelo.activities.homescreen;

import android.content.Context;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.thabiso.emotshelo.models.HomeScreenObject;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class HomeScreenSectionsPagerAdapter extends FragmentPagerAdapter {

    private HomeScreenObject objHomeScreenObject;
    private final Context mContext;
    private OnHomeScreenListManagerInteractionListener mListener;

    private HomeScreenPlaceholderFragment enterpriseFragment;
    private HomeScreenPlaceholderFragment groupsFragment;
    private HomeScreenPlaceholderFragment personalFragment;
    private HomeScreenPlaceholderFragment homeFragment;

    public HomeScreenSectionsPagerAdapter(Context context, FragmentManager fm, HomeScreenObject objHomeScreenObject, OnHomeScreenListManagerInteractionListener listener) {
        super(fm);
        mContext = context;
        this.objHomeScreenObject = objHomeScreenObject;
        this.mListener = listener;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        if (getPageTitle(position).equals(Defaults.TAB_TITLE_ENTERPRISE)) {
            Utility.sendUpdateListBroadCast(Defaults.ACTION_FETCH_COMPANY_LIST_FROM_SERVER, mContext, null);
            return enterpriseFragment = HomeScreenPlaceholderFragment.newInstance(position + 1, objHomeScreenObject, mListener, Defaults.SCREEN_TYPE_ENTERPRISE);
        } else if (getPageTitle(position).equals(Defaults.TAB_TITLE_GROUPS)) {
            return groupsFragment = HomeScreenPlaceholderFragment.newInstance(position + 1, objHomeScreenObject, mListener, Defaults.SCREEN_TYPE_GROUPS);
        } else if (getPageTitle(position).equals(Defaults.TAB_TITLE_PERSONAL)) {
            return personalFragment = HomeScreenPlaceholderFragment.newInstance(position + 1, objHomeScreenObject, mListener, Defaults.SCREEN_TYPE_PERSONAL);
        } else if (getPageTitle(position).equals(Defaults.TAB_TITLE_HOME)) {
            return homeFragment = HomeScreenPlaceholderFragment.newInstance(position + 1, objHomeScreenObject, mListener, Defaults.SCREEN_TYPE_HOME);
        } else {
            return enterpriseFragment = HomeScreenPlaceholderFragment.newInstance(position + 1, objHomeScreenObject, mListener, Defaults.SCREEN_TYPE_GROUPS);
        }
    }

    // Here we can finally safely save a reference to the created
    // Fragment, no matter where it came from (either getItem() or
    // FragmentManger). Simply save the returned Fragment from
    // super.instantiateItem() into an appropriate reference depending
    // on the ViewPager position.
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
        // save the appropriate reference depending on position
        switch (position) {
            case 3:
                enterpriseFragment = (HomeScreenPlaceholderFragment) createdFragment;
                break;
            case 2:
                groupsFragment = (HomeScreenPlaceholderFragment) createdFragment;
                break;
            case 1:
                personalFragment = (HomeScreenPlaceholderFragment) createdFragment;
                break;
            case 0:
                homeFragment = (HomeScreenPlaceholderFragment) createdFragment;
                break;
        }
        return createdFragment;
    }

    public HomeScreenPlaceholderFragment getFragment(int screenType) {
        // do work on the referenced Fragments, but first check if they
        // even exist yet, otherwise you'll get an NPE.
        switch (screenType) {
            case Defaults.SCREEN_TYPE_ENTERPRISE:
                return enterpriseFragment;
            case Defaults.SCREEN_TYPE_GROUPS:
                return groupsFragment;
            case Defaults.SCREEN_TYPE_PERSONAL:
                return personalFragment;
            case Defaults.SCREEN_TYPE_HOME:
                return homeFragment;
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return objHomeScreenObject.getSectionList().get(position);
    }

    @Override
    public int getCount() {
        return objHomeScreenObject.getSectionList().size();
    }
}