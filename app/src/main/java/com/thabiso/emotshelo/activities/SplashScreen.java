package com.thabiso.emotshelo.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.activities.homescreen.HomeScreenActivity;
import com.thabiso.emotshelo.activities.signup.EditProfileActivity;
import com.thabiso.emotshelo.activities.signup.SignUpActivity;
import com.thabiso.emotshelo.network.ApiCallerUtility;
import com.thabiso.emotshelo.util.preferences.Prefs;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

//        AppSignatureHelper obj = new AppSignatureHelper(this);
//        ArrayList<String> resign = obj.getAppSignatures();
//        for (int i = 0; i < resign.size(); i++) {
//            Log.e("RAVI", resign.get(i));
//        }

        if (!Prefs.getAuthKey().trim().equals("")) {
            takeUserIntoTheApp();
        } else {
            Prefs.clearAllPreferenceValues();
            ApiCallerUtility.callRegisterDeviceApi(this);
        }
    }

    public void takeUserIntoTheApp() {
        Intent inte;
        if (Prefs.isPhoneNumberVerified()) {
            if (Prefs.isProfileCompleted()) {
                inte = new Intent(SplashScreen.this, HomeScreenActivity.class);
            } else {
                inte = new Intent(SplashScreen.this, EditProfileActivity.class);
                inte.putExtra("navigate", true);
            }
        } else {
            inte = new Intent(SplashScreen.this, SignUpActivity.class);
        }
        startActivity(inte);
        SplashScreen.this.finish();
    }
}
