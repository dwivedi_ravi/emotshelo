package com.thabiso.emotshelo.activities.signup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.material.snackbar.Snackbar;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.activities.base.SharedActivity;
import com.thabiso.emotshelo.databinding.ActivitySignUpBinding;
import com.thabiso.emotshelo.models.RequestOTP;
import com.thabiso.emotshelo.network.ApiCallerUtility;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;
import com.thabiso.emotshelo.util.preferences.Prefs;

/**
 * The Main Activity used to display Albums / Media.
 */
public class SignUpActivity extends SharedActivity {

    private ActivitySignUpBinding binding;

    private MyReceiver objMyReceiver;
    private boolean isDialogShown = false;
    private String mobileNumber;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivitySignUpBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());

        objMyReceiver = new MyReceiver();
        registerReciever();

        initUi();

        Utility.checkAndShowIntroScreen(this);
    }

    private void initUi() {
        Utility.setStatusBarColor(SignUpActivity.this);

        binding.btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFormValid()) {
                    mobileNumber = binding.editPhoneNumber.getText().toString().trim();

                    if (mobileNumber.startsWith(binding.editCountryCode.getSelectedCountryCodeWithPlus()))
                        mobileNumber = mobileNumber.substring(binding.editCountryCode.getSelectedCountryCodeWithPlus().length(), mobileNumber.length());

                    AlertDialog textDialog = Utility.getTextDialog_Material(SignUpActivity.this, "", getResources().getString(R.string.verify_phone_dialog_detail_1) + binding.editCountryCode.getSelectedCountryCodeWithPlus() + " " + mobileNumber + getResources().getString(R.string.verify_phone_dialog_detail_2));
                    textDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok_action).toUpperCase(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            Utility.startSmsRetriever(SignUpActivity.this);

                            RequestOTP objRequestOTP = new RequestOTP();
                            objRequestOTP.setMobile_number(mobileNumber);
                            objRequestOTP.setCountry_code(binding.editCountryCode.getSelectedCountryCodeWithPlus());
                            objRequestOTP.setCountry_name(binding.editCountryCode.getSelectedCountryName());

                            ApiCallerUtility.callRequestOTPApi(SignUpActivity.this, objRequestOTP, true);
                        }
                    });
                    textDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.edit_action).toUpperCase(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            textDialog.dismiss();
                        }
                    });
                    textDialog.show();

                    textDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.app_text_color));
                    textDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.app_text_color));
                    textDialog.getButton(DialogInterface.BUTTON_POSITIVE).setBackground(getResources().getDrawable(R.drawable.ripple));
                    textDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setBackground(getResources().getDrawable(R.drawable.ripple));
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!Prefs.getIsUserFirstTimeFromSharedPreferences() && !isDialogShown) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Utility.requestHint(SignUpActivity.this);
                    isDialogShown = true;
                }
            }, 500);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_REFRESH_MEDIA_AFTER_PHOTO_EDITING);
        Utility.registerReciever(SignUpActivity.this, filter, objMyReceiver);
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Defaults.ACTION_REFRESH_MEDIA_AFTER_PHOTO_EDITING)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private boolean isFormValid() {
        if (binding.editCountryCode.getSelectedCountryCode().isEmpty()) {
            Snackbar.make(binding.editCountryCode, "Please select country", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.editPhoneNumber.getText().toString().trim().length() < 8) {
            Snackbar.make(binding.editPhoneNumber, "Please enter valid phone number", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.editPhoneNumber.getText().toString().trim().startsWith("+") && !binding.editPhoneNumber.getText().toString().trim().startsWith(binding.editCountryCode.getSelectedCountryCodeWithPlus())) {
            Snackbar.make(binding.editPhoneNumber, "Country codes do not match", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }

        return true;
    }

    // Obtain the phone number from the result
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Defaults.RESOLVE_HINT) {
            if (resultCode == RESULT_OK) {
                Credential credential = data.getParcelableExtra(Credential.EXTRA_KEY);
                // credential.getId();  <-- will need to process phone number string
                binding.editPhoneNumber.setText(credential.getId());
            }
        }
    }


//    private void createNotificationChannelForOreoAndAboveDevice() {
//        try {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                // Create channel to show notifications.
//                String channelId = getString(R.string.default_notification_channel_id);
//                String channelName = getString(R.string.default_notification_channel_name);
//                NotificationManager notificationManager =
//                        getSystemService(NotificationManager.class);
//                notificationManager.createNotificationChannel(new NotificationChannel(channelId,
//                        channelName, NotificationManager.IMPORTANCE_LOW));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
