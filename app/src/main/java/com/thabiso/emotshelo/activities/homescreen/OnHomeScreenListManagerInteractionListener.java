package com.thabiso.emotshelo.activities.homescreen;

import com.thabiso.emotshelo.models.HomeScreenObject;

public interface OnHomeScreenListManagerInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(HomeScreenObject item);
    }