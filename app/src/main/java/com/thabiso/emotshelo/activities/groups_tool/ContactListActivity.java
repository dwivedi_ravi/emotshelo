package com.thabiso.emotshelo.activities.groups_tool;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.selection.Selection;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.snackbar.Snackbar;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.databinding.ActivityContactListBinding;
import com.thabiso.emotshelo.fragments.FragmentContactItem;
import com.thabiso.emotshelo.models.RequestContactSync;
import com.thabiso.emotshelo.models.creategroup.RequestCreateGroup;
import com.thabiso.emotshelo.models.getmyconnections.Data;
import com.thabiso.emotshelo.models.getmyconnections.ResponseGetMyConnections;
import com.thabiso.emotshelo.models.getmyconnections.UserContactsItem;
import com.thabiso.emotshelo.network.ApiCallerUtility;
import com.thabiso.emotshelo.util.DateTimeUtils;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;
import com.thabiso.emotshelo.util.preferences.Prefs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ContactListActivity extends AppCompatActivity implements FragmentContactItem.OnListFragmentInteractionListener {

    private ActivityContactListBinding binding;

    @FragmentMode
    private int fragmentMode;
    private FragmentContactItem fragmentContactItem;
    private boolean searchPerformed = false;
    private MyReceiver objMyReceiver;
    private BottomSheetBehavior bottomSheetBehavior;
    private int totalMembers = 0;
    private SearchView searchView;

    private ActivityResultLauncher<String> mRequestPermission = registerForActivityResult(new ActivityResultContracts.RequestPermission(), new ActivityResultCallback<Boolean>() {
        @Override
        public void onActivityResult(Boolean result) {
            if (result) {
                new RetrieveContactsFromPhone().execute();
            } else {
                Toast.makeText(ContactListActivity.this, getString(R.string.contact_permission_denied), Toast.LENGTH_LONG).show();
            }
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding =  ActivityContactListBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());

        setSupportActionBar(binding.appbarLayout.toolbar);
        Utility.setStatusBarColor(this);
        objMyReceiver = new MyReceiver();
        registerReciever();

        binding.appbarLayout.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        binding.appbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getBundle("bundle") != null) {
            final Bundle bundle = getIntent().getExtras().getBundle("bundle");
            totalMembers = bundle.getInt("totalMembers", 0);

            RequestCreateGroup objRequestCreateGroup = (RequestCreateGroup) bundle.getSerializable("objRequestCreateGroup");
            if (objRequestCreateGroup != null) {
//                SHOW CREATE GROUP BUTTON
            }
        }

        binding.fab.shrink();
        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    if (isFormValid()) {
                        UserContactsItem objUserContactsItem = new UserContactsItem();
                        objUserContactsItem.setUserId(DateTimeUtils.getDate(DateTimeUtils.WHOLE_DATE_WITHOUT_SEPARATOR));
                        objUserContactsItem.setUserName(binding.contentBottomSheet.editMemberName.getText().toString());
                        objUserContactsItem.setMobile(binding.contentBottomSheet.editMobileNumber.getText().toString());
                        objUserContactsItem.setAvatar("");

//                        contactItemFragment.myConnectionList.add(0, objUserContactsItem);
                        fragmentContactItem.myConnectionList.add(objUserContactsItem);
                        fragmentContactItem.refreshList(fragmentContactItem.myConnectionList);
//                        contactItemFragment.setOrUpdateRecyclerViewAdapter();
                        binding.contentBottomSheet.editMemberName.setText("");
                        binding.contentBottomSheet.editMobileNumber.setText("");
                        Toast.makeText(ContactListActivity.this, "Member Added Successfully", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    binding.fab.extend();
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    binding.viewOverlay.setVisibility(View.VISIBLE);
                }
            }
        });

        fragmentMode = FragmentMode.MODE_CONTACT_LIST;

        initContactsFragment();
        setContentFragment();
        initializeBottomSheetLayout();

        mRequestPermission.launch(Manifest.permission.READ_CONTACTS);
    }

    private void initContactsFragment() {
        unReferenceFragments();
        fragmentContactItem = new FragmentContactItem(this, binding.appbarLayout.toolbar);
    }

    private void setContentFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.txtAddress, fragmentContactItem, FragmentContactItem.TAG)
                .addToBackStack(null)
                .commit();
    }

    private void unReferenceFragments() {
        fragmentContactItem = null;
    }

    @Override
    public void onListFragmentInteraction(UserContactsItem item) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_contact_list, menu);

        MenuItem searchItem = menu.findItem(R.id.search_action).setIcon(R.drawable.ic_baseline_search_24);
        searchView = (SearchView) searchItem.getActionView();
        if (searchView != null) {

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    searchPerformed = true;
                    fragmentContactItem.contactItemRecyclerViewAdapter.getFilter().filter(query);
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    if (searchPerformed && newText.trim().equals("")) {
                        searchPerformed = false;
//                        displayAlbums(); RESET ADAPTER HERE
                    }
                    return false;
                }
            });
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.search_action: {
                return true;
            }
            case R.id.action_sync_contacts: {
                new RetrieveContactsFromPhone().execute();
                return true;
            }
            case R.id.action_done: {
                if (fragmentContactItem.contactItemRecyclerViewAdapter.getAllSelectionTrackerItems().size() < totalMembers - 1) {
                    Snackbar.make(binding.contentBottomSheet.editMobileNumber, "Please select at least " + (totalMembers - 1) + " members", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                } else if (fragmentContactItem.contactItemRecyclerViewAdapter.getAllSelectionTrackerItems().size() > totalMembers - 1) {
                    Snackbar.make(binding.contentBottomSheet.editMobileNumber, "Member limit is: " + totalMembers + " including you.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                } else {
                    addMembersToGroup();
                }

                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_UPDATE_CONNECTIONS_LIST);
        filter.addAction(Defaults.ACTION_CALL_GET_MY_CONNECTIONS);
        Utility.registerReciever(ContactListActivity.this, filter, objMyReceiver);
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }

    public @interface FragmentMode {
        int MODE_CONTACT_LIST = 1001;
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Defaults.ACTION_UPDATE_CONNECTIONS_LIST)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        ResponseGetMyConnections objResponseGetMyConnections = (ResponseGetMyConnections) bundle.getSerializable("ResponseGetMyConnections");
                        fragmentContactItem.updateList(objResponseGetMyConnections.getData().getUserContacts());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (intent.getAction().equals(Defaults.ACTION_CALL_GET_MY_CONNECTIONS)) {
                fragmentContactItem.getMyConnectionsList();
            }
        }
    }

    private class RetrieveContactsFromPhone extends AsyncTask<Void, Void, Void> {
        RequestContactSync objRequestContactSync = new RequestContactSync();
        AlertDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = Utility.getProgressDialog(ContactListActivity.this, "", "Please wait...");
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            objRequestContactSync.setUserId(Prefs.getUserId());
            objRequestContactSync.setContacts(fragmentContactItem.getMyContactList());

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
            ApiCallerUtility.callSyncMyContactsApi(ContactListActivity.this, objRequestContactSync);
        }
    }

    private void addMembersToGroup() {
        try {
            if (fragmentContactItem.contactItemRecyclerViewAdapter != null) {
                Selection<Long> selectionList = fragmentContactItem.contactItemRecyclerViewAdapter.getAllSelectionTrackerItems();
                ResponseGetMyConnections objResponseGetMyConnections = new ResponseGetMyConnections();
                List<UserContactsItem> userContactsItemList = new ArrayList<>();
                Iterator<Long> itemIterable = selectionList.iterator();

                while (itemIterable.hasNext()) {
                    UserContactsItem objUserContactsItem = fragmentContactItem.myConnectionList.get(itemIterable.next().intValue());
                    userContactsItemList.add(objUserContactsItem);
                }
                objResponseGetMyConnections.setData(new Data());
                objResponseGetMyConnections.getData().setUserContacts(userContactsItemList);

                Bundle mBundle = new Bundle();
                mBundle.putSerializable("selectionList", objResponseGetMyConnections);
                Utility.sendUpdateListBroadCast(Defaults.ACTION_BROADCAST_SELECTED_CONTACT_LIST, ContactListActivity.this, mBundle);

                ContactListActivity.this.finish();
            } else {
                Toast.makeText(ContactListActivity.this, "No contacts selected", Toast.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
        }
    }

    private void initializeBottomSheetLayout() {
        bottomSheetBehavior = BottomSheetBehavior.from(binding.contentBottomSheet.bottomSheet);
        TextView bottomSheetTitle = findViewById(R.id.bottomSheetTitle);
        ImageView closeBottomSheet = findViewById(R.id.closeBottomSheet);

        closeBottomSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                binding.fab.shrink();
                binding.viewOverlay.setVisibility(View.GONE);
            }
        });

        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    Utility.hideKeyboard(ContactListActivity.this, closeBottomSheet.getWindowToken());
                    binding.fab.shrink();
                    binding.viewOverlay.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
    }

    private boolean isFormValid() {
        if (binding.contentBottomSheet.editMemberName.getVisibility() == View.VISIBLE && binding.contentBottomSheet.editMemberName.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentBottomSheet.editMemberName, "Member name can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentBottomSheet.editMobileNumber.getVisibility() == View.VISIBLE && binding.contentBottomSheet.editMobileNumber.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentBottomSheet.editMobileNumber, "Mobile Number can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            binding.viewOverlay.setVisibility(View.GONE);
        } else
            finish();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                Rect outRect = new Rect();
                binding.contentBottomSheet.bottomSheet.getGlobalVisibleRect(outRect);

                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    binding.viewOverlay.setVisibility(View.GONE);
                    return true;
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
}
