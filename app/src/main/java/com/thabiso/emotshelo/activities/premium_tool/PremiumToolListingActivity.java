package com.thabiso.emotshelo.activities.premium_tool;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.adapters.MarketPlaceAdapter;
import com.thabiso.emotshelo.database.DBHandler;
import com.thabiso.emotshelo.databinding.ActivityPremiumToolListingBinding;
import com.thabiso.emotshelo.models.MarketToolListBO;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;

import java.util.ArrayList;

public class PremiumToolListingActivity extends AppCompatActivity {

    private ActivityPremiumToolListingBinding binding;

    private ArrayList<MarketToolListBO> toolListBOArrayList;
    private MarketPlaceAdapter mAdapter;
    private DBHandler dbHandler;
    private static final String ANDROID_TRANSITION = "switchAndroid";
    private static final String BLUE_TRANSITION = "switchBlue";
    private MyReceiver objMyReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityPremiumToolListingBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initToolbar();
        init();
    }

    private void initToolbar() {
        binding.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Utility.setTransperentStatusBarColor(this);
    }

    private void init() {
        dbHandler = DBHandler.getInstance(this);
        objMyReceiver = new MyReceiver();
        registerReciever();

        Bundle bundle = getIntent().getExtras();
        int clickedTool = Defaults.TOOL_ID_PREMIUM_ENTERPRICE;
        if (bundle != null) {
            clickedTool = bundle.getInt("clickedTool", Defaults.TOOL_ID_PREMIUM_ENTERPRICE);
        }
        getMarketPlaceData(clickedTool);
        mAdapter = new MarketPlaceAdapter(this, toolListBOArrayList);

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(mAdapter);
    }

    private void getMarketPlaceData(int clickedTool) {
        toolListBOArrayList = new ArrayList<>();

        switch (clickedTool) {
            case Defaults.TOOL_ID_PREMIUM_ENTERPRICE: {
                binding.txtTitle.setText(getResources().getString(R.string.title_enterprise_premium_tool));
                binding.topbarIcon.setBackgroundResource(R.drawable.market_place_enterprise);

                MarketToolListBO objMarketToolListBO = new MarketToolListBO();
                objMarketToolListBO.setToolID(Defaults.TOOL_ID_PREMIUM_ENTERPRICE);
                objMarketToolListBO.setSubToolId(Defaults.SUB_TOOL_ID_PREMIUM_INVOICING_AND_QUOTATION);
                objMarketToolListBO.setToolTitle(getResources().getString(R.string.card_title_premium_invoicing));
                objMarketToolListBO.setToolShortDescription(getResources().getString(R.string.card_description_premium_invoicing));
                objMarketToolListBO.setToolLongDescription(getResources().getString(R.string.card_long_description_premium_invoicing));
                objMarketToolListBO.setTotalSales(150);
                toolListBOArrayList.add(objMarketToolListBO);

                objMarketToolListBO = new MarketToolListBO();
                objMarketToolListBO.setToolID(Defaults.TOOL_ID_PREMIUM_ENTERPRICE);
                objMarketToolListBO.setSubToolId(Defaults.SUB_TOOL_ID_PREMIUM_INVENTORY_MANAGEMENT);
                objMarketToolListBO.setToolTitle(getResources().getString(R.string.card_title_premium_inventory_managerment));
                objMarketToolListBO.setToolShortDescription(getResources().getString(R.string.card_description_premium_inventory_managerment));
                objMarketToolListBO.setToolLongDescription(getResources().getString(R.string.card_long_description_premium_inventory_managerment));
                objMarketToolListBO.setTotalSales(150);
                toolListBOArrayList.add(objMarketToolListBO);

                objMarketToolListBO = new MarketToolListBO();
                objMarketToolListBO.setToolID(Defaults.TOOL_ID_PREMIUM_ENTERPRICE);
                objMarketToolListBO.setSubToolId(Defaults.SUB_TOOL_ID_PREMIUM_BUSINESS_INCOME_AND_EXPENSE_TRACKER);
                objMarketToolListBO.setToolTitle(getResources().getString(R.string.card_title_premium_business_income_expense_tracker));
                objMarketToolListBO.setToolShortDescription(getResources().getString(R.string.card_description_premium_business_income_expense_tracker));
                objMarketToolListBO.setToolLongDescription(getResources().getString(R.string.card_long_description_premium_business_income_expense_tracker));
                objMarketToolListBO.setTotalSales(150);
                toolListBOArrayList.add(objMarketToolListBO);

                objMarketToolListBO = new MarketToolListBO();
                objMarketToolListBO.setToolID(Defaults.TOOL_ID_PREMIUM_ENTERPRICE);
                objMarketToolListBO.setSubToolId(Defaults.SUB_TOOL_ID_PREMIUM_ACCESS_TO_CREDIT);
                objMarketToolListBO.setToolTitle(getResources().getString(R.string.card_title_premium_access_to_credit));
                objMarketToolListBO.setToolShortDescription(getResources().getString(R.string.card_description_premium_access_to_credit));
                objMarketToolListBO.setToolLongDescription(getResources().getString(R.string.card_long_description_premium_access_to_credit));
                objMarketToolListBO.setTotalSales(150);
                toolListBOArrayList.add(objMarketToolListBO);

                objMarketToolListBO = new MarketToolListBO();
                objMarketToolListBO.setToolID(Defaults.TOOL_ID_PREMIUM_ENTERPRICE);
                objMarketToolListBO.setSubToolId(Defaults.SUB_TOOL_ID_PREMIUM_ENTERPRISE_WALLET_SERVICES);
                objMarketToolListBO.setToolTitle(getResources().getString(R.string.card_title_premium_wallet_services));
                objMarketToolListBO.setToolShortDescription(getResources().getString(R.string.card_description_premium_wallet_services));
                objMarketToolListBO.setToolLongDescription(getResources().getString(R.string.card_long_description_premium_wallet_services));
                objMarketToolListBO.setTotalSales(150);
                toolListBOArrayList.add(objMarketToolListBO);
            }
            break;
            case Defaults.TOOL_ID_PREMIUM_GROUPS: {
                binding.txtTitle.setText(getResources().getString(R.string.title_group_finance_premium_tool));
                binding.topbarIcon.setBackgroundResource(R.drawable.market_place_groups);

                MarketToolListBO objMarketToolListBO = new MarketToolListBO();
                objMarketToolListBO.setToolID(Defaults.TOOL_ID_GROUPS);
                objMarketToolListBO.setSubToolId(Defaults.SUB_TOOL_ID_ROTATING_SAVINGS);
                objMarketToolListBO.setToolTitle(getResources().getString(R.string.card_title_rotating_savings));
                objMarketToolListBO.setToolShortDescription(getResources().getString(R.string.card_description_rotating_savings));
                objMarketToolListBO.setToolLongDescription(getResources().getString(R.string.card_long_description_rotating_savings));
                objMarketToolListBO.setTotalSales(150);
                toolListBOArrayList.add(objMarketToolListBO);

                objMarketToolListBO = new MarketToolListBO();
                objMarketToolListBO.setToolID(Defaults.TOOL_ID_GROUPS);
                objMarketToolListBO.setSubToolId(Defaults.SUB_TOOL_ID_PEER_TO_PEER);
                objMarketToolListBO.setToolTitle(getResources().getString(R.string.card_title_peer_to_peer));
                objMarketToolListBO.setToolShortDescription(getResources().getString(R.string.card_description_peer_to_peer));
                objMarketToolListBO.setToolLongDescription(getResources().getString(R.string.card_long_description_peer_to_peer));
                objMarketToolListBO.setTotalSales(100);
                toolListBOArrayList.add(objMarketToolListBO);

                objMarketToolListBO = new MarketToolListBO();
                objMarketToolListBO.setToolID(Defaults.TOOL_ID_PREMIUM_GROUPS);
                objMarketToolListBO.setSubToolId(Defaults.SUB_TOOL_ID_PREMIUM_GROUPS_GOAL_BASED_SAVINGS);
                objMarketToolListBO.setToolTitle(getResources().getString(R.string.card_title_premium_goal_based_savings));
                objMarketToolListBO.setToolShortDescription(getResources().getString(R.string.card_description_premium_goal_based_savings));
                objMarketToolListBO.setToolLongDescription(getResources().getString(R.string.card_long_description_premium_goal_based_savings));
                objMarketToolListBO.setTotalSales(150);
                toolListBOArrayList.add(objMarketToolListBO);

                objMarketToolListBO = new MarketToolListBO();
                objMarketToolListBO.setToolID(Defaults.TOOL_ID_PREMIUM_GROUPS);
                objMarketToolListBO.setSubToolId(Defaults.SUB_TOOL_ID_PREMIUM_GROUPS_LOYALTY_REWARDS);
                objMarketToolListBO.setToolTitle(getResources().getString(R.string.card_title_premium_groups_loyalty));
                objMarketToolListBO.setToolShortDescription(getResources().getString(R.string.card_description_premium_groups_loyalty));
                objMarketToolListBO.setToolLongDescription(getResources().getString(R.string.card_long_description_premium_groups_loyalty));
                objMarketToolListBO.setTotalSales(250);
                toolListBOArrayList.add(objMarketToolListBO);

                objMarketToolListBO = new MarketToolListBO();
                objMarketToolListBO.setToolID(Defaults.TOOL_ID_PREMIUM_GROUPS);
                objMarketToolListBO.setSubToolId(Defaults.SUB_TOOL_ID_PREMIUM_GROUPS_WALLET_SERVICES);
                objMarketToolListBO.setToolTitle(getResources().getString(R.string.card_title_premium_group_wallet_services));
                objMarketToolListBO.setToolShortDescription(getResources().getString(R.string.card_description_premium_group_wallet_services));
                objMarketToolListBO.setToolLongDescription(getResources().getString(R.string.card_long_description_premium_group_wallet_services));
                objMarketToolListBO.setTotalSales(250);
                toolListBOArrayList.add(objMarketToolListBO);
            }
            break;
            case Defaults.TOOL_ID_PREMIUM_PERSONAL: {
                binding.txtTitle.setText(getResources().getString(R.string.title_personal_finance_premium_tool));
                binding.topbarIcon.setBackgroundResource(R.drawable.market_place_personal);

                MarketToolListBO objMarketToolListBO = new MarketToolListBO();
                objMarketToolListBO.setToolID(Defaults.TOOL_ID_PREMIUM_PERSONAL);
                objMarketToolListBO.setSubToolId(Defaults.SUB_TOOL_ID_PREMIUM_PERSONAL_BUDGET_EXPENSE_TRACKER);
                objMarketToolListBO.setToolTitle(getResources().getString(R.string.card_title_premium_budget_expense_tracker));
                objMarketToolListBO.setToolShortDescription(getResources().getString(R.string.card_description_premium_budget_expense_tracker));
                objMarketToolListBO.setToolLongDescription(getResources().getString(R.string.card_long_description_premium_budget_expense_tracker));
                objMarketToolListBO.setTotalSales(150);
                toolListBOArrayList.add(objMarketToolListBO);

                objMarketToolListBO = new MarketToolListBO();
                objMarketToolListBO.setToolID(Defaults.TOOL_ID_PREMIUM_PERSONAL);
                objMarketToolListBO.setSubToolId(Defaults.SUB_TOOL_ID_PREMIUM_PERSONAL_LOYALTY_REWARDS);
                objMarketToolListBO.setToolTitle(getResources().getString(R.string.card_title_premium_personal_loyalty_program));
                objMarketToolListBO.setToolShortDescription(getResources().getString(R.string.card_description_premium_personal_loyalty_program));
                objMarketToolListBO.setToolLongDescription(getResources().getString(R.string.card_long_description_premium_personal_loyalty_program));
                objMarketToolListBO.setTotalSales(150);
                toolListBOArrayList.add(objMarketToolListBO);

                objMarketToolListBO = new MarketToolListBO();
                objMarketToolListBO.setToolID(Defaults.TOOL_ID_PREMIUM_PERSONAL);
                objMarketToolListBO.setSubToolId(Defaults.SUB_TOOL_ID_PREMIUM_PERSONAL_WALLET_SERVICES);
                objMarketToolListBO.setToolTitle(getResources().getString(R.string.card_title_premium_personal_wallet_services));
                objMarketToolListBO.setToolShortDescription(getResources().getString(R.string.card_description_premium_personal_wallet_services));
                objMarketToolListBO.setToolLongDescription(getResources().getString(R.string.card_long_description_premium_personal_wallet_services));
                objMarketToolListBO.setTotalSales(150);
                toolListBOArrayList.add(objMarketToolListBO);
            }
            break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    public void switchAnimation(final ImageView androidImage, final ImageView otherImage,
                                final Intent intent, final Context context) {
        //Set the transition details and start the second activity
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation
                    ((Activity) context, Pair.create((View) androidImage, ANDROID_TRANSITION),
                            Pair.create((View) otherImage, BLUE_TRANSITION));
            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
        }
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_TOOL_ENABLED_REFRESH_MARKETPLACE);
        Utility.registerReciever(PremiumToolListingActivity.this, filter, objMyReceiver);
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Defaults.ACTION_TOOL_ENABLED_REFRESH_MARKETPLACE)) {
                if (mAdapter != null) {
                    mAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }
}
