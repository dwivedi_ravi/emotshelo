package com.thabiso.emotshelo.activities.homescreen;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.thabiso.emotshelo.models.HomeScreenObject;

public class HomeScreenPageViewModel extends ViewModel {

    private MutableLiveData<Integer> mIndex = new MutableLiveData<>();
    private LiveData<String> mText = Transformations.map(mIndex, new Function<Integer, String>() {
        @Override
        public String apply(Integer input) {
            return "Hello world from section: " + input;
        }
    });

    public void setIndex(int index) {
        mIndex.setValue(index);
    }

    public LiveData<String> getText() {
        return mText;
    }


    public MutableLiveData<Integer> mScreenType = new MutableLiveData<>();
    private LiveData<Integer> mScreenTypeLD = Transformations.map(mScreenType, new Function<Integer, Integer>() {
        @Override
        public Integer apply(Integer input) {
            return input;
        }
    });

    public void setScreentType(int screentType) {
        mScreenType.setValue(screentType);
    }

    public LiveData<Integer> getScreentType() {
        return mScreenTypeLD;
    }


    private MutableLiveData<HomeScreenObject> verseMutableLiveData = new MutableLiveData<>();
    private LiveData<HomeScreenObject> mTextVerse = Transformations.map(verseMutableLiveData, new Function<HomeScreenObject, HomeScreenObject>() {
        @Override
        public HomeScreenObject apply(HomeScreenObject input) {
            return input;
        }
    });

    public void setVerseObject(HomeScreenObject verseObject) {
        verseMutableLiveData.setValue(verseObject);
    }

    public LiveData<HomeScreenObject> getVerseObject() {
        return mTextVerse;
    }
}