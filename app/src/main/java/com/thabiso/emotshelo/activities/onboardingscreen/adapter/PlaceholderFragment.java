package com.thabiso.emotshelo.activities.onboardingscreen.adapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.databinding.FragmentPagerBinding;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    private FragmentPagerBinding binding;

    //        int[] bgs = new int[]{R.drawable.ic_flight_24dp, R.drawable.ic_mail_24dp, R.drawable.ic_explore_24dp, R.drawable.ic_flight_24dp};
//    int[] bgs = new int[]{R.drawable.onboarding_screen_1_image_squares_24dp, R.drawable.onboarding_screen_2_money_24dp, R.drawable.onboarding_screen_3_home_24dp};
    int[] bgs = new int[]{R.drawable.img_wizard_2, R.drawable.onboarding_screen_2_money_24dp, R.drawable.ic_shopping_cart, R.drawable.onboarding_screen_4_wallet_24dp};

    int[] pageTitleList = new int[]{R.string.title_page_1, R.string.title_page_2, R.string.title_page_3, R.string.title_page_4};

    int[] pageDescriptionList = new int[]{R.string.content_page_1, R.string.content_page_2, R.string.content_page_3, R.string.content_page_4};

    public PlaceholderFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PlaceholderFragment newInstance(int sectionNumber) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        View rootView = inflater.inflate(R.layout.fragment_pager, container, false);
        binding = FragmentPagerBinding.inflate(inflater, container, false);
        View rootView = binding.getRoot();

//        textView.setText(getString(R.str01ing.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
        binding.pageTitle.setText(getResources().getString(pageTitleList[getArguments().getInt(ARG_SECTION_NUMBER) - 1]));

        binding.pageDescription.setText(getResources().getString(pageDescriptionList[getArguments().getInt(ARG_SECTION_NUMBER) - 1]));

//        binding.sectionImg.setBackgroundResource(bgs[getArguments().getInt(ARG_SECTION_NUMBER) - 1]);
        binding.sectionImg.setImageResource(bgs[getArguments().getInt(ARG_SECTION_NUMBER) - 1]);

        return rootView;
    }
}