package com.thabiso.emotshelo.activities.business_tool.invoicing;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.databinding.ActivityAddNewClientBinding;
import com.thabiso.emotshelo.fragments.FragmentAddInvoiceEstimate;
import com.thabiso.emotshelo.models.creategroup.UserGroups;
import com.thabiso.emotshelo.util.Tools;
import com.thabiso.emotshelo.util.preferences.Defaults;

public class AddInvoiceEstimateActivity extends AppCompatActivity {

    private ActivityAddNewClientBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAddNewClientBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initToolbar();
        initComponent();

        displayFragment();
    }

    private void initComponent() {
//        TextView txtADD = findViewById(R.id.txtADD);

        Bundle bundle = getIntent().getExtras();
        int screenType = Defaults.SCREEN_TYPE_INVOICES;
        if (bundle != null) {
            screenType = bundle.getInt("screenType", 0);
        }

        if (screenType == Defaults.SCREEN_TYPE_INVOICES) {
            binding.txtActivityTitle.setText(getResources().getString(R.string.lbl_add_invoice));
        } else if (screenType == Defaults.SCREEN_TYPE_ESTIMATES) {
            binding.txtActivityTitle.setText(getResources().getString(R.string.lbl_add_estimate));
        }
    }

    private void initToolbar() {
        binding.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        binding.toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.grey_60), PorterDuff.Mode.SRC_ATOP);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this, android.R.color.white);
        Tools.setSystemBarLight(this);
    }

    private void displayFragment() {
        Bundle bundle = getIntent().getExtras();
        UserGroups currentGroup = null;
        if (bundle != null) {
            currentGroup = (UserGroups) bundle.getSerializable("currentGroup");
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragment = new FragmentAddInvoiceEstimate(currentGroup);

        if (fragment == null) return;

        fragmentTransaction.replace(R.id.frame_content, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_profile, menu);
        Tools.changeMenuIconColor(menu, getResources().getColor(R.color.grey_60));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }
}

