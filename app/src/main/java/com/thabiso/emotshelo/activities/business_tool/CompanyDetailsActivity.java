package com.thabiso.emotshelo.activities.business_tool;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.activities.base.BaseActivity;
import com.thabiso.emotshelo.activities.business_tool.invoicing.AddInvoiceEstimateActivity;
import com.thabiso.emotshelo.activities.business_tool.invoicing.AddNewClientActivity;
import com.thabiso.emotshelo.activities.business_tool.invoicing.InvoicingActivity;
import com.thabiso.emotshelo.databinding.ActivityCompanyDetailsBinding;
import com.thabiso.emotshelo.models.addcompany.DataItem;
import com.thabiso.emotshelo.models.addcompany.RequestUpdateToolStatus;
import com.thabiso.emotshelo.models.addcompany.ResponseAddCompany;
import com.thabiso.emotshelo.network.ApiCallerUtility;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;
import com.thabiso.emotshelo.util.preferences.Prefs;


public class CompanyDetailsActivity extends BaseActivity implements View.OnClickListener {

    private ActivityCompanyDetailsBinding binding;

    private MyReceiver objMyReceiver;
    private Typeface robotoBold, robotoMedium;
    private DataItem selectedCompany;
    private int position;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityCompanyDetailsBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        objMyReceiver = new MyReceiver();
        registerReciever();

        binding.toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white_24dp));
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        this.robotoBold = Typeface.createFromAsset(getAssets(),
                "roboto_bold.ttf");
        this.robotoMedium = Typeface.createFromAsset(getAssets(),
                "roboto_medium.ttf");

        binding.toolbarLayout.setExpandedTitleTextAppearance(R.style.Toolbar_TitleText_Expanded);
        binding.toolbarLayout.setCollapsedTitleTextAppearance(R.style.Toolbar_TitleText);
        binding.toolbarLayout.setCollapsedTitleTypeface(robotoBold);

        binding.txtCompanyName.setTypeface(robotoBold);
        binding.txtCompanyName.setTypeface(robotoMedium);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            position = bundle.getInt("position");
            selectedCompany = (DataItem) bundle.getSerializable("selectedCompany");
        }

        if (selectedCompany != null) {
            if (selectedCompany.getTool() != null && selectedCompany.getTool().size() > 0 && selectedCompany.getTool().get(0).getToolId().equals(Defaults.TOOL_ID_ENTERPRICE + "") && selectedCompany.getTool().get(0).getSubToolId().equals(Defaults.SUB_TOOL_ID_INVOICING_AND_QUOTATION + "") && selectedCompany.getTool().get(0).getStatus().equals(Defaults.TOOL_ACTIVATED)) {
                setValues();
            } else {
                RequestUpdateToolStatus objRequestUpdateGroupStatus = new RequestUpdateToolStatus();
                objRequestUpdateGroupStatus.setUserId(Prefs.getUserId());
                objRequestUpdateGroupStatus.setCompanyId(selectedCompany.getId());
                objRequestUpdateGroupStatus.setToolId(Defaults.TOOL_ID_ENTERPRICE + "");
                objRequestUpdateGroupStatus.setSubToolId(Defaults.SUB_TOOL_ID_INVOICING_AND_QUOTATION + "");
                objRequestUpdateGroupStatus.setName(getResources().getString(R.string.card_title_invoicing));
                objRequestUpdateGroupStatus.setStatus(Defaults.TOOL_ACTIVATED);
//                objRequestUpdateGroupStatus.setStatus(Defaults.TOOL_DEACTIVATED);

                ApiCallerUtility.callUpdateToolStatusApi(this, objRequestUpdateGroupStatus, position);
            }
        }

        binding.appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                try {
                    if (scrollRange == -1) {
                        scrollRange = appBarLayout.getTotalScrollRange();
                    }
                    if (scrollRange + verticalOffset == 0) {
                        binding.toolbarLayout.setTitle(selectedCompany.getCompanyName());
                        isShow = true;
                    } else if (isShow) {
                        binding.toolbarLayout.setTitle(" ");//careful there should a space between double quote otherwise it wont work
                        isShow = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        binding.fabNext.shrink();
        binding.fabNext.setOnClickListener(this);
        binding.contentCompanyDetails.lytAddCustomer.setOnClickListener(this);
        binding.contentCompanyDetails.lytCreateEstimate.setOnClickListener(this);
        binding.contentCompanyDetails.lytCreateInvoice.setOnClickListener(this);
        binding.contentCompanyDetails.lytCustomerInvoiceEstimateTool.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_invoicing_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_edit_company_details) {
            Intent intent = new Intent(CompanyDetailsActivity.this, EditCompanyActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("selectedCompany", selectedCompany);
            intent.putExtras(bundle);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setValues() {
        try {
            binding.contentCompanyDetails.getRoot().setVisibility(View.VISIBLE);
            binding.toolbar.setTitle(selectedCompany.getCompanyName());
            binding.toolbarLayout.setTitle(selectedCompany.getCompanyName());
            binding.toolbarLayout.setExpandedTitleColor(getResources().getColor(R.color.transparent));
            Glide.with(CompanyDetailsActivity.this).setDefaultRequestOptions(Utility.getCustomizedRequestOptions(R.color.mdtp_line_background, R.drawable.no_image_place_holder)).load(selectedCompany.getLogo())
                    .into(binding.imgCompanyLogo);

            binding.txtCompanyName.setText(selectedCompany.getCompanyName());
            binding.txtCompanyDescription.setText(selectedCompany.getCompanyAddress());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_TOOL_STATUS_UPDATED);
        Utility.registerReciever(CompanyDetailsActivity.this, filter, objMyReceiver);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_next: {
//                if (selectedCompany != null) {
//                    Intent detailIntent = new Intent(CompanyDetailsActivity.this, GroupExpenseManagerScreen.class);
//                    Bundle bundle = new Bundle();
//                    bundle.putString("user_id", objUserGroups.getUserId());
//                    detailIntent.putExtras(bundle);
//                    startActivity(detailIntent);
//                }
            }
            break;
            case R.id.lytAddCustomer: {
                Intent detailIntent = new Intent(CompanyDetailsActivity.this, AddNewClientActivity.class);
                CompanyDetailsActivity.this.startActivity(detailIntent);
            }
            break;
            case R.id.lytCreateEstimate: {
                Intent detailIntent = new Intent(CompanyDetailsActivity.this, AddInvoiceEstimateActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("screenType", Defaults.SCREEN_TYPE_ESTIMATES);
                detailIntent.putExtras(bundle);
                CompanyDetailsActivity.this.startActivity(detailIntent);
            }
            break;
            case R.id.lytCreateInvoice: {
                Intent detailIntent = new Intent(CompanyDetailsActivity.this, AddInvoiceEstimateActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("screenType", Defaults.SCREEN_TYPE_INVOICES);
                detailIntent.putExtras(bundle);
                CompanyDetailsActivity.this.startActivity(detailIntent);

            }
            break;
            case R.id.lytCustomerInvoiceEstimateTool: {
                Intent detailIntent = new Intent(CompanyDetailsActivity.this, InvoicingActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("selectedCompany", selectedCompany);
                detailIntent.putExtras(bundle);
                CompanyDetailsActivity.this.startActivity(detailIntent);
            }
            break;
        }
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Defaults.ACTION_TOOL_STATUS_UPDATED)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        int position = bundle.getInt("position");
                        ResponseAddCompany objResponseAddCompany = (ResponseAddCompany) bundle.getSerializable("ResponseAddCompany");
                        if (objResponseAddCompany != null && objResponseAddCompany.getData() != null) {

                            if (objResponseAddCompany.getData().getTools().get(0).getStatus().equals(Defaults.TOOL_ACTIVATED)) {
                                Toast.makeText(CompanyDetailsActivity.this, CompanyDetailsActivity.this.getResources().getString(R.string.enable_tool_success), Toast.LENGTH_LONG).show();
                                setValues();
                            } else if (objResponseAddCompany.getData().getTools().get(0).getStatus().equals(Defaults.TOOL_DEACTIVATED)) {
                                Toast.makeText(CompanyDetailsActivity.this, CompanyDetailsActivity.this.getResources().getString(R.string.disable_tool_success), Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }
}
