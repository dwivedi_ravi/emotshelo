package com.thabiso.emotshelo.activities.homescreen;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.activities.business_tool.conversationalform.AddCompanyActivity;
import com.thabiso.emotshelo.activities.groups_tool.GroupDetailsActivity;
import com.thabiso.emotshelo.activities.groups_tool.TimelineSimple;
import com.thabiso.emotshelo.activities.howitworks.FinancialEducationActivity;
import com.thabiso.emotshelo.activities.howitworks.HowItWorksActivity;
import com.thabiso.emotshelo.activities.notification.NotificationsActivity;
import com.thabiso.emotshelo.activities.premium_tool.LoginToPremiumAccount;
import com.thabiso.emotshelo.activities.signup.EditProfileActivity;
import com.thabiso.emotshelo.activities.socialmedia.SocialMediaLogin;
import com.thabiso.emotshelo.activities.tools.ToolMarketPlaceActivity;
import com.thabiso.emotshelo.databinding.ActivityHomeScreenBinding;
import com.thabiso.emotshelo.models.HomeScreenObject;
import com.thabiso.emotshelo.models.addcompany.ResponseAddCompany;
import com.thabiso.emotshelo.models.addcompany.ResponseCompanyList;
import com.thabiso.emotshelo.models.creategroup.ResponseCreateGroup;
import com.thabiso.emotshelo.models.creategroup.UserGroups;
import com.thabiso.emotshelo.models.editprofile.ResponseEditProfile;
import com.thabiso.emotshelo.models.fcmregistration.RequestFCMPushToken;
import com.thabiso.emotshelo.models.grouplist.RequestGroupList;
import com.thabiso.emotshelo.models.grouplist.ResponseGroupList;
import com.thabiso.emotshelo.models.grouplist.UserGroupsItem;
import com.thabiso.emotshelo.network.ApiCallerUtility;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;
import com.thabiso.emotshelo.util.preferences.Prefs;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeScreenActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnHomeScreenListManagerInteractionListener {

    private ActivityHomeScreenBinding binding;

    private TextView textCartItemCount;
    private ImageView imgNotification;
    private TextView userMobile, userName;
    private CircleImageView navigationDrawerProfileImage;

    private MyReceiver objMyReceiver;
    private int screenType;
    private HomeScreenSectionsPagerAdapter homeScreenSectionsPagerAdapter;
    private HomeScreenObject objHomeScreenObject;
    private BottomSheetBehavior bottomSheetBehavior;
    private Typeface robotoRegular, robotoMedium;
    private boolean isShowingTraining = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);

            binding = ActivityHomeScreenBinding.inflate(getLayoutInflater());

            setContentView(binding.getRoot());

            setSupportActionBar(binding.contentHomeScreen.appbarLayout.toolbar);

            initComponents();
            initializeDataFromDB();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    fetchGroupListFromServer();
                    registerPushTokenIfMissing();
                    ApiCallerUtility.callCheckUserStatusApi(HomeScreenActivity.this);
                }
            }, 1000);

            handleNotificationClick(getIntent());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initComponents() {
        objMyReceiver = new MyReceiver();
        screenType = Defaults.SCREEN_TYPE_GROUPS;
        objHomeScreenObject = new HomeScreenObject();
        registerReciever();

        binding.contentHomeScreen.fab.extend();
        binding.contentHomeScreen.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleAddButtonClick();
            }
        });

        robotoRegular = Typeface.createFromAsset(getAssets(),
                "roboto_regular.ttf");
        robotoMedium = Typeface.createFromAsset(getAssets(),
                "roboto_medium.ttf");

        setUpNavigationDrawer();
        initializeBottomSheetLayout();
        setupViewPager();
    }

    public void handleAddButtonClick() {
        if (binding.contentHomeScreen.appbarLayout.tabs.getSelectedTabPosition() == 0) {
            Toast.makeText(HomeScreenActivity.this, getResources().getString(R.string.lbl_under_development), Toast.LENGTH_LONG).show();
        } else if (binding.contentHomeScreen.appbarLayout.tabs.getSelectedTabPosition() == 1) {
            Toast.makeText(HomeScreenActivity.this, getResources().getString(R.string.lbl_under_development), Toast.LENGTH_LONG).show();
        } else if (binding.contentHomeScreen.appbarLayout.tabs.getSelectedTabPosition() == 2) {
            if (Prefs.getBooleanValue(Defaults.TOOL_ID_GROUPS + "")) {
                Intent intent = new Intent(HomeScreenActivity.this, ToolMarketPlaceActivity.class);
                intent.putExtra("toolID", Defaults.TOOL_ID_GROUPS);
                startActivity(intent);
            } else {
                Intent intent = new Intent(HomeScreenActivity.this, ToolMarketPlaceActivity.class);
                startActivity(intent);
            }
        } else if (binding.contentHomeScreen.appbarLayout.tabs.getSelectedTabPosition() == 3) {
            if (Prefs.getBooleanValue(Defaults.TOOL_ID_ENTERPRICE + "")) {
                Intent detailIntent = new Intent(HomeScreenActivity.this, AddCompanyActivity.class);
                HomeScreenActivity.this.startActivity(detailIntent);
            } else {
                Intent intent = new Intent(HomeScreenActivity.this, ToolMarketPlaceActivity.class);
                startActivity(intent);
            }
        }
    }

    private void fetchGroupListFromServer() {
        if (Prefs.getBooleanValue(Defaults.TOOL_ID_GROUPS + "")) {
            RequestGroupList objRequestGroupList = new RequestGroupList();
            objRequestGroupList.setUserId(Prefs.getUserId());
            if (homeScreenSectionsPagerAdapter != null && homeScreenSectionsPagerAdapter.getFragment(screenType) != null) {
                homeScreenSectionsPagerAdapter.getFragment(Defaults.SCREEN_TYPE_GROUPS).getNoInternetLayout().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ApiCallerUtility.callGroupListApi(HomeScreenActivity.this, objRequestGroupList, homeScreenSectionsPagerAdapter.getFragment(Defaults.SCREEN_TYPE_GROUPS).getNoInternetLayout());
                    }
                });
                ApiCallerUtility.callGroupListApi(this, objRequestGroupList, homeScreenSectionsPagerAdapter.getFragment(Defaults.SCREEN_TYPE_GROUPS).getNoInternetLayout());
            } else {
                ApiCallerUtility.callGroupListApi(this, objRequestGroupList, null);
            }
        }
    }

    private void registerPushTokenIfMissing() {
        if (!Prefs.isPushTokenRegistered() && !Prefs.getFirebaseInstanceIdFromSharedPreferences().equals("") && !Prefs.getUserId().equals("")) {
            RequestFCMPushToken objRequestFCMPushToken = new RequestFCMPushToken();
            objRequestFCMPushToken.setUserId(Prefs.getUserId());
            objRequestFCMPushToken.setGcmId(Prefs.getFirebaseInstanceIdFromSharedPreferences());
            ApiCallerUtility.callRegisterPushTokenApi(getApplicationContext(), objRequestFCMPushToken);
        }
    }

    @Override
    public void onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        } else if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            binding.contentHomeScreen.viewOverlay.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.group_list, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_notification);
        View actionView = MenuItemCompat.getActionView(menuItem);
        textCartItemCount = actionView.findViewById(R.id.notification_badge);
        imgNotification = actionView.findViewById(R.id.imgNotification);

        setupBadge();
        showWelcomeScreenDialog(this);

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });

        return true;
    }

    @Override
    public boolean
    onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_settings: {
                return true;
            }
            case R.id.action_refresh: {
                fetchGroupListFromServer();
                return true;
            }
            case R.id.action_activity_log: {
                Intent intent = new Intent(HomeScreenActivity.this, TimelineSimple.class);
                startActivity(intent);
                return true;
            }
            case R.id.action_notification: {
                Intent inte = new Intent(HomeScreenActivity.this, NotificationsActivity.class);
                startActivity(inte);
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_activity_log) {
//            Intent intent = new Intent(GroupListActivity.this, TimelineSimple.class);
//            startActivity(intent);
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_market_place) {
            Intent intent = new Intent(HomeScreenActivity.this, ToolMarketPlaceActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_how_it_works) {
            Intent intent = new Intent(HomeScreenActivity.this, HowItWorksActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_financial_education) {
            Intent intent = new Intent(HomeScreenActivity.this, FinancialEducationActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_premium_account) {
            if (Prefs.getSocialMediaAuthKey().equals("")) {
                Intent intent = new Intent(HomeScreenActivity.this, SocialMediaLogin.class);
                startActivity(intent);
            } else {
                Intent intent = new Intent(HomeScreenActivity.this, LoginToPremiumAccount.class);
                startActivity(intent);
            }
        } else if (id == R.id.nav_tools) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        binding.drawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_REFRESH_GROUP_LIST_SCREEN);
        filter.addAction(Defaults.ACTION_CALL_REFRESH_GROUP_LIST_API);
        filter.addAction(Defaults.ACTION_REFRESH_COMPANY_LIST_SCREEN);
        filter.addAction(Defaults.ACTION_TOOL_STATUS_UPDATED);
        filter.addAction(Defaults.ACTION_FETCH_GROUP_LIST_FROM_SERVER);
        filter.addAction(Defaults.ACTION_FETCH_COMPANY_LIST_FROM_SERVER);
        filter.addAction(Defaults.ACTION_TOOL_ENABLED_REFRESH_HOMESCREEN);
        filter.addAction(Defaults.ACTION_PROFILE_DATA_UPDATED);
        filter.addAction(Defaults.ACTION_PROFILE_PIC_UPLOAD_SUCCESS);
        Utility.registerReciever(HomeScreenActivity.this, filter, objMyReceiver);
    }

    @Override
    public void onListFragmentInteraction(HomeScreenObject item) {
        Toast.makeText(this, " Clicked", Toast.LENGTH_SHORT).show();
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Defaults.ACTION_CALL_REFRESH_GROUP_LIST_API)) {
                fetchGroupListFromServer();
            }

            if (intent.getAction().equals(Defaults.ACTION_REFRESH_GROUP_LIST_SCREEN)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        ResponseGroupList objResponseGroupList = (ResponseGroupList) bundle.getSerializable("ResponseGroupList");
                        if (objResponseGroupList != null && objResponseGroupList.getData() != null && objResponseGroupList.getData().getUserGroups() != null) {
                            objHomeScreenObject.setGroupList(objResponseGroupList.getData().getUserGroups());
                            if (homeScreenSectionsPagerAdapter != null && homeScreenSectionsPagerAdapter.getFragment(screenType) != null) {
                                homeScreenSectionsPagerAdapter.getFragment(Defaults.SCREEN_TYPE_GROUPS).refreshList(objHomeScreenObject);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (intent.getAction().equals(Defaults.ACTION_REFRESH_COMPANY_LIST_SCREEN)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        ResponseCompanyList objResponseCompanyList = (ResponseCompanyList) bundle.getSerializable("ResponseCompanyList");
                        if (objResponseCompanyList != null && objResponseCompanyList.getData() != null) {
                            objHomeScreenObject.setEnterpriseList(objResponseCompanyList.getData());
                            if (homeScreenSectionsPagerAdapter != null && homeScreenSectionsPagerAdapter.getFragment(screenType) != null) {
                                homeScreenSectionsPagerAdapter.getFragment(Defaults.SCREEN_TYPE_ENTERPRISE).refreshList(objHomeScreenObject);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (intent.getAction().equals(Defaults.ACTION_TOOL_STATUS_UPDATED)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        int position = bundle.getInt("position");
                        ResponseAddCompany objResponseAddCompany = (ResponseAddCompany) bundle.getSerializable("ResponseAddCompany");
                        if (objResponseAddCompany != null && objResponseAddCompany.getData() != null) {

                            try {
                                objHomeScreenObject.getEnterpriseList().get(position).setWebsite(objResponseAddCompany.getData().getWebsite());
                                objHomeScreenObject.getEnterpriseList().get(position).setAddress(objResponseAddCompany.getData().getAddress());
                                objHomeScreenObject.getEnterpriseList().get(position).setAddressId(objResponseAddCompany.getData().getAddressId());
//                                    objHomeScreenObject.getEnterpriseList().get(position).setToolId(objResponseAddCompany.getData().gett);
                                objHomeScreenObject.getEnterpriseList().get(position).setCreatedAt(objResponseAddCompany.getData().getCreatedAt());
                                objHomeScreenObject.getEnterpriseList().get(position).setEmailid(objResponseAddCompany.getData().getEmailid());
                                objHomeScreenObject.getEnterpriseList().get(position).setAdditionalTitle(objResponseAddCompany.getData().getAdditionalTitle());
                                objHomeScreenObject.getEnterpriseList().get(position).setLanguage(objResponseAddCompany.getData().getLanguage());
//                                    objHomeScreenObject.getEnterpriseList().get(position).setDeletedAt(objResponseAddCompany.getData().getde);
                                objHomeScreenObject.getEnterpriseList().get(position).setTaxId(objResponseAddCompany.getData().getTaxId());
                                objHomeScreenObject.getEnterpriseList().get(position).setCompanyAddress(objResponseAddCompany.getData().getCompanyAddress());
//                                        HERE A CHANGE NEEDS TO BE DONE BY SHUBHAM "tools" key in AddCompanyResponse needs to be renamed to "tool" as in all other apis it is "tool" only
                                objHomeScreenObject.getEnterpriseList().get(position).setTool(objResponseAddCompany.getData().getTools());
//                                        objHomeScreenObject.getEnterpriseList().get(position).getTool().get(0).setStatus(objRequestUpdateGroupStatus.getStatus());
                                objHomeScreenObject.getEnterpriseList().get(position).setTags(objResponseAddCompany.getData().getTags());
                                objHomeScreenObject.getEnterpriseList().get(position).setUserType(objResponseAddCompany.getData().getUserType());
                                objHomeScreenObject.getEnterpriseList().get(position).setUpdatedAt(objResponseAddCompany.getData().getUpdatedAt());
                                objHomeScreenObject.getEnterpriseList().get(position).setUserId(objResponseAddCompany.getData().getUserId());
                                objHomeScreenObject.getEnterpriseList().get(position).setCompanyName(objResponseAddCompany.getData().getCompanyName());
                                objHomeScreenObject.getEnterpriseList().get(position).setLogo(objResponseAddCompany.getData().getLogo());
                                objHomeScreenObject.getEnterpriseList().get(position).setPhoneNumber(objResponseAddCompany.getData().getPhoneNumber());
                                objHomeScreenObject.getEnterpriseList().get(position).setId(objResponseAddCompany.getData().getId());
                                objHomeScreenObject.getEnterpriseList().get(position).setDesignation(objResponseAddCompany.getData().getDesignation());
                                objHomeScreenObject.getEnterpriseList().get(position).setMobileNumber(objResponseAddCompany.getData().getMobileNumber());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if (homeScreenSectionsPagerAdapter != null && homeScreenSectionsPagerAdapter.getFragment(screenType) != null) {
                                homeScreenSectionsPagerAdapter.getFragment(Defaults.SCREEN_TYPE_ENTERPRISE).refreshList(objHomeScreenObject);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (intent.getAction().equals(Defaults.ACTION_FETCH_GROUP_LIST_FROM_SERVER)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    Prefs.setBooleanValue(Defaults.TOOL_ID_GROUPS + "", true);
                    Toast.makeText(HomeScreenActivity.this, HomeScreenActivity.this.getResources().getString(R.string.group_added), Toast.LENGTH_LONG).show();

                    if (bundle != null) {
                        ResponseCreateGroup responseCreateGroup = (ResponseCreateGroup) bundle.getSerializable("ResponseCreateGroup");
                        if (responseCreateGroup != null && responseCreateGroup.getData() != null && responseCreateGroup.getData().getUserGroups() != null) {

                            UserGroups objUserGroups = responseCreateGroup.getData().getUserGroups();

                            UserGroupsItem objUserGroupsItem = new UserGroupsItem();
                            objUserGroupsItem.setNumberOfMembers(objUserGroups.getNumberOfMembers());
                            objUserGroupsItem.setDescription(objUserGroups.getDescription());
                            objUserGroupsItem.setCreatedAt(objUserGroups.getCreatedAt());
                            objUserGroupsItem.setGroupType(objUserGroups.getGroupType());
                            objUserGroupsItem.setGroupAvatar(objUserGroups.getGroupAvatar());
//                            if (objUserGroups.getIsAdmin() != null && objUserGroups.getIsAdmin().trim().equals("1"))
                            if (objUserGroups.isAdmin())
                                objUserGroupsItem.setIsAdmin(true);
                            else
                                objUserGroupsItem.setIsAdmin(false);

                            objUserGroupsItem.setTotalWithdrawalLimit(objUserGroups.getTotalWithdrawalLimit());
                            objUserGroupsItem.setUpdatedAt(objUserGroups.getUpdatedAt());
                            objUserGroupsItem.setUserId(objUserGroups.getUserId());
                            objUserGroupsItem.setInvitationStatus(objUserGroups.getInvitationStatus());
                            objUserGroupsItem.setName(objUserGroups.getName());
                            objUserGroupsItem.setInterestRate(objUserGroups.getInterestRate());
                            objUserGroupsItem.setId(objUserGroups.getId());
                            objUserGroupsItem.setAmountPerRound(objUserGroups.getAmountPerRound());
                            objUserGroupsItem.setTotalAmountPerRound(objUserGroups.getTotalAmountPerRound());
                            objUserGroupsItem.setStatus(objUserGroups.getStatus());

                            objHomeScreenObject.getGroupList().add(0, objUserGroupsItem);

//                            if (homeScreenSectionsPagerAdapter != null && homeScreenSectionsPagerAdapter.getFragment(screenType) != null) {
                            if (homeScreenSectionsPagerAdapter != null) {
                                homeScreenSectionsPagerAdapter.getFragment(Defaults.SCREEN_TYPE_GROUPS).refreshList(objHomeScreenObject);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (intent.getAction().equals(Defaults.ACTION_FETCH_COMPANY_LIST_FROM_SERVER)) {
                try {
                    if (intent != null && intent.getExtras() != null && intent.getExtras().getBundle("bundle") != null) {
                        final Bundle bundle = intent.getExtras().getBundle("bundle");
                        ResponseAddCompany responseAddCompany = (ResponseAddCompany) bundle.getSerializable("ResponseAddCompany");
                        if (responseAddCompany != null) {
                        }
                    }
                    RequestGroupList objRequestGroupList = new RequestGroupList();
                    objRequestGroupList.setUserId(Prefs.getUserId());
                    if (homeScreenSectionsPagerAdapter != null && homeScreenSectionsPagerAdapter.getFragment(screenType) != null) {
                        homeScreenSectionsPagerAdapter.getFragment(Defaults.SCREEN_TYPE_ENTERPRISE).getNoInternetLayout().setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ApiCallerUtility.callCompanyListApi(HomeScreenActivity.this, objRequestGroupList, homeScreenSectionsPagerAdapter.getFragment(Defaults.SCREEN_TYPE_ENTERPRISE).getNoInternetLayout());
                            }
                        });
                        ApiCallerUtility.callCompanyListApi(HomeScreenActivity.this, objRequestGroupList, homeScreenSectionsPagerAdapter.getFragment(Defaults.SCREEN_TYPE_ENTERPRISE).getNoInternetLayout());
                    } else {
                        ApiCallerUtility.callCompanyListApi(HomeScreenActivity.this, objRequestGroupList, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (intent.getAction().equals(Defaults.ACTION_TOOL_ENABLED_REFRESH_HOMESCREEN)) {
                try {
                    if (homeScreenSectionsPagerAdapter != null) {
                        homeScreenSectionsPagerAdapter.getFragment(Defaults.SCREEN_TYPE_ENTERPRISE).refreshList(objHomeScreenObject);
                        homeScreenSectionsPagerAdapter.getFragment(Defaults.SCREEN_TYPE_GROUPS).refreshList(objHomeScreenObject);
                        homeScreenSectionsPagerAdapter.getFragment(Defaults.SCREEN_TYPE_PERSONAL).refreshList(objHomeScreenObject);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (intent.getAction().equals(Defaults.ACTION_PROFILE_DATA_UPDATED)) {
                try {
                    userName.setText(Prefs.getUserName());
                    userMobile.setText(Prefs.getMobileNumber());
                    Glide.with(HomeScreenActivity.this).setDefaultRequestOptions(Utility.getCustomizedRequestOptions(R.color.mdtp_line_background, R.drawable.profile_place_holder)).load(Prefs.getUserProfilePicUrl())
                            .thumbnail(0.25f).into(navigationDrawerProfileImage);

                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        ResponseEditProfile objResponseEditProfile = (ResponseEditProfile) bundle.getSerializable("ResponseEditProfile");
                        if (objResponseEditProfile != null) {
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (intent.getAction().equals(Defaults.ACTION_PROFILE_PIC_UPLOAD_SUCCESS)) {
                try {
                    Glide.with(HomeScreenActivity.this).setDefaultRequestOptions(Utility.getCustomizedRequestOptions(R.color.mdtp_line_background, R.drawable.profile_place_holder)).load(Prefs.getUserProfilePicUrl())
                            .thumbnail(0.25f).into(navigationDrawerProfileImage);

                    if (intent != null && intent.getExtras() != null && intent.getExtras().getBundle("bundle") != null) {
                        Bundle bundle = intent.getExtras().getBundle("bundle");
                        ResponseEditProfile objResponseEditProfile = (ResponseEditProfile) bundle.getSerializable("ResponseEditProfile");
                        if (objResponseEditProfile != null && objResponseEditProfile.getData() != null && objResponseEditProfile.getData().getUser() != null) {
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void setupBadge() {
        try {
            if (textCartItemCount != null) {
                if (Prefs.getNotificationCountFromSharedPrefs() == 0) {
                    if (textCartItemCount.getVisibility() != View.GONE) {
                        textCartItemCount.setVisibility(View.GONE);
                    }
                } else {
                    textCartItemCount.setText(Prefs.getNotificationCountFromSharedPrefs() + "");
                    if (textCartItemCount.getVisibility() != View.VISIBLE) {
                        textCartItemCount.setVisibility(View.VISIBLE);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupBadge();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleNotificationClick(intent);
    }

    private void initializeDataFromDB() {
    }

    private void initializeBottomSheetLayout() {
        bottomSheetBehavior = BottomSheetBehavior.from(binding.contentHomeScreen.contentBottomSheet.bottomSheet);

        binding.contentHomeScreen.contentBottomSheet.closeBottomSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                binding.contentHomeScreen.fab.shrink();
                binding.contentHomeScreen.viewOverlay.setVisibility(View.GONE);
            }
        });

        bottomSheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    Utility.hideKeyboard(HomeScreenActivity.this, binding.contentHomeScreen.contentBottomSheet.closeBottomSheet.getWindowToken());
                    binding.contentHomeScreen.fab.shrink();
                    binding.contentHomeScreen.viewOverlay.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
    }

    private boolean isFormValid() {
        if (binding.contentHomeScreen.contentBottomSheet.editTransactionDate.getVisibility() == View.VISIBLE && binding.contentHomeScreen.contentBottomSheet.editTransactionDate.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentHomeScreen.contentBottomSheet.editTransactionDate, "Date can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentHomeScreen.contentBottomSheet.spnCategory.getVisibility() == View.VISIBLE && binding.contentHomeScreen.contentBottomSheet.spnCategory.getSelectedItemPosition() == 0) {
            Snackbar.make(binding.contentHomeScreen.contentBottomSheet.spnCategory, "Please select category", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentHomeScreen.contentBottomSheet.spnMode.getVisibility() == View.VISIBLE && binding.contentHomeScreen.contentBottomSheet.spnMode.getSelectedItemPosition() == 0) {
            Snackbar.make(binding.contentHomeScreen.contentBottomSheet.spnMode, "Please select mode", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentHomeScreen.contentBottomSheet.editAmount.getVisibility() == View.VISIBLE && binding.contentHomeScreen.contentBottomSheet.editAmount.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentHomeScreen.contentBottomSheet.editAmount, "Amount can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentHomeScreen.contentBottomSheet.editParticulars.getVisibility() == View.VISIBLE && binding.contentHomeScreen.contentBottomSheet.editParticulars.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentHomeScreen.contentBottomSheet.editParticulars, "Particulars can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }
        return true;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        try {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    Rect outRect = new Rect();
                    binding.contentHomeScreen.contentBottomSheet.bottomSheet.getGlobalVisibleRect(outRect);

                    if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        binding.contentHomeScreen.viewOverlay.setVisibility(View.GONE);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return super.dispatchTouchEvent(event);
    }

    private void setupViewPager() {
        if (objHomeScreenObject != null) {
            binding.contentHomeScreen.appbarLayout.toolbar.setTitle(getResources().getString(R.string.title_activity_home_screen));
            homeScreenSectionsPagerAdapter = new HomeScreenSectionsPagerAdapter(this, getSupportFragmentManager(), objHomeScreenObject, this);
            binding.contentHomeScreen.viewPager.setAdapter(homeScreenSectionsPagerAdapter);
            binding.contentHomeScreen.appbarLayout.tabs.setupWithViewPager(binding.contentHomeScreen.viewPager);

            binding.contentHomeScreen.appbarLayout.tabs.getTabAt(0).setIcon(R.drawable.ic_home_filled_vector);
            binding.contentHomeScreen.appbarLayout.tabs.getTabAt(1).setIcon(R.drawable.ic_person_filled_vector);
            binding.contentHomeScreen.appbarLayout.tabs.getTabAt(2).setIcon(R.drawable.ic_group_filled_vector);
            binding.contentHomeScreen.appbarLayout.tabs.getTabAt(3).setIcon(R.drawable.ic_shopping_cart_vector);

            binding.contentHomeScreen.viewPager.setCurrentItem(screenType);
            binding.contentHomeScreen.viewPager.setOffscreenPageLimit(3);

            binding.contentHomeScreen.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int pos) {
                    screenType = pos;

                    switch (screenType) {
                        case Defaults.SCREEN_TYPE_ENTERPRISE:
                            binding.contentHomeScreen.fab.shrink();
                            binding.contentHomeScreen.fab.setText(getResources().getString(R.string.lbl_add_company));
                            binding.contentHomeScreen.fab.extend();
                            Glide.with(HomeScreenActivity.this).load(R.drawable.ad_image_2).into(binding.contentHomeScreen.imgAdImage);
                            binding.contentHomeScreen.txtAdvertiseText.setText("Get Best Enterprise Benefits");
                            break;
                        case Defaults.SCREEN_TYPE_GROUPS:
                            binding.contentHomeScreen.fab.shrink();
                            binding.contentHomeScreen.fab.setText(getResources().getString(R.string.lbl_add_group));
                            binding.contentHomeScreen.fab.extend();
                            Glide.with(HomeScreenActivity.this).load(R.drawable.group_savings_tool).into(binding.contentHomeScreen.imgAdImage);
                            binding.contentHomeScreen.txtAdvertiseText.setText("Get Best Saving Benefits");
                            break;
                        case Defaults.SCREEN_TYPE_PERSONAL:
                            binding.contentHomeScreen.fab.shrink();
                            binding.contentHomeScreen.fab.setText(getResources().getString(R.string.lbl_add_budget));
                            binding.contentHomeScreen.fab.extend();
                            Glide.with(HomeScreenActivity.this).load(R.drawable.ad_image_1).into(binding.contentHomeScreen.imgAdImage);
                            binding.contentHomeScreen.txtAdvertiseText.setText("Get Best Personal Benefits");
                            break;
                        case Defaults.SCREEN_TYPE_HOME:
                            binding.contentHomeScreen.fab.shrink();
                            Glide.with(HomeScreenActivity.this).load(R.drawable.ad_image_1).into(binding.contentHomeScreen.imgAdImage);
                            binding.contentHomeScreen.txtAdvertiseText.setText("Home Screen");
                            break;
                        default:
                            break;
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });
        }
    }

    private void setUpNavigationDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, binding.drawerLayout, binding.contentHomeScreen.appbarLayout.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        binding.drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        binding.navView.setNavigationItemSelectedListener(this);

        userMobile = binding.navView.getHeaderView(0).findViewById(R.id.userMobile);
        userName = binding.navView.getHeaderView(0).findViewById(R.id.userName);
        navigationDrawerProfileImage = binding.navView.getHeaderView(0).findViewById(R.id.imgProfile);

        userName.setTypeface(robotoMedium);
        userMobile.setTypeface(robotoRegular);

        LinearLayout navBarHeaderLayout = binding.navView.getHeaderView(0).findViewById(R.id.navBarHeaderLayout);

        userName.setText(Prefs.getUserName());
        userMobile.setText(Prefs.getMobileNumber());
        Glide.with(this).setDefaultRequestOptions(Utility.getCustomizedRequestOptions(R.color.mdtp_line_background, R.drawable.profile_place_holder)).load(Prefs.getUserProfilePicUrl())
                .thumbnail(0.25f).into(navigationDrawerProfileImage);

        navBarHeaderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inte = new Intent(HomeScreenActivity.this, EditProfileActivity.class);
                inte.putExtra("navigate", false);
                HomeScreenActivity.this.startActivity(inte);
            }
        });
    }

    private void showWelcomeScreenDialog(Context context) {
        if (Prefs.getIsShowTrainingOnHomeScreen() && !isShowingTraining) {
            isShowingTraining = true;

            Utility.showInAppWelcomeNotification(context);

            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
            dialog.setContentView(R.layout.dialog_welcome_screen);
            dialog.setCancelable(false);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            (dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showTrainging();
                    dialog.dismiss();
                }
            });
            dialog.show();
            dialog.getWindow().setAttributes(lp);
        } else {
//            bannerAdContainer.setVisibility(View.VISIBLE);
        }
    }

    private void showTrainging() {
        try {
//                final Display display = getWindowManager().getDefaultDisplay();
//                final Drawable droidSearch = ThemeHelper.getToolbarIcon(HomeScreenActivity.this, (GoogleMaterial.Icon.gmd_search));
//                final Rect droidTargetSearch = new Rect(0, 0, droidSearch.getIntrinsicWidth() * 2, droidSearch.getIntrinsicHeight() * 2);
//                droidTargetSearch.offset((int) (display.getWidth() - Measure.dpToPx(140, HomeScreenActivity.this)), (int) Measure.dpToPx(35, HomeScreenActivity.this));
//
//                final Drawable droidFilter = ThemeHelper.getToolbarIcon(HomeScreenActivity.this, (GoogleMaterial.Icon.gmd_sort));
//                final Rect droidTargetFilter = new Rect(0, 0, droidFilter.getIntrinsicWidth() * 2, droidFilter.getIntrinsicHeight() * 2);
//                droidTargetFilter.offset((int) (display.getWidth() - Measure.dpToPx(80, HomeScreenActivity.this)), (int) Measure.dpToPx(40, HomeScreenActivity.this));

            final SpannableString spnNavBarDescription = new SpannableString(getResources().getString(R.string.training_description_nav_bar));
            spnNavBarDescription.setSpan(new StyleSpan(Typeface.BOLD_ITALIC), spnNavBarDescription.length() - 31, spnNavBarDescription.length(), 0);
            final SpannableString spnHomeScreenAddGroupDescription = new SpannableString(getResources().getString(R.string.training_description_homescreen_add_group));
            spnHomeScreenAddGroupDescription.setSpan(new StyleSpan(Typeface.BOLD_ITALIC), spnHomeScreenAddGroupDescription.length() - 31, spnHomeScreenAddGroupDescription.length(), 0);
            final SpannableString spnHomeScreenNotificationsDescription = new SpannableString(getResources().getString(R.string.training_description_homescreen_notifications));
            spnHomeScreenNotificationsDescription.setSpan(new StyleSpan(Typeface.BOLD_ITALIC), spnHomeScreenNotificationsDescription.length() - 31, spnHomeScreenNotificationsDescription.length(), 0);
            final SpannableString spnHomeScreenEnableToolDescription = new SpannableString(getResources().getString(R.string.training_description_homescreen_enable_tool));
            spnHomeScreenEnableToolDescription.setSpan(new StyleSpan(Typeface.BOLD_ITALIC), spnHomeScreenEnableToolDescription.length() - 31, spnHomeScreenEnableToolDescription.length(), 0);

            final TapTargetSequence sequence = new TapTargetSequence(this)
                    .targets(
                            TapTarget.forToolbarNavigationIcon(binding.contentHomeScreen.appbarLayout.toolbar, getResources().getString(R.string.training_title_nav_bar), spnNavBarDescription)
                                    .dimColor(R.color.black)
                                    .outerCircleColorInt(getResources().getColor(R.color.colorAccent))
                                    .targetCircleColorInt(getResources().getColor(R.color.colorPrimary))
                                    .transparentTarget(true)
                                    .descriptionTextAlpha(1)
                                    .textColor(android.R.color.white)
                                    .tintTarget(true)
                                    .descriptionTextColor(android.R.color.white)
                                    .cancelable(false)
                                    .id(1),
                            TapTarget.forView(imgNotification, getResources().getString(R.string.training_title_homescreen_notifications), spnHomeScreenNotificationsDescription)
                                    .dimColor(R.color.black)
                                    .outerCircleColorInt(getResources().getColor(R.color.colorAccent))
                                    .targetCircleColorInt(getResources().getColor(R.color.colorPrimary))
                                    .transparentTarget(true)
                                    .descriptionTextAlpha(1).tintTarget(true)
                                    .descriptionTextColor(android.R.color.white)
                                    .textColor(android.R.color.white)
                                    .cancelable(false)
                                    .id(2),
                            TapTarget.forView(binding.contentHomeScreen.fab, getResources().getString(R.string.training_title_homescreen_add_group), spnHomeScreenAddGroupDescription)
                                    .dimColor(R.color.black)
                                    .outerCircleColorInt(getResources().getColor(R.color.colorAccent))
                                    .targetCircleColorInt(getResources().getColor(R.color.colorPrimary))
                                    .transparentTarget(true)
                                    .descriptionTextAlpha(1).tintTarget(true)
                                    .descriptionTextColor(android.R.color.white)
                                    .textColor(android.R.color.white)
                                    .cancelable(false)
                                    .id(3)/*,
                            TapTarget.forView(homeScreenSectionsPagerAdapter.getFragment(Defaults.SCREEN_TYPE_GROUPS).getEnableToolButton(), getResources().getString(R.string.training_title_homescreen_enable_tool), spnHomeScreenEnableToolDescription)
                                    .dimColor(R.color.white)
                                    .outerCircleColorInt(getResources().getColor(R.color.colorAccentLightCard))
                                    .targetCircleColorInt(getResources().getColor(R.color.colorPrimaryDark))
                                    .transparentTarget(true)
                                    .descriptionTextAlpha(1)
                                    .tintTarget(true)
                                    .descriptionTextColor(android.R.color.white)
                                    .textColor(android.R.color.white)
                                    .cancelable(false)
                                    .id(4)*/
                                /*,
                                TapTarget.forBounds(droidTargetSearch, getResources().getString(R.string.training_title_search), spnSearchDescription)
                                        .dimColor(android.R.color.transparent)
                                        .outerCircleColorInt(getResources().getColor(R.color.colorAccent))
                                        .targetCircleColorInt(getResources().getColor(R.color.colorPrimary))
                                        .transparentTarget(false)
                                        .tintTarget(false)
                                        .textColor(android.R.color.white)
                                        .cancelable(false)
                                        .descriptionTextAlpha(1)
                                        .descriptionTextColor(android.R.color.white)
                                        .icon(getResources().getColor(R.color.colorAccent))
                                        .id(4),
                                TapTarget.forBounds(droidTargetFilter, getResources().getString(R.string.training_title_sorting), spnSortingDescription)
                                        .dimColor(android.R.color.transparent)
                                        .outerCircleColorInt(getResources().getColor(R.color.colorAccent))
                                        .targetCircleColorInt(getResources().getColor(R.color.colorPrimary))
                                        .transparentTarget(false)
                                        .textColor(android.R.color.white)
                                        .cancelable(false)
                                        .tintTarget(false)
                                        .descriptionTextAlpha(1)
                                        .icon(droidFilter)
                                        .descriptionTextColor(android.R.color.white)
                                        .id(5)*/
                    )
                    .listener(new TapTargetSequence.Listener() {
                        @Override
                        public void onSequenceFinish() {
                            //                        Snackbar.make(binding.contentHomeScreen.fab, "Congratulations! You're educated now!", Snackbar.LENGTH_LONG)
                            //                                .setAction("Action", null).show();
                            Prefs.setIsShowTrainingOnHomeScreen(false);
//                            bannerAdContainer.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                            //                        Log.d("TapTargetView", "Clicked on " + lastTarget.id());
                        }

                        @Override
                        public void onSequenceCanceled(TapTarget lastTarget) {
                        }
                    });
            sequence.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleNotificationClick(Intent intent) {
        try {
            if (intent != null && intent.getExtras() != null) {
                String notificationTitle = intent.getExtras().getString("title", "");
                String notificationMessage = intent.getExtras().getString("message", "");
                String groupId = intent.getExtras().getString("group_id", "");

                Intent detailIntent = new Intent(this, GroupDetailsActivity.class);
                Bundle bundle = new Bundle();
                UserGroupsItem currentGroup = new UserGroupsItem();
                currentGroup.setUserId(Prefs.getUserId());
                currentGroup.setId(groupId);
                currentGroup.setName(" ");
                bundle.putSerializable("currentGroup", currentGroup);
                detailIntent.putExtras(bundle);
                this.startActivity(detailIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
