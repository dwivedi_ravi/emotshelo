package com.thabiso.emotshelo.activities.howitworks;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.activities.base.SharedActivity;
import com.thabiso.emotshelo.databinding.ActivityDetailHowItWorksBinding;
import com.thabiso.emotshelo.models.HowItWorksBO;


public class HowItWorksDetailsActivity extends SharedActivity {

    private ActivityDetailHowItWorksBinding binding;

    private HowItWorksBO objChapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.black_trans80));
        }

        binding = ActivityDetailHowItWorksBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        binding.toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white_24dp));
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent detailIntent = new Intent(DetailActivity.this, ReadChapterActivity.class);
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("currentChapter", objChapter);
//                detailIntent.putExtras(bundle);
//                startActivity(detailIntent);
            }
        });

        binding.toolbarLayout.setTitle("");
        binding.toolbar.setTitle("");

        Typeface robotoBold = Typeface.createFromAsset(getAssets(),
                "roboto_bold.ttf");
        Typeface robotoMedium = Typeface.createFromAsset(getAssets(),
                "roboto_medium.ttf");

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            objChapter = (HowItWorksBO) bundle.getSerializable("currentChapter");
        }

        if (objChapter != null) {
            binding.contentDetailHowItWorks.title.setText(objChapter.getName());
            binding.contentDetailHowItWorks.lblDuration.setText("Mins: " + objChapter.getVersesCount() + ".30");
            binding.contentDetailHowItWorks.lblShortDescription.setText(objChapter.getNameMeaning());
            binding.contentDetailHowItWorks.lblLongDescription.setText(objChapter.getNameTranslation());

//            binding.toolbarLayout.setTitle("");
//            binding.toolbarLayout.setExpandedTitleTextAppearance(R.style.Toolbar_TitleText_Expanded);
//            binding.toolbarLayout.setCollapsedTitleTextAppearance(R.style.Toolbar_TitleText);
//            binding.toolbarLayout.setExpandedTitleTypeface(robotoMedium);
//            binding.toolbarLayout.setCollapsedTitleTypeface(robotoMedium);

            binding.contentDetailHowItWorks.title.setTypeface(robotoMedium);
            binding.contentDetailHowItWorks.lblDuration.setTypeface(robotoBold);
            binding.contentDetailHowItWorks.lblLongDescription.setTypeface(robotoMedium);
            binding.contentDetailHowItWorks.lblShortDescription.setTypeface(robotoBold);

            Glide.with(this).load(objChapter.getImageResource())
                    .into(binding.sportsImage);
        }
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onBackPressed() {
        binding.fab.setVisibility(View.GONE);
        super.onBackPressed();
    }
}
