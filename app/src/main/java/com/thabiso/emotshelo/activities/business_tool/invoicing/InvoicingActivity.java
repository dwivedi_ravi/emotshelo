package com.thabiso.emotshelo.activities.business_tool.invoicing;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.activities.business_tool.EditCompanyActivity;
import com.thabiso.emotshelo.databinding.ActivityInvoicingBinding;
import com.thabiso.emotshelo.models.InvoicingObject;
import com.thabiso.emotshelo.models.addcompany.DataItem;
import com.thabiso.emotshelo.models.addcustomer.ResponseAddEditCustomer;
import com.thabiso.emotshelo.models.addcustomer.customerlist.ResponseGetCustomerList;
import com.thabiso.emotshelo.models.creategroup.ResponseCreateGroup;
import com.thabiso.emotshelo.models.creategroup.UserGroups;
import com.thabiso.emotshelo.models.getmyconnections.RequestGetMyConnections;
import com.thabiso.emotshelo.models.grouplist.ResponseGroupList;
import com.thabiso.emotshelo.models.grouplist.UserGroupsItem;
import com.thabiso.emotshelo.network.ApiCallerUtility;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;
import com.thabiso.emotshelo.util.preferences.Prefs;

public class InvoicingActivity extends AppCompatActivity
        implements OnInvoicingListManagerInteractionListener {

    private ActivityInvoicingBinding binding;

    private MyReceiver objMyReceiver;
    private int screenType;
    private InvoicingSectionsPagerAdapter invoicingSectionsPagerAdapter;
    private InvoicingObject objInvoicingObject;

    private DataItem selectedCompany;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            binding = ActivityInvoicingBinding.inflate(getLayoutInflater());
            setContentView(binding.getRoot());
            setSupportActionBar(binding.appbarLayout.toolbar);

            binding.appbarLayout.toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white_24dp));
            binding.appbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });

            initComponents();
            initializeDataFromDB();
            fetchGroupListFromServer();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initComponents() {
        objMyReceiver = new MyReceiver();
        screenType = Defaults.SCREEN_TYPE_INVOICES;
        objInvoicingObject = new InvoicingObject();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            selectedCompany = (DataItem) bundle.getSerializable("selectedCompany");
        }
        if (selectedCompany != null) {
            binding.appbarLayout.toolbar.setTitle(selectedCompany.getCompanyName());
        }
        registerReciever();

        binding.fab.shrink();
        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleAddButtonClick();
            }
        });
        setupViewPager();
    }

    private void fetchGroupListFromServer() {
//        RequestGroupList objRequestGroupList = new RequestGroupList();
//        objRequestGroupList.setUserId(Prefs.getUserId());
//        ApiCallerUtility.callGroupListApi(this, objRequestGroupList);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_invoicing_activity, menu);
        getMenuInflater().inflate(R.menu.menu_edit_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit_company_details: {
                Intent intent = new Intent(InvoicingActivity.this, EditCompanyActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("selectedCompany", selectedCompany);
                intent.putExtras(bundle);
                startActivity(intent);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_FETCH_INVOICE_LIST_FROM_SERVER);
        filter.addAction(Defaults.ACTION_FETCH_CUSTOMER_LIST_FROM_SERVER);
        filter.addAction(Defaults.ACTION_REFRESH_INVOICE_LIST_SCREEN);
        filter.addAction(Defaults.ACTION_REFRESH_CUSTOMER_LIST_SCREEN);
        filter.addAction(Defaults.ACTION_TOOL_ENABLED_REFRESH_INVOICESCREEN);
        Utility.registerReciever(InvoicingActivity.this, filter, objMyReceiver);
    }

    @Override
    public void onListFragmentInteraction(InvoicingObject item) {
        Toast.makeText(this, " Clicked", Toast.LENGTH_SHORT).show();
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Defaults.ACTION_FETCH_INVOICE_LIST_FROM_SERVER)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        ResponseCreateGroup responseCreateGroup = (ResponseCreateGroup) bundle.getSerializable("ResponseCreateGroup");
                        if (responseCreateGroup != null && responseCreateGroup.getData() != null && responseCreateGroup.getData().getUserGroups() != null) {

                            UserGroups objUserGroups = responseCreateGroup.getData().getUserGroups();

                            UserGroupsItem objUserGroupsItem = new UserGroupsItem();
                            objUserGroupsItem.setNumberOfMembers(objUserGroups.getNumberOfMembers());
                            objUserGroupsItem.setDescription(objUserGroups.getDescription());
                            objUserGroupsItem.setCreatedAt(objUserGroups.getCreatedAt());
                            objUserGroupsItem.setGroupType(objUserGroups.getGroupType());
                            objUserGroupsItem.setGroupAvatar(objUserGroups.getGroupAvatar());
//                            if (objUserGroups.getIsAdmin() != null && objUserGroups.getIsAdmin().trim().equals("1"))
                            if (objUserGroups.isAdmin())
                                objUserGroupsItem.setIsAdmin(true);
                            else
                                objUserGroupsItem.setIsAdmin(false);
                            objUserGroupsItem.setTotalWithdrawalLimit(objUserGroups.getTotalWithdrawalLimit());
                            objUserGroupsItem.setUpdatedAt(objUserGroups.getUpdatedAt());
                            objUserGroupsItem.setUserId(objUserGroups.getUserId());
                            objUserGroupsItem.setInvitationStatus(objUserGroups.getInvitationStatus());
                            objUserGroupsItem.setName(objUserGroups.getName());
                            objUserGroupsItem.setInterestRate(objUserGroups.getInterestRate());
                            objUserGroupsItem.setId(objUserGroups.getId());
                            objUserGroupsItem.setAmountPerRound(objUserGroups.getAmountPerRound());
                            objUserGroupsItem.setTotalAmountPerRound(objUserGroups.getTotalAmountPerRound());
                            objUserGroupsItem.setStatus(objUserGroups.getStatus());

//                            objInvoicingObject.getEnterpriseList().add(0, objUserGroupsItem);
                            objInvoicingObject.getEstimateList().add(0, objUserGroupsItem);
//                            objInvoicingObject.getPersonalList().add(0, objUserGroupsItem);

//                            if (invoicingSectionsPagerAdapter != null && invoicingSectionsPagerAdapter.getFragment(screenType) != null) {
                            if (invoicingSectionsPagerAdapter != null) {
//                                invoicingSectionsPagerAdapter.getFragment(Defaults.SCREEN_TYPE_INVOICES).refreshList(objInvoicingObject);
                                invoicingSectionsPagerAdapter.getFragment(Defaults.SCREEN_TYPE_ESTIMATES).refreshList(objInvoicingObject);
//                                invoicingSectionsPagerAdapter.getFragment(Defaults.SCREEN_TYPE_CLIENTS).refreshList(objInvoicingObject);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (intent.getAction().equals(Defaults.ACTION_FETCH_CUSTOMER_LIST_FROM_SERVER)) {
                try {
                    if (intent != null && intent.getExtras() != null && intent.getExtras().getBundle("bundle") != null) {
                        final Bundle bundle = intent.getExtras().getBundle("bundle");
                        ResponseAddEditCustomer objResponseAddEditCustomer = (ResponseAddEditCustomer) bundle.getSerializable("ResponseAddEditCustomer");
                        if (objResponseAddEditCustomer != null) {
                        }
                    }

                    RequestGetMyConnections objRequestGetMyConnections = new RequestGetMyConnections();
                    objRequestGetMyConnections.setUserId(Prefs.getUserId());
                    if (invoicingSectionsPagerAdapter != null) {
                        ApiCallerUtility.callGetCustomerListApi(InvoicingActivity.this, objRequestGetMyConnections, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (intent.getAction().equals(Defaults.ACTION_REFRESH_INVOICE_LIST_SCREEN)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        ResponseGroupList objResponseGroupList = (ResponseGroupList) bundle.getSerializable("ResponseGroupList");
                        if (objResponseGroupList != null && objResponseGroupList.getData() != null && objResponseGroupList.getData().getUserGroups() != null) {
                            objInvoicingObject.setEstimateList(objResponseGroupList.getData().getUserGroups());
//                            if (invoicingSectionsPagerAdapter != null && invoicingSectionsPagerAdapter.getFragment(screenType) != null) {
                            if (invoicingSectionsPagerAdapter != null) {
                                invoicingSectionsPagerAdapter.getFragment(Defaults.SCREEN_TYPE_ESTIMATES).refreshList(objInvoicingObject);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (intent.getAction().equals(Defaults.ACTION_REFRESH_CUSTOMER_LIST_SCREEN)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        ResponseGetCustomerList objResponseGetCustomerList = (ResponseGetCustomerList) bundle.getSerializable("ResponseGetCustomerList");
                        if (objResponseGetCustomerList != null && objResponseGetCustomerList.getData() != null) {
                            objInvoicingObject.setClientList(objResponseGetCustomerList.getData());

                            if (invoicingSectionsPagerAdapter != null && invoicingSectionsPagerAdapter.getFragment(screenType) != null) {
                                invoicingSectionsPagerAdapter.getFragment(Defaults.SCREEN_TYPE_CLIENTS).refreshList(objInvoicingObject);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (intent.getAction().equals(Defaults.ACTION_TOOL_ENABLED_REFRESH_INVOICESCREEN)) {
                try {
                    if (invoicingSectionsPagerAdapter != null) {
                        invoicingSectionsPagerAdapter.getFragment(Defaults.SCREEN_TYPE_INVOICES).refreshList(objInvoicingObject);
                        invoicingSectionsPagerAdapter.getFragment(Defaults.SCREEN_TYPE_ESTIMATES).refreshList(objInvoicingObject);
                        invoicingSectionsPagerAdapter.getFragment(Defaults.SCREEN_TYPE_CLIENTS).refreshList(objInvoicingObject);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    private void initializeDataFromDB() {
    }

    private void setupViewPager() {
        if (objInvoicingObject != null) {
            invoicingSectionsPagerAdapter = new InvoicingSectionsPagerAdapter(this, getSupportFragmentManager(), objInvoicingObject, this);
            binding.viewPager.setAdapter(invoicingSectionsPagerAdapter);
            binding.appbarLayout.tabs.setupWithViewPager(binding.viewPager);
            binding.viewPager.setCurrentItem(screenType);
            binding.viewPager.setOffscreenPageLimit(2);

            binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    screenType = position;

                    switch (screenType) {
                        case Defaults.SCREEN_TYPE_INVOICES:
                            binding.fab.shrink();
                            binding.fab.setText(getResources().getString(R.string.lbl_add_invoice));
                            binding.fab.extend();
                            break;
                        case Defaults.SCREEN_TYPE_ESTIMATES:
                            binding.fab.shrink();
                            binding.fab.setText(getResources().getString(R.string.lbl_add_estimate));
                            binding.fab.extend();
                            break;
                        case Defaults.SCREEN_TYPE_CLIENTS:
                            binding.fab.shrink();
                            binding.fab.setText(getResources().getString(R.string.lbl_add_customer));
                            binding.fab.extend();
                            break;
                        default:
                            break;
                    }

                }

                @Override
                public void onPageSelected(int pos) {
                    screenType = pos;
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });
        }
    }

    public void handleAddButtonClick() {
        if (binding.appbarLayout.tabs.getSelectedTabPosition() == 0) {
            Intent detailIntent = new Intent(InvoicingActivity.this, AddInvoiceEstimateActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt("screenType", Defaults.SCREEN_TYPE_INVOICES);
            detailIntent.putExtras(bundle);
            InvoicingActivity.this.startActivity(detailIntent);
        } else if (binding.appbarLayout.tabs.getSelectedTabPosition() == 1) {
            Intent detailIntent = new Intent(InvoicingActivity.this, AddInvoiceEstimateActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt("screenType", Defaults.SCREEN_TYPE_ESTIMATES);
            detailIntent.putExtras(bundle);
            InvoicingActivity.this.startActivity(detailIntent);
        } else if (binding.appbarLayout.tabs.getSelectedTabPosition() == 2) {
            Intent detailIntent = new Intent(InvoicingActivity.this, AddNewClientActivity.class);
            InvoicingActivity.this.startActivity(detailIntent);
        }
    }
}
