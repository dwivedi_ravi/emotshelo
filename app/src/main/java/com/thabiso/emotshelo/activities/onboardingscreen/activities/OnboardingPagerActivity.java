package com.thabiso.emotshelo.activities.onboardingscreen.activities;

import android.animation.ArgbEvaluator;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.activities.onboardingscreen.adapter.SectionsPagerAdapter;
import com.thabiso.emotshelo.databinding.ActivityPagerBinding;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Prefs;

public class OnboardingPagerActivity extends AppCompatActivity {

    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    private ActivityPagerBinding binding;

    ImageView[] indicators;

    static final String TAG = "OnboardingPagerActivity";

    int page = 0;   //  to track page position

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.black_trans80));
        }

        binding = ActivityPagerBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
            binding.introBtnNext.setImageDrawable(
                    Utility.tintMyDrawable(ContextCompat.getDrawable(this, R.drawable.ic_chevron_right_24dp), Color.WHITE)
            );

        indicators = new ImageView[]{binding.introIndicator0, binding.introIndicator1, binding.introIndicator2, binding.introIndicator3};

        // Set up the ViewPager with the sections adapter.
        binding.viewPager.setAdapter(mSectionsPagerAdapter);
        binding.viewPager.setCurrentItem(page);
        updateIndicators(page);

//        final int color1 = ContextCompat.getColor(this, R.color.green);
//        final int color2 = ContextCompat.getColor(this, R.color.orange);
//        final int color3 = ContextCompat.getColor(this, R.color.blue);
//        final int color4 = ContextCompat.getColor(this, R.color.cyan);

        final int color1 = ContextCompat.getColor(this, R.color.green_700);
        final int color2 = ContextCompat.getColor(this, R.color.teal);
        final int color3 = ContextCompat.getColor(this, R.color.orange);
        final int color4 = ContextCompat.getColor(this, R.color.blue);

        final int[] colorList = new int[]{color1, color2, color3, color4};

        final ArgbEvaluator evaluator = new ArgbEvaluator();

        binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                int colorUpdate = (Integer) evaluator.evaluate(positionOffset, colorList[position], colorList[position == 3 ? position : position + 1]);
                int colorUpdate = (Integer) evaluator.evaluate(positionOffset, colorList[position], colorList[position == 3 ? position : position + 1]);
                binding.viewPager.setBackgroundColor(colorUpdate);
            }

            @Override
            public void onPageSelected(int position) {
                page = position;
                updateIndicators(page);

                switch (position) {
                    case 0:
                        binding.viewPager.setBackgroundColor(color1);
                        break;
                    case 1:
                        binding.viewPager.setBackgroundColor(color2);
                        break;
                    case 2:
                        binding.viewPager.setBackgroundColor(color3);
                        break;
                    case 3:
                        binding.viewPager.setBackgroundColor(color4);
                        break;
                }

                binding.introBtnNext.setVisibility(position == 3 ? View.GONE : View.VISIBLE);
                binding.introBtnFinish.setVisibility(position == 3 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        binding.introBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page += 1;
                binding.viewPager.setCurrentItem(page, true);
            }
        });

        binding.introBtnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Prefs.setIsUserFirstTimeInSharedPreferences(false);
                finish();
            }
        });

        binding.introBtnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Prefs.setIsUserFirstTimeInSharedPreferences(false);
                //  update 1st time pref
                finish();
            }
        });
    }

    void updateIndicators(int position) {
        for (int i = 0; i < indicators.length; i++) {
            indicators[i].setBackgroundResource(
                    i == position ? R.drawable.indicator_selected : R.drawable.indicator_unselected
            );
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}