package com.thabiso.emotshelo.activities.business_tool.invoicing;

import com.thabiso.emotshelo.models.InvoicingObject;

public interface OnInvoicingListManagerInteractionListener {
    // TODO: Update argument type and name
    void onListFragmentInteraction(InvoicingObject item);
}