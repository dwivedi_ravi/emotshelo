package com.thabiso.emotshelo.activities.groups_tool;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.snackbar.Snackbar;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.adapters.ContactItemRecyclerViewAdapter;
import com.thabiso.emotshelo.databinding.ActivityAddContactScreenBinding;
import com.thabiso.emotshelo.models.creategroup.RequestCreateGroup;
import com.thabiso.emotshelo.models.getmyconnections.Data;
import com.thabiso.emotshelo.models.getmyconnections.ResponseGetMyConnections;
import com.thabiso.emotshelo.models.getmyconnections.UserContactsItem;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;
import com.thabiso.emotshelo.util.preferences.Prefs;

import java.util.ArrayList;
import java.util.List;

public class AddContactScreen extends AppCompatActivity {

    private ActivityAddContactScreenBinding binding;

    BottomSheetBehavior bottomSheetBehavior;
    public ContactItemRecyclerViewAdapter contactItemRecyclerViewAdapter;
    public List<UserContactsItem> memberList;
    private int mColumnCount = 1, totalMembers = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityAddContactScreenBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);
        Utility.setStatusBarColor(this);

        binding.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        binding.toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    binding.viewOverlay.setVisibility(View.GONE);
                } else
                    finish();
            }
        });

        memberList = new ArrayList<>();
        binding.fab.shrink();

//        ADD CURRENT USER TO THE LIST AS A DEFAULT GROUP MEMBER
        UserContactsItem objUserContactsItem = new UserContactsItem();
        objUserContactsItem.setUserId(Prefs.getUserId());
        objUserContactsItem.setUserName(Prefs.getUserName());
        objUserContactsItem.setMobile(Prefs.getMobileNumber());
        objUserContactsItem.setAvatar(Prefs.getUserProfilePicUrl());
        memberList.add(0, objUserContactsItem);
        setOrUpdateRecyclerViewAdapter();

        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getBundle("bundle") != null) {
            final Bundle bundle = getIntent().getExtras().getBundle("bundle");
            totalMembers = bundle.getInt("totalMembers", 0);

            RequestCreateGroup objRequestCreateGroup = (RequestCreateGroup) bundle.getSerializable("objRequestCreateGroup");
            if (objRequestCreateGroup != null) {
//                SHOW CREATE GROUP BUTTON
            }
        }

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    if (isFormValid()) {
                        UserContactsItem objUserContactsItem = new UserContactsItem();
                        objUserContactsItem.setUserId(Prefs.getUniqueIntegerValueFromSharedPrefs() + "");
                        objUserContactsItem.setUserName(binding.contentBottomSheet.editMemberName.getText().toString());
                        objUserContactsItem.setMobile(binding.contentBottomSheet.editMobileNumber.getText().toString());
                        objUserContactsItem.setAvatar("");

                        memberList.add(1, objUserContactsItem);
                        setOrUpdateRecyclerViewAdapter();
                        binding.contentBottomSheet.editMemberName.setText("");
                        binding.contentBottomSheet.editMobileNumber.setText("");
                        Toast.makeText(AddContactScreen.this, "Member Added Successfully", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    binding.fab.extend();
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    binding.viewOverlay.setVisibility(View.VISIBLE);
                }
            }
        });

        initializeBottomSheetLayout();
    }

    private void initializeBottomSheetLayout() {
        // init the bottom sheet behavior
        bottomSheetBehavior = BottomSheetBehavior.from(binding.contentBottomSheet.bottomSheet);
//        // change the state of the bottom sheet
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
//        // set the peek height
//        bottomSheetBehavior.setPeekHeight(340);
//        // set hideable or not
//        bottomSheetBehavior.setHideable(false);

//        binding.contentBottomSheet.closeBottomSheet.setOnClickListener(v -> bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
//                Utility.hideKeyboard());

        binding.contentBottomSheet.closeBottomSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                binding.fab.shrink();
                binding.viewOverlay.setVisibility(View.GONE);
            }
        });

        // set callback for changes
        bottomSheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    Utility.hideKeyboard(AddContactScreen.this, binding.contentBottomSheet.closeBottomSheet.getWindowToken());
                    binding.fab.shrink();
                    binding.viewOverlay.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_group, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_done: {
                if (binding.contentAddContactScreen.recyclerViewMemberList != null && binding.contentAddContactScreen.recyclerViewMemberList.getAdapter() != null) {
                    ResponseGetMyConnections objResponseGetMyConnections = new ResponseGetMyConnections();
                    List<UserContactsItem> userContactsItemList = new ArrayList<>();

                    for (int i = 0; i < binding.contentAddContactScreen.recyclerViewMemberList.getAdapter().getItemCount(); i++) {
                        UserContactsItem objUserContactsItem = memberList.get(i);
                        userContactsItemList.add(objUserContactsItem);
                    }
                    objResponseGetMyConnections.setData(new Data());
                    objResponseGetMyConnections.getData().setUserContacts(userContactsItemList);

                    Bundle mBundle = new Bundle();
                    mBundle.putSerializable("selectionList", objResponseGetMyConnections);
                    mBundle.putBoolean("addDefaultMember", false);
                    Utility.sendUpdateListBroadCast(Defaults.ACTION_BROADCAST_SELECTED_CONTACT_LIST, AddContactScreen.this, mBundle);

                    AddContactScreen.this.finish();
                } else {
                    Toast.makeText(AddContactScreen.this, "No contacts selected", Toast.LENGTH_LONG).show();
                }
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void setOrUpdateRecyclerViewAdapter() {
        if (binding.contentAddContactScreen.recyclerViewMemberList != null && binding.contentAddContactScreen.recyclerViewMemberList.getAdapter() == null) {
            if (mColumnCount <= 1) {
                binding.contentAddContactScreen.recyclerViewMemberList.setLayoutManager(new LinearLayoutManager(AddContactScreen.this));
            } else {
                binding.contentAddContactScreen.recyclerViewMemberList.setLayoutManager(new GridLayoutManager(AddContactScreen.this, mColumnCount));
            }
            contactItemRecyclerViewAdapter = new ContactItemRecyclerViewAdapter(AddContactScreen.this, memberList, null, true);
            binding.contentAddContactScreen.recyclerViewMemberList.setAdapter(contactItemRecyclerViewAdapter);

        } else if (binding.contentAddContactScreen.recyclerViewMemberList != null) {
            binding.contentAddContactScreen.recyclerViewMemberList.getAdapter().notifyItemInserted(1);
            binding.contentAddContactScreen.recyclerViewMemberList.getAdapter().notifyDataSetChanged();
        }
    }

    private boolean isFormValid() {
        if (binding.contentBottomSheet.editMemberName.getVisibility() == View.VISIBLE && binding.contentBottomSheet.editMemberName.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentBottomSheet.editMemberName, "Member name can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentBottomSheet.editMobileNumber.getVisibility() == View.VISIBLE && binding.contentBottomSheet.editMobileNumber.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentBottomSheet.editMobileNumber, "Mobile Number can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (memberList.size() >= totalMembers) {
            Snackbar.make(binding.contentBottomSheet.editMobileNumber, "Total members already added", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            binding.viewOverlay.setVisibility(View.GONE);
        } else
            super.onBackPressed();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                Rect outRect = new Rect();
                binding.contentBottomSheet.bottomSheet.getGlobalVisibleRect(outRect);

                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    binding.viewOverlay.setVisibility(View.GONE);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
}
