package com.thabiso.emotshelo.activities.business_tool.invoicing;

import android.content.Context;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.thabiso.emotshelo.models.InvoicingObject;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class InvoicingSectionsPagerAdapter extends FragmentPagerAdapter {

    private InvoicingObject objInvoicingObject;
    private final Context mContext;
    private OnInvoicingListManagerInteractionListener mListener;

    private InvoicingPlaceholderFragment invoicesFragment;
    private InvoicingPlaceholderFragment estimatesFragment;
    private InvoicingPlaceholderFragment clientsFragment;

    public InvoicingSectionsPagerAdapter(Context context, FragmentManager fm, InvoicingObject objInvoicingObject, OnInvoicingListManagerInteractionListener listener) {
        super(fm);
        mContext = context;
        this.objInvoicingObject = objInvoicingObject;
        this.mListener = listener;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        if (getPageTitle(position).equals(Defaults.TAB_TITLE_INVOICES)) {
            return invoicesFragment = InvoicingPlaceholderFragment.newInstance(position + 1, objInvoicingObject, mListener, Defaults.SCREEN_TYPE_INVOICES);
        } else if (getPageTitle(position).equals(Defaults.TAB_TITLE_ESTIMATES)) {
            return estimatesFragment = InvoicingPlaceholderFragment.newInstance(position + 1, objInvoicingObject, mListener, Defaults.SCREEN_TYPE_ESTIMATES);
        } else if (getPageTitle(position).equals(Defaults.TAB_TITLE_CLIENTS)) {
            Utility.sendUpdateListBroadCast(Defaults.ACTION_FETCH_CUSTOMER_LIST_FROM_SERVER, mContext, null);
            return clientsFragment = InvoicingPlaceholderFragment.newInstance(position + 1, objInvoicingObject, mListener, Defaults.SCREEN_TYPE_CLIENTS);
        } else {
            return invoicesFragment = InvoicingPlaceholderFragment.newInstance(position + 1, objInvoicingObject, mListener, Defaults.SCREEN_TYPE_ESTIMATES);
        }
    }

    // Here we can finally safely save a reference to the created
    // Fragment, no matter where it came from (either getItem() or
    // FragmentManger). Simply save the returned Fragment from
    // super.instantiateItem() into an appropriate reference depending
    // on the ViewPager position.
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
        // save the appropriate reference depending on position
        switch (position) {
            case 0:
                invoicesFragment = (InvoicingPlaceholderFragment) createdFragment;
                break;
            case 1:
                estimatesFragment = (InvoicingPlaceholderFragment) createdFragment;
                break;
            case 2:
                clientsFragment = (InvoicingPlaceholderFragment) createdFragment;
                break;
        }
        return createdFragment;
    }

    public InvoicingPlaceholderFragment getFragment(int screenType) {
        // do work on the referenced Fragments, but first check if they
        // even exist yet, otherwise you'll get an NPE.
        switch (screenType) {
            case Defaults.SCREEN_TYPE_INVOICES:
                return invoicesFragment;
            case Defaults.SCREEN_TYPE_ESTIMATES:
                return estimatesFragment;
            case Defaults.SCREEN_TYPE_CLIENTS:
                return clientsFragment;
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return objInvoicingObject.getSectionList().get(position);
    }

    @Override
    public int getCount() {
        return objInvoicingObject.getSectionList().size();
    }
}