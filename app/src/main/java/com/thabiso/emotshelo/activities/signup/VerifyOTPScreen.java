package com.thabiso.emotshelo.activities.signup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;

import androidx.annotation.Nullable;

import com.google.android.material.snackbar.Snackbar;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.activities.base.SharedActivity;
import com.thabiso.emotshelo.databinding.ActivityVerifyOtpBinding;
import com.thabiso.emotshelo.models.RequestOTP;
import com.thabiso.emotshelo.network.ApiCallerUtility;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;

import java.util.concurrent.TimeUnit;

/**
 * The Main Activity used to display Albums / Media.
 */
public class VerifyOTPScreen extends SharedActivity {

    private ActivityVerifyOtpBinding binding;

    private MyReceiver objMyReceiver;
    private String mobileNumber = "";
    private RequestOTP objRequestOTP;
    private CountDownTimer objCountDownTimer;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityVerifyOtpBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());

        objMyReceiver = new MyReceiver();
        registerReciever();

        initUi();
    }

    private void initUi() {
        if (getIntent() != null && getIntent().getExtras() != null) {
            objRequestOTP = (RequestOTP) getIntent().getExtras().getSerializable("objRequestOTP");

            mobileNumber = objRequestOTP.getMobile_number();
            binding.description.setText(binding.description.getText().toString() + " " + objRequestOTP.getCountry_code() + " " + mobileNumber + ". " + getResources().getString(R.string.verify_otp_please_enter_it));
        }
        binding.toolbar.setTitle(null);
        setSupportActionBar(binding.toolbar);

        Utility.setStatusBarColor(VerifyOTPScreen.this);

        binding.btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFormValid()) {
                    ApiCallerUtility.callVerifyOTPApi(VerifyOTPScreen.this, objRequestOTP, binding.editOTP.getText().toString());
                }
            }
        });

        objCountDownTimer = new CountDownTimer(120000, 1000) {
            public void onTick(long millisUntilFinished) {
                try {
                    if (binding.lblTimer != null)
                        binding.lblTimer.setText("" + String.format("%d:%d",
                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            public void onFinish() {
                try {
                    if (binding.lblTimer != null) {
                        binding.lblTimer.setText("0:00");
                        binding.lblResendSMS.setEnabled(true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();

        binding.lblResendSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (objCountDownTimer != null) {
                    binding.lblResendSMS.setEnabled(false);
                    objCountDownTimer.start();
                    ApiCallerUtility.callRequestOTPApi(VerifyOTPScreen.this, objRequestOTP, false);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        objCountDownTimer.cancel();
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_OTP_RECEIVED);
        Utility.registerReciever(VerifyOTPScreen.this, filter, objMyReceiver);
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Defaults.ACTION_OTP_RECEIVED)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        String otp = bundle.getString("OTP", "");
                        binding.editOTP.getHandler().post(new Runnable() {
                            @Override
                            public void run() {
                                binding.editOTP.setText(otp);
                                if (isFormValid()) {
                                    ApiCallerUtility.callVerifyOTPApi(VerifyOTPScreen.this, objRequestOTP, binding.editOTP.getText().toString());
                                }
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private boolean isFormValid() {
        if (binding.editOTP.getText().toString().trim().length() < 6) {
            Snackbar.make(binding.editOTP, "Wrong OTP", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }
        return true;
    }
}
