package com.thabiso.emotshelo.activities.groups_tool;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.adapters.AdapterListSectioned;
import com.thabiso.emotshelo.databinding.ActivityViewGroupStatementsBinding;
import com.thabiso.emotshelo.models.People;
import com.thabiso.emotshelo.util.Tools;
import com.thabiso.emotshelo.util.preferences.Defaults;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ViewGroupStatementsActivity extends AppCompatActivity {

    private ActivityViewGroupStatementsBinding binding;

    private View parent_view;
    private AdapterListSectioned mAdapter;
    private int screenType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityViewGroupStatementsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        parent_view = findViewById(android.R.id.content);

        initToolbar();
        initComponent();
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Group Statement");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this);
    }

    private void initComponent() {
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setHasFixedSize(true);

        if (getIntent() != null && getIntent().getExtras() != null) {
            screenType = getIntent().getExtras().getInt("screenType");
        }

        switch (screenType) {
            case Defaults.SCREEN_TYPE_INTEREST_STATEMENT:
                getSupportActionBar().setTitle("Interest Statement");
                break;
            case Defaults.SCREEN_TYPE_WITHDRAWALS_PAID_STATEMENT:
                getSupportActionBar().setTitle("Withdrawals Paid Statement");
                break;
            case Defaults.SCREEN_TYPE_WITHDRAWALS_OUTSTANDING_STATEMENT:
                getSupportActionBar().setTitle("Widhdrawals Outstanding Statement");
                break;
        }

        List<People> items = getPeopleData(this);
        items.addAll(getPeopleData(this));
        items.addAll(getPeopleData(this));

        int sect_count = 0;
        int sect_idx = 0;
        List<String> months = getStringsMonth(this);
        for (int i = 0; i < items.size() / 6; i++) {
            items.add(sect_count, new People(months.get(sect_idx), true));
            sect_count = sect_count + 5;
            sect_idx++;
        }

        //set data and list adapter
        mAdapter = new AdapterListSectioned(this, items,screenType);
        binding.recyclerView.setAdapter(mAdapter);

        // on item list clicked
        mAdapter.setOnItemClickListener(new AdapterListSectioned.OnItemClickListener() {
            @Override
            public void onItemClick(View view, People obj, int position) {
                Snackbar.make(parent_view, "Item " + obj.name + " clicked", Snackbar.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Generate dummy data people
     *
     * @param ctx android context
     * @return list of object
     */
    public static List<People> getPeopleData(Context ctx) {
        List<People> items = new ArrayList<>();
        TypedArray drw_arr = ctx.getResources().obtainTypedArray(R.array.people_images);
        String name_arr[] = ctx.getResources().getStringArray(R.array.people_names);

        for (int i = 0; i < drw_arr.length(); i++) {
            People obj = new People();
            obj.image = drw_arr.getResourceId(i, -1);
            obj.name = name_arr[i];
            obj.email = Tools.getEmailFromName(obj.name);
            obj.imageDrw = ctx.getResources().getDrawable(obj.image);
            items.add(obj);
        }
        Collections.shuffle(items);
        return items;
    }

    public static List<String> getStringsMonth(Context ctx) {
        List<String> items = new ArrayList<>();
        String arr[] = ctx.getResources().getStringArray(R.array.month);
        for (String s : arr) items.add(s);
        Collections.shuffle(items);
        return items;
    }
}
