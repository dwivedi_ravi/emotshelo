package com.thabiso.emotshelo.activities.groups_tool;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Dimension;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.appbar.AppBarLayout;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.activities.base.BaseActivity;
import com.thabiso.emotshelo.activities.personal_tool.PersonalExpenseManagerScreen;
import com.thabiso.emotshelo.adapters.DetailAdsAdapter;
import com.thabiso.emotshelo.databinding.ActivityGroupDetailBinding;
import com.thabiso.emotshelo.models.RequestRotateGroup;
import com.thabiso.emotshelo.models.ads.ModelAds;
import com.thabiso.emotshelo.models.creategroup.UserGroups;
import com.thabiso.emotshelo.models.groupdetails.RequestGroupDetails;
import com.thabiso.emotshelo.models.groupdetails.ResponseGroupDetails;
import com.thabiso.emotshelo.models.groupdetails.UserMembersItem;
import com.thabiso.emotshelo.models.groupdetails.UserProfile;
import com.thabiso.emotshelo.models.grouplist.UserGroupsItem;
import com.thabiso.emotshelo.models.groupstatus.RequestUpdateGroupStatus;
import com.thabiso.emotshelo.network.ApiCallerUtility;
import com.thabiso.emotshelo.util.Measure;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;
import com.thabiso.emotshelo.util.preferences.Prefs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class GroupDetailsActivity extends BaseActivity implements View.OnClickListener {

    private ActivityGroupDetailBinding binding;

    private Typeface robotoBold, robotoRegular, robotoMedium, robotoCondensed_bold;
    private MyReceiver objMyReceiver;
    private UserGroupsItem clickedAndPassedGroup;
    private UserGroups objUserGroups;
    private String currency = "";
    private String groupStatus = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityGroupDetailBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        objMyReceiver = new MyReceiver();
        registerReciever();

        binding.toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white_24dp));
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        this.robotoBold = Typeface.createFromAsset(getAssets(),
                "roboto_bold.ttf");
        this.robotoRegular = Typeface.createFromAsset(getAssets(),
                "roboto_regular.ttf");
        this.robotoMedium = Typeface.createFromAsset(getAssets(),
                "roboto_medium.ttf");
        this.robotoCondensed_bold = Typeface.createFromAsset(getAssets(),
                "robotocondensed_bold.ttf");

        binding.toolbarLayout.setExpandedTitleTextAppearance(R.style.Toolbar_TitleText_Expanded);
        binding.toolbarLayout.setCollapsedTitleTextAppearance(R.style.Toolbar_TitleText);

        binding.toolbarLayout.setCollapsedTitleTypeface(robotoBold);
        binding.contentGroupDetail.title.setTypeface(robotoMedium);
        binding.contentGroupDetail.verseCount.setTypeface(robotoMedium);
        binding.contentGroupDetail.newsTitle.setTypeface(robotoRegular);
        binding.contentGroupDetail.subTitle.setTypeface(robotoRegular);
        binding.contentGroupDetail.btnViewGroupInterestStatements.setTypeface(robotoMedium);
        binding.contentGroupDetail.btnViewWithdrawalsPaidStatements.setTypeface(robotoMedium);
        binding.contentGroupDetail.btnViewWithdrawalsOutstandingStatements.setTypeface(robotoMedium);
        binding.contentGroupDetail.btnRequestWithdrawal.setTypeface(robotoMedium);
        binding.contentGroupDetail.btnEndGroupAndSplitMoney.setTypeface(robotoMedium);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            clickedAndPassedGroup = (UserGroupsItem) bundle.getSerializable("currentGroup");
        }

        if (clickedAndPassedGroup != null) {
            RequestGroupDetails objRequestGroupDetails = new RequestGroupDetails();
            objRequestGroupDetails.setUserId(clickedAndPassedGroup.getUserId());
            objRequestGroupDetails.setGroupId(clickedAndPassedGroup.getId());
            ApiCallerUtility.callGroupDetailsApi(this, objRequestGroupDetails);
        }

        binding.appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    if (objUserGroups != null)
                        binding.toolbarLayout.setTitle(objUserGroups.getName());
                    else
                        binding.toolbarLayout.setTitle(clickedAndPassedGroup.getName());
                    isShow = true;
                } else if (isShow) {
                    binding.toolbarLayout.setTitle(" ");//careful there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });
        binding.fabNext.shrink();

        binding.fabNext.setOnClickListener(this);
        binding.contentGroupDetail.verseCount.setOnClickListener(this);
        binding.contentGroupDetail.btnViewGroupInterestStatements.setOnClickListener(this);
        binding.contentGroupDetail.btnViewWithdrawalsPaidStatements.setOnClickListener(this);
        binding.contentGroupDetail.btnViewWithdrawalsOutstandingStatements.setOnClickListener(this);
        binding.contentGroupDetail.btnRequestWithdrawal.setOnClickListener(this);
        binding.contentGroupDetail.btnEndGroupAndSplitMoney.setOnClickListener(this);

        binding.contentGroupDetail.btnStartOrCloseGroup.setOnClickListener(this);
        binding.contentGroupDetail.btnDeleteGroup.setOnClickListener(this);
        binding.contentGroupDetail.btnRotateGroup.setOnClickListener(this);
        binding.contentGroupDetail.btnAcceptInvitation.setOnClickListener(this);
        binding.contentGroupDetail.btnRejectInvitation.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_group_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_activity_log) {
            Intent intent = new Intent(GroupDetailsActivity.this, TimelineSimple.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_change_group_icon) {
            Intent intent = new Intent(GroupDetailsActivity.this, EditGroupDetailsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("currentGroup", objUserGroups);
            intent.putExtras(bundle);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_share_group_details) {
            try {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "*Money Chaps Group Join Invite*\n\n" + "Group Name: *" + binding.contentGroupDetail.title.getText().toString() + "*\n\n" + binding.contentGroupDetail.subTitle.getText().toString() + "\n\n" + binding.contentGroupDetail.newsTitle.getText().toString() + "\n\n" + "Join this group using below Link:\n" + "https://moneychaps.com/groups/09j4h6fh76hfh");
                sendIntent.setType("text/plain");
                this.startActivity(Intent.createChooser(sendIntent, this.getResources().getText(R.string.send_to)));
            } catch (Exception e) {
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setValues(ResponseGroupDetails objResponseGroupDetails) {
        try {
            objUserGroups = objResponseGroupDetails.getData().getUserGroups();
            List<UserMembersItem> groupMembers = objUserGroups.getUserMembers();

            if (objUserGroups != null) {
                binding.toolbarLayout.setTitle(objUserGroups.getName());
                binding.toolbarLayout.setExpandedTitleColor(getResources().getColor(R.color.transparent));
                binding.contentGroupDetail.title.setText(objUserGroups.getName());
                binding.contentGroupDetail.verseCount.setVisibility(View.VISIBLE);
                binding.contentGroupDetail.verseCount.setText("Members: " + objUserGroups.getNumberOfMembers());

                String text = "";
                if (objUserGroups.getStartDate() != null && !objUserGroups.getStartDate().equals(""))
                    text += "Start Date: " + objUserGroups.getStartDate().substring(0, 10) + "\n";

                switch (objUserGroups.getGroupType()) {
                    case Defaults.SUB_TOOL_ID_BIRTHDAY + "":
                        text += "\nGroup Type: Birthday\n";
                        break;
                    case Defaults.SUB_TOOL_ID_STOCKVEL + "":
                        text += "\nGroup Type: Stokvel\n";
                        break;
                    case Defaults.SUB_TOOL_ID_WEDDINGS + "":
                        text += "\nGroup Type: Weddings\n";
                        break;
                    case Defaults.SUB_TOOL_ID_FUNERALS + "":
                        text += "\nGroup Type: Funerals\n";
                        break;
                    case Defaults.SUB_TOOL_ID_BABY_SHOWER + "":
                        text += "\nGroup Type: Baby Shower\n";
                        break;
                    case Defaults.SUB_TOOL_ID_HOLIDAY_ROAD_TRIP + "":
                        text += "\nGroup Type: Holiday/Road Trip\n";
                        break;
                    case Defaults.SUB_TOOL_ID_PEER_TO_PEER + "":
                        text += "\nGroup Type: Peer To Peer Savings\n";
                        binding.contentGroupDetail.containerPeerToPeerButtons.setVisibility(View.VISIBLE);
                        break;
                }

                switch (objUserGroups.getCurrency()) {
                    case "1":
                        currency = "P";
                        text += "\nCurrency: Botswana Pula (" + currency + ")\n";
                        break;
                    case "2":
                        currency = "R";
                        text += "\nCurrency: South African Rand (" + currency + ")\n";
                        break;
                    case "3":
                        currency = "$";
                        text += "\nCurrency: United States Dollar (" + currency + ")\n";
                        break;
                    case "4":
                        currency = "₹";
                        text += "\nCurrency: Indian National Rupees (" + currency + ")\n";
                        break;
                }

                text += "\nNumber of members: " + objUserGroups.getNumberOfMembers() + "\n";
                text += "\nNumber of rounds: " + objUserGroups.getNumberOfRounds() + "\n";
                text += "\nMember contribution per round: " + objUserGroups.getAmountPerRound() + " " + currency + "\n";
                text += "\nTotal amount per round: " + objUserGroups.getTotalAmountPerRound() + " " + currency + "\n";

                switch (objUserGroups.getMode()) {
                    case "weekly":
                    case "1":
                        text += "\nMode: Weekly\n";
                        break;
                    case "monthly":
                    case "2":
                        text += "\nMode: Monthly\n";
                        break;
                    case "yearly":
                    case "3":
                        text += "\nMode: Yearly\n";
                        break;
                }

                if (objUserGroups.getEndDate() != null && !objUserGroups.getEndDate().equals(""))
                    text += "\nEnd Date: " + objUserGroups.getEndDate().substring(0, 10) + "\n";

                text += "\nGroup Rotating Policy:\n\nAdmin will be rotating the group once all deposits are made in the group. And the money will go to the current receiver of this cycle.\n";

                binding.contentGroupDetail.newsTitle.setText(text);

                binding.contentGroupDetail.subTitle.setText("Group Description: " + objUserGroups.getDescription());
                try {
                    if (objUserGroups.getTotalAmount().contains(".")) {
                        objUserGroups.setTotalAmount(objUserGroups.getTotalAmount().substring(0, objUserGroups.getTotalAmount().lastIndexOf('.')));
                    }
                    if (objUserGroups.getAmountPerRound().contains(".")) {
                        objUserGroups.setAmountPerRound(objUserGroups.getAmountPerRound().substring(0, objUserGroups.getAmountPerRound().lastIndexOf('.')));
                    }
//                    Long amount = Long.parseLong(objUserGroups.getTotalAmount()) - Long.parseLong(objUserGroups.getAmountPerRound());
//                    binding.txtAmount.setText(currency + amount);
                    binding.txtAmount.setText(currency + objUserGroups.getTotalAmount());
                } catch (Exception e) {
                    e.printStackTrace();
                    if (objUserGroups.getTotalAmount().contains("."))
                        binding.txtAmount.setText(currency + objUserGroups.getTotalAmount().substring(0, objUserGroups.getTotalAmount().lastIndexOf('.')));
                    else
                        binding.txtAmount.setText(currency + objUserGroups.getTotalAmount());
                }
            }

            if (groupMembers != null && groupMembers.size() > 0) {
                for (int i = 0; i < groupMembers.size(); i++) {
                    try {
                        LinearLayout view = binding.circleConstraintLayout.findViewById(i);
                        view.removeAllViews();
                        binding.circleConstraintLayout.removeView(view);
                    } catch (Exception e) {
                    }
                }
                ConstraintLayout.LayoutParams params =
                        (ConstraintLayout.LayoutParams) binding.txtAmount.getLayoutParams();

                int membersCount = groupMembers.size();
                int radius;
                if (membersCount > 8)
                    radius = 140;
                else
                    radius = 120;

                Collections.sort(groupMembers, new Comparator<UserMembersItem>() {
                    public int compare(UserMembersItem obj1, UserMembersItem obj2) {
                        try {
                            return (Integer.parseInt(obj1.getGroupMemberDisplayIndex()) > Integer.parseInt(obj2.getGroupMemberDisplayIndex()) ? 1 : -1);     //ascending
//                            return (Integer.parseInt(obj1.getGroupMemberDisplayIndex()) > Integer.parseInt(obj2.getGroupMemberDisplayIndex()) ? -1 : 1);     //descending
                        } catch (NumberFormatException e) {
                            return 1;
                        }
                    }
                });

                RequestOptions requestOptions = Utility.getCustomizedRequestOptions(R.color.mdtp_line_background, R.drawable.generic_profile_place_holder);
                for (int i = 0; i < membersCount; i++) {
                    LinearLayout linearLayout = new LinearLayout(this);
                    linearLayout.setOrientation(LinearLayout.VERTICAL);

                    TextView textView = new TextView(this);
                    textView.setTextColor(getResources().getColor(R.color.white));
                    textView.setTypeface(robotoCondensed_bold);
                    textView.setTextSize(Dimension.SP, 12);
                    textView.setSingleLine(true);
                    textView.setEllipsize(TextUtils.TruncateAt.END);
                    textView.setLayoutParams(new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT));
                    textView.setGravity(Gravity.CENTER_HORIZONTAL);

                    CircleImageView circleImageView = new CircleImageView(this);
                    circleImageView.setBorderColor(getResources().getColor(R.color.white));
                    circleImageView.setBorderWidth((int) Measure.dpToPx(4, this));

                    ConstraintLayout.LayoutParams newParams;
                    circleImageView.setLayoutParams(new LinearLayout.LayoutParams(
                            (int) Measure.dpToPx(60, this),
                            (int) Measure.dpToPx(60, this)));
                    newParams = new ConstraintLayout.LayoutParams(
                            (int) Measure.dpToPx(75, this),
                            (int) Measure.dpToPx(75, this));

                    if (groupMembers.get(i).getUserProfile().getUserId().equals(objUserGroups.getCurrentReceiver())) {
                        circleImageView.setBorderColor(getResources().getColor(R.color.green));
                    } else if (groupMembers.get(i).getUserProfile().getUserId().equals(objUserGroups.getNextReceiver())) {
                        circleImageView.setBorderColor(getResources().getColor(R.color.yellow));
                    } else {
                        circleImageView.setBorderColor(getResources().getColor(R.color.red));
                    }

                    textView.setText(groupMembers.get(i).getUserProfile().getName());
                    linearLayout.setTag(groupMembers.get(i).getUserProfile());
                    linearLayout.setId(i);

                    Glide.with(this).setDefaultRequestOptions(requestOptions).load(groupMembers.get(i).getUserProfile().getAvtar())
                            .into(circleImageView);

                    newParams.circleConstraint = R.id.txtAmount;
                    newParams.circleRadius = (int) Measure.dpToPx(radius, this);
                    int equalAngle = 360 / membersCount;
                    newParams.circleAngle = equalAngle * i;

                    linearLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent detailIntent = new Intent(GroupDetailsActivity.this, PersonalExpenseManagerScreen.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("user_id", ((UserProfile) v.getTag()).getUserId() + "");
                            bundle.putString("group_id", objUserGroups.getId());
                            bundle.putString("group_type", objUserGroups.getGroupType());
                            detailIntent.putExtras(bundle);
                            startActivity(detailIntent);
                        }
                    });

                    linearLayout.setGravity(Gravity.CENTER_HORIZONTAL);
                    linearLayout.setHorizontalGravity(Gravity.CENTER_HORIZONTAL);
                    linearLayout.addView(circleImageView, 0);
                    linearLayout.addView(textView, 1);
                    binding.circleConstraintLayout.addView(linearLayout, i, newParams);
                }
            }

//            BELOW LINE TEMPORARILY COMMENTED AS IS_ADMIN FLAG IS ALWAYS COMING AS 0 FROM THE SERVER.
//            if (objUserGroups.isAdmin()) {
//            if ((objUserGroups.getGroupAdmin() != null &&  objUserGroups.getGroupAdmin().trim().equals(Prefs.getUserId())) || (objUserGroups.getGroupSubAdmin() != null &&  objUserGroups.getGroupSubAdmin().trim().equals(Prefs.getUserId()))){
            if (objUserGroups.getGroupAdmin() != null && objUserGroups.getGroupAdmin().trim().equals(Prefs.getUserId())) {
                binding.contentGroupDetail.containerStartStopButtons.setVisibility(View.VISIBLE);
                groupStatus = objUserGroups.getStatus().trim();
                binding.contentGroupDetail.btnRotateGroup.setVisibility(View.GONE);

                if (groupStatus.equals(Defaults.GROUP_STATUS_PENDING_TO_START)) {
                    binding.contentGroupDetail.btnStartOrCloseGroup.setText(getResources().getString(R.string.lbl_start_group));
                } else if (groupStatus.equals(Defaults.GROUP_STATUS_STARTED)) {
                    binding.contentGroupDetail.btnStartOrCloseGroup.setText(getResources().getString(R.string.lbl_close_group));
                    binding.contentGroupDetail.btnRotateGroup.setVisibility(View.VISIBLE);
                } else if (groupStatus.equals(Defaults.GROUP_STATUS_CLOSED)) {
                    binding.contentGroupDetail.btnStartOrCloseGroup.setVisibility(View.GONE);
                }
            } else {
                boolean found = false;
                for (int j = 0; j < objUserGroups.getUserMembers().size(); j++) {
                    if (objUserGroups.getUserMembers().get(j).getUserId().trim().equals(Prefs.getUserId())) {
                        if (objUserGroups.getUserMembers().get(j).getInvitationStatus() != null && objUserGroups.getUserMembers().get(j).getInvitationStatus().trim().equals(Defaults.INVITATION_STATUS_PENDING_OR_INVITED)) {
                            binding.contentGroupDetail.containerAcceptRejectButtons.setVisibility(View.VISIBLE);
                            found = true;
                        }
                        break;
                    }
                }
                if (!found)
                    binding.contentGroupDetail.containerAcceptRejectButtons.setVisibility(View.GONE);
            }

            setAdsAdapter();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_FILL_GROUP_DETAILS_DATA);
        Utility.registerReciever(GroupDetailsActivity.this, filter, objMyReceiver);
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Defaults.ACTION_FILL_GROUP_DETAILS_DATA)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        ResponseGroupDetails objResponseGroupDetails = (ResponseGroupDetails) bundle.getSerializable("ResponseGroupDetails");
                        setValues(objResponseGroupDetails);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }

    private void showDialogChampion() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_achievement_champion);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        ((TextView) dialog.findViewById(R.id.lblGroupBalance)).setText("Group Balance\n" + binding.txtAmount.getText().toString() + ".00");
        ((TextView) dialog.findViewById(R.id.lblWithdrawalLimit)).setText("Withdrawal Limit: " + binding.txtAmount.getText().toString());
        dialog.findViewById(R.id.bt_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Request Sent", Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    private void setAdsAdapter() {
        try {
            binding.contentGroupDetail.rcvHorizontalAds.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

            List<ModelAds> items = getModelAdsDate(this);
            binding.contentGroupDetail.rcvHorizontalAds.setAdapter(new DetailAdsAdapter(this, items, R.layout.item_snap_basic));
            binding.contentGroupDetail.containerAds.setVisibility(View.VISIBLE);
//            binding.contentGroupDetail.rcvHorizontalAds.setOnFlingListener(null);
//            new StartSnapHelper().attachToRecyclerView(binding.contentGroupDetail.rcvHorizontalAds);
        } catch (Exception ex) {
        }
    }

    private List<ModelAds> getModelAdsDate(Context ctx) {
        List<ModelAds> items = new ArrayList<>();
        TypedArray drw_arr = ctx.getResources().obtainTypedArray(R.array.sample_images);
        String name_arr[] = ctx.getResources().getStringArray(R.array.sample_images_name);
        String date_arr[] = ctx.getResources().getStringArray(R.array.general_date);
        for (int i = 0; i < drw_arr.length(); i++) {
            ModelAds obj = new ModelAds();
            obj.image = drw_arr.getResourceId(i, -1);
            obj.name = name_arr[i];
            obj.brief = "brief";
            obj.counter = Prefs.getUniqueIntegerValueFromSharedPrefs();
            obj.imageDrw = ctx.getResources().getDrawable(obj.image);
            items.add(obj);
        }
        Collections.shuffle(items);
        return items;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_next: {
                if (objUserGroups != null) {
                    Intent detailIntent = new Intent(GroupDetailsActivity.this, GroupExpenseManagerScreen.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("user_id", objUserGroups.getUserId());
                    bundle.putString("group_id", objUserGroups.getId());
                    bundle.putString("group_type", objUserGroups.getGroupType());
                    detailIntent.putExtras(bundle);
                    startActivity(detailIntent);
                }
            }
            break;
            case R.id.verseCount: {

            }
            break;
            case R.id.btnViewGroupInterestStatements: {
                Intent intent = new Intent(GroupDetailsActivity.this, ViewGroupStatementsActivity.class);
                intent.putExtra("screenType", Defaults.SCREEN_TYPE_INTEREST_STATEMENT);
                startActivity(intent);
            }
            break;
            case R.id.btnViewWithdrawalsPaidStatements: {
                Intent intent = new Intent(GroupDetailsActivity.this, ViewGroupStatementsActivity.class);
                intent.putExtra("screenType", Defaults.SCREEN_TYPE_WITHDRAWALS_PAID_STATEMENT);
                startActivity(intent);
            }
            break;
            case R.id.btnViewWithdrawalsOutstandingStatements: {
                Intent intent = new Intent(GroupDetailsActivity.this, ViewGroupStatementsActivity.class);
                intent.putExtra("screenType", Defaults.SCREEN_TYPE_WITHDRAWALS_OUTSTANDING_STATEMENT);
                startActivity(intent);
            }
            break;
            case R.id.btnRequestWithdrawal: {
                showDialogChampion();
            }
            break;
            case R.id.btnEndGroupAndSplitMoney: {
                Intent intent = new Intent(GroupDetailsActivity.this, EndGroupAndSplitMoney.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("currentGroup", objUserGroups);
                bundle.putString("displayGroupBalance", binding.txtAmount.getText().toString() + ".00");
                bundle.putString("currency", currency);
                intent.putExtras(bundle);
                startActivity(intent);
            }
            break;
            case R.id.btnStartOrCloseGroup: {
                RequestUpdateGroupStatus objRequestUpdateGroupStatus = new RequestUpdateGroupStatus();
                objRequestUpdateGroupStatus.setUserId(Prefs.getUserId());
                objRequestUpdateGroupStatus.setGroupId(objUserGroups.getId());

                if (groupStatus.equals(Defaults.GROUP_STATUS_PENDING_TO_START)) {
                    objRequestUpdateGroupStatus.setStatus(Defaults.GROUP_STATUS_STARTED);
                } else if (groupStatus.equals(Defaults.GROUP_STATUS_STARTED)) {
                    objRequestUpdateGroupStatus.setStatus(Defaults.GROUP_STATUS_CLOSED);
                } else if (groupStatus.equals(Defaults.GROUP_STATUS_CLOSED) || groupStatus.equals(Defaults.GROUP_STATUS_DELETED)) {
                    binding.contentGroupDetail.btnStartOrCloseGroup.setVisibility(View.GONE);
                }
                ApiCallerUtility.callUpdateGroupStatusApi(GroupDetailsActivity.this, objRequestUpdateGroupStatus);
            }
            break;
            case R.id.btnRotateGroup: {
                RequestRotateGroup objRequestRotateGroup = new RequestRotateGroup();
                objRequestRotateGroup.setUserId(Prefs.getUserId());
                objRequestRotateGroup.setGroupId(objUserGroups.getId());
                ApiCallerUtility.callRotateGroupApi(GroupDetailsActivity.this, objRequestRotateGroup);
            }
            break;
            case R.id.btnDeleteGroup: {
                RequestUpdateGroupStatus objRequestUpdateGroupStatus = new RequestUpdateGroupStatus();
                objRequestUpdateGroupStatus.setUserId(Prefs.getUserId());
                objRequestUpdateGroupStatus.setGroupId(objUserGroups.getId());
                objRequestUpdateGroupStatus.setStatus(Defaults.GROUP_STATUS_DELETED);
                ApiCallerUtility.callUpdateGroupStatusApi(GroupDetailsActivity.this, objRequestUpdateGroupStatus);
            }
            break;
            case R.id.btnAcceptInvitation: {
                RequestUpdateGroupStatus objRequestUpdateGroupStatus = new RequestUpdateGroupStatus();
                objRequestUpdateGroupStatus.setUserId(Prefs.getUserId());
                objRequestUpdateGroupStatus.setGroupId(objUserGroups.getId());
                objRequestUpdateGroupStatus.setStatus(Defaults.INVITATION_STATUS_ACCEPTED_OR_JOINED);
                ApiCallerUtility.callUpdateInvitationStatusApi(GroupDetailsActivity.this, objRequestUpdateGroupStatus);
            }
            break;
            case R.id.btnRejectInvitation: {
                RequestUpdateGroupStatus objRequestUpdateGroupStatus = new RequestUpdateGroupStatus();
                objRequestUpdateGroupStatus.setUserId(Prefs.getUserId());
                objRequestUpdateGroupStatus.setGroupId(objUserGroups.getId());
                objRequestUpdateGroupStatus.setStatus(Defaults.INVITATION_STATUS_REJECTED);
                ApiCallerUtility.callUpdateInvitationStatusApi(GroupDetailsActivity.this, objRequestUpdateGroupStatus);
            }
            break;
        }
    }
}
