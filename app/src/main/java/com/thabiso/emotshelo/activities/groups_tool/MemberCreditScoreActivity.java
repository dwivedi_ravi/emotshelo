package com.thabiso.emotshelo.activities.groups_tool;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.databinding.ActivityMemberCreditScoreBinding;

public class MemberCreditScoreActivity extends AppCompatActivity {

    private ActivityMemberCreditScoreBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMemberCreditScoreBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());

        initToolbar();
    }

    private void initToolbar() {
        binding.appbarLayout.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
//        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        setSupportActionBar(binding.appbarLayout.toolbar);
        getSupportActionBar().setTitle("Credit Score");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        Tools.setSystemBarColor(this, android.R.color.white);
//        Tools.setSystemBarLight(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_profile, menu);
//        Tools.changeMenuIconColor(menu, getResources().getColor(R.color.light_green_A700));
//        Tools.changeOverflowMenuIconColor(toolbar, getResources().getColor(R.color.light_green_A700));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }
}
