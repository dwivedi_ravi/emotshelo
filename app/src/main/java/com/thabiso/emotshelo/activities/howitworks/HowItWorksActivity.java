package com.thabiso.emotshelo.activities.howitworks;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.activities.base.SharedActivity;
import com.thabiso.emotshelo.adapters.HowItWorksAdapter;
import com.thabiso.emotshelo.database.DBHandler;
import com.thabiso.emotshelo.databinding.ActivityHowItWorksBinding;
import com.thabiso.emotshelo.models.HowItWorksBO;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;
import com.thabiso.emotshelo.util.preferences.Prefs;

import java.util.ArrayList;

public class HowItWorksActivity extends SharedActivity {

    private ActivityHowItWorksBinding binding;

    private ArrayList<HowItWorksBO> mChapterData;
    private HowItWorksAdapter mAdapter;
    private DBHandler dbHandler;
    private AlertDialog progressDialog;
    private MyReceiver objMyReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityHowItWorksBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());

        initComponents();
    }

    private void initComponents() {
        try {
            objMyReceiver = new MyReceiver();
            registerReciever();

            setSupportActionBar(binding.appbarLayout.toolbar);
            Utility.setStatusBarColor(HowItWorksActivity.this);
            binding.appbarLayout.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
            binding.appbarLayout.toolbar.setTitleTextColor(getResources().getColor(R.color.white));
            binding.appbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });

            binding.contentHowItWorks.recyclerView.setLayoutManager(new LinearLayoutManager(this));
            mChapterData = new ArrayList<>();
            mAdapter = new HowItWorksAdapter(this, mChapterData);
            binding.contentHowItWorks.recyclerView.setAdapter(mAdapter);


            binding.fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });

            initializeData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeData() {
        //Clear the existing data (to avoid duplication)
        mChapterData.clear();

        HowItWorksBO objChapter = new HowItWorksBO();
        objChapter.setChapterNumber(1);
        objChapter.setChapterSummary("Description");
        objChapter.setName("Introduction");
        objChapter.setNameMeaning("Introduction to Money Chaps");
        objChapter.setNameTranslation(getResources().getString(R.string.lorem_ipsum));
        objChapter.setVersesCount(5);
        objChapter.setImageResource(R.drawable.ad_image_1);
        mChapterData.add(objChapter);

        objChapter = new HowItWorksBO();
        objChapter.setChapterNumber(2);
        objChapter.setChapterSummary("Description");
        objChapter.setName("How To Create Groups?");
        objChapter.setNameMeaning("Know how you'll be creating groups");
        objChapter.setNameTranslation(getResources().getString(R.string.lorem_ipsum));
        objChapter.setVersesCount(5);
        objChapter.setImageResource(R.drawable.ad_image_2);
        mChapterData.add(objChapter);

        objChapter = new HowItWorksBO();
        objChapter.setChapterNumber(3);
        objChapter.setChapterSummary("Description");
        objChapter.setName("How to Use My Business Tools?");
        objChapter.setNameMeaning("Know how you'll be using my business tools");
        objChapter.setNameTranslation(getResources().getString(R.string.lorem_ipsum));
        objChapter.setVersesCount(5);
        objChapter.setImageResource(R.drawable.ad_image_1);
        mChapterData.add(objChapter);

        objChapter = new HowItWorksBO();
        objChapter.setChapterNumber(4);
        objChapter.setChapterSummary("Description");
        objChapter.setName("How to Use Personal Tool?");
        objChapter.setNameMeaning("Know how you'll be using personal tools");
        objChapter.setNameTranslation(getResources().getString(R.string.lorem_ipsum));
        objChapter.setVersesCount(5);
        objChapter.setImageResource(R.drawable.ad_image_2);
        mChapterData.add(objChapter);

        objChapter = new HowItWorksBO();
        objChapter.setChapterNumber(5);
        objChapter.setChapterSummary("Description");
        objChapter.setName("How It Works?");
        objChapter.setNameMeaning("Know how you can add tools from the market place");
        objChapter.setNameTranslation(getResources().getString(R.string.lorem_ipsum));
        objChapter.setVersesCount(5);
        objChapter.setImageResource(R.drawable.ad_image_1);
        mChapterData.add(objChapter);


        mAdapter.notifyDataSetChanged();

        if (Prefs.getCurrentHowItWorksVideoIndexFromSharedPreference() >= 0 && Prefs.getCurrentHowItWorksVideoIndexFromSharedPreference() <= mAdapter.getItemCount())
            binding.contentHowItWorks.recyclerView.scrollToPosition(Prefs.getCurrentHowItWorksVideoIndexFromSharedPreference());
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_REFRESH_GROUP_LIST_SCREEN);
        filter.addAction(Defaults.ACTION_FETCH_GROUP_LIST_FROM_SERVER);
        filter.addAction(Defaults.ACTION_TOOL_ENABLED_REFRESH_HOMESCREEN);
        Utility.registerReciever(HowItWorksActivity.this, filter, objMyReceiver);
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Defaults.ACTION_REFRESH_GROUP_LIST_SCREEN)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
//                        ResponseGroupList objResponseGroupList = (ResponseGroupList) bundle.getSerializable("ResponseGroupList");
//                        if (objResponseGroupList != null && objResponseGroupList.getData() != null && objResponseGroupList.getData().getUserGroups() != null) {
//
//                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
