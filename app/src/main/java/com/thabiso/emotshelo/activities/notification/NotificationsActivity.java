package com.thabiso.emotshelo.activities.notification;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.adapters.NotificationItemRecyclerViewAdapter;
import com.thabiso.emotshelo.databinding.ActivityNotificationsBinding;
import com.thabiso.emotshelo.models.getallnotifications.RequestGetAllNotification;
import com.thabiso.emotshelo.models.getallnotifications.ResponseGetAllNotifications;
import com.thabiso.emotshelo.models.getallnotifications.UserNotificationsItem;
import com.thabiso.emotshelo.network.ApiCallerUtility;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;
import com.thabiso.emotshelo.util.preferences.Prefs;

import java.util.ArrayList;
import java.util.List;

public class NotificationsActivity extends AppCompatActivity {

    private ActivityNotificationsBinding binding;

    private MyReceiver objMyReceiver;
    private NotificationItemRecyclerViewAdapter notificationItemRecyclerViewAdapter;
    private LinearLayoutManager layoutManager;
    private List<UserNotificationsItem> notificationsList;
    private RecyclerView.OnScrollListener recyclerViewOnScrollListener;
    private int PAGE_SIZE = 25, apiHitCounter;
    private boolean isLoading = false, isLastPage = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityNotificationsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        objMyReceiver = new MyReceiver();
        notificationsList = new ArrayList<>();

        setSupportActionBar(binding.appbarLayout.toolbar);
        Utility.setStatusBarColor(this);
        registerReciever();

        binding.appbarLayout.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        binding.appbarLayout.toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        binding.appbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        RequestGetAllNotification objRequestGetAllNotification = new RequestGetAllNotification();
        objRequestGetAllNotification.setToUserId(Prefs.getUserId());
        objRequestGetAllNotification.setFromUserId("");
        ApiCallerUtility.callGetAllNotificationsApi(this, objRequestGetAllNotification);

        notificationItemRecyclerViewAdapter = new NotificationItemRecyclerViewAdapter(this, notificationsList);
        layoutManager = new LinearLayoutManager(this);
        binding.contentNotifications.rcvNotifications.setLayoutManager(layoutManager);
        binding.contentNotifications.rcvNotifications.setAdapter(notificationItemRecyclerViewAdapter);

        clearAllNotifications();

        recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (!isLoading && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= PAGE_SIZE) {
                        loadMoreItems();
                    }
                }
            }
        };

        binding.contentNotifications.rcvNotifications.addOnScrollListener(recyclerViewOnScrollListener);
    }

    private void loadMoreItems() {
        isLoading = true;
        binding.contentNotifications.loadingProgressbar.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (apiHitCounter++ == 5)
                    isLastPage = true;
                else {
                    for (int i = 0; i < PAGE_SIZE; i++) {
//                        COMMENTED BY RAVI AS PAGINATION IS NOT DONE FROM BACKEND
//                        notificationsList.add(new DummyContent.DummyItem(apiHitCounter + i + "", "Mavis has invited you to the group \"Rotating Savings\"", "Detailed description about this notification", ""));
                    }
                    binding.contentNotifications.rcvNotifications.getAdapter().notifyDataSetChanged();
                }
                isLoading = false;
                binding.contentNotifications.loadingProgressbar.setVisibility(View.GONE);
            }
        }, 2000);
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_REFRESH_NOTIFICATION_LIST);
        Utility.registerReciever(NotificationsActivity.this, filter, objMyReceiver);
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Defaults.ACTION_REFRESH_NOTIFICATION_LIST)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        ResponseGetAllNotifications objResponseGetAllNotifications = (ResponseGetAllNotifications) bundle.getSerializable("ResponseGetAllNotifications");
                        if (objResponseGetAllNotifications != null && objResponseGetAllNotifications.getData() != null) {
                            for (int i = 0; i < objResponseGetAllNotifications.getData().getUserNotifications().size(); i++) {
                                notificationsList.add(objResponseGetAllNotifications.getData().getUserNotifications().get(i));
                            }
                            notificationItemRecyclerViewAdapter.sort();
                            binding.contentNotifications.rcvNotifications.getAdapter().notifyDataSetChanged();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void clearAllNotifications() {
        Prefs.setNotificationCountInSharedPrefs(true);
        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }
}
