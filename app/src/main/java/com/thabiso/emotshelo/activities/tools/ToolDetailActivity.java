package com.thabiso.emotshelo.activities.tools;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.activities.base.BaseActivity;
import com.thabiso.emotshelo.activities.business_tool.conversationalform.AddCompanyActivity;
import com.thabiso.emotshelo.databinding.ActivityToolDetailBinding;
import com.thabiso.emotshelo.models.MarketToolListBO;

public class ToolDetailActivity extends BaseActivity {

    private ActivityToolDetailBinding binding;

    private Typeface robotoRegular, robotoMedium;
    private MarketToolListBO marketToolListBO;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityToolDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        binding.toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white_24dp));
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        Utility.setTransperentStatusBarColor(this);

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ToolDetailActivity.this, AddCompanyActivity.class);
                startActivity(intent);
            }
        });

        this.robotoRegular = Typeface.createFromAsset(getAssets(),
                "roboto_regular.ttf");
        this.robotoMedium = Typeface.createFromAsset(getAssets(),
                "roboto_medium.ttf");

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            marketToolListBO = (MarketToolListBO) bundle.getSerializable("currentTool");
        }

        if (marketToolListBO != null) {
            binding.contentToolDetail.mToolTitle.setText(marketToolListBO.getToolTitle());
            binding.contentToolDetail.mToolShortDescription.setText(marketToolListBO.getToolShortDescription() + "\n\n" + getResources().getString(R.string.lbl_how_this_works));
            binding.contentToolDetail.mToolLongDescription.setText(marketToolListBO.getToolLongDescription());
//            binding.contentToolDetail.mToolLongDescription.setText(getResources().getString(R.string.large_text));

            binding.toolbarLayout.setTitle(marketToolListBO.getToolTitle());
            binding.contentToolDetail.mToolSalesCount.setText("Sales: " + marketToolListBO.getTotalSales());
            binding.contentToolDetail.mToolSalesCount.setVisibility(View.GONE);
            binding.toolbarLayout.setExpandedTitleTextAppearance(R.style.Toolbar_TitleText_Expanded);
            binding.toolbarLayout.setCollapsedTitleTextAppearance(R.style.Toolbar_TitleText);

            binding.toolbarLayout.setExpandedTitleTypeface(robotoMedium);
            binding.toolbarLayout.setCollapsedTitleTypeface(robotoMedium);
            binding.contentToolDetail.mToolTitle.setTypeface(robotoMedium);
            binding.contentToolDetail.mToolSalesCount.setTypeface(robotoMedium);
            binding.contentToolDetail.mToolShortDescription.setTypeface(robotoMedium);
            binding.contentToolDetail.mToolLongDescription.setTypeface(robotoRegular);

//            if (marketToolListBO.getToolID() == Defaults.TOOL_ID_ENTERPRICE)
//                Glide.with(this).load(getResources().getDrawable(R.drawable.enterprise_tool))
//                        .into(binding.sportsImage);
//            else if (marketToolListBO.getToolID() == Defaults.TOOL_ID_GROUPS)
//                Glide.with(this).load(getResources().getDrawable(R.drawable.group_savings_tool))
//                        .into(binding.sportsImage);
//            else if (marketToolListBO.getToolID() == Defaults.TOOL_ID_PERSONAL)
//                Glide.with(this).load(getResources().getDrawable(R.drawable.personal_tool))
//                        .into(binding.sportsImage);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }


    @SuppressLint("RestrictedApi")
    @Override
    public void onBackPressed() {
        binding.fab.setVisibility(View.GONE);
        super.onBackPressed();
    }
}
