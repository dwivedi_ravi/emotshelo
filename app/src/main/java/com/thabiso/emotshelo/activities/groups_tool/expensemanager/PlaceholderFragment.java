package com.thabiso.emotshelo.activities.groups_tool.expensemanager;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.adapters.ExpenseRecyclerViewAdapter;
import com.thabiso.emotshelo.databinding.FragmentReadChapterBinding;
import com.thabiso.emotshelo.interfaces.OnExpenseListManagerInteractionListener;
import com.thabiso.emotshelo.models.expenses.Expense;
import com.thabiso.emotshelo.util.preferences.Defaults;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_SCREEN_TYPE = "screen_type";
    private static final String ARG_VERSE_OBJECT = "verse_object";

    private FragmentReadChapterBinding binding;

    private PageViewModel pageViewModel;
    private static OnExpenseListManagerInteractionListener mListener;
    private Typeface robotoMedium;

    public static PlaceholderFragment newInstance(int index, Expense objExpense, OnExpenseListManagerInteractionListener listener, int type) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        bundle.putInt(ARG_SCREEN_TYPE, type);
        bundle.putSerializable(ARG_VERSE_OBJECT, objExpense);
        mListener = listener;
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        pageViewModel =  new ViewModelProvider(this).get(PageViewModel.class);
        int index = 1;
        int screenType = Defaults.SCREEN_TYPE_DEPOSITS;

        Expense objExpense = null;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
            screenType = getArguments().getInt(ARG_SCREEN_TYPE);
            objExpense = (Expense) getArguments().getSerializable(ARG_VERSE_OBJECT);
        }
        pageViewModel.setIndex(index);
        pageViewModel.setScreentType(screenType);
        pageViewModel.setVerseObject(objExpense);
        robotoMedium = Typeface.createFromAsset(getActivity().getAssets(),
                "roboto_medium.ttf");
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
//        View root = inflater.inflate(R.layout.fragment_read_chapter, container, false);
        binding = FragmentReadChapterBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        binding.txtBalanceAmount.setTypeface(robotoMedium);

        pageViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
            }
        });

        pageViewModel.getVerseObject().observe(getViewLifecycleOwner(), new Observer<Expense>() {
            @Override
            public void onChanged(Expense objExpense) {
                try {
                    switch (pageViewModel.mScreenType.getValue()) {
                        case Defaults.SCREEN_TYPE_DEPOSITS:
                            if (binding.recyclerViewMemberList != null && binding.recyclerViewMemberList.getAdapter() == null) {
                                binding.recyclerViewMemberList.setLayoutManager(new LinearLayoutManager(getActivity()));
                                ExpenseRecyclerViewAdapter expenseRecyclerViewAdapter = new ExpenseRecyclerViewAdapter(getActivity(), objExpense.getDepositList(), mListener, false, Defaults.SCREEN_TYPE_DEPOSITS);
                                binding.recyclerViewMemberList.setAdapter(expenseRecyclerViewAdapter);
                            } else if (binding.recyclerViewMemberList != null) {
                                binding.recyclerViewMemberList.getAdapter().notifyItemInserted(0);
                                binding.recyclerViewMemberList.getAdapter().notifyDataSetChanged();
                            }
                            break;
                        case Defaults.SCREEN_TYPE_EXPENSES:
                            if (binding.recyclerViewMemberList != null && binding.recyclerViewMemberList.getAdapter() == null) {
                                binding.recyclerViewMemberList.setLayoutManager(new LinearLayoutManager(getActivity()));
                                ExpenseRecyclerViewAdapter expenseRecyclerViewAdapter = new ExpenseRecyclerViewAdapter(getActivity(), objExpense.getExpenseList(), mListener, false, Defaults.SCREEN_TYPE_EXPENSES);
                                binding.recyclerViewMemberList.setAdapter(expenseRecyclerViewAdapter);
                            } else if (binding.recyclerViewMemberList != null) {
                                binding.recyclerViewMemberList.getAdapter().notifyItemInserted(0);
                                binding.recyclerViewMemberList.getAdapter().notifyDataSetChanged();
                            }
                            break;
                        case Defaults.SCREEN_TYPE_INTEREST:
                            if (binding.recyclerViewMemberList != null && binding.recyclerViewMemberList.getAdapter() == null) {
                                binding.recyclerViewMemberList.setLayoutManager(new LinearLayoutManager(getActivity()));
                                ExpenseRecyclerViewAdapter expenseRecyclerViewAdapter = new ExpenseRecyclerViewAdapter(getActivity(), objExpense.getInterestList(), mListener, false, Defaults.SCREEN_TYPE_INTEREST);
                                binding.recyclerViewMemberList.setAdapter(expenseRecyclerViewAdapter);
                            } else if (binding.recyclerViewMemberList != null) {
                                binding.recyclerViewMemberList.getAdapter().notifyItemInserted(0);
                                binding.recyclerViewMemberList.getAdapter().notifyDataSetChanged();
                            }
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        try {
            if (pageViewModel.mScreenType.getValue() == Defaults.SCREEN_TYPE_DEPOSITS) {
                binding.txtBalanceAmount.setTextColor(getResources().getColor(R.color.colorAccent));
                binding.cardBalance.setStrokeColor(getResources().getColor(R.color.colorAccent));
            } else if (pageViewModel.mScreenType.getValue() == Defaults.SCREEN_TYPE_EXPENSES) {
                binding.txtBalanceAmount.setTextColor(getResources().getColor(R.color.red));
                binding.cardBalance.setStrokeColor(getResources().getColor(R.color.red));
            } else if (pageViewModel.mScreenType.getValue() == Defaults.SCREEN_TYPE_INTEREST) {
                binding.txtBalanceAmount.setTextColor(getResources().getColor(R.color.red));
                binding.cardBalance.setStrokeColor(getResources().getColor(R.color.red));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return root;
    }

    public void refreshList(Expense objExpense) {
        pageViewModel.setVerseObject(objExpense);
    }

    public RecyclerView getRecyclerViewForPrinting() {
        return binding.recyclerViewMemberList;
    }
}