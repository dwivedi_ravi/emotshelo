package com.thabiso.emotshelo.activities.signup;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.snackbar.Snackbar;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.databinding.ActivityEditProfileBinding;
import com.thabiso.emotshelo.interfaces.DateTimeUpdateListener;
import com.thabiso.emotshelo.models.RequestOTP;
import com.thabiso.emotshelo.models.editprofile.RequestEditProfile;
import com.thabiso.emotshelo.models.editprofile.ResponseEditProfile;
import com.thabiso.emotshelo.network.ApiCallerUtility;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;
import com.thabiso.emotshelo.util.preferences.Prefs;
import com.thabiso.emotshelo.util.storage.LegacyCompatFileProvider;
import com.thabiso.emotshelo.widget.DateTimePicker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

public class EditProfileActivity extends AppCompatActivity implements DateTimeUpdateListener, View.OnClickListener, View.OnTouchListener {

    private ActivityEditProfileBinding binding;

    private Animator currentAnimator;
    private int shortAnimationDuration;

    private final int CAMERA_REQUEST = 52;
    private final int PICK_REQUEST = 53;

    private DateTimeUpdateListener updateListener;
    private MyReceiver objMyReceiver;
    private boolean navigate = false;
    private RequestOptions requestOptions;
    private ResponseEditProfile objResponseEditProfile;

    private ActivityResultLauncher mRequestMultiplePermissions = registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), new ActivityResultCallback<Map<String, Boolean>>() {
        @Override
        public void onActivityResult(Map<String, Boolean> result) {
            for (Map.Entry<String, Boolean> entry : result.entrySet()) {
                switch (entry.getKey()) {
                    case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                        if (entry.getValue()) {
                            mGetContent.launch("image/*");
                        } else {
                            Toast.makeText(EditProfileActivity.this, getString(R.string.storage_permission), Toast.LENGTH_LONG).show();
                        }
                        break;
                    case Manifest.permission.READ_EXTERNAL_STORAGE:
//                        if (entry.getValue()) {
//                            mGetContent.launch("image/*");
//                        } else {
////                            Toast.makeText(EditProfileActivity.this, getString(R.string.storage_permission), Toast.LENGTH_LONG).show();
//                        }
                        break;
                }
            }
        }
    });

    private ActivityResultLauncher<String> mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(), new ActivityResultCallback<Uri>() {
        @Override
        public void onActivityResult(Uri resultUri) {
            // Handle the returned Uri
            try {
                if (resultUri != null) {
                    try {
                        Glide.with(EditProfileActivity.this).setDefaultRequestOptions(requestOptions).load(resultUri)
                                .thumbnail(0.25f).into(binding.contentEditProfile.imgProfile);
                        Glide.with(EditProfileActivity.this).setDefaultRequestOptions(requestOptions).load(resultUri)
                                .thumbnail(0.25f).into(binding.imgFullView);

                        crossfade(binding.contentEditProfile.imgProfile, binding.fullScreenContainer);
                        binding.contentEditProfile.uploadProgress.setVisibility(View.VISIBLE);
                        binding.imgEditProfile.setClickable(false);
                        binding.appbarLayout.toolbar.setVisibility(View.VISIBLE);
                        binding.fabNext.setVisibility(View.VISIBLE);
                        Utility.setStatusBarColor(EditProfileActivity.this);

                        ApiCallerUtility.callUploadProfileImageApi(EditProfileActivity.this, resultUri);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityEditProfileBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());

        objMyReceiver = new MyReceiver();

        setSupportActionBar(binding.appbarLayout.toolbar);
        Utility.setStatusBarColor(EditProfileActivity.this);
        updateListener = this;
        registerReciever();
        requestOptions = Utility.getCustomizedRequestOptions(R.color.mdtp_line_background, R.drawable.profile_place_holder);

        binding.contentEditProfile.editMobile.setText(Prefs.getMobileNumber());

        binding.fullScreenContainer.setOnTouchListener(this);
        binding.contentEditProfile.editDOB.setOnTouchListener(this);
        binding.fabNext.shrink();
        binding.fabNext.setOnClickListener(this);
        binding.contentEditProfile.imgProfile.setOnClickListener(this);
        binding.imgEditProfile.setOnClickListener(this);
        binding.imgShare.setOnClickListener(this);
        binding.imgBack.setOnClickListener(this);

        shortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);

        if (getIntent() != null) {
            navigate = getIntent().getExtras().getBoolean("navigate", false);
        }

        RequestOTP objRequestOTP = new RequestOTP();
        objRequestOTP.setMobile_number(Prefs.getMobileNumber());
        ApiCallerUtility.callGetProfileDataApi(this, objRequestOTP);
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (binding.fullScreenContainer.getVisibility() == View.VISIBLE) {
//            binding.fullScreenContainer.setVisibility(View.GONE);
            crossfade(binding.contentEditProfile.imgProfile, binding.fullScreenContainer);
            binding.appbarLayout.toolbar.setVisibility(View.VISIBLE);
            binding.fabNext.setVisibility(View.VISIBLE);
            Utility.setStatusBarColor(EditProfileActivity.this);
        } else {
            super.onBackPressed();
        }
    }

    private void crossfade(View view1, View view2) {

        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        view1.setAlpha(0f);
        view1.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        view1.animate()
                .alpha(1f)
                .setDuration(shortAnimationDuration)
                .setListener(null);

        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        view2.animate()
                .alpha(0f)
                .setDuration(shortAnimationDuration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view2.setVisibility(View.GONE);
                    }
                });
    }

    private boolean isFormValid() {
        if (binding.contentEditProfile.editFirstName.getVisibility() == View.VISIBLE && binding.contentEditProfile.editFirstName.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentEditProfile.editFirstName, "First name can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentEditProfile.editLastName.getVisibility() == View.VISIBLE && binding.contentEditProfile.editLastName.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentEditProfile.editLastName, "Last name can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentEditProfile.editEmail.getVisibility() == View.VISIBLE && binding.contentEditProfile.editEmail.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentEditProfile.editEmail, "Email can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentEditProfile.editMobile.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentEditProfile.editMobile, "Mobile Number can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentEditProfile.editDOB.getVisibility() == View.VISIBLE && binding.contentEditProfile.editDOB.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentEditProfile.editDOB, "Date can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_next:
                if (isFormValid()) {
                    RequestEditProfile objRequestEditProfile = new RequestEditProfile();

                    objRequestEditProfile.setFirstname(binding.contentEditProfile.editFirstName.getText().toString().trim());
                    objRequestEditProfile.setLastname("");
                    objRequestEditProfile.setMobileNumber(binding.contentEditProfile.editMobile.getText().toString().trim());
                    objRequestEditProfile.setEmail("");
//                    objRequestEditProfile.setDob(Defaults.DUMMY_YEAR_PREFIX + binding.contentEditProfile.editDOB.getText().toString());
                    objRequestEditProfile.setDob(binding.contentEditProfile.editDOB.getText().toString().trim());
                    objRequestEditProfile.setAbout("");

                    ApiCallerUtility.callEditProfileApi(EditProfileActivity.this, objRequestEditProfile, navigate);
                }
                break;
            case R.id.imgProfile:
//                binding.fullScreenContainer.setVisibility(View.VISIBLE);
                crossfade(binding.fullScreenContainer, binding.contentEditProfile.imgProfile);

                setImageFromServer();

                binding.appbarLayout.toolbar.setVisibility(View.GONE);
                binding.fabNext.setVisibility(View.GONE);
                Utility.setTransperentStatusBarColor(EditProfileActivity.this);
                break;
            case R.id.imgEditProfile:
                mRequestMultiplePermissions.launch(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE});
                break;
            case R.id.imgShare:
                try {
                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.setType("image/*");
                    BitmapDrawable drawable = (BitmapDrawable) binding.contentEditProfile.imgProfile.getDrawable();
                    Bitmap bitmap = drawable.getBitmap();

//                    File filepath = Environment.getExternalStorageDirectory();
//                    File filepath = StorageHelper.getSdcardFile(EditProfileActivity.this);
//                    File filepath = getExternalFilesDir("external");
//                    File filepath = getFilesDir();

                    // Create a new folder AndroidBegin in SD Card
                    File internalDirectoryPath = new File(getFilesDir().getAbsolutePath() + "/" + Defaults.FOLDER_PATH_PROFILE);
                    if (!internalDirectoryPath.exists())
                        internalDirectoryPath.mkdirs();

                    // Create a name for the saved image
                    File imageFile = new File(internalDirectoryPath, "profile_pic.png");
                    if (!imageFile.exists())
                        imageFile.createNewFile();

                    try {
                        // Saves the file into SD Card
                        OutputStream output = new FileOutputStream(imageFile);
                        // Compress into png format image from 0% - 100%, using 100% for this tutorial
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
                        output.flush();
                        output.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (imageFile != null) {
                        Uri uri1 = LegacyCompatFileProvider.getUri(this, imageFile);
                        share.putExtra(Intent.EXTRA_STREAM, uri1);
                        share.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        startActivity(Intent.createChooser(share, getString(R.string.send_to)));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.imgBack:
                if (binding.fullScreenContainer.getVisibility() == View.VISIBLE) {
//                    binding.fullScreenContainer.setVisibility(View.GONE);
                    crossfade(binding.contentEditProfile.imgProfile, binding.fullScreenContainer);
                    binding.appbarLayout.toolbar.setVisibility(View.VISIBLE);
                    binding.fabNext.setVisibility(View.VISIBLE);
                    Utility.setStatusBarColor(EditProfileActivity.this);
                }
                break;
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        switch (view.getId()) {
            case R.id.editDOB:
                if (event.getAction() == MotionEvent.ACTION_DOWN)
                    getDate(view);
                return true;
            case R.id.fullScreenContainer:
                return true;
        }
        return false;
    }

    public void getDate(View view) {
        DateTimePicker.getInstance().getDate(EditProfileActivity.this, updateListener, null, (TextView) view);
    }

    @Override
    public void onDateUpdate(String year, String monthOfYear, String dayOfMonth, TextView textView) {
//        String date = dayOfMonth + "-" + (monthOfYear) + "-" + year;
        String date = year + "-" + (monthOfYear) + "-" + dayOfMonth;
//        String date = dayOfMonth + "-" + (monthOfYear);
//        String date = (monthOfYear) + "-" + dayOfMonth;
        textView.setText(date);
    }

    @Override
    public void onTimeUpdate(int hourOfDay, int minute, int second, String am_pm, TextView textView) {

    }

    @Override
    public void onTimeUpdate(int hourOfDay, int minute, int second, TextView textView) {

    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_FILL_PROFILE_DATA);
        filter.addAction(Defaults.ACTION_PROFILE_PIC_UPLOAD_SUCCESS);
        filter.addAction(Defaults.ACTION_PROFILE_PIC_UPLOAD_FAILED);
        Utility.registerReciever(EditProfileActivity.this, filter, objMyReceiver);
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Defaults.ACTION_FILL_PROFILE_DATA)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        objResponseEditProfile = (ResponseEditProfile) bundle.getSerializable("ResponseEditProfile");

                        if (objResponseEditProfile != null) {
//                        binding.contentEditProfile.editFirstName.setText(objResponseEditProfile.getData().getUser().getFirstname());
                            binding.contentEditProfile.editFirstName.setText(objResponseEditProfile.getData().getUser().getName());
                            binding.contentEditProfile.editLastName.setText(objResponseEditProfile.getData().getUser().getLastname());
                            binding.contentEditProfile.editEmail.setText(objResponseEditProfile.getData().getUser().getEmail());
                            binding.contentEditProfile.editMobile.setText(objResponseEditProfile.getData().getUser().getMobileNumber());
//                        if (objResponseEditProfile.getData().getUser().getDob().contains(Defaults.DUMMY_YEAR_PREFIX) || objResponseEditProfile.getData().getUser().getDob().length() > 5) {
//                            binding.contentEditProfile.editDOB.setText(objResponseEditProfile.getData().getUser().getDob().substring(Defaults.DUMMY_YEAR_PREFIX.length(), objResponseEditProfile.getData().getUser().getDob().length()));
//                        } else
//                            binding.contentEditProfile.editDOB.setText(objResponseEditProfile.getData().getUser().getDob());
                            binding.contentEditProfile.editDOB.setText(objResponseEditProfile.getData().getUser().getDob());
                            binding.contentEditProfile.editStatusText.setText(objResponseEditProfile.getData().getUser().getAbout());
                            Glide.with(EditProfileActivity.this).setDefaultRequestOptions(requestOptions).load(objResponseEditProfile.getData().getUser().getAvtar())
                                    .thumbnail(0.25f).into(binding.contentEditProfile.imgProfile);
                            Glide.with(EditProfileActivity.this).setDefaultRequestOptions(requestOptions).load(objResponseEditProfile.getData().getUser().getAvtar())
                                    .thumbnail(0.25f).into(binding.imgFullView);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (intent.getAction().equals(Defaults.ACTION_PROFILE_PIC_UPLOAD_SUCCESS)) {
                try {
                    binding.contentEditProfile.uploadProgress.setVisibility(View.GONE);
                    binding.imgEditProfile.setClickable(true);
                    Toast.makeText(context, "Profile Pic Uploaded Successfully", Toast.LENGTH_LONG).show();
                    objResponseEditProfile.getData().getUser().setAvtar(Prefs.getUserProfilePicUrl());

                    if (intent != null && intent.getExtras() != null && intent.getExtras().getBundle("bundle") != null) {
                        Bundle bundle = intent.getExtras().getBundle("bundle");
                        ResponseEditProfile objResponseEditProfile = (ResponseEditProfile) bundle.getSerializable("ResponseEditProfile");
                        if (objResponseEditProfile != null && objResponseEditProfile.getData() != null && objResponseEditProfile.getData().getUser() != null) {
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (intent.getAction().equals(Defaults.ACTION_PROFILE_PIC_UPLOAD_FAILED)) {
                try {
                    binding.contentEditProfile.uploadProgress.setVisibility(View.GONE);
                    binding.imgEditProfile.setClickable(true);
                    Toast.makeText(context, "Profile Pic Upload Failed", Toast.LENGTH_LONG).show();
                    if (intent != null && intent.getExtras() != null && intent.getExtras().getBundle("bundle") != null) {
                        Bundle bundle = intent.getExtras().getBundle("bundle");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void setImageFromServer() {
        try {
            if (objResponseEditProfile != null) {
//                bigImageProgressBar.setVisibility(View.VISIBLE);
                binding.imgFullView.setImageResource(R.drawable.profile_place_holder);
                Glide.with(EditProfileActivity.this).setDefaultRequestOptions(requestOptions).load(objResponseEditProfile.getData().getUser().getAvtar())/*.listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        bigImageProgressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        bigImageProgressBar.setVisibility(View.GONE);
                        return false;
                    }
                })*/.thumbnail(0.25f).into(binding.imgFullView);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (resultCode == RESULT_OK) {
//            switch (requestCode) {
//                case CAMERA_REQUEST:
//                    Bitmap photo = (Bitmap) data.getExtras().get("data");
////                    binding.contentEditProfile.imgProfile.setImageBitmap(photo);
////                    binding.imgFullView.setImageBitmap(photo);
//                    Glide.with(EditProfileActivity.this).setDefaultRequestOptions(requestOptions).load(photo)
//                            .thumbnail(0.25f).into(binding.contentEditProfile.imgProfile);
//                    Glide.with(EditProfileActivity.this).setDefaultRequestOptions(requestOptions).load(photo)
//                            .thumbnail(0.25f).into(binding.imgFullView);
//                    break;
//            }
//            super.onActivityResult(requestCode, resultCode, data);
//        }
//    }
}
