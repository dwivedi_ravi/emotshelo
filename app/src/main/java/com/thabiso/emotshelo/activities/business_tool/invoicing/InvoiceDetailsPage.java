package com.thabiso.emotshelo.activities.business_tool.invoicing;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.adapters.ProductItemsAdapter;
import com.thabiso.emotshelo.databinding.ActivityInvoiceDetailsPageBinding;
import com.thabiso.emotshelo.models.getmyconnections.UserContactsItem;
import com.thabiso.emotshelo.util.Tools;
import com.thabiso.emotshelo.widget.ViewAnimation;

import java.util.ArrayList;
import java.util.List;

public class InvoiceDetailsPage extends AppCompatActivity {

    private ActivityInvoiceDetailsPageBinding binding;

    private List<UserContactsItem> clientList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityInvoiceDetailsPageBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());

        initToolbar();
        initComponent();
    }

    private void initToolbar() {
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Tools.setSystemBarColor(this, R.color.colorPrimaryDark);
    }

    private void initComponent() {

        // section items
        binding.btToggleItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleSection(view, binding.lytExpandItems);
            }
        });

        // section address
        binding.btToggleItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleSection(view, binding.lytExpandAddress);
            }
        });

        // section description
        binding.btToggleDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleSection(view, binding.lytExpandDescription);
            }
        });

//        toggleSection(binding.btToggleItems, binding.lytExpandItems);
//        toggleSection(binding.btToggleItems, binding.lytExpandAddress);
//        toggleSection(binding.btToggleDescription, binding.lytExpandDescription);

//        toggleArrow(binding.btToggleItems);
        toggleArrow(binding.btToggleAddress);
        toggleArrow(binding.btToggleDescription);

        // copy to clipboard
        binding.btCopyCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Tools.copyToClipboard(getApplicationContext(), binding.tvInvoiceCode.getText().toString());
            }
        });

        clientList = new ArrayList<>();

        UserContactsItem objUserContactsItem = new UserContactsItem();
        objUserContactsItem.setUserId("1");
        objUserContactsItem.setUserName("Web Design");
        objUserContactsItem.setTemp("Rate: 45     Qty: 10     Tax: 5.62");
        objUserContactsItem.setMobile("$ 455.62");
        objUserContactsItem.setAvatar("Website designed for the product");
        clientList.add(objUserContactsItem);

        objUserContactsItem = new UserContactsItem();
        objUserContactsItem.setUserId("2");
        objUserContactsItem.setUserName("E-Book Design");
        objUserContactsItem.setTemp("Rate: 27     Qty: 10     Tax: 8.12");
        objUserContactsItem.setMobile("$ 278.12");
        objUserContactsItem.setAvatar("E-Book designed for the product");
        clientList.add(objUserContactsItem);

        objUserContactsItem = new UserContactsItem();
        objUserContactsItem.setUserId("3");
        objUserContactsItem.setUserName("Hosting Plan");
        objUserContactsItem.setTemp("Rate: 70     Qty: 10     Tax: 19.00");
        objUserContactsItem.setMobile("$ 719.00");
        objUserContactsItem.setAvatar("Hosting plan purchased for the product");
        clientList.add(objUserContactsItem);

        objUserContactsItem = new UserContactsItem();
        objUserContactsItem.setUserId("4");
        objUserContactsItem.setUserName("Broucher Design");
        objUserContactsItem.setTemp("Rate: 57     Qty: 10     Tax: 3.50");
        objUserContactsItem.setMobile("$ 573.50");
        objUserContactsItem.setAvatar("Brochure designed for the product");
        clientList.add(objUserContactsItem);

        binding.rcvItems.setLayoutManager(new LinearLayoutManager(this));
        ProductItemsAdapter mProductItemsAdapter = new ProductItemsAdapter(this, clientList, null, false);
        binding.rcvItems.setAdapter(mProductItemsAdapter);
    }


    private void toggleSection(View bt, final View lyt) {
        boolean show = toggleArrow(bt);
        if (show) {
            ViewAnimation.expand(lyt, new ViewAnimation.AnimListener() {
                @Override
                public void onFinish() {
                    Tools.nestedScrollTo(binding.nestedScrollView, lyt);
                }
            });
        } else {
            ViewAnimation.collapse(lyt);
        }
    }

    public boolean toggleArrow(View view) {
        if (view.getRotation() == 0) {
            view.animate().setDuration(200).rotation(180);
            return true;
        } else {
            view.animate().setDuration(200).rotation(0);
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {

        }
        return super.onOptionsItemSelected(item);
    }
}
