package com.thabiso.emotshelo.activities.homescreen;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.activities.tools.ToolMarketPlaceActivity;
import com.thabiso.emotshelo.adapters.EnterpriseAdapter;
import com.thabiso.emotshelo.adapters.GroupsAdapter;
import com.thabiso.emotshelo.adapters.PersonalAdapter;
import com.thabiso.emotshelo.databinding.ContentHomeScreenListBinding;
import com.thabiso.emotshelo.models.HomeScreenObject;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;
import com.thabiso.emotshelo.util.preferences.Prefs;

/**
 * A placeholder fragment containing a simple view.
 */
public class HomeScreenPlaceholderFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_SCREEN_TYPE = "screen_type";
    private static final String ARG_VERSE_OBJECT = "verse_object";

    private ContentHomeScreenListBinding binding;

    private HomeScreenPageViewModel pageViewModel;
    private static OnHomeScreenListManagerInteractionListener mListener;
    private Typeface robotoMedium;

    public static HomeScreenPlaceholderFragment newInstance(int index, HomeScreenObject homeScreenObject, OnHomeScreenListManagerInteractionListener listener, int type) {
        HomeScreenPlaceholderFragment fragment = new HomeScreenPlaceholderFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        bundle.putInt(ARG_SCREEN_TYPE, type);
        bundle.putSerializable(ARG_VERSE_OBJECT, homeScreenObject);
        mListener = listener;
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = new ViewModelProvider(this).get(HomeScreenPageViewModel.class);
        int index = 1;
        int screenType = Defaults.SCREEN_TYPE_ENTERPRISE;

        HomeScreenObject homeScreenObject = null;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
            screenType = getArguments().getInt(ARG_SCREEN_TYPE);
            homeScreenObject = (HomeScreenObject) getArguments().getSerializable(ARG_VERSE_OBJECT);
        }
        pageViewModel.setIndex(index);
        pageViewModel.setScreentType(screenType);
        pageViewModel.setVerseObject(homeScreenObject);
        robotoMedium = Typeface.createFromAsset(getActivity().getAssets(),
                "roboto_medium.ttf");
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
//        View root = inflater.inflate(R.layout.content_home_screen_list, container, false);

        binding = ContentHomeScreenListBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        binding.noItemsAvailable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeScreenActivity) getActivity()).handleAddButtonClick();
            }
        });

        binding.itemCardWizardBG.btnEnableTool.setOnClickListener(v -> {
            if (Utility.checkViewTraingingCompleted(getActivity(), v, Defaults.TRAINING_SCREEN_HOMESCREEN, true, getContext().getResources().getString(R.string.training_title_homescreen_enable_tool), getContext().getResources().getString(R.string.training_description_homescreen_enable_tool))) {
                if (pageViewModel.mScreenType.getValue().equals(Defaults.SCREEN_TYPE_PERSONAL)) {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.lbl_under_development), Toast.LENGTH_LONG).show();
                } else {
                    Intent intent = new Intent(getActivity(), ToolMarketPlaceActivity.class);
                    startActivity(intent);
                }
            }
        });

        pageViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
            }
        });

        pageViewModel.getVerseObject().observe(getViewLifecycleOwner(), new Observer<HomeScreenObject>() {
            @Override
            public void onChanged(HomeScreenObject homeScreenObject) {
                try {
                    switch (pageViewModel.mScreenType.getValue()) {
                        case Defaults.SCREEN_TYPE_ENTERPRISE:
                            if (Prefs.getBooleanValue(Defaults.TOOL_ID_ENTERPRICE + "")) {
                                binding.recyclerView.setVisibility(View.VISIBLE);
                                binding.itemCardWizardBG.getRoot().setVisibility(View.GONE);

                                if (binding.recyclerView != null && binding.recyclerView.getAdapter() == null) {
                                    binding.noInternetConnection.lytNoConnection.setVisibility(View.GONE);
                                    binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                                    EnterpriseAdapter mAdapter = new EnterpriseAdapter(getActivity(), homeScreenObject.getEnterpriseList());
                                    binding.recyclerView.setAdapter(mAdapter);
                                } else if (binding.recyclerView != null) {
                                    binding.noInternetConnection.lytNoConnection.setVisibility(View.GONE);
                                    ((EnterpriseAdapter) binding.recyclerView.getAdapter()).refreshList(homeScreenObject.getEnterpriseList());
                                    binding.recyclerView.getAdapter().notifyDataSetChanged();

                                    if (homeScreenObject.getEnterpriseList().size() == 0) {
                                        binding.noItemsAvailable.setText(getResources().getString(R.string.no_companies_added));
                                        binding.noItemsAvailable.setVisibility(View.VISIBLE);
                                    } else {
                                        binding.noItemsAvailable.setVisibility(View.GONE);
                                    }
                                }
                            } else {
                                binding.itemCardWizardBG.title.setText(getActivity().getResources().getString(R.string.card_title_enterprise));
                                binding.itemCardWizardBG.description.setText(getActivity().getResources().getString(R.string.card_description_enterprise));
                                binding.recyclerView.setVisibility(View.GONE);
                                binding.itemCardWizardBG.getRoot().setVisibility(View.VISIBLE);
                            }
                            break;
                        case Defaults.SCREEN_TYPE_GROUPS:
                            if (Prefs.getBooleanValue(Defaults.TOOL_ID_GROUPS + "")) {
                                binding.recyclerView.setVisibility(View.VISIBLE);
                                binding.itemCardWizardBG.getRoot().setVisibility(View.GONE);

                                if (binding.recyclerView != null && binding.recyclerView.getAdapter() == null) {
                                    binding.noInternetConnection.lytNoConnection.setVisibility(View.GONE);
                                    binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                                    GroupsAdapter mAdapter = new GroupsAdapter(getActivity(), homeScreenObject.getGroupList());
                                    binding.recyclerView.setAdapter(mAdapter);
                                } else if (binding.recyclerView != null) {
                                    binding.noInternetConnection.lytNoConnection.setVisibility(View.GONE);
                                    ((GroupsAdapter) binding.recyclerView.getAdapter()).refreshList(homeScreenObject.getGroupList());
                                    binding.recyclerView.getAdapter().notifyDataSetChanged();
                                    if (homeScreenObject.getGroupList().size() == 0) {
                                        binding.noItemsAvailable.setText(getResources().getString(R.string.no_groups_added));
                                        binding.noItemsAvailable.setVisibility(View.VISIBLE);
                                    } else {
                                        binding.noItemsAvailable.setVisibility(View.GONE);
                                    }
                                }
                            } else {
                                binding.itemCardWizardBG.title.setText(getActivity().getResources().getString(R.string.card_title_groups));
                                binding.itemCardWizardBG.description.setText(getActivity().getResources().getString(R.string.card_description_groups));
                                binding.recyclerView.setVisibility(View.GONE);
                                binding.itemCardWizardBG.getRoot().setVisibility(View.VISIBLE);
                            }

                            break;
                        case Defaults.SCREEN_TYPE_PERSONAL:
                            if (Prefs.getBooleanValue(Defaults.TOOL_ID_PERSONAL + "")) {
                                binding.recyclerView.setVisibility(View.VISIBLE);
                                binding.itemCardWizardBG.getRoot().setVisibility(View.GONE);

                                if (binding.recyclerView != null && binding.recyclerView.getAdapter() == null) {
                                    binding.noInternetConnection.lytNoConnection.setVisibility(View.GONE);
                                    binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                                    PersonalAdapter mPersonalAdapter = new PersonalAdapter(getActivity(), homeScreenObject.getPersonalList());
                                    binding.recyclerView.setAdapter(mPersonalAdapter);

                                    if (homeScreenObject.getPersonalList().size() == 0) {
                                        binding.noItemsAvailable.setText(getResources().getString(R.string.no_budget_added));
                                        binding.noItemsAvailable.setVisibility(View.VISIBLE);
                                    } else {
                                        binding.noItemsAvailable.setVisibility(View.GONE);
                                    }
                                } else if (binding.recyclerView != null) {
                                    binding.noInternetConnection.lytNoConnection.setVisibility(View.GONE);
                                    ((PersonalAdapter) binding.recyclerView.getAdapter()).refreshList(homeScreenObject.getPersonalList());
                                    binding.recyclerView.getAdapter().notifyDataSetChanged();
                                }
                            } else {
                                binding.itemCardWizardBG.title.setText(getActivity().getResources().getString(R.string.card_title_personal) + " - " + getActivity().getResources().getString(R.string.lbl_under_development));
                                binding.itemCardWizardBG.description.setText(getActivity().getResources().getString(R.string.card_description_personal));
                                binding.recyclerView.setVisibility(View.GONE);
                                binding.itemCardWizardBG.getRoot().setVisibility(View.VISIBLE);
                            }
                            break;
                        case Defaults.SCREEN_TYPE_HOME:
                            if (Prefs.getBooleanValue(Defaults.TOOL_ID_HOME + "")) {
                                binding.recyclerView.setVisibility(View.VISIBLE);
                                binding.itemCardWizardBG.getRoot().setVisibility(View.GONE);

                                if (binding.recyclerView != null && binding.recyclerView.getAdapter() == null) {
                                    binding.noInternetConnection.lytNoConnection.setVisibility(View.GONE);
                                    binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                                    PersonalAdapter mPersonalAdapter = new PersonalAdapter(getActivity(), homeScreenObject.getHomeList());
                                    binding.recyclerView.setAdapter(mPersonalAdapter);
                                } else if (binding.recyclerView != null) {
                                    binding.noInternetConnection.lytNoConnection.setVisibility(View.GONE);
                                    ((PersonalAdapter) binding.recyclerView.getAdapter()).refreshList(homeScreenObject.getHomeList());
                                    binding.recyclerView.getAdapter().notifyDataSetChanged();
                                }
                            } else {
                                binding.itemCardWizardBG.title.setText(getActivity().getResources().getString(R.string.card_title_personal) + " - " + getActivity().getResources().getString(R.string.lbl_under_development));
                                binding.itemCardWizardBG.description.setText(getActivity().getResources().getString(R.string.card_description_personal));
                                binding.recyclerView.setVisibility(View.GONE);
                                binding.itemCardWizardBG.getRoot().setVisibility(View.GONE);
                            }
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        return root;
    }

    public void refreshList(HomeScreenObject objExpense) {
        pageViewModel.setVerseObject(objExpense);
    }

    public Button getEnableToolButton() {
        return binding.itemCardWizardBG.btnEnableTool;
    }

    public RecyclerView getRecyclerViewForPrinting() {
        return binding.recyclerView;
    }

    public LinearLayout getNoInternetLayout() {
        return binding.noInternetConnection.lytNoConnection;
    }
}