package com.thabiso.emotshelo.activities.groups_tool;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;

import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.adapters.EndGroupSplitMoneyRecyclerViewAdapter;
import com.thabiso.emotshelo.databinding.ActivityEndGroupSplitMoneyBinding;
import com.thabiso.emotshelo.models.creategroup.UserGroups;
import com.thabiso.emotshelo.util.Tools;
import com.thabiso.emotshelo.util.Utility;

public class EndGroupAndSplitMoney extends AppCompatActivity {

    private ActivityEndGroupSplitMoneyBinding binding;

    UserGroups objUserGroups;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityEndGroupSplitMoneyBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());

        initToolbar();
        initComponent();
    }

    private void initToolbar() {
        binding.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("Split Money");
        Tools.setSystemBarColor(this, R.color.colorPrimaryDark);
    }

    private void initComponent() {
        String displayGroupBalance = "", currency = "";
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            objUserGroups = (UserGroups) bundle.getSerializable("currentGroup");
            displayGroupBalance = bundle.getString("displayGroupBalance");
            currency = bundle.getString("currency");
        }

        if (objUserGroups != null) {
            binding.contentEndGroupSplitMoney.groupName.setText(objUserGroups.getName());
            binding.contentEndGroupSplitMoney.groupDescription.setText(objUserGroups.getDescription());
            if (!displayGroupBalance.trim().equals(""))
                binding.txtGroupBalance.setText(displayGroupBalance);
            else
                binding.txtGroupBalance.setText(objUserGroups.getTotalAmount());

            int perPersonAmount = 0;
            try {
                perPersonAmount = Integer.parseInt(objUserGroups.getTotalAmount()) / objUserGroups.getUserMembers().size();
            } catch (Exception e) {
                e.printStackTrace();
            }

            binding.contentEndGroupSplitMoney.rcvUsersOfGroup.setLayoutManager(new GridLayoutManager(EndGroupAndSplitMoney.this, 3));
            EndGroupSplitMoneyRecyclerViewAdapter endGroupSplitMoneyRecyclerViewAdapter = new EndGroupSplitMoneyRecyclerViewAdapter(EndGroupAndSplitMoney.this, objUserGroups.getUserMembers(), currency + perPersonAmount);
            binding.contentEndGroupSplitMoney.rcvUsersOfGroup.setAdapter(endGroupSplitMoneyRecyclerViewAdapter);
        }

        binding.btnEndGroupAndSplitMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog textDialog = Utility.getTextDialog_Material(EndGroupAndSplitMoney.this, "Confirmation", "Are you sure? You want to end this group?");
                textDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok_action).toUpperCase(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(EndGroupAndSplitMoney.this, "Group Ended Successfully", Toast.LENGTH_LONG).show();
                    }
                });
                textDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel_action).toUpperCase(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        textDialog.dismiss();
                    }
                });
                textDialog.show();

                textDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.app_text_color));
                textDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.app_text_color));
                textDialog.getButton(DialogInterface.BUTTON_POSITIVE).setBackground(getResources().getDrawable(R.drawable.ripple));
                textDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setBackground(getResources().getDrawable(R.drawable.ripple));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }
}
