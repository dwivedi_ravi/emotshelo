package com.thabiso.emotshelo.activities.base;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;

import com.orhanobut.hawk.Hawk;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.storage.StorageHelper;

import java.io.File;

/**
 * Created by dnld on 03/08/16.
 */

public abstract class SharedActivity extends BaseActivity {

    private int REQUEST_CODE_SD_CARD_PERMISSIONS = 42;

    public void requestSdCardPermissionsIfMissing() {
        boolean askForSDCardPermission = false;
        File[] files = ContextCompat.getExternalFilesDirs(this, null);
        if (files != null && files.length > 1) {
            if (files[0] != null && files[1] != null)
                askForSDCardPermission = true;
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP && askForSDCardPermission && Hawk.get(getString(R.string.preference_internal_uri_extsdcard_photos), null) == null) {
            AlertDialog textDialog = Utility.getTextDialogWithColoredTitleBar(this, R.string.sd_card_write_permission_title, this.getResources().getString( R.string.sd_card_permissions_message));
            textDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok_action).toUpperCase(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP)
                        startActivityForResult(new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE), REQUEST_CODE_SD_CARD_PERMISSIONS);
                }
            });
            textDialog.show();
        }
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent resultData) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_SD_CARD_PERMISSIONS) {
                Uri treeUri = resultData.getData();
                // Persist URI in shared preference so that you can use it later.
                StorageHelper.saveSdCardInfo(getApplicationContext(), treeUri);
                getContentResolver().takePersistableUriPermission(treeUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                Toast.makeText(this, R.string.got_permission_wr_sdcard, Toast.LENGTH_SHORT).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, resultData);
    }
}
