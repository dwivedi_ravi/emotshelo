package com.thabiso.emotshelo.activities.business_tool;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.snackbar.Snackbar;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.databinding.ActivityEditCompanyBinding;
import com.thabiso.emotshelo.models.addcompany.Data;
import com.thabiso.emotshelo.models.addcompany.DataItem;
import com.thabiso.emotshelo.models.addcompany.RequestCompanyDetails;
import com.thabiso.emotshelo.models.addcompany.RequestEditCompanyDetails;
import com.thabiso.emotshelo.models.addcompany.ResponseAddCompany;
import com.thabiso.emotshelo.network.ApiCallerUtility;
import com.thabiso.emotshelo.util.Tools;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;
import com.thabiso.emotshelo.util.preferences.Prefs;

import java.io.IOException;
import java.util.Map;

public class EditCompanyActivity extends AppCompatActivity {

    private int toolId;
    private String toolName;
    private DataItem selectedCompany;
    private Data companyDetailsObject;
    private MyReceiver objMyReceiver;
    private RequestOptions requestOptions;

    private ActivityEditCompanyBinding binding;

    private ActivityResultLauncher mRequestMultiplePermissions = registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), new ActivityResultCallback<Map<String, Boolean>>() {
        @Override
        public void onActivityResult(Map<String, Boolean> result) {
            for (Map.Entry<String, Boolean> entry : result.entrySet()) {
                switch (entry.getKey()) {
                    case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                        if (entry.getValue()) {
                            mGetContent.launch("image/*");
                        } else {
                            Toast.makeText(EditCompanyActivity.this, getString(R.string.storage_permission), Toast.LENGTH_LONG).show();
                        }
                        break;
                    case Manifest.permission.READ_EXTERNAL_STORAGE:
//                        if (entry.getValue()) {
//                            mGetContent.launch("image/*");
//                        } else {
//                            //Toast.makeText(EditProfileActivity.this, getString(R.string.storage_permission), Toast.LENGTH_LONG).show();
//                        }
                        break;
                }
            }
        }
    });

    private ActivityResultLauncher<String> mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(), new ActivityResultCallback<Uri>() {
        @Override
        public void onActivityResult(Uri resultUri) {
            // Handle the returned Uri
            try {
                if (resultUri != null) {
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(EditCompanyActivity.this.getContentResolver(), resultUri);

                        Glide.with(EditCompanyActivity.this).setDefaultRequestOptions(requestOptions).load(bitmap)
                                .thumbnail(0.25f).into(binding.contentEditCompany.imgProfile);

                        binding.contentEditCompany.uploadProgress.setVisibility(View.VISIBLE);
                        binding.contentEditCompany.imgProfile.setClickable(false);

                        ApiCallerUtility.callUploadCompanyLogoApi(EditCompanyActivity.this, resultUri, selectedCompany.getId());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityEditCompanyBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());

        initToolbar();
        initComponent();

        RequestCompanyDetails objRequestCompanyDetails = new RequestCompanyDetails();
        objRequestCompanyDetails.setUserId(Prefs.getUserId());
        objRequestCompanyDetails.setCompanyId(selectedCompany.getId());
        ApiCallerUtility.callGetCompanyDetailsApi(this, objRequestCompanyDetails);
    }

    private void initComponent() {
        objMyReceiver = new MyReceiver();
        requestOptions = Utility.getCustomizedRequestOptions(R.color.mdtp_line_background, R.drawable.no_image_place_holder);
        registerReciever();

        if (getIntent() != null && getIntent().getExtras() != null) {
            toolId = getIntent().getExtras().getInt("toolId", Defaults.TOOL_ID_ENTERPRICE);
            toolName = getIntent().getExtras().getString("toolName", "");
            selectedCompany = (DataItem) getIntent().getExtras().getSerializable("selectedCompany");

            if (toolId == Defaults.TOOL_ID_ENTERPRICE) {
//
            } else if (toolId == Defaults.TOOL_ID_GROUPS) {
//
            } else if (toolId == Defaults.TOOL_ID_PERSONAL) {
//
            }
        }

        binding.benSaveCompanyDetails.setOnClickListener(v -> {
            if (isFormValid()) {
                RequestEditCompanyDetails objRequestEditCompanyDetails = new RequestEditCompanyDetails();

                objRequestEditCompanyDetails.setUserId(Prefs.getUserId());
                objRequestEditCompanyDetails.setCompanyId(selectedCompany.getId());
                objRequestEditCompanyDetails.setWebsite(binding.contentEditCompany.editWebsite.getText().toString().trim());
                objRequestEditCompanyDetails.setCity(binding.contentEditCompany.editCity.getText().toString().trim());
                objRequestEditCompanyDetails.setEmailid(binding.contentEditCompany.editEmail.getText().toString().trim());
                objRequestEditCompanyDetails.setAdditionalTitle(binding.contentEditCompany.editTitle.getText().toString().trim());
                objRequestEditCompanyDetails.setLanguage(binding.contentEditCompany.spnLanguage.getSelectedItemPosition() + "");
                objRequestEditCompanyDetails.setTaxId(binding.contentEditCompany.editTexid.getText().toString().trim());
                objRequestEditCompanyDetails.setZipCode(binding.contentEditCompany.editZip.getText().toString().trim());
                objRequestEditCompanyDetails.setTags(binding.contentEditCompany.spnTags.getSelectedItemPosition() + "");
                objRequestEditCompanyDetails.setCompanyName(binding.contentEditCompany.editName.getText().toString().trim());
                objRequestEditCompanyDetails.setPhoneNumber(binding.contentEditCompany.editPhone.getText().toString().trim());
                objRequestEditCompanyDetails.setState(binding.contentEditCompany.spnState.getSelectedItemPosition() + "");
                objRequestEditCompanyDetails.setDesignation(binding.contentEditCompany.editJobPosition.getText().toString().trim());
                objRequestEditCompanyDetails.setMobileNumber(binding.contentEditCompany.editMobile.getText().toString().trim());
                objRequestEditCompanyDetails.setCompanyAddress(binding.contentEditCompany.editStreet1.getText().toString().trim());
                objRequestEditCompanyDetails.setPostalAddress(binding.contentEditCompany.editPostalAddress.getText().toString().trim());
                objRequestEditCompanyDetails.setVatNumber(binding.contentEditCompany.editVatNumber.getText().toString().trim());

                ApiCallerUtility.callEditCompanyDetailsApi(this, objRequestEditCompanyDetails);
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.grey_60), PorterDuff.Mode.SRC_ATOP);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this, android.R.color.white);
        Tools.setSystemBarLight(this);
    }

    private void setValues() {
        Glide.with(this).setDefaultRequestOptions(requestOptions).load(companyDetailsObject.getLogo())
                .into(binding.contentEditCompany.imgProfile);

        if (companyDetailsObject.getUserType().equals("1"))
            binding.contentEditCompany.rdBtnIndividual.setChecked(true);
        else
            binding.contentEditCompany.rdBtnCompany.setChecked(true);

        try {
            binding.contentEditCompany.spnState.setSelection(Integer.parseInt(companyDetailsObject.getAddress().getState()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            binding.contentEditCompany.spnCountry.setSelection(Integer.parseInt(companyDetailsObject.getAddress().getCountry()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            binding.contentEditCompany.spnLanguage.setSelection(Integer.parseInt(companyDetailsObject.getLanguage()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            binding.contentEditCompany.spnTags.setSelection(Integer.parseInt(companyDetailsObject.getTags()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        binding.contentEditCompany.editName.setText(companyDetailsObject.getCompanyName());
        binding.contentEditCompany.editStreet1.setText(companyDetailsObject.getCompanyAddress());
        binding.contentEditCompany.editStreet2.setText(companyDetailsObject.getAddress().getCity() + " " + companyDetailsObject.getAddress().getZipCode());
        binding.contentEditCompany.editCity.setText(companyDetailsObject.getAddress().getCity());
        binding.contentEditCompany.editZip.setText(companyDetailsObject.getAddress().getZipCode());
        binding.contentEditCompany.editTexid.setText(companyDetailsObject.getTaxId());
        binding.contentEditCompany.editJobPosition.setText(companyDetailsObject.getDesignation());
        binding.contentEditCompany.editPhone.setText(companyDetailsObject.getPhoneNumber());
        binding.contentEditCompany.editMobile.setText(companyDetailsObject.getMobileNumber());
        binding.contentEditCompany.editEmail.setText(companyDetailsObject.getEmailid());
        binding.contentEditCompany.editWebsite.setText(companyDetailsObject.getWebsite());
        binding.contentEditCompany.editTitle.setText(companyDetailsObject.getAdditionalTitle());
        binding.contentEditCompany.editPostalAddress.setText(companyDetailsObject.getPostalAddress());
        binding.contentEditCompany.editVatNumber.setText(companyDetailsObject.getVatNumber());

        binding.contentEditCompany.btnDeleteCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog textDialog = Utility.getTextDialog_Material(EditCompanyActivity.this, "Confirmation", "Are you sure? You want to delete this company?");
                textDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok_action).toUpperCase(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        RequestCompanyDetails objRequestCompanyDetails = new RequestCompanyDetails();
                        objRequestCompanyDetails.setUserId(Prefs.getUserId());
                        objRequestCompanyDetails.setCompanyId(companyDetailsObject.getId());
                        ApiCallerUtility.callDeleteCompanyApi(EditCompanyActivity.this, objRequestCompanyDetails);
                    }
                });
                textDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel_action).toUpperCase(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        textDialog.dismiss();
                    }
                });
                textDialog.show();

                textDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.app_text_color));
                textDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.app_text_color));
                textDialog.getButton(DialogInterface.BUTTON_POSITIVE).setBackground(getResources().getDrawable(R.drawable.ripple));
                textDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setBackground(getResources().getDrawable(R.drawable.ripple));
            }
        });

        binding.contentEditCompany.imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRequestMultiplePermissions.launch(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE});
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_edit_profile, menu);
//        Tools.changeMenuIconColor(menu, getResources().getColor(R.color.grey_60));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_FILL_COMPANY_DETAILS);
        filter.addAction(Defaults.ACTION_COMPANY_EDITED);
        filter.addAction(Defaults.ACTION_COMPANY_LOGO_UPLOAD_SUCCESS);
        filter.addAction(Defaults.ACTION_COMPANY_LOGO_UPLOAD_FAILED);
        Utility.registerReciever(this, filter, objMyReceiver);
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Defaults.ACTION_FILL_COMPANY_DETAILS)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        ResponseAddCompany objResponseAddCompany = (ResponseAddCompany) bundle.getSerializable("ResponseAddCompany");
                        if (objResponseAddCompany != null && objResponseAddCompany.getData() != null) {
                            companyDetailsObject = objResponseAddCompany.getData();
                            setValues();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (intent.getAction().equals(Defaults.ACTION_COMPANY_EDITED)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        ResponseAddCompany objResponseAddCompany = (ResponseAddCompany) bundle.getSerializable("ResponseAddCompany");
                        if (objResponseAddCompany != null && objResponseAddCompany.getData() != null) {
                            Toast.makeText(EditCompanyActivity.this, "Company details updated successfully", Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (intent.getAction().equals(Defaults.ACTION_COMPANY_LOGO_UPLOAD_SUCCESS)) {
                try {
                    binding.contentEditCompany.uploadProgress.setVisibility(View.GONE);
                    binding.contentEditCompany.imgProfile.setClickable(true);
                    Toast.makeText(context, "Company Logo Uploaded Successfully", Toast.LENGTH_LONG).show();

                    if (intent != null && intent.getExtras() != null && intent.getExtras().getBundle("bundle") != null) {
                        Bundle bundle = intent.getExtras().getBundle("bundle");
                        ResponseAddCompany objResponseAddCompany = (ResponseAddCompany) bundle.getSerializable("ResponseAddCompany");
                        if (objResponseAddCompany != null && objResponseAddCompany.getData() != null && objResponseAddCompany.getData().getLogo() != null) {
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (intent.getAction().equals(Defaults.ACTION_COMPANY_LOGO_UPLOAD_FAILED)) {
                try {
                    binding.contentEditCompany.uploadProgress.setVisibility(View.GONE);
                    binding.contentEditCompany.imgProfile.setClickable(true);
                    Toast.makeText(context, "Company Logo Upload Failed", Toast.LENGTH_LONG).show();
                    if (intent != null && intent.getExtras() != null && intent.getExtras().getBundle("bundle") != null) {
                        Bundle bundle = intent.getExtras().getBundle("bundle");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (resultCode == RESULT_OK) {
//            switch (requestCode) {
//                case CAMERA_REQUEST:
//                    Bitmap photo = (Bitmap) data.getExtras().get("data");
////                    binding.contentEditCompany.imgProfile.setImageBitmap(photo);
////                    imgFullView.setImageBitmap(photo);
//                    Glide.with(EditCompanyActivity.this).setDefaultRequestOptions(requestOptions).load(photo)
//                            .thumbnail(0.25f).into(binding.contentEditCompany.imgProfile);
//                    break;
//            }
//            super.onActivityResult(requestCode, resultCode, data);
//        }
//    }

    private boolean isFormValid() {
        if (binding.contentEditCompany.editName.getVisibility() == View.VISIBLE && binding.contentEditCompany.editName.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentEditCompany.editName, "Name can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentEditCompany.editStreet1.getVisibility() == View.VISIBLE && binding.contentEditCompany.editStreet1.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentEditCompany.editStreet1, "Street-1 can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentEditCompany.editPostalAddress.getVisibility() == View.VISIBLE && binding.contentEditCompany.editPostalAddress.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentEditCompany.editPostalAddress, "Postal address can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentEditCompany.editStreet2.getVisibility() == View.VISIBLE && binding.contentEditCompany.editStreet2.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentEditCompany.editStreet2, "Street-2 can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentEditCompany.editCity.getVisibility() == View.VISIBLE && binding.contentEditCompany.editCity.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentEditCompany.editCity, "City can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentEditCompany.editZip.getVisibility() == View.VISIBLE && binding.contentEditCompany.editZip.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentEditCompany.editZip, "Zip can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentEditCompany.editTexid.getVisibility() == View.VISIBLE && binding.contentEditCompany.editTexid.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentEditCompany.editTexid, "Tax ID can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentEditCompany.editJobPosition.getVisibility() == View.VISIBLE && binding.contentEditCompany.editJobPosition.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentEditCompany.editJobPosition, "Job Position can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentEditCompany.editPhone.getVisibility() == View.VISIBLE && binding.contentEditCompany.editPhone.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentEditCompany.editPhone, "Phone can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentEditCompany.editMobile.getVisibility() == View.VISIBLE && binding.contentEditCompany.editMobile.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentEditCompany.editMobile, "Mobile can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentEditCompany.editEmail.getVisibility() == View.VISIBLE && binding.contentEditCompany.editEmail.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentEditCompany.editEmail, "Email can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentEditCompany.editWebsite.getVisibility() == View.VISIBLE && binding.contentEditCompany.editWebsite.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentEditCompany.editWebsite, "Website can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentEditCompany.editTitle.getVisibility() == View.VISIBLE && binding.contentEditCompany.editTitle.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentEditCompany.editTitle, "Title can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentEditCompany.spnState.getVisibility() == View.VISIBLE && binding.contentEditCompany.spnState.getSelectedItemPosition() == 0) {
            Snackbar.make(binding.contentEditCompany.spnState, "Please select State", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentEditCompany.spnCountry.getVisibility() == View.VISIBLE && binding.contentEditCompany.spnCountry.getSelectedItemPosition() == 0) {
            Snackbar.make(binding.contentEditCompany.spnCountry, "Please select Country", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentEditCompany.spnLanguage.getVisibility() == View.VISIBLE && binding.contentEditCompany.spnLanguage.getSelectedItemPosition() == 0) {
            Snackbar.make(binding.contentEditCompany.spnLanguage, "Please select language", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentEditCompany.spnTags.getVisibility() == View.VISIBLE && binding.contentEditCompany.spnTags.getSelectedItemPosition() == 0) {
            Snackbar.make(binding.contentEditCompany.spnTags, "Please select tags", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }
        return true;
    }
}

