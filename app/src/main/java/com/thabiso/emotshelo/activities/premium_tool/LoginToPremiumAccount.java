package com.thabiso.emotshelo.activities.premium_tool;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.databinding.ActivityLoginToPremiumAccountBinding;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Keys;
import com.thabiso.emotshelo.util.preferences.Prefs;

public class LoginToPremiumAccount extends AppCompatActivity {

    private ActivityLoginToPremiumAccountBinding binding;

    private View parent_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.black_trans80));
        }
        binding = ActivityLoginToPremiumAccountBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

//        parent_view = findViewById(android.R.id.content);
        parent_view = binding.getRoot();

        Utility.setTransperentStatusBarColor(LoginToPremiumAccount.this);

        binding.signUpForAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(parent_view, "Sign up for an account", Snackbar.LENGTH_SHORT).show();
            }
        });

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Prefs.setBooleanValue(Keys.PREF_APPLICATION_PASSWORD_SET, true);
                searchAction();
            }
        });

        if (Prefs.getBooleanValue(Keys.PREF_APPLICATION_PASSWORD_SET)) {
            binding.editPassword.setVisibility(View.VISIBLE);
            binding.editConfirmPassWord.setVisibility(View.GONE);
        } else {
            binding.editPassword.setVisibility(View.VISIBLE);
            binding.editConfirmPassWord.setVisibility(View.VISIBLE);
        }
    }

    private void searchAction() {
        binding.progressBar.setVisibility(View.GONE);

        if (isFormValid()) {
            Intent intent = new Intent(LoginToPremiumAccount.this, PremiumToolsMarketPlaceActivity.class);
            startActivity(intent);
            this.finish();
        }
    }

    private boolean isFormValid() {
        if (binding.editPassword.getVisibility() == View.VISIBLE && binding.editPassword.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.editPassword, "Password can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.editConfirmPassWord.getVisibility() == View.VISIBLE && binding.editConfirmPassWord.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.editConfirmPassWord, "Confirm Password can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }
        return true;
    }
}
