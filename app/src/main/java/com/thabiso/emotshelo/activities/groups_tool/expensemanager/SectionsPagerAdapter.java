package com.thabiso.emotshelo.activities.groups_tool.expensemanager;

import android.content.Context;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.thabiso.emotshelo.interfaces.OnExpenseListManagerInteractionListener;
import com.thabiso.emotshelo.models.expenses.Expense;
import com.thabiso.emotshelo.util.preferences.Defaults;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    private Expense objExpense;
    private final Context mContext;
    private OnExpenseListManagerInteractionListener mListener;
    private PlaceholderFragment depositFragment;
    private PlaceholderFragment expenseFragment;
    private PlaceholderFragment interestFragment;

    public SectionsPagerAdapter(Context context, FragmentManager fm, Expense objExpense, OnExpenseListManagerInteractionListener listener) {
        super(fm);
        mContext = context;
        this.objExpense = objExpense;
        this.mListener = listener;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        if (getPageTitle(position).equals(Defaults.TAB_TITLE_DEPOSITS)) {
            return depositFragment = PlaceholderFragment.newInstance(position + 1, objExpense, mListener, Defaults.SCREEN_TYPE_DEPOSITS);
        } else if (getPageTitle(position).equals(Defaults.TAB_TITLE_EXPENSES) || getPageTitle(position).equals(Defaults.TAB_TITLE_WITHDRAWAL)) {
            return expenseFragment = PlaceholderFragment.newInstance(position + 1, objExpense, mListener, Defaults.SCREEN_TYPE_EXPENSES);
        } else if (getPageTitle(position).equals(Defaults.TAB_TITLE_INTEREST) || getPageTitle(position).equals(Defaults.TAB_TITLE_TRANSFERS)) {
            return interestFragment = PlaceholderFragment.newInstance(position + 1, objExpense, mListener, Defaults.SCREEN_TYPE_INTEREST);
        } else {
            return depositFragment = PlaceholderFragment.newInstance(position + 1, objExpense, mListener, Defaults.SCREEN_TYPE_DEPOSITS);
        }
    }

    // Here we can finally safely save a reference to the created
    // Fragment, no matter where it came from (either getItem() or
    // FragmentManger). Simply save the returned Fragment from
    // super.instantiateItem() into an appropriate reference depending
    // on the ViewPager position.
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
        // save the appropriate reference depending on position
        switch (position) {
            case 0:
                depositFragment = (PlaceholderFragment) createdFragment;
                break;
            case 1:
                expenseFragment = (PlaceholderFragment) createdFragment;
                break;
            case 2:
                interestFragment = (PlaceholderFragment) createdFragment;
                break;
        }
        return createdFragment;
    }

    public PlaceholderFragment getFragment(int screenType) {
        // do work on the referenced Fragments, but first check if they
        // even exist yet, otherwise you'll get an NPE.
        switch (screenType) {
            case Defaults.SCREEN_TYPE_DEPOSITS:
                return depositFragment;
            case Defaults.SCREEN_TYPE_EXPENSES:
                return expenseFragment;
            case Defaults.SCREEN_TYPE_INTEREST:
                return interestFragment;
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return objExpense.getSectionList().get(position);
    }

    @Override
    public int getCount() {
        return objExpense.getSectionList().size();
    }
}