package com.thabiso.emotshelo.activities.howitworks;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AlertDialog;

import com.github.barteksc.pdfviewer.listener.OnErrorListener;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageErrorListener;
import com.github.barteksc.pdfviewer.listener.OnRenderListener;
import com.google.android.material.snackbar.Snackbar;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.activities.base.SharedActivity;
import com.thabiso.emotshelo.database.DBHandler;
import com.thabiso.emotshelo.databinding.ActivityFinancialEducationBinding;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;

public class FinancialEducationActivity extends SharedActivity {

    private ActivityFinancialEducationBinding binding;

    private DBHandler dbHandler;
    private AlertDialog progressDialog;
    private MyReceiver objMyReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityFinancialEducationBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());

        initComponents();
    }

    private void initComponents() {
        try {
            objMyReceiver = new MyReceiver();
            registerReciever();

            setSupportActionBar(binding.appbarLayout.toolbar);
            Utility.setStatusBarColor(FinancialEducationActivity.this);
            binding.appbarLayout.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
            binding.appbarLayout.toolbar.setTitleTextColor(getResources().getColor(R.color.white));
            binding.appbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });

            binding.fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });

            initializeData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeData() {
//        pdfView.fromAsset("financial_literacy_tool.pdf")
//                .enableSwipe(true) // allows to block changing pages using swipe
//                .swipeHorizontal(false)
//                .enableDoubletap(true)
//                .defaultPage(0)
//                .onLoad(new OnLoadCompleteListener() {
//                    @Override
//                    public void loadComplete(int nbPages) {
//
//                    }
//                }) // called after document is loaded and starts to be rendered
//                .onPageChange(new OnPageChangeListener() {
//                    @Override
//                    public void onPageChanged(int page, int pageCount) {
//
//                    }
//                })
//                .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
//                .password(null)
//                .scrollHandle(null)
//                .enableAntialiasing(true) // improve rendering a little bit on low-res screens
//                // spacing between pages in dp. To define spacing color, set view background
//                .spacing(0)
//                .load();

        binding.pdfView.fromAsset("financial_literacy_tool.pdf").password(null) // if password protected, then write password
                .defaultPage(0) // set the default page to open
                .spacing(0)
                .onPageError(new OnPageErrorListener() {
                    @Override
                    public void onPageError(int page, Throwable t) {
                    }
                })
                .onError(new OnErrorListener() {
                    @Override
                    public void onError(Throwable t) {
                    }
                })
                .onLoad(new OnLoadCompleteListener() {
                    @Override
                    public void loadComplete(int nbPages) {
                    }
                })
                .onRender(new OnRenderListener() {
                    @Override
                    public void onInitiallyRendered(int nbPages, float pageWidth, float pageHeight) {
                        binding.pdfView.fitToWidth();
                    }
                })
                .load();
//        pdfView.documentFitsView();
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_REFRESH_GROUP_LIST_SCREEN);
        Utility.registerReciever(FinancialEducationActivity.this, filter, objMyReceiver);
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Defaults.ACTION_REFRESH_GROUP_LIST_SCREEN)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
//                        ResponseGroupList objResponseGroupList = (ResponseGroupList) bundle.getSerializable("ResponseGroupList");
//                        if (objResponseGroupList != null && objResponseGroupList.getData() != null && objResponseGroupList.getData().getUserGroups() != null) {
//
//                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
