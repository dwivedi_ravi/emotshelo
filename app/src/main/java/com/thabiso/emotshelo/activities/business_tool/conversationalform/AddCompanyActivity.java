package com.thabiso.emotshelo.activities.business_tool.conversationalform;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.annotations.SerializedName;
import com.hbb20.CountryCodePicker;
import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.adapters.AdapterChatWhatsapp;
import com.thabiso.emotshelo.databinding.ActivityConversationalAddCompanyBinding;
import com.thabiso.emotshelo.interfaces.DateTimeUpdateListener;
import com.thabiso.emotshelo.models.Message;
import com.thabiso.emotshelo.models.addcompany.RequestRegisterCompany;
import com.thabiso.emotshelo.models.addcompany.ResponseAddCompany;
import com.thabiso.emotshelo.models.addcompany.ToolItem;
import com.thabiso.emotshelo.network.ApiCallerUtility;
import com.thabiso.emotshelo.util.Tools;
import com.thabiso.emotshelo.util.Utility;
import com.thabiso.emotshelo.util.preferences.Defaults;
import com.thabiso.emotshelo.util.preferences.Prefs;
import com.thabiso.emotshelo.widget.DateTimePicker;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;

public class AddCompanyActivity extends AppCompatActivity implements DateTimeUpdateListener {

    private ActivityConversationalAddCompanyBinding binding;

    private ActionBar actionBar;
    private AdapterChatWhatsapp adapter;
    private RequestRegisterCompany objRequestRegisterCompany;
    private Field fieldToSetAnswer;
    private String msg = "";
    private DateTimeUpdateListener updateListener;
    private boolean isFormComplete = false;
    private MyReceiver objMyReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityConversationalAddCompanyBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());

        initToolbar();
        iniComponent();
    }

    public void initToolbar() {
        setSupportActionBar(binding.toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setTitle(null);
        Utility.setStatusBarColor(this);
    }

    public void iniComponent() {
        objRequestRegisterCompany = new RequestRegisterCompany();
        updateListener = this;
        objMyReceiver = new MyReceiver();
        registerReciever();

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        binding.recyclerView.setLayoutManager(layoutManager);
        binding.recyclerView.setHasFixedSize(true);

        adapter = new AdapterChatWhatsapp(this);
        binding.recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new AdapterChatWhatsapp.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Message obj, int position) {
//                Toast.makeText(ChatWhatsapp.this, "Item Clicked", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onButtonClick(View view, Message obj, int position, TextView text_content) {
                if (obj.getTypeOfButton() == Defaults.SHOW_DATE_POP_UP) {
                    text_content.setTag(position);
                    getDate(text_content);
                } else if (obj.getTypeOfButton() == Defaults.SHOW_SPINNER_POP_UP) {

                } else if (obj.getTypeOfButton() == Defaults.SHOW_ANOTHER_SCREEN) {
//                    Intent inte = new Intent(AddCompanyActivity.this, ContactListActivity.class);
//
//                    Bundle bundle = new Bundle();
//                    bundle.putInt("totalMembers", Integer.parseInt(objRequestRegisterCompany.getNumberOfMembers()));
//                    bundle.putSerializable("objRequestRegisterCompany", objRequestRegisterCompany);
//                    inte.putExtra("bundle", bundle);
//
//                    startActivity(inte);
                } else if (obj.getTypeOfButton() == Defaults.SHOW_CONFIRMATION_DIALOG) {
                    showConfirmationDialog(true, "Are you sure, You want to proceed?", false);
                } else if (obj.getTypeOfButton() == Defaults.SHOW_YES_NO_BUTTON) {
                    if (obj.getShowButtonText().equals(getResources().getString(R.string.yes_action))) {
                        proceedWithSelection(true, false);
                    } else {
                        proceedWithSelection(false, true);
                    }
                }
            }
        });

        binding.btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isFormComplete) {
                    sendChat();
                } else {
                    callRegisterCompanyApi();
                }
            }
        });
        binding.textContent.addTextChangedListener(contentWatcher);

        binding.lytBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Bundle bundle = getIntent().getExtras();
        int subToolId = Defaults.SUB_TOOL_ID_ROTATING_SAVINGS;
        int toolId;
        String toolName;
        if (bundle != null) {
            subToolId = bundle.getInt("subToolId", Defaults.SUB_TOOL_ID_ROTATING_SAVINGS);
            toolId = bundle.getInt("toolId", Defaults.TOOL_ID_GROUPS);
            toolName = bundle.getString("toolName", "");
        }

//        if (subToolId == Defaults.SUB_TOOL_ID_ROTATING_SAVINGS) {
//            objRequestRegisterCompany.setInterestType("");
//            objRequestRegisterCompany.setInterestRate("");
//            objRequestRegisterCompany.setTotalWithdrawalLimit("");
//            objRequestRegisterCompany.setNumberOfWithdrawalLimit("");
//            objRequestRegisterCompany.setGapingBetweenWithdrawals("");
//        }

        startChatFromBot();
    }

    private void sendChat() {
        if (msg.contains(Defaults.NUMERIC_FIELD_IDENTIFIER_FOR_OPTIONS)) {
            if (msg.contains(binding.textContent.getText().toString().trim())) {
                proceedWithAnswer();
            } else {
                Snackbar.make(binding.textContent, "Please enter valid option", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        } else {
            proceedWithAnswer();
        }
    }

    private void proceedWithAnswer() {
        final String msg = binding.textContent.getText().toString();
        if (msg.isEmpty()) return;

        adapter.insertItem(new Message(adapter.getItemCount(), msg, true, adapter.getItemCount() % 5 == 0, Tools.getFormattedTimeEvent(System.currentTimeMillis())));
        binding.textContent.setText("");
        binding.recyclerView.scrollToPosition(adapter.getItemCount() - 1);

        storeAnswerInBO(msg);

//        NEXT AUTOMATED ANSWER FROM BOT. LOGIC NEEDS TO BE IMPLEMENTED HERE.
        askAutomatedQuestion(objRequestRegisterCompany);
    }

    private void proceedWithSelection(boolean isYesPressed, boolean closeActivity) {
        if (isYesPressed) {
            binding.textContent.setEnabled(true);
            binding.textContent.requestFocus();
            if (objRequestRegisterCompany.getCompanyName() != null) {
                objRequestRegisterCompany.setVatRegistered("");
            }
            askAutomatedQuestion(objRequestRegisterCompany);
        } else {
            if (objRequestRegisterCompany.getCompanyName() == null) {
                showConfirmationDialog(false, "Are you sure, You want to exit?", closeActivity);
            } else {
                binding.textContent.setEnabled(true);
                objRequestRegisterCompany.setVatRegistered("");
                objRequestRegisterCompany.setVatNumber("");
                callRegisterCompanyApi();
            }
        }
    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        hideKeyboard();
    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private TextWatcher contentWatcher = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable etd) {
//            if (etd.toString().trim().length() == 0) {
//                binding.btnSend.setImageResource(R.drawable.ic_mic);
//            } else {
//                binding.btnSend.setImageResource(R.drawable.ic_send);
//            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle() + " clicked", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean askAutomatedQuestion(Object objectPassed) {
        boolean questionAsked = false;
        try {
            Field[] listOfFields = objectPassed.getClass().getDeclaredFields();
            fieldToSetAnswer = null;
            for (Field field : listOfFields) {
                Class<?> individualFieldClass = field.getClass();

                if (field.get(objectPassed) == null) {
                    // not initialized
                    getQuestion(field);
                    questionAsked = true;
                    break;
                } else if (individualFieldClass.equals(int.class)) {
                    if (field.getInt(objectPassed) == 0) {
                        // not initialized
                        getQuestion(field);
                        questionAsked = true;
                        break;
                    }
                } else if (individualFieldClass.equals(double.class)) {
                    if (field.getDouble(objectPassed) == 0) {
                        getQuestion(field);
                        questionAsked = true;
                        break;
                    }
                } else if (individualFieldClass.equals(boolean.class)) {
                    if (field.getBoolean(objectPassed) == false) {
                        getQuestion(field);
                        questionAsked = true;
                        break;
                    }
                } else if (individualFieldClass.equals(float.class)) {
                    if (field.getFloat(objectPassed) == 0) {
                        // not initialized
                        getQuestion(field);
                        questionAsked = true;
                        break;
                    }
                } else if (individualFieldClass.equals(char.class)) {
                    if (field.getChar(objectPassed) == 0) {
                        // not initialized
                        getQuestion(field);
                        questionAsked = true;
                        break;
                    }
                } else if (individualFieldClass.equals(byte.class)) {
                    if (field.getByte(objectPassed) == 0) {
                        // not initialized
                        getQuestion(field);
                        questionAsked = true;
                        break;
                    }
                } else if (individualFieldClass.equals(long.class)) {
                    if (field.getLong(objectPassed) == 0) {
                        // not initialized
                        getQuestion(field);
                        questionAsked = true;
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            String tmp;
            tmp = "";
        }
        return questionAsked;
    }

    private void getQuestion(Field field) {
        msg = "";
        for (int i = 0; i < field.getAnnotations().length; i++) {
            try {
                if (field.getAnnotations()[i] instanceof BindString) {
                    int resourceID = ((BindString) field.getAnnotations()[i]).value();
                    msg = getResources().getString(resourceID);
                    break;
                } else if (field.getAnnotations()[i] instanceof SerializedName) {
                    String name = ((SerializedName) field.getAnnotations()[i]).value();
                    if (name.equals("tag")) {
                        msg = getResources().getString(R.string.create_group_question_list_of_group_members);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        fieldToSetAnswer = field;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Message objMessage = new Message(adapter.getItemCount(), msg, false, adapter.getItemCount() % 5 == 0, Tools.getFormattedTimeEvent(System.currentTimeMillis()));
                adapter.insertItem(objMessage);
                binding.recyclerView.scrollToPosition(adapter.getItemCount() - 1);

                if (msg.toLowerCase().contains("date")) {
                    objMessage = new Message(adapter.getItemCount(), "Select Date", true, adapter.getItemCount() % 5 == 0, Tools.getFormattedTimeEvent(System.currentTimeMillis()));

                    objMessage.setShowButton(true);
                    objMessage.setTypeOfButton(Defaults.SHOW_DATE_POP_UP);

                    adapter.insertItem(objMessage);
                    binding.recyclerView.scrollToPosition(adapter.getItemCount() - 1);

                    binding.textContent.setEnabled(false);
                } else if (msg.equalsIgnoreCase(getResources().getString(R.string.create_group_question_list_of_group_members))) {
                    objMessage = new Message(adapter.getItemCount(), "Select Group Members", true, adapter.getItemCount() % 5 == 0, Tools.getFormattedTimeEvent(System.currentTimeMillis()));

                    objMessage.setShowButton(true);
                    objMessage.setTypeOfButton(Defaults.SHOW_ANOTHER_SCREEN);

                    adapter.insertItem(objMessage);
                    binding.recyclerView.scrollToPosition(adapter.getItemCount() - 1);

                    binding.textContent.setEnabled(false);
                } else if (msg.equalsIgnoreCase(getResources().getString(R.string.add_company_setup_question_vat_registration))) {
                    objMessage = new Message(adapter.getItemCount(), "Select", true, adapter.getItemCount() % 5 == 0, Tools.getFormattedTimeEvent(System.currentTimeMillis()));

                    objMessage.setShowButton(true);
                    objMessage.setTypeOfButton(Defaults.SHOW_YES_NO_BUTTON);

                    adapter.insertItem(objMessage);
                    binding.recyclerView.scrollToPosition(adapter.getItemCount() - 1);

                    binding.textContent.setEnabled(false);
                }

                if (msg.contains(Defaults.NUMERIC_FIELD_IDENTIFIER))
                    binding.textContent.setInputType(InputType.TYPE_CLASS_NUMBER);
                else
                    binding.textContent.setInputType(InputType.TYPE_CLASS_TEXT);
            }
        }, 500);
    }

    /**
     * STORE ANSWER IN BO
     *
     * @param value Value to store in business object
     */
    private void storeAnswerInBO(String value) {
        try {
            if (fieldToSetAnswer != null) {
                switch (fieldToSetAnswer.getName()) {
                    case "a11_registerUserType":
                        objRequestRegisterCompany.setRegisterUserType(value);
                        break;
                    case "a12_companyName":
                        objRequestRegisterCompany.setCompanyName(value);
                        break;
                    case "a13_companyAddress":
                        objRequestRegisterCompany.setCompanyAddress(value);
                        break;
                    case "a14_postal_address":
                        objRequestRegisterCompany.setPostalAddress(value);
                        break;
                    case "a15_state":
                        objRequestRegisterCompany.setState(value);
                        break;
                    case "a16_country":
                        objRequestRegisterCompany.setCountry(value);
                        break;
                    case "a17_zipCode":
                        objRequestRegisterCompany.setZipCode(value);
                        break;
                    case "a18_taxId":
                        objRequestRegisterCompany.setTaxId(value);
                        break;
                    case "a19_designation":
                        objRequestRegisterCompany.setDesignation(value);
                        break;
                    case "a20_phoneNumber":
                        objRequestRegisterCompany.setPhoneNumber(value);
                        break;
                    case "a21_mobileNumber":
                        objRequestRegisterCompany.setMobileNumber(value);
                        break;
                    case "a22_emailId":
                        objRequestRegisterCompany.setEmailId(value);
//                        callRegisterCompanyApi();
                        break;
                    case "a23_website":
                        objRequestRegisterCompany.setWebsite(value);
                        break;
                    case "a24_additionalTitle":
                        objRequestRegisterCompany.setAdditionalTitle(value);
                        break;
                    case "a25_language":
                        objRequestRegisterCompany.setLanguage(value);
                        break;
                    case "a26_tag":
                        objRequestRegisterCompany.setTag(value);
//                        callRegisterCompanyApi();
                        break;
                    case "a27_city":
                        objRequestRegisterCompany.setCity(value);
                        break;
                    case "a28_vat_registered":
                        objRequestRegisterCompany.setVatRegistered("");
                        break;
                    case "a29_vat_number":
                        objRequestRegisterCompany.setVatNumber(value);
                        callRegisterCompanyApi();
                        break;
                }
            }
        } catch (Exception ex) {
        }
    }

    @Override
    public void onDateUpdate(String year, String monthOfYear, String dayOfMonth, TextView textView) {
        try {
            // String date = dayOfMonth + "-" + (monthOfYear) + "-" + year;
            String date = year + "-" + (monthOfYear) + "-" + dayOfMonth;
            storeAnswerInBO(date);

            date = dayOfMonth + "-" + (monthOfYear) + "-" + year;
            textView.setText(date);
            adapter.getItems().get((int) textView.getTag()).setContent(textView.getText().toString());

            binding.textContent.setEnabled(true);
            askAutomatedQuestion(objRequestRegisterCompany);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTimeUpdate(int hourOfDay, int minute, int second, String am_pm, TextView textView) {

    }

    @Override
    public void onTimeUpdate(int hourOfDay, int minute, int second, TextView textView) {

    }

    public void getDate(View view) {
        DateTimePicker.getInstance().getDate(AddCompanyActivity.this, updateListener, null, (TextView) view);
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_FETCH_COMPANY_LIST_FROM_SERVER);
        Utility.registerReciever(AddCompanyActivity.this, filter, objMyReceiver);
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Defaults.ACTION_FETCH_COMPANY_LIST_FROM_SERVER)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        ResponseAddCompany responseAddCompany = (ResponseAddCompany) bundle.getSerializable("ResponseAddCompany");
                        if (responseAddCompany != null) {

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void callRegisterCompanyApi() {
        if (!isFormComplete) {
            isFormComplete = true;
            adapter.insertItem(new Message(adapter.getItemCount(), "Thanks!\nYou have Successfully Completed This Form", false, adapter.getItemCount() % 5 == 0, Tools.getFormattedTimeEvent(System.currentTimeMillis())));
            binding.textContent.setEnabled(false);
            binding.btnSend.setImageResource(R.drawable.ic_check_white_24dp);
            binding.btnSend.setEnabled(false);
            binding.recyclerView.scrollToPosition(adapter.getItemCount() - 1);
        }

        Message objMessage = new Message(adapter.getItemCount(), "Confirm Setup", true, adapter.getItemCount() % 5 == 0, Tools.getFormattedTimeEvent(System.currentTimeMillis()));
        objMessage.setShowButton(true);
        objMessage.setShowButtonText(getResources().getString(R.string.lbl_confirm));
        objMessage.setTypeOfButton(Defaults.SHOW_CONFIRMATION_DIALOG);
        adapter.insertItem(objMessage);
        binding.recyclerView.scrollToPosition(adapter.getItemCount() - 1);
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }

    private void startChatFromBot() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                adapter.insertItem(new Message(adapter.getItemCount(), "Hi " + Prefs.getUserName(), false, adapter.getItemCount() % 5 == 0, Tools.getFormattedTimeEvent(System.currentTimeMillis())));

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        adapter.insertItem(new Message(adapter.getItemCount(), "Let us help you setup your business", false, adapter.getItemCount() % 5 == 0, Tools.getFormattedTimeEvent(System.currentTimeMillis())));

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                adapter.insertItem(new Message(adapter.getItemCount(), "Shall We Start?", false, adapter.getItemCount() % 5 == 0, Tools.getFormattedTimeEvent(System.currentTimeMillis())));

                                Message objMessage = new Message(adapter.getItemCount(), "Select", true, adapter.getItemCount() % 5 == 0, Tools.getFormattedTimeEvent(System.currentTimeMillis()));
                                objMessage.setShowButton(true);
                                objMessage.setShowButtonText(getResources().getString(R.string.yes_action));
                                objMessage.setTypeOfButton(Defaults.SHOW_YES_NO_BUTTON);
                                adapter.insertItem(objMessage);
                                binding.recyclerView.scrollToPosition(adapter.getItemCount() - 1);

                                binding.textContent.setEnabled(false);
                            }
                        }, 500);
                    }
                }, 700);
            }
        }, 500);
    }

    private void showConfirmationDialog(boolean isFormComplete, String message, boolean closeActivity) {
        AlertDialog textDialog = Utility.getTextDialog_Material(AddCompanyActivity.this, "", message);
        textDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.yes_action).toUpperCase(), (dialogInterface, i) -> {
            if (isFormComplete) {
                List<ToolItem> toolsItems = new ArrayList<>();

//                ToolItem objToolItem=new ToolItem();
//                objToolItem.setToolId(Defaults.TOOL_ID_ENTERPRICE+"");
//                objToolItem.setSubToolId(Defaults.SUB_TOOL_ID_INVOICING_AND_QUOTATION+"");
//                objToolItem.setName(getResources().getString(R.string.card_title_invoicing));
//                objToolItem.setStatus(Defaults.TOOL_DEACTIVATED);
//                toolsItems.add(objToolItem);

                objRequestRegisterCompany.setTools(toolsItems);
                try {
                    CountryCodePicker editCountryCode = new CountryCodePicker(this);
                    editCountryCode.setDefaultCountryUsingNameCode("BW");
                    editCountryCode.setAutoDetectedCountry(true);
                    objRequestRegisterCompany.setPhoneNumber(editCountryCode.getSelectedCountryCodeWithPlus() + " " + objRequestRegisterCompany.getPhoneNumber());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                objRequestRegisterCompany.setUserId(Prefs.getUserId());

                ApiCallerUtility.callAddCompanyApi(AddCompanyActivity.this, objRequestRegisterCompany);
                textDialog.dismiss();
            } else {
                textDialog.dismiss();
                if (!isFormComplete && closeActivity)
                    AddCompanyActivity.this.finish();
            }
        });
        textDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.no_action).toUpperCase(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                textDialog.dismiss();
                proceedWithSelection(true, false);
            }
        });
        textDialog.show();

        textDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.app_text_color));
        textDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.app_text_color));
        textDialog.getButton(DialogInterface.BUTTON_POSITIVE).setBackground(getResources().getDrawable(R.drawable.ripple));
        textDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setBackground(getResources().getDrawable(R.drawable.ripple));
    }
}