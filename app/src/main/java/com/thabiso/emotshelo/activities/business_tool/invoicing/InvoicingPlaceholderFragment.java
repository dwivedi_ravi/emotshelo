package com.thabiso.emotshelo.activities.business_tool.invoicing;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.thabiso.emotshelo.R;
import com.thabiso.emotshelo.adapters.CustomersAdapter;
import com.thabiso.emotshelo.adapters.EstimateListAdapter;
import com.thabiso.emotshelo.adapters.InvoiceListAdapter;
import com.thabiso.emotshelo.databinding.ContentInvoiceListScreenBinding;
import com.thabiso.emotshelo.models.InvoicingObject;
import com.thabiso.emotshelo.util.preferences.Defaults;

/**
 * A placeholder fragment containing a simple view.
 */
public class InvoicingPlaceholderFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_SCREEN_TYPE = "screen_type";
    private static final String ARG_VERSE_OBJECT = "verse_object";

    private ContentInvoiceListScreenBinding binding;

    private InvoicingPageViewModel pageViewModel;
    private static OnInvoicingListManagerInteractionListener mListener;
    private Typeface robotoMedium;

    public static InvoicingPlaceholderFragment newInstance(int index, InvoicingObject invoicingObject, OnInvoicingListManagerInteractionListener listener, int type) {
        InvoicingPlaceholderFragment fragment = new InvoicingPlaceholderFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        bundle.putInt(ARG_SCREEN_TYPE, type);
        bundle.putSerializable(ARG_VERSE_OBJECT, invoicingObject);
        mListener = listener;
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = new ViewModelProvider(this).get(InvoicingPageViewModel.class);
        int index = 1;
        int screenType = Defaults.SCREEN_TYPE_INVOICES;

        InvoicingObject homeScreenObject = null;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
            screenType = getArguments().getInt(ARG_SCREEN_TYPE);
            homeScreenObject = (InvoicingObject) getArguments().getSerializable(ARG_VERSE_OBJECT);
        }
        pageViewModel.setIndex(index);
        pageViewModel.setScreentType(screenType);
        pageViewModel.setVerseObject(homeScreenObject);
        robotoMedium = Typeface.createFromAsset(getActivity().getAssets(),
                "roboto_medium.ttf");
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
//        View root = inflater.inflate(R.layout.content_invoice_list_screen, container, false);
        binding = ContentInvoiceListScreenBinding.inflate(inflater);
        View root = binding.getRoot();

        binding.noItemsAvailable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((InvoicingActivity) getActivity()).handleAddButtonClick();
            }
        });

        pageViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
            }
        });

        pageViewModel.getVerseObject().observe(getViewLifecycleOwner(), new Observer<InvoicingObject>() {
            @Override
            public void onChanged(InvoicingObject invoiceScreenObject) {
                try {
                    switch (pageViewModel.mScreenType.getValue()) {
                        case Defaults.SCREEN_TYPE_INVOICES:
                            if (binding.recyclerView != null && binding.recyclerView.getAdapter() == null) {
                                binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                                InvoiceListAdapter mInvoiceListAdapter = new InvoiceListAdapter(getActivity(), invoiceScreenObject.getInvoiceList());
                                binding.recyclerView.setAdapter(mInvoiceListAdapter);
                            } else if (binding.recyclerView != null) {
                                ((InvoiceListAdapter) binding.recyclerView.getAdapter()).refreshList(invoiceScreenObject.getInvoiceList());
                                binding.recyclerView.getAdapter().notifyDataSetChanged();
                            }

                            if (invoiceScreenObject.getInvoiceList().size() == 0) {
                                binding.noItemsAvailable.setText(getResources().getString(R.string.no_invoices_added));
                                binding.noItemsAvailable.setVisibility(View.VISIBLE);
                            } else {
                                binding.noItemsAvailable.setVisibility(View.GONE);
                            }
                            break;
                        case Defaults.SCREEN_TYPE_ESTIMATES:
                            if (binding.recyclerView != null && binding.recyclerView.getAdapter() == null) {
                                binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                                EstimateListAdapter mAdapter = new EstimateListAdapter(getActivity(), invoiceScreenObject.getEstimateList());
                                binding.recyclerView.setAdapter(mAdapter);
                            } else if (binding.recyclerView != null) {
                                ((EstimateListAdapter) binding.recyclerView.getAdapter()).refreshList(invoiceScreenObject.getEstimateList());
                                binding.recyclerView.getAdapter().notifyDataSetChanged();
                            }

                            if (invoiceScreenObject.getEstimateList().size() == 0) {
                                binding.noItemsAvailable.setText(getResources().getString(R.string.no_estimates_added));
                                binding.noItemsAvailable.setVisibility(View.VISIBLE);
                            } else {
                                binding.noItemsAvailable.setVisibility(View.GONE);
                            }
//                            if (Prefs.getBooleanValue(Defaults.TOOL_ID_GROUPS + "")) {
//                                binding.recyclerView.setVisibility(View.VISIBLE);
//                                itemCardWizardBG.setVisibility(View.GONE);
//                            } else {
//                                title.setText(getActivity().getResources().getString(R.string.card_title_groups));
//                                description.setText(getActivity().getResources().getString(R.string.card_description_groups));
//                                binding.recyclerView.setVisibility(View.GONE);
//                                itemCardWizardBG.setVisibility(View.VISIBLE);
//                            }

//                            binding.recyclerView.setVisibility(View.VISIBLE);
                            break;
                        case Defaults.SCREEN_TYPE_CLIENTS:
                            if (binding.recyclerView != null && binding.recyclerView.getAdapter() == null) {
                                binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                                CustomersAdapter mCustomersAdapter = new CustomersAdapter(getActivity(), invoiceScreenObject.getClientList(), null, false);
                                binding.recyclerView.setAdapter(mCustomersAdapter);
                            } else if (binding.recyclerView != null) {
                                ((CustomersAdapter) binding.recyclerView.getAdapter()).refreshList(invoiceScreenObject.getClientList());
                                binding.recyclerView.getAdapter().notifyDataSetChanged();
                            }

                            if (invoiceScreenObject.getClientList().size() == 0) {
                                binding.noItemsAvailable.setText(getResources().getString(R.string.no_customers_added));
                                binding.noItemsAvailable.setVisibility(View.VISIBLE);
                            } else {
                                binding.noItemsAvailable.setVisibility(View.GONE);
                            }

//                            if (Prefs.getBooleanValue(Defaults.TOOL_ID_PERSONAL + "")) {
//                                binding.recyclerView.setVisibility(View.VISIBLE);
//                            } else {
//                                binding.recyclerView.setVisibility(View.GONE);
//                            }
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

//        try {
//            if (pageViewModel.mScreenType.getValue() == Defaults.SCREEN_TYPE_INVOICES) {
////                txtBalanceAmount.setTextColor(getResources().getColor(R.color.md_green_700));
////                cardBalance.setStrokeColor(getResources().getColor(R.color.md_green_700));
//                txtBalanceAmount.setTextColor(getResources().getColor(R.color.colorAccent));
//                cardBalance.setStrokeColor(getResources().getColor(R.color.colorAccent));
//            } else if (pageViewModel.mScreenType.getValue() == Defaults.SCREEN_TYPE_ESTIMATES) {
//                txtBalanceAmount.setTextColor(getResources().getColor(R.color.red));
//                cardBalance.setStrokeColor(getResources().getColor(R.color.red));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        return root;
    }

    public void refreshList(InvoicingObject objExpense) {
        pageViewModel.setVerseObject(objExpense);
    }
}